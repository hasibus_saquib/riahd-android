package com.codecs;

import android.preference.ListPreference;

public class G729 implements Codec {

	protected String CODEC_NAME;
	protected String CODEC_USER_NAME;
	protected int CODEC_NUMBER;
	protected int CODEC_SAMPLE_RATE = 8000;		// default for most narrow band codecs
	protected int CODEC_FRAME_SIZE = 160;		// default for most narrow band codecs
	protected String CODEC_DESCRIPTION;
	
	private boolean loaded = false, failed = false;
	
	public G729() {
		
		CODEC_NAME = "G729";
		CODEC_USER_NAME = "G729";
		CODEC_DESCRIPTION = "8kbit";
		CODEC_NUMBER = 18;
	}

	void load() {
		try {
			
			System.loadLibrary("cilib");
			loaded = true;
		}
		catch (Throwable e) {
			loaded = false;
			failed = true;
		}
	}

	public native int open();

	public native int decode(byte encoded[], short lin[], int size);

	public native int encode(short lin[], int offset, byte encoded[], int size);

	public native void close();

	public void init() {
		load();
		if (isLoaded())
			open();
	}

	@Override
	public int samp_rate() {
		return CODEC_SAMPLE_RATE;
	}

	@Override
	public int frame_size() {
		return CODEC_FRAME_SIZE;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isLoaded() {
		return loaded;
	}

	@Override
	public boolean isFailed() {
		return failed;
	}

	@Override
	public void fail() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return CODEC_NAME;
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return CODEC_NAME;
	}

	@Override
	public String key() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String userName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int number() {
		// TODO Auto-generated method stub
		return CODEC_NUMBER;
	}

	@Override
	public void setListPreference(ListPreference l) {
		// TODO Auto-generated method stub
		
	}
}
