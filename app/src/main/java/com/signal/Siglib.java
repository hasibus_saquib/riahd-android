package com.signal;


import android.util.Log;

public class Siglib {

	public boolean loaded;

	public Siglib() {
		loadLibrary();
	}

	public void loadLibrary() {
		try {
			System.loadLibrary("siglib");

			loaded = true;
			init();
		} catch (Exception e) {
			Log.e("",e.getMessage());
		}
	}

	public native void init();

	public native void connectAuthServer();

	public native void setCustomAuth(String strCustomAuth,
									 boolean bUseCustomAuth);

	public native void useCustomAuth(boolean bUseCustomAuth);

	public native void setPlatinumOpcode(int platinumOpcode);

	public native void setCredentials(
			int iDialerId, String strOs, String strOsVersion, String strDeviceModel, String strImeiAndPackageName,
			String iCountrycode, String iTeleOpcode, int iOperatorCode,
			String strUserName, String strPassword, String strPhoneNumber, int iCachedOpCode,
			long lLastAuthInfoUpdateTime, int iDialerMajorVersion, int iDialerMinorVersion, String appVersion,
			int numberOfDomainResolved, boolean authenticationHappened, int iLastSuccessMethod,
			boolean useCustomMccMnc, boolean hasMultiSim
	);

	public native void doAuthentication();

	public native void sendAuthCSUM(int callDuration, int iCallStartTime,
									String strCallee);

	public native void connectVpn();

	public native void CallToNumber(String strCallee);

	public native void SendBusy();

	public native void CancelCall();

	public native void AcceptCall();

	public native void SendRtp(byte[] rtpData);

	public native void SilentPacket(byte[] rtpData);

	public native void SendDtmf(int KeyCode);

	public native void SendDtmfSipInfo(int iKeycode);

	public native void SetMute(boolean value);

	public native void SetHold(boolean value);

	public native void SetPingIntervalTime(int iSeconds);

	public native void ConnectionLost();

	public native void ConnectionStablished();

	public native void ConnectBalanceServer();

	public native void RequestBalance();

	public native void RequestBalanceUrl();

	public native void balanceURLRequestTimeout();

	public native void SetLocalSocketPort(int iPort);

	public native void onExit();

	public native int GetComponentVersion();

	public native int GetVpnProtocolVersion();

	public native String GetAuthProtocolVersion();

	public native String GetAuthAddress();

	public native String GetAAAc();

	public native void setReportSettings(boolean bSendReport);

	public native void ReportException(String strException);

	public native String GetVpnAddress();

	public native String GetDnsAddress();

	public native void setCachedAttributes(
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls, byte[] pVpnPortListTls,
			boolean bQuickRegistration, boolean bUseVpn, boolean bAllowSipIncoming, boolean bUsePhoneNo);

	public native void SetAddresses(int numberOfAuthIp, byte[] authIpList,
									int numberOfDnsIp, byte[] dnsIpList, int numberOfReportIp,
									byte[] reportIpList);

	public native void SetAuthDomainResolved(boolean bResolved);

	public native String EncryptDecryptAndReturnValue(String strValue, String strKey);

	public native boolean IsReady();

	public void ReadyToAuthenticate() {
		doAuthentication();
	}

	public void ReAuthenticate() {
	}

	public void AuthenticationTimeout() {
	}

	public void AuthenticationVoid() {

	}

	public void usingNewAuthSettings(
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls, byte[] pVpnPortListTls,
			boolean bQuickRegistration, boolean bUseVpn, boolean bAllowSipIncoming, boolean bUsePhoneNo,
			boolean bEnableOpcodeEdit, boolean bUseBalanceServer, String strBrandName, String strIvr, String strFooter,
			long iUpdateTime, int iNumberOfDnsIp, byte[] pDnsIpList, int iNumberOfAuthIp,
			byte[] pAuthIpList, int iNumberOfReportIp, byte[] pReportIpList, boolean bReportSend) {
	}

	public void UseCashedAuthSettings() {

	}

	public void receivedRtp(byte[] rtpData, int rtpLength, int rtpIndex) {
		// Log.i("Recieving Rtp", "................");
	}
	public void BufferAndSerializeRtp(boolean val){
	}

	public void IncommingCallFrom(String caller) {

	}

	public void UpdateDialerState(int State, String strInfo) {
	}

	public void UpdateInfo(String strInfo, int strLength) {
	}

	public void StoreServerAddress(byte[] pAuthIps, int iNumberOfServerIp) {
	}

	public void receivedBalance(String strBalance) {
	}

	public void receivedBalanceURL(String strUrl) {
	}

	public void UpdateDialerInformation(String strBrandName, String strIvr, String strFooter, long lUpdateTime) {
	}

	public void ReRegister(
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls, byte[] pVpnPortListTls) {
	}

	public void StoreReportAddress(byte[] pReportIps, int iNumberOfReportIp) {
	}

	public void RegistrationCompleted(
			byte[] pDnsIps, int iNumberOfDnsIp, byte[] pAuthIpList, int iNumberOfAuthIp,
			byte[] pReportIps, int iNumberOfReportIp, boolean bReportSend, int iSuccessProtocol, boolean bIgnoreSilence,
			String profileID, boolean forceUpdate, String mcc, String mnc) {
	}
}
