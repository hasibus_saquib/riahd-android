package com.signal;


public enum CkslibraryEnum {

	EAccessPoint(0),
	EAuthenticating(1),
	EAuthenticationError(2),
	ERegistering(3),
	ERegistered(4),
	EUnregistered(5),
	ECalling(6),
	ERinging(7),
	ECallReceiving(8),
	ECallEstablished(9),
	EPaused(10),
	EStateInvalid(11),
	EReconnecting(12),
	EConnectionError(13),
	
	//For Internal Use
	JBalanceReceived(14),
	JContact(15),
	EAuthTrying(16),
	EInvited(17);
	
	private int status;
	
	private CkslibraryEnum(int i) {
		status = i;
	}
	
	public int getStatus() {
		return status;
	}
	
	public static CkslibraryEnum findById(int status) {
		
		for(CkslibraryEnum ckslibraryEnum : CkslibraryEnum.values() ) {
			
			if(ckslibraryEnum.getStatus() == status) {
				return ckslibraryEnum;
			}
		}
		
		return EStateInvalid;
	}

	public static String getStatusMessage(int state) {
		String statusMessage = "";

		switch (CkslibraryEnum.findById(state)) {
		case EAuthenticating:
			statusMessage = "Searching";
			break;

		case EAuthenticationError:
			statusMessage = "INVALID";
			break;

		case ERegistering:
			statusMessage = "Connecting";
			break;

		case ERegistered:
			statusMessage = "Connected";
			break;

		case EUnregistered:
			statusMessage = "INVALID";
			break;

		case EConnectionError:
			statusMessage = "INVALID";
			break;

		case ECalling:
			statusMessage = "Calling";
			break;
			
		case ERinging:
			statusMessage = "Ringing";
			break;
		case ECallReceiving:
			statusMessage = "Incomming Call";
			break;
		case ECallEstablished:
			statusMessage = "Call Connected";
			break;

		default:
			statusMessage = "";
			break;
		}
		return statusMessage;
	}
}
