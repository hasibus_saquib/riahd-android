package com.ci.connectdialer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.ui.NotificationActivity;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by Mahim-Ul Asad on 7/4/17.
 */

public class PushNotification {

    public static Bundle pushNotificationBundle = null;

//    public static final String DEBUG_TAG = "Push Notification";

//    public PushNotification(Bundle bundle){
//        pushNotificationBundle = bundle;
//        Log.d(DEBUG_TAG, "PushNotification.java Date initialized: pushNotificationBundle: "+pushNotificationBundle);
//    }

    public static void saveData( Bundle bundle) {
        //if bundle is not null check if the key is for notification message
        //>=Android.L system puts profile value in intent.putExtra
        if (bundle != null && bundle.containsKey(DialerSettings.PUSH_NOTIFICATION_MSG_TITLE)){
            pushNotificationBundle = bundle;
        }

//        Log.d(DEBUG_TAG, "PushNotification.java Date saved: pushNotificationBundle: "+pushNotificationBundle
//                +" keyset: "+(bundle==null?"[null]":bundle.keySet()));
//        Log.d(DEBUG_TAG, "PushNotification.java bundle.containsKey(): "+(bundle==null?"null":bundle.containsKey(DialerSettings.PUSH_NOTIFICATION_MSG_TITLE)));
    }

    public static boolean hasData(){
//        Log.d(DEBUG_TAG, "PushNotification.java pushNotificationBundle:" + pushNotificationBundle);
        if (pushNotificationBundle == null)
            return false;
        else
            return true;
    }

    public static void displayNotification(Context context){
        Log.d("Push notification", "PushNotification.java hasData(): "+hasData());
        if (hasData()){
            Intent intent = new Intent(context, NotificationActivity.class);
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(pushNotificationBundle);
            pushNotificationBundle = null;
//            Log.d(DEBUG_TAG, "PushNotification.java pushNotificationBundle: "+
//                            pushNotificationBundle+" set to null before calling the activity");
            context.startActivity(intent);
        }
    }

}
