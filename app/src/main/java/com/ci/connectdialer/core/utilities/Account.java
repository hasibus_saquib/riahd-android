package com.ci.connectdialer.core.utilities;

/**
 * Created by Masud Rashid on 12/12/2016.
 */
public class Account {

    private String accountName="";
    private boolean opCodeEditable=false;
    private int opCode=0;
    private String username="";
    private String password="";
    private String phoneNumber="";
    private String customAuthIp="";
    private String customDnsIp="";

    @Override
    public String toString(){

       return String.valueOf(opCodeEditable) + '|' +
                        opCode + '|' +
                        username + '|' +
                        password + '|' +
                        phoneNumber + '|' +
                        customAuthIp + '|' +
                        customDnsIp + '|' +
                        accountName;
    }

    public static Account getFromString( String accountStringItem ){

        String[] parts = accountStringItem.split("[|]");

        Account account = new Account();
        if (parts.length < 8)
            return account;
        account.opCodeEditable = Boolean.parseBoolean(parts[0]);
        account.opCode = Integer.parseInt(parts[1]);
        account.username = parts[2];
        account.password = parts[3];
        account.phoneNumber = parts[4];
        account.customAuthIp = parts[5];
        account.customDnsIp = parts[6];
        account.accountName = parts[7];
        return account;
    }

    public static Account resetCustomSettings(String accountStringItem) {
        String[] parts = accountStringItem.split("[|]");

        Account account = new Account();
        if (parts.length < 8)
            return account;
        account.opCodeEditable = Boolean.parseBoolean(parts[0]);
        account.opCode = Integer.parseInt(parts[1]);
        account.username = parts[2];
        account.password = parts[3];
        account.phoneNumber = parts[4];
        account.customAuthIp = "";
        account.customDnsIp = "";
        account.accountName = parts[7];
        return account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public boolean isOpCodeEditable() {
        return opCodeEditable;
    }

    public void setOpCodeEditable(boolean opCodeEditable) {
        this.opCodeEditable = opCodeEditable;
    }

    public int getOpCode() {
        return opCode;
    }

    public void setOpCode(int opCode) {
        this.opCode = opCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomAuthIp() {
        return customAuthIp;
    }

    public void setCustomAuthIp(String customAuthIp) {
        this.customAuthIp = customAuthIp;
    }

    public String getCustomDnsIp() {
        return customDnsIp;
    }

    public void setCustomDnsIp(String customDnsIp) {
        this.customDnsIp = customDnsIp;
    }

}
