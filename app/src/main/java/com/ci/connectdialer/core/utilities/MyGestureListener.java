package com.ci.connectdialer.core.utilities;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Erfan on 3/27/17.
 */

public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_MIN_DISTANCE = 200;
    private static final int SWIPE_MAX_OFF_PATH = 100;
    private static final int SWIPE_THRESHOLD_VELOCITY = 1000;
    private DialpadVisibilityInterface dialpadInterface;

    public void SetListener (DialpadVisibilityInterface _dialpadInterface) {
        this.dialpadInterface = _dialpadInterface;
    }

    @Override

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,

                           float velocityY) {

        float dX = e2.getX()-e1.getX();

        float dY = e1.getY()-e2.getY();

//        if (/*Math.abs(dX) < SWIPE_MAX_OFF_PATH ||*/
//        Math.abs(velocityY) >= SWIPE_THRESHOLD_VELOCITY ||
//        Math.abs(dY) >= SWIPE_MIN_DISTANCE)
        if (Math.abs(dY) >= SWIPE_MIN_DISTANCE)
        {

            if (dY>0) {
                // Up Swipe
            } else {

                dialpadInterface.SetDialpadVisibilityFromInterface(View.GONE);
            }

            return true;

        }

        return false;

    }
}
