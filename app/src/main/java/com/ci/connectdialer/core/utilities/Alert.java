package com.ci.connectdialer.core.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by Masud Rashid on 15/11/2016.
 */
public class Alert {

    public static final int TYPE_EXIT_APPLICATION = 9903;
    public static final int TYPE_INVALID_APPLICATION = 9904;


    public static void showSimple(Context context, String message){

        new AlertDialog.Builder(context).setMessage(message)
                .create().show();
    }

    public static void showOk(Context context, String message){

        new AlertDialog.Builder(context).setMessage(message)
                .setPositiveButton("Ok", null).setCancelable(true)
                .create().show();
    }

    public static void showOk(Context context, String title, String message){

        new AlertDialog.Builder(context).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", null).setCancelable(false)
                .create().show();
    }

    public static void showWarning(Context context, String title, String message){

        new AlertDialog.Builder(context).setTitle(title).setMessage(message)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setPositiveButton("Ok", null).setCancelable(true)
                .create().show();
    }

    public static void showOk(Context context, String title, String message, DialogInterface.OnClickListener okOnClick){

        new AlertDialog.Builder(context).setTitle(title).setMessage(message).setCancelable(false)
                .setPositiveButton("Ok", okOnClick)
                .create().show();
    }

    public static void showOkCancel(
            Context context, String title, String message,
            DialogInterface.OnClickListener okOnClick, DialogInterface.OnClickListener cancelOnClick){

        new AlertDialog.Builder(context).setTitle(title).setMessage(message).setCancelable(false)
                .setPositiveButton("Ok", okOnClick).setNegativeButton("Cancel", cancelOnClick)
                .create().show();
    }

    public static void showPositiveNegative(
            Context context, String title, String message, String positiveBtnText, String negativeBtnText,
            DialogInterface.OnClickListener positiveOnClick, DialogInterface.OnClickListener negativeOnClick){

        new AlertDialog.Builder(context).setTitle(title).setMessage(message).setCancelable(false)
                .setPositiveButton(positiveBtnText, positiveOnClick)
                .setNegativeButton(negativeBtnText, negativeOnClick)
                .create().show();
    }

    public static void showOkScroll(
            Context context, String title, String message,
            DialogInterface.OnClickListener okOnClick){

        View view = getScrollTextView(context, message);
        new AlertDialog.Builder(context).setTitle(title).setView(view).setCancelable(true)
                .setPositiveButton("Ok", okOnClick)
                .create().show();
    }

    public static void showOkCancelScroll(
            Context context, String title, String message,
            DialogInterface.OnClickListener okOnClick, DialogInterface.OnClickListener cancelOnClick){

        View view = getScrollTextView(context, message);
        new AlertDialog.Builder(context).setTitle(title).setView(view).setCancelable(false)
                .setPositiveButton("Ok", okOnClick).setNegativeButton("Cancel", cancelOnClick)
                .create().show();
    }

    public static void showPositiveNegativeScroll(
            Context context, String title, String message,
            String positiveBtnText, String negativeBtnText,
            DialogInterface.OnClickListener positiveOnClick, DialogInterface.OnClickListener negativeOnClick){

        View view = getScrollTextView(context, message);
        new AlertDialog.Builder(context).setTitle(title).setView(view).setCancelable(false)
                .setPositiveButton(positiveBtnText, positiveOnClick)
                .setNegativeButton(negativeBtnText, negativeOnClick)
                .create().show();
    }

    private static View getScrollTextView(Context context, String message){
        TextView textView = new TextView(context);
        textView.setText(message);
        textView.setPadding(40,0,0,0);
        textView.setTextColor(Color.BLACK);
        textView.setBackgroundColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            textView.setTextIsSelectable(true);
        }
        ScrollView scrollView = new ScrollView(context);
        scrollView.addView(textView);
        return scrollView;
    }

    public static void showOkView(Context context, String title, View view, DialogInterface.OnClickListener okOnClick){

        new AlertDialog.Builder(context).setTitle(title).setView(view).setCancelable(false)
                .setPositiveButton("Ok", okOnClick)
                .create().show();
    }

    public static void showView(Context context, String title, View view){

        AlertDialog dialog = new AlertDialog.Builder(context).setTitle(title).setView(view).setCancelable(true)
                .create();
        //can be used to show rounded corner but view should have a custom background
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
