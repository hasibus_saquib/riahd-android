package com.ci.connectdialer.core.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * Created by Masud Rashid on 24/11/2016.
 */
public class PermissionManager {

    private IPermissionMgr permissionMgrListener;

    private static String[] permissions = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECEIVE_SMS,
    };

    private static int[] showExplanation = new int[permissions.length];
    private static int[] notGranted = new int[permissions.length];
    private boolean uninstall = false;

    private static final int PERMISSION_REQUEST_ALL = 10;
    private static final int PERMISSION_REQUEST_CONTACTS = 11;
    private static final int PERMISSION_REQUEST_MICROPHONE = 12;
    private static final int PERMISSION_REQUEST_PHONE = 13;

    public PermissionManager(IPermissionMgr listener){
        this.permissionMgrListener = listener;
    }

    public void checkAndAskPermissionThenProceed(Activity thisActivity, String[] permissions, int permissionId){

        int granted = 0, count = 0;
        for( String perm : permissions) {
            // Here, thisActivity is the current activity
            if( ContextCompat.checkSelfPermission(thisActivity,perm) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity, perm)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    showExplanation[count]++;
                }
                else {
                    notGranted[count]++;
                }
            }
            else granted++;
            count++;
        }


        for( count=0; count<permissions.length; count++) {
            if (showExplanation[count] > 0 && notGranted[count] > 0) {
                uninstall = true;
            }
        }

        if(granted < permissions.length ) {
            // No explanation needed, we can request the permission.
            try {
                ActivityCompat.requestPermissions(thisActivity, permissions, permissionId);
            } catch (Exception e) {
                Log.e("Request Permission", e.getMessage());
            }

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
        else {
            permissionMgrListener.startApplication();
        }
    }

    private void showAlertAndExit(Context context, String msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionMgrListener.exitApplication();
                    }
                });
        alertDialogBuilder.create().show();
    }


    public void onRequestPermissionsResult(Context context, int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_ALL: {
                // If request is cancelled, the result arrays are empty.
                int granted = 0;
                if (grantResults.length > 0) {

                    for( int res : grantResults) {
                        if( res == PackageManager.PERMISSION_GRANTED) granted++;
                    }
                }

//                if( uninstall) {
//                    showAlertAndExit(context, "Please uninstall then reinstall and grant permission to proceed");
//                }
//                else
                if( granted < grantResults.length) {
                    showAlertAndExit( context, "Needed all permission to proceed");
                }
                else {
                    permissionMgrListener.startApplication();
                }

                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void checkPermissionAndStartApp(Activity activity) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndAskPermissionThenProceed(activity, permissions, PERMISSION_REQUEST_ALL );
        } else {
            permissionMgrListener.startApplication();
        }
    }
}
