package com.ci.connectdialer.core.db;

public class CallInformation {
	
	private long id;
	private String name;
	private String phone;
	private long contactID;
	private long time;
	private int state;
	private long duration;
	private long callaccepttime;
	private boolean showSectionHeader;

	public long base;
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPhoneNo(String phone) {
		this.phone = phone;
	}

	public String getPhoneNo() {
		return phone;
	}

	public void setContactID(long contactID) {
		this.contactID= contactID;
	}

	public long getContactID() {
		return contactID;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getTime() {
		return time;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getCallaccepttime() {
		return callaccepttime;
	}

	public void setCallaccepttime(long callaccepttime) {
		this.callaccepttime = callaccepttime;
	}

	public boolean isShowSectionHeader() { return showSectionHeader; }

	public void setShowSectionHeader(boolean showSectionHeader) { this.showSectionHeader = showSectionHeader; }
}
