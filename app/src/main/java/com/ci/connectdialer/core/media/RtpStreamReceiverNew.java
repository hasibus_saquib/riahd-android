package com.ci.connectdialer.core.media;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.ToneGenerator;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;

import com.codecs.Codec;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateManager;
import com.ci.connectdialer.core.utilities.CallRecorder;
import com.signal.CkslibraryEnum;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class RtpStreamReceiverNew extends Thread {

	private Codec p_type;

	public static final int BUFFER_SIZE = 1200 * 80;

	AudioFixer_SDK16 audioFixer = new AudioFixer_SDK16();

	boolean running;

	private AudioManager am;

	private PowerManager.WakeLock pwl, pwl2;
	private WifiManager.WifiLock wwl;
	static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;
	boolean lockLast, lockFirst;

	private Packet rtp_packet;
	private AudioTrack track;
	private ToneGenerator tg;
	public static int timeout;

	private int user;
	CksNativeLibrary cksLib;

	public RtpStreamReceiverNew(Codec payload_type, CksNativeLibrary lib) {
		p_type = payload_type;
		cksLib = lib;
	}

	public boolean isRunning() {
		return running;
	}

	public void powerOn() throws IllegalThreadStateException {
		running = true;
		start();
	}

	public void halt() {
		running = false;
	}

	public boolean speaker(boolean speakerOn) {
		Context context = CksTabService.getContext();
		if(context != null){
			AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			if (speakerOn)
				am.setSpeakerphoneOn(true);
			else
				am.setSpeakerphoneOn(false);

			return am.isSpeakerphoneOn();
		}
		return false;
	}

	public static void adjust(int keyCode, boolean down) {
		Context context = CksTabService.getContext();
		if (context != null) {
			AudioManager mAudioManager = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			mAudioManager
					.adjustStreamVolume(
							AudioManager.STREAM_VOICE_CALL,
							keyCode == KeyEvent.KEYCODE_VOLUME_UP ? AudioManager.ADJUST_RAISE
									: AudioManager.ADJUST_LOWER,
							AudioManager.FLAG_SHOW_UI);
		}
	}

	public static void adjustInCallVolume(int volume) {
		Context context = CksTabService.getContext();
		if (context != null) {
			AudioManager mAudioManager = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
			int gain = volume > maxVolume ? volume % maxVolume : 0;
			Log.e("VolumeSlider-max", "maxVol: " + maxVolume + " currentVol: " + volume);
			mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, volume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
			StateManager.CURRENT_SPEAKER_GAIN = StateManager.SPEAKER_GAIN_PRODUCT * gain;
			StateManager.SPEAKER_GAIN_FACTOR = (float)Math.pow( 10., StateManager.CURRENT_SPEAKER_GAIN/ 20. );
			PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.SPEAKER_GAIN_IN_DB, StateManager.CURRENT_SPEAKER_GAIN);
		}
	}

	static boolean samsung;
	static boolean lenovo;

	public static void initMode() {
		samsung = Build.MODEL.contains("SAMSUNG")
				|| Build.MODEL.contains("SPH-") || Build.MODEL.contains("SGH-")
				|| Build.MODEL.contains("GT-");

		lenovo = Build.MODEL.contains("Lenovo")
				|| Build.MODEL.contains("lenovo");
	}

	public static void setMode(int mode) {
		initMode();
		Context context = CksTabService.getContext();
		if (context != null) {
			AudioManager am = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);

			if (am.isSpeakerphoneOn())
				am.setSpeakerphoneOn(false);

			if (android.os.Build.VERSION.SDK_INT > 10) {
				am.setMode(AudioManager.MODE_IN_COMMUNICATION);
			} else {
				am.setMode(AudioManager.MODE_IN_CALL);
			}
		}
	}

	private void initAudioTrack() {
		synchronized (this) {
			// AudioTrack oldtrack;

			int maxjitter = AudioTrack
					.getMinBufferSize(p_type.samp_rate(),
							AudioFormat.CHANNEL_OUT_MONO,
							AudioFormat.ENCODING_PCM_16BIT) * 2;

			// if (maxjitter < 4096) {
			// maxjitter = 4096;
			// }

			if (track != null) {
				track.stop();
				track.release();
			}

			// oldtrack = track;

			float maxVolume = AudioTrack.getMaxVolume();

			track = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
					p_type.samp_rate(), AudioFormat.CHANNEL_OUT_MONO,
					AudioFormat.ENCODING_PCM_16BIT, maxjitter,
					AudioTrack.MODE_STREAM);

			track.setStereoVolume(maxVolume, maxVolume);

			audioFixer.fixAudioTrack(track);

			timeout = 1;
			user = 0;

		}
	}

	private byte[] short2byte(short[] sData) {
		int shortArrsize = sData.length;
		byte[] bytes = new byte[shortArrsize * 2];
		for (int i = 0; i < shortArrsize; i++) {
			bytes[i * 2] = (byte) (sData[i] & 0x00FF);
			bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
			sData[i] = 0;
		}
		return bytes;
	}

	synchronized void write(short a[], int b, int c) {

		int offset = b;
		int totalLength = c;

		int length = 80;

		if (a == null || a.length < (b + c))
			return;
		float gainFactor = StateManager.SPEAKER_GAIN_FACTOR;

		try {

			for (int i = offset; i + length <= totalLength; i += length) {

				int stop = (i + length) <= (offset + totalLength) ? (i + length) : (offset + totalLength);
				short output[] = Arrays.copyOfRange(a, i, stop);


				byte[] recBuffer = short2byte(output);
				int reallySampledBytes = recBuffer.length;
				int recBufferBytePtr = 0;

				int j = 0;
				while (j < reallySampledBytes) {
					float sample = (float) (recBuffer[recBufferBytePtr + j] & 0xFF | recBuffer[recBufferBytePtr
							+ j + 1] << 8);
					sample *= gainFactor;
					if (sample >= 32767f) {
						recBuffer[recBufferBytePtr + j] = (byte) 0xFF;
						recBuffer[recBufferBytePtr + j + 1] = 0x7F;
					} else if (sample <= -32768f) {
						recBuffer[recBufferBytePtr + j] = 0x00;
						recBuffer[recBufferBytePtr + j + 1] = (byte) 0x80;
					} else {
						int s = (int) (0.5f + sample); // Here, dithering would be
						// more appropriate
						recBuffer[recBufferBytePtr + j] = (byte) (s & 0xFF);
						recBuffer[recBufferBytePtr + j + 1] = (byte) (s >> 8 & 0xFF);
					}
					j += 2;
				}

				short[] shorts = new short[recBuffer.length / 2];

				ByteBuffer.wrap(recBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);


				if (JAudioLauncher.mobileAEC != null && DialerSettings.ECHO_CANCELLATION_ENABLED) {
					JAudioLauncher.t_analize = System.currentTimeMillis();
					try {
						JAudioLauncher.mobileAEC.farendBuffer(shorts, length);
					} catch (Exception e) {

					}
					if (track.getPlaybackHeadPosition() <= 0)
						user = 0;
					user += track.write(shorts, 0, length);
					JAudioLauncher.t_render = System.currentTimeMillis();
				 	JAudioLauncher.t_player_delay = (JAudioLauncher.t_render +
							(user - track.getPlaybackHeadPosition()) / 16) -
							JAudioLauncher.t_analize;
				}
				else {
					user += track.write(shorts, 0, length);
				}

				CallRecorder.getInstance().bufferIncomingData(CksTabService.getContext(), shorts);
			}
		} catch (Exception e) {
			Log.e("track write", e.getMessage());
		}
	}

	void lock(boolean lock) {
		try {
			if (lock) {
				boolean lockNew = false;
				if (lockFirst || lockLast != lockNew) {
					lockLast = lockNew;
					lock(false);
					lockFirst = false;
					if (pwl == null) {
						Context context = CksTabService.getContext();
						if(context != null){
							PowerManager pm = (PowerManager) context
									.getSystemService(Context.POWER_SERVICE);
							pwl = pm.newWakeLock(
									lockNew ? (PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP)
											: PROXIMITY_SCREEN_OFF_WAKE_LOCK,
									"connect.Receiver");
							pwl.acquire();
						}
					}
				}
			} else {
				lockFirst = true;
				if (pwl != null) {
					pwl.release();
					pwl = null;
				}
			}
		} catch (Exception e) {
		}
		if (lock) {
			if (pwl2 == null) {
				Context context = CksTabService.getContext();
				if(context != null){
					PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
					WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
					pwl2 = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
							"connect.Receiver");
					pwl2.acquire();
					wwl = wm.createWifiLock(3, "connect.Receiver");
					wwl.acquire();
				}
			}
		} else if (pwl2 != null) {
			pwl2.release();
			pwl2 = null;
			wwl.release();
		}
	}

	public void run() {

		Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);

		initAudioTrack();
		Context context = CksTabService.getContext();
		if(context != null){
			am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		}
		short lin[] = new short[BUFFER_SIZE];
		int len = 0;

		try {
			tg = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, ToneGenerator.MAX_VOLUME);
		} catch (Exception e) {
			tg = null;
		}
		boolean reset = false;

		System.gc();
		CksNativeLibrary.getInstance().initRtpQueue();

		int ringTimeout = 0;
		int queueTimeout = 1000;
		lockFirst = true;
		while (running) {
			lock(true);

			try {
				rtp_packet = CksNativeLibrary.rtpQueue.poll(queueTimeout, TimeUnit.MILLISECONDS);

				if (rtp_packet == null) {
					if (StateManager.mState != null && StateManager.mState == CkslibraryEnum.ERinging) {
						ringTimeout += queueTimeout;

						if (ringTimeout >= 5000 && tg != null) {
							try {
								tg.startTone(ToneGenerator.TONE_SUP_RINGTONE);
							} catch (Exception err) {
								tg.stopTone();
								tg.release();
								tg = null;
							}
							ringTimeout = 0;
						}
					}
					continue;
				}

				if (timeout != 0) {
					if (tg != null) {
						tg.stopTone();
						tg.release();
						tg = null;
					}

					if (StateManager.mState != null
							&& StateManager.mState == CkslibraryEnum.ECallEstablished && !reset) {
						track.pause();
						track.flush();
						track.play();
						reset = true;
					}

					if (track != null && track.getState() != AudioTrack.STATE_INITIALIZED) {
						CksNativeLibrary.getInstance().CancelCall();
						continue;
					}

					if (track.getPlayState() != AudioTrack.PLAYSTATE_PLAYING) {
						track.play();
					}

				}
				timeout = 0;

				if (StateManager.SPEAKER_ON != am.isSpeakerphoneOn()) {
					am.setSpeakerphoneOn(StateManager.SPEAKER_ON);
				}
			} catch (Exception e) {
				Log.e("",e.getMessage());
			}
			if (running && timeout == 0) {
				try {
					if (rtp_packet.getRtp() != null) {

//						len = p_type.decode(rtp_packet.getRtp(), lin, rtp_packet.getLength());
//						write(lin, 0, len);
						int choplen = 20;
						byte [] rtpByte = rtp_packet.getRtp();
						int nextIndex = 0;
						int packetlen = rtp_packet.getLength();
						short [] decoded_short = new short[400];
						int decoded;
						len = 0;
						for(int i = 0; i + choplen <= packetlen; i+=choplen)
						{
							byte[] slice = Arrays.copyOfRange(rtpByte, nextIndex, nextIndex+choplen);
							decoded = p_type.decode(slice, decoded_short, slice.length);
							System.arraycopy(decoded_short, 0, lin, len, decoded);
							nextIndex += choplen;
							len += decoded;
						}
						int remByteSize = packetlen % choplen;
						if (remByteSize > 0) {
							byte[] slice = Arrays.copyOfRange(rtpByte, nextIndex, nextIndex+remByteSize);
							decoded = p_type.decode(slice, decoded_short, slice.length);
							System.arraycopy(decoded_short, 0, lin, len, decoded);
							len += decoded;
						}

						write(lin, 0, len);

						// send the received rtp if loopback is enable
						Context innerContext = CksTabService.getContext();
						if (innerContext != null) {
							if (PreferenceSetting.getBool(innerContext,	DialerSettings.KEY_ENABLE_LOOP_BACK, false)) {
								cksLib.SendRtp(rtp_packet.getRtp());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		lock(false);

		if (track != null && track.getState() == AudioTrack.STATE_INITIALIZED) {
			track.stop();
			track.release();
		} else if (track != null
				&& track.getState() != AudioTrack.STATE_INITIALIZED) {
			track.release();
		}

		audioFixer.dispose();

		track = null;

		if (null != tg) {
			tg.stopTone();
			tg.release();
		}
	}

}
