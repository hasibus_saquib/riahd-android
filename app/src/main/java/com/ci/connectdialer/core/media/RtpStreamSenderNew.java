package com.ci.connectdialer.core.media;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Process;
import android.util.Log;

import com.codecs.Codec;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateManager;
import com.ci.connectdialer.core.utilities.CallRecorder;
import com.signal.CkslibraryEnum;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RtpStreamSenderNew extends Thread {

	public static boolean gainEnabled = true;
	private AudioFixer_SDK16 audioFixer = new AudioFixer_SDK16();
	public float micGain = 2.0f;
	Codec p_type;
	int frame_size;
	int total_silence = 0;
	boolean running = false;
	public static boolean changed;

	private CksNativeLibrary cksLib;

	public RtpStreamSenderNew(boolean do_sync, Codec payload_type,
							  long frame_rate, int frame_size, CksNativeLibrary lib) {
		init(do_sync, payload_type, frame_rate, frame_size, lib);
	}

	private void init(boolean do_sync, Codec payload_type, long frame_rate,
					  int frame_size, CksNativeLibrary lib) {
		this.p_type = payload_type;
		this.frame_size = frame_size;

		this.cksLib = lib;

		init();
	}

	public void init() {
		if (Build.MANUFACTURER.toLowerCase().contains("Lenovo".toLowerCase())
				|| Build.MANUFACTURER.toLowerCase().contains(
				"Sony".toLowerCase())
				|| Build.MANUFACTURER.toLowerCase().contains(
				"Asus".toLowerCase())) {
			micGain = 3.0f;
		}
	}

	public void run() {

		Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
		RtpStreamReceiverNew.setMode(AudioManager.MODE_IN_CALL);
		AudioRecord record = null;

		int inputBuffSizeRec = AudioRecord.getMinBufferSize(p_type.samp_rate(),
				AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

		if (inputBuffSizeRec <= 4096) {
			inputBuffSizeRec = 4096 * 3 / 2;
		}

		int audioSource;
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			audioSource = MediaRecorder.AudioSource.VOICE_COMMUNICATION;
		} else {
			audioSource = MediaRecorder.AudioSource.MIC;
		}

		frame_size = 80;

		byte[] sendBuffer = new byte[frame_size / 8];

		int num = 0;
		short[] lin = new short[frame_size];
		short[] linR = new short[frame_size];
		// byte[] recBuffer = new byte[frame_size * 2];

		InputStream alerting = null;
		try {
			Context context = CksTabService.getContext();
			if (context != null) {
				alerting = context.getAssets().open("alerting");
			}
		} catch (IOException e2) {
		}

		p_type.init();
		total_silence = 0;
		boolean bIgnoreSilence = PreferenceSetting.getBool(CksTabService.getContext(), DialerSettings.PREF_IGNORE_SILENCE, false);
		DialerSettings.isEchoCancelationOn(CksTabService.getContext());

		while (running) {

			if (record == null || changed) {
				try {
					if (changed) {
						dispose(record);
						record = null;
						changed = false;

						Context context = CksTabService.getContext();
						if (context != null) {
							AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
							if (android.os.Build.VERSION.SDK_INT >= 11) {
								am.setMode(AudioManager.MODE_IN_COMMUNICATION);
							} else {
								am.setMode(AudioManager.MODE_IN_CALL);
							}
							am.setStreamSolo(2, true);
						}

					}
					record = new AudioRecord(audioSource, p_type.samp_rate(),
							AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, inputBuffSizeRec);

					if (record != null
							&& record.getState() != AudioRecord.STATE_INITIALIZED) {
						dispose(record);
						record = null;
					}

					if (record != null) {
						audioFixer.fixAudioRecord(record);
						sleep(1000);
						record.startRecording();

						if (record.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
							dispose(record);
							record = null;
						}
					}
				} catch (Exception e) {
					Log.e("",e.toString());
					dispose(record);
					record = null;
				}
			}

			if (record == null)
				continue;

			num = record.read(linR, 0, frame_size);

			if (DialerSettings.ECHO_CANCELLATION_ENABLED) {
				long total_delay_cal = JAudioLauncher.t_player_delay + 10;
				try {
					JAudioLauncher.mobileAEC.echoCancellation(linR, null, lin, (short) (frame_size), (short) total_delay_cal);
				} catch (Exception e) {
					lin = linR;
				}
			}
			else {
				lin = linR;
			}

//			CallRecorder.saveOutgoingData(CksTabService.getContext(), lin);
			CallRecorder.getInstance().saveIncomingAndOutgoingData(CksTabService.getContext(), lin);

			if (num == AudioRecord.ERROR_BAD_VALUE) {
				continue;
			}

			/*test silence
			 */

			boolean bSilent_pakcet = false;

			if ((StateManager.mState == CkslibraryEnum.ECallEstablished || StateManager.mState == CkslibraryEnum.ERinging)
					&& bIgnoreSilence && DialerSettings.lastReceivedRtpIndex != -1) {
				int peakindex;
				int threshold = 1000;
				boolean voicedata = false;
				for (peakindex = 0; peakindex < frame_size; peakindex++) {
					if (lin[peakindex] >= threshold || lin[peakindex] <= -threshold)
						voicedata = true;
				}
				if (voicedata) {
					Log.e("record", "voice data");
					bSilent_pakcet = false;
				}
				else {
					bSilent_pakcet = true;
					Log.e("record", "silence");
				}
			}

			/*
			end
			 */

			byte[] recBuffer = short2byte(lin);
			int reallySampledBytes = recBuffer.length;
			int recBufferBytePtr = 0;

			float gainFactor = StateManager.MIC_GAIN_FACTOR;

			int i = 0;
			while (i < reallySampledBytes) {
				float sample = (float) (recBuffer[recBufferBytePtr + i] & 0xFF | recBuffer[recBufferBytePtr
						+ i + 1] << 8);
				// THIS is the point were the work is done:
				// Increase level by about 6dB:
				sample *= gainFactor;
				// Or increase level by 20dB:
				// sample *= 10;
				// Or if you prefer any dB value, then calculate the gain factor outside the loop
				// float gainFactor = (float)Math.pow( 10., dB / 20. );    // dB to gain factor
				// sample *= gainFactor;
				if (sample >= 32767f) {
					recBuffer[recBufferBytePtr + i] = (byte) 0xFF;
					recBuffer[recBufferBytePtr + i + 1] = 0x7F;
				} else if (sample <= -32768f) {
					recBuffer[recBufferBytePtr + i] = 0x00;
					recBuffer[recBufferBytePtr + i + 1] = (byte) 0x80;
				} else {
					int s = (int) (0.5f + sample); // Here, dithering would be
					// more appropriate
					recBuffer[recBufferBytePtr + i] = (byte) (s & 0xFF);
					recBuffer[recBufferBytePtr + i + 1] = (byte) (s >> 8 & 0xFF);
				}
				i += 2;
			}

			short[] shorts = new short[recBuffer.length / 2];

			ByteBuffer.wrap(recBuffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);

			// Here RECORD outgoing voice data (shorts)

			num = p_type.encode(shorts, 0, sendBuffer, num);

			if (StateManager.mIncoming == true) {
				try {
					if (alerting != null) {
						if (alerting.available() < 20)
							alerting.reset();
						num = alerting.read(sendBuffer, 0, 20);
					}
				} catch (Exception e) {
				}
			}

			if (bSilent_pakcet)
				cksLib.SilentPacket(sendBuffer);
			else
				cksLib.SendRtp(sendBuffer);

		}

		dispose(record);
		record = null;

		p_type.close();

		Context context = CksTabService.getContext();
		if (context != null) {
			AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			am.setSpeakerphoneOn(false);
			am.setMode(AudioManager.MODE_NORMAL);
		}
	}

	private byte[] short2byte(short[] sData) {
		int shortArrsize = sData.length;
		byte[] bytes = new byte[shortArrsize * 2];
		for (int i = 0; i < shortArrsize; i++) {
			bytes[i * 2] = (byte) (sData[i] & 0x00FF);
			bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
			sData[i] = 0;
		}
		return bytes;
	}

	public boolean isRunning() {
		return running;
	}

	public void powerOn() throws IllegalStateException {
		running = true;
		start();
	}

	public void halt() {
		running = false;
	}

	private void dispose(AudioRecord record) {
		if (record != null
				&& record.getState() == AudioRecord.STATE_INITIALIZED) {
			record.stop();
			record.release();

			audioFixer.dispose();
		} else if (record != null
				&& record.getState() != AudioRecord.STATE_INITIALIZED) {
			record.release();
		}
	}

}
