package com.ci.connectdialer.core.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.auth.AuthenticationManager;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Masud Rashid on 30/11/2016.
 */
public class AsyncTasks {

    public static class AppUpdateCheck extends AsyncTask<Void, String, String> {

        public interface UpdateAppListener{
            void onAppUpdateChecked(String result);
        }

        private Context mContext;
        private UpdateAppListener mListener;

        public AppUpdateCheck(Context context, UpdateAppListener listener){
            mContext = context;
            mListener = listener;
        }

        @Override
        protected String doInBackground(Void... params) {

            String url = "https://play.google.com/store/apps/details?id="+mContext.getPackageName().trim();
            try {
                Document doc = Jsoup.connect(url).get();
                Elements elements = doc.select("[itemprop=softwareVersion]");

                return elements.first().text();
            }
            catch(Exception e) {
                Log.e("", e.toString());
            }

            return null;
        }

        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            mListener.onAppUpdateChecked(result);
        }
    }


    public static class ShowServerInfo extends AsyncTask<String, Void, String> {

        private Context mContext;
        public ShowServerInfo(Context context){
            mContext = context;
        }

        private ProgressDialog mProgressDialog ;

        @Override
        protected void onPreExecute() {
            if(mProgressDialog == null){
                mProgressDialog = new ProgressDialog(mContext);
                mProgressDialog.setCancelable(true);
            }
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            CksNativeLibrary lib = CksNativeLibrary.getInstance();
            String authServer = "";
            String vpnServer = "";
            String dnsServer = "";
            if(lib != null){
                authServer = AuthenticationManager.AUTHENTICATION_BY_HTTPS ? "https" : lib.GetAuthAddress();
                vpnServer = lib.GetVpnAddress();
                dnsServer = lib.GetDnsAddress();
            }
            StringBuilder sb = new StringBuilder()
                    .append("Auth Ip : ").append(authServer).append("\n")
                    .append("Vpn Ip : ").append(vpnServer).append("\n")
                    .append("Dns Ip : ").append(dnsServer).append("\n")
                    .append("MCC: ").append(AuthenticationManager.mCountryCode)
                    .append(" MNC: ").append(AuthenticationManager.mTeleOperatorCode).append("\n")
                    .append("Profile ID: ").append(DialerSettings.DIALER_PROFILE_ID).append("\n");

//                    .append("Balance Ip : ");
//                    .append(
//                            PhoneInformation.getIpAddressFromInt(PreferenceSetting.getInt(
//                                    mContext, DialerSettings.BALANCE_IP,0)))
//                    .append(":").append(PreferenceSetting.getInt(
//                            mContext, DialerSettings.BALANCE_PORT, 0));

            String ipListEncoded = PreferenceSetting.getString(mContext,DialerSettings.PREF_VPN_IP_LIST, "");
            List<String> ipList = PhoneInformation.getIpAddressListFromByteArray(Base64.decode(ipListEncoded, Base64.DEFAULT));
            String formatted = "";
            if (ipList.size() > 0) {
                sb.append("vpn list:").append("\n");
            }
            for( int i=0; i<ipList.size(); i++){
                formatted = (i+1) + ". " + ipList.get(i) + "\n";
                sb.append(formatted);
            }

            return sb.toString();
        }


        @Override
        protected void onPostExecute(String result) {
            if(mProgressDialog != null){
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }

            Alert.showOk(mContext, result);
        }
    }



    public static class GoogleDnsDomainResolve extends AsyncTask<String, Void, String> {

        public interface IGoogleDNSDomainResolve{
            void showGoogleDNSAuthDomainResponse(String response);
        }

        private Context mContext;
        private IGoogleDNSDomainResolve mListener;


        public GoogleDnsDomainResolve(Context context, IGoogleDNSDomainResolve listener){
            mContext = context;
            mListener = listener;
        }

        private ProgressDialog progressGoogleDNS;

        @Override
        protected void onPreExecute() {
            if(progressGoogleDNS == null){
                progressGoogleDNS = new ProgressDialog(mContext);
                progressGoogleDNS.setTitle("Loading");
                //progressGoogleDNS.setMessage("Wait while loading...");
                progressGoogleDNS.setCancelable(true); // disable dismiss by tapping outside of the dialog
            }
            progressGoogleDNS.show();
        }

        @Override
        protected String doInBackground(String... param) {
            try{
                String domain = param[0];

                String url = "https://dns.google.com/resolve?name="+ domain;

                String response;
                DefaultHttpClient httpClient = new DefaultHttpClient();

                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                response = EntityUtils.toString(httpResponse.getEntity());
                if (response != null && !response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
                    return jsonObject.toString();
                }

            } catch (Exception e) {
                Log.e("Domain Resolve Error", e.toString());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            progressGoogleDNS.dismiss();
            mListener.showGoogleDNSAuthDomainResponse(result);
        }
    }

    public static class DnsDomainResolvTask extends AsyncTask<String, Void, List<String>> {

        public interface DnsDomainResolveListener{
            void onDnsReloved(List<String> result);
        }

        private DnsDomainResolveListener mListener;

        public DnsDomainResolvTask(DnsDomainResolveListener listener){
            mListener = listener;
        }

        @Override
        protected List<String> doInBackground(String... strings) {
            List<String> ipList = new ArrayList<>();

            try {
                String domain1 = PhoneInformation.decodeDnsServerUrl(ApplicationValidator.ENCRYPTION_KEY, ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST[0]);
                String domain2 = PhoneInformation.decodeDnsServerUrl(ApplicationValidator.ENCRYPTION_KEY, ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST[1]);

                String url1 = "https://dns.google.com/resolve?name="+ domain1;
                String url2 = "https://dns.google.com/resolve?name="+ domain2;

//				String response = sendRequest(url1);
//				ipList = getIpListFromJsonResponse(response, ipList);
//				response = sendRequest(url2);
//				ipList = getIpListFromJsonResponse(response, ipList);

                String response;
                final HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
                HttpConnectionParams.setSoTimeout(httpParams, 2000);
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

                HttpGet httpGet = new HttpGet(url1);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                response = EntityUtils.toString(httpResponse.getEntity());
                if (response != null && !response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Answer");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ipList.add(jsonArray.getJSONObject(i).getString("data"));
                    }
                }

                httpGet = new HttpGet(url2);
                httpResponse = httpClient.execute(httpGet);
                response = EntityUtils.toString(httpResponse.getEntity());
                if (response != null && !response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Answer");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ipList.add(jsonArray.getJSONObject(i).getString("data"));
                    }
                }
            } catch (Exception e) {
                Log.e("Domain Resolve Error", e.toString());
                e.printStackTrace();
            }
            return ipList;
        }

        @Override
        protected void onPostExecute(List<String> result) {
            mListener.onDnsReloved(result);
        }
    }


    public static final int CONNECTION_TIMEOUT = 5000;
    public static final int IP_REQ_TIMEOUT = 10000;
    public static final int REQ_TIMEOUT_IN_SECONDS = 10;

    public static class UrlConnector implements Callable<Object> {
        private String domain;
        private boolean success=false;

        public UrlConnector(String domain) {
            this.domain = domain;
        }

        @Override
        public Object call() throws Exception {
            try {
                URL myUrl =  new URL(domain);
                URLConnection connection = myUrl.openConnection();
                connection.setConnectTimeout(IP_REQ_TIMEOUT);
                connection.connect();
                success = true;
            } catch (Exception e) {
                success = false;
            }
            return null;
        }

        public synchronized boolean get() {
            return success;
        }
    }


    private static String COUNTRY_CODE = "0";
    private static String ISP_NAME = "0";

    public static class HttpRequesttoIpApi implements Callable<Object> {
        private String domain = "http://ip-api.com/json";
        private boolean success=false;
        private String country = "0";
        private String isp = "0";

        public HttpRequesttoIpApi() {
        }

        @Override
        public Object call() throws Exception {
            try {

                URL myUrl =  new URL(domain);
                URLConnection connection = myUrl.openConnection();
                connection.setConnectTimeout(IP_REQ_TIMEOUT);
                connection.setReadTimeout(IP_REQ_TIMEOUT);
                InputStream response = connection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream, result="";
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

                JSONObject jsonObject = new JSONObject(result);
                COUNTRY_CODE = jsonObject.getString("countryCode");
                ISP_NAME = jsonObject.getString("isp");
                success = true;
                Log.d("Authentication", result);

            } catch (Exception e) {
                Log.d("Authentication", e.toString());
                success = false;
            }
            if (success) {
                country = COUNTRY_CODE;
                isp = ISP_NAME;
            }
            COUNTRY_CODE = "0";
            ISP_NAME = "0";
            return null;
        }

        public synchronized boolean get() {
            return success;
        }

        public String getCountry() {
            return country;
        }

        public String getIsp() {
            return isp;
        }
    }

    public static class HttpRequesttoEurekapi implements Callable<Object> {
        private String domain = "http://api.eurekapi.com/iplocation/v1.8/locateip?key=SAKD8UG736JU745SKQYZ&ip=local-ip&format=json&&compact=y";
        private boolean success=false;
        private String country = "0";
        private String isp = "0";

        public HttpRequesttoEurekapi() {
        }

        @Override
        public Object call() throws Exception {
            try {

                URL myUrl =  new URL(domain);
                URLConnection connection = myUrl.openConnection();
                connection.setConnectTimeout(IP_REQ_TIMEOUT);
                connection.setReadTimeout(IP_REQ_TIMEOUT);
                InputStream response = connection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream, result="";
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

                JSONObject jsonFullResponse = new JSONObject(result);
                JSONObject jsonObject = jsonFullResponse.getJSONObject("geolocation_data");

//                JSONObject jsonObject = new JSONObject(result);
                COUNTRY_CODE = jsonObject.getString("country_code_iso3166alpha2");
                ISP_NAME = jsonObject.getString("isp");
                success = true;
                Log.d("Authentication", result);

            } catch (Exception e) {
                Log.d("Authentication", e.toString());
                success = false;
            }
            if (success) {
                country = COUNTRY_CODE;
                isp = ISP_NAME;
            }
            COUNTRY_CODE = "0";
            ISP_NAME = "0";
            return null;
        }

        public synchronized boolean get() {
            return success;
        }

        public String getCountry() {
            return country;
        }

        public String getIsp() {
            return isp;
        }
    }


//    public static CountryNetworkIPInfo GetNetworkFromIp() {
//
//        CountryNetworkIPInfo countryNetworkIPInfo = new CountryNetworkIPInfo(COUNTRY_CODE, ISP_NAME, DialerSettings.ConnectivityType.Connectivity_Limited);
//        try{
//
//            UrlConnector urlConnectorFb = new UrlConnector("https://www.facebook.com");
//            UrlConnector urlConnectorGoogle = new UrlConnector("https://www.google.com");
//            HttpRequesttoIpApi httpRequesttoIpApi = new HttpRequesttoIpApi();
////            HttpRequesttoEurekapi httpRequesttoIpApi = new HttpRequesttoEurekapi();
//
//            ExecutorService service = Executors.newFixedThreadPool(3);
//            service.submit(urlConnectorFb);
//            service.submit(urlConnectorGoogle);
//            service.submit(httpRequesttoIpApi);
//
//            service.shutdown();
//            service.awaitTermination(IP_REQ_TIMEOUT, TimeUnit.SECONDS);
//
//            if( urlConnectorGoogle.get()) {
//                countryNetworkIPInfo.setConnectivityType(DialerSettings.ConnectivityType.Connectivity_Full);
//            } else if (urlConnectorFb.get()) {
//                countryNetworkIPInfo.setConnectivityType(DialerSettings.ConnectivityType.Connectivity_Selected);
//            } else {
//                countryNetworkIPInfo.setConnectivityType(DialerSettings.ConnectivityType.Connectivity_Limited);
//            }
//
//            if (httpRequesttoIpApi.get()) {
//                countryNetworkIPInfo.setCountry(httpRequesttoIpApi.getCountry());
//                countryNetworkIPInfo.setIsp(httpRequesttoIpApi.getIsp());
//            }
//
//
//        } catch (Exception e) {
//            // Handle your exceptions
//            Log.e("CountryCodeFromIP", e.toString());
//        }
//        return countryNetworkIPInfo;
//    }

    public static class DNSResolver implements Runnable {
        private String domain;
        private boolean success=false;

        public DNSResolver(String domain) {
            this.domain = domain;
        }

        public void run() {
            try {
                URL myUrl =  new URL(domain);
                URLConnection connection = myUrl.openConnection();
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.connect();
                success = true;
            } catch (Exception e) {
                success = false;
            }
        }

        public synchronized boolean get() {
            return success;
        }
    }

    public static DialerSettings.ConnectivityType httpsConnectionCheckSync() {

        DialerSettings.ConnectivityType type;
        try{

            UrlConnector urlConnectorFb = new UrlConnector("https://www.facebook.com");
            UrlConnector urlConnectorGoogle = new UrlConnector("https://www.google.com");

            ExecutorService service = Executors.newFixedThreadPool(2);
            service.submit(urlConnectorFb);
            service.submit(urlConnectorGoogle);

            service.shutdown();
            service.awaitTermination(REQ_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

            if( urlConnectorGoogle.get()) {
                type = DialerSettings.ConnectivityType.Connectivity_Full;
            } else if (urlConnectorFb.get()) {
                type = DialerSettings.ConnectivityType.Connectivity_Selected;
            } else {
                type = DialerSettings.ConnectivityType.Connectivity_Limited;
            }

        } catch (Exception e) {
            // Handle your exceptions
            Log.d("Connectivity-check", e.toString());
            type = DialerSettings.ConnectivityType.Connectivity_Limited;
        }
        return type;
    }

    public static class DomainFrontingTask extends AsyncTask<String, Void, String> {

        public interface DomainFrontingListener{
            void onDomainFronting(String result);
        }

        private DomainFrontingListener mListener;

        public DomainFrontingTask(DomainFrontingListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{
//                String v1 = "{" +
//                        "\"opc\":70761," + //
//                        "\"popc\":0," +
//                        "\"apv\":\"10.03\"," + //
//                        "\"vpv\":21," + //
//                        "\"mcc\":\"470\"," +
//                        "\"mnc\":\"03\"," +
//                        "\"os\":2" +
//                        "\"updateEpoch\":1486541277" +
//                        "}";

                String v1 = param[0];

                String url = "https://www.google.com";
                String X_Auth_Req = Base64.encodeToString(v1.getBytes(), Base64.DEFAULT).replaceAll("[\r\n]", "");
                CksNativeLibrary lib = CksNativeLibrary.getInstance();
                String Host = lib.GetAAAc();
                URL myUrl =  new URL(url);
                URLConnection connection = myUrl.openConnection();
                int timeoutseconds = 5000;
                connection.setConnectTimeout(timeoutseconds);
                connection.setReadTimeout(timeoutseconds);
                connection.setRequestProperty("Host", Host);
                connection.setRequestProperty("X-Auth-Req", X_Auth_Req);

                InputStream response = connection.getInputStream();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream;
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

//                System.out.println("result: " + result);

            } catch (Exception e) {
                Log.e("domainFrontTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onDomainFronting(result);
            }
        }
    }

	public static class CountryCodeFromIP extends AsyncTask<String, Void, String> {

        public interface CountryCodeListener {
            void onCountryCodeFetch(String result);
        }

        private CountryCodeListener mListener;

        public CountryCodeFromIP(CountryCodeListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... param) {
            try{

                String url = "http://ip-api.com/json";
                URL myUrl =  new URL(url);
                URLConnection connection = myUrl.openConnection();
                InputStream response = connection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream, result="";
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

                JSONObject jsonObject = new JSONObject(result);
                return jsonObject.getString("countryCode");

            } catch (Exception e) {
                Log.e("CountryCodeFromIP", e.toString());
            }
            return "0";
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onCountryCodeFetch(result);
            }
        }
    }

public static class ResolveDomainByGoogle implements Callable<Object> {
        private String domain;
        private boolean success=false;
        private String result = "";

        public ResolveDomainByGoogle(String domain) {
            this.domain = domain;
        }

        @Override
        public Object call() throws Exception {
            try {
                URL myUrl =  new URL(domain);
                URLConnection connection = myUrl.openConnection();
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(CONNECTION_TIMEOUT);
                InputStream response = connection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream;
                while ((stream = streamReader.readLine()) != null)
                    result += stream;
                success = true;
            } catch (Exception e) {
                success = false;
            }
            return null;
        }

        public synchronized boolean get() {
            return success;
        }

        public synchronized String getResult() {
            return result;
        }
    }

    public static List<String> getAuthIpListFromDomains() {

        List<String> ipList = new ArrayList<>();
        try{
            String domain1 = PhoneInformation.decodeDnsServerUrl(ApplicationValidator.ENCRYPTION_KEY, ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST[0]);
            String domain2 = PhoneInformation.decodeDnsServerUrl(ApplicationValidator.ENCRYPTION_KEY, ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST[1]);

            String url1 = "https://dns.google.com/resolve?name="+ domain1;
            String url2 = "https://dns.google.com/resolve?name="+ domain2;

            ResolveDomainByGoogle resolveDomainOneByGoogle = new ResolveDomainByGoogle(url1);
            ResolveDomainByGoogle resolveDomainTwoByGoogle = new ResolveDomainByGoogle(url2);

            ExecutorService service = Executors.newFixedThreadPool(2);
            service.submit(resolveDomainOneByGoogle);
            service.submit(resolveDomainTwoByGoogle);

            service.shutdown();
            service.awaitTermination(REQ_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

            String response = resolveDomainOneByGoogle.getResult();
            if (response != null && !response.isEmpty()) {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("Answer");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ipList.add(jsonArray.getJSONObject(i).getString("data"));
                }
            }

            response = resolveDomainTwoByGoogle.getResult();
            if (response != null && !response.isEmpty()) {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("Answer");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ipList.add(jsonArray.getJSONObject(i).getString("data"));
                }
            }

        } catch (Exception e) {
            // Handle your exceptions
            Log.e("CountryCodeFromIP", e.toString());
        }
        return ipList;
    }

    public static class AuthenticationByDomainFronting implements Callable<Object> {
        private String requstString;
        private String result = "";

        public AuthenticationByDomainFronting(String requstString) {
            this.requstString = requstString;
        }

        @Override
        public Object call() throws Exception {
            try {

                String url = "https://www.google.com";
                String X_Auth_Req = Base64.encodeToString(requstString.getBytes(), Base64.DEFAULT).replaceAll("[\r\n]", "");
                CksNativeLibrary lib = CksNativeLibrary.getInstance();
                String Host = lib.GetAAAc();
                URL myUrl =  new URL(url);
                URLConnection connection = myUrl.openConnection();
                int timeoutseconds = CONNECTION_TIMEOUT;
                connection.setConnectTimeout(timeoutseconds);
                connection.setReadTimeout(timeoutseconds);
                connection.setRequestProperty("Host", Host);
                connection.setRequestProperty("X-Auth-Req", X_Auth_Req);

                InputStream response = connection.getInputStream();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream;
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

               Log.e("https-auth", "HTTPS result: " + result);
            } catch (Exception e) {
                Log.e("https-auth", "HTTPS result: " + e.toString());
            }
            return null;
        }
        public synchronized String getResult() {
            return result;
        }
    }

    public static String doAuthenticationByDomainFronting(String requestParams) {

        String result = "";

        try{
            AuthenticationByDomainFronting authenticationByDomainFronting = new AuthenticationByDomainFronting(requestParams);

            ExecutorService service = Executors.newFixedThreadPool(1);
            service.submit(authenticationByDomainFronting);

            service.shutdown();
            service.awaitTermination(REQ_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

            result = authenticationByDomainFronting.getResult();

            Log.d("Authentication", "doAuthenticationByDomainFronting: " + result);

        } catch (Exception e) {
            // Handle your exceptions
            Log.e("Authentication", "doAuthenticationByDomainFronting: " + e.toString());
        }
        return result;
    }


    public static class SendOTPTask extends AsyncTask<String, Void, String> {

        public interface SendOTPListener{
            void onSendOTP(String result);
        }

        private SendOTPListener mListener;

        public SendOTPTask(SendOTPListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                String phoneNumber = param[0];

                String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/ott/ott.jsp?actionId=sendOTP&msisdn=" + phoneNumber;
                URL myUrl =  new URL(url);
                URLConnection connection = myUrl.openConnection();
                int timeoutseconds = 5000;
                connection.setConnectTimeout(timeoutseconds);
                connection.setReadTimeout(timeoutseconds);

                InputStream response = connection.getInputStream();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                String stream;
                while ((stream = streamReader.readLine()) != null)
                    result += stream;

//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("domainFrontTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onSendOTP(result);
            }
        }
    }

    public static class GetCredentialsTask extends AsyncTask<String, Void, String> {

        public interface GetCredentialsListener{
            void onGetCredentials(String result);
        }

        private GetCredentialsListener mListener;
        private ProgressDialog pd;
        public GetCredentialsTask(GetCredentialsListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                if (param.length > 1) {
                    String phoneNumber = param[0];
                    String otp = param[1];

                    //phoneNumber = "919310771933";
                    String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/ott/ott.jsp?actionId=getCredentials&msisdn=" + phoneNumber + "&otp=" + otp;
                    URL myUrl =  new URL(url);
                    URLConnection connection = myUrl.openConnection();
                    int timeoutSeconds = 5000;
                    connection.setConnectTimeout(timeoutSeconds);
                    connection.setReadTimeout(timeoutSeconds);

                    InputStream response = connection.getInputStream();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                    String stream;
                    while ((stream = streamReader.readLine()) != null)
                        result += stream;
                }

//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("domainFrontTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onGetCredentials(result);
            }
        }
    }

    public static class RedeemVoucherTask extends AsyncTask<String, Void, String> {

        public interface RedeemVoucherListener{
            void onRedeemVoucher(String result);
        }

        private RedeemVoucherListener mListener;
        private ProgressDialog pd;
        public RedeemVoucherTask(RedeemVoucherListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                if(param.length > 2) {
                    String subscriberId = param[0];
                    String password = param[1];
                    String pin = param[2];

                    //phoneNumber = "919310771933";
                    String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/RechargeUserJson.jsp?actionId=rechargeWithPassword&subscriberId=" + subscriberId + "&pin=" + pin + "&password=" + password + "&userId=" + subscriberId;
                    URL myUrl = new URL(url);
                    URLConnection connection = myUrl.openConnection();
                    int timeoutSeconds = 5000;
                    connection.setConnectTimeout(timeoutSeconds);
                    connection.setReadTimeout(timeoutSeconds);

                    InputStream response = connection.getInputStream();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                    String stream;
                    while ((stream = streamReader.readLine()) != null)
                        result += stream;
                }
//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("redeemVoucherTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onRedeemVoucher(result);
            }
        }
    }

    public static class PaymentHistoryTask extends AsyncTask<String, Void, String> {

        public interface PaymentHistoryListener{
            void onReceivedPaymentHistory(String result);
        }

        private PaymentHistoryListener mListener;
        private ProgressDialog pd;
        public PaymentHistoryTask(PaymentHistoryListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                if (param.length > 3) {
                    String subscriberId = param[0];
                    String password = param[1];
                    String fromDateString = Uri.encode(param[2]);
                    String toDateString = Uri.encode(param[3]);

                    //phoneNumber = "919310771933";
                    //from=2012-11-26%2010:42:27
                    //to=2012-11-26%2012:42:27
                    String parameters = "subscriberId=" + subscriberId + "&from=" + fromDateString + "&to=" + toDateString + "&password=" + password + "&userId=" + subscriberId;
                    String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/softphoneRequestJson.jsp?actionId=mypaymentWithPassword&" + parameters;
                    URL myUrl =  new URL(url);
                    URLConnection connection = myUrl.openConnection();
                    int timeoutSeconds = 5000;
                    connection.setConnectTimeout(timeoutSeconds);
                    connection.setReadTimeout(timeoutSeconds);

                    InputStream response = connection.getInputStream();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                    String stream;
                    while ((stream = streamReader.readLine()) != null)
                        result += stream;
                }

//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("paymentHistoryTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onReceivedPaymentHistory(result);
            }
        }
    }

    public static class FundTransferTask extends AsyncTask<String, Void, String> {

        public interface FundTransferListener{
            void onFundTransferCompletion(String result);
        }

        private FundTransferListener mListener;
        private ProgressDialog pd;
        public FundTransferTask(FundTransferListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                if (param.length > 3) {
                    String subscriberId = param[0];
                    String password = param[1];
                    String toUserId = Uri.encode(param[2]);
                    String transferAmount = Uri.encode(param[3]);

                    //softphoneRequestJson.jsp?actionId=endUserAmountTransferWithPassword&subscriberId=9090&password=9191&to=43333&amount=10.0&userId=9090
                    String parameters = "subscriberId=" + subscriberId + "&password=" + password + "&to=" + toUserId + "&amount=" + transferAmount + "&userId=" + subscriberId;
                    String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/softphoneRequestJson.jsp?actionId=endUserAmountTransferWithPassword&" + parameters;
                    URL myUrl =  new URL(url);
                    URLConnection connection = myUrl.openConnection();
                    int timeoutSeconds = 5000;
                    connection.setConnectTimeout(timeoutSeconds);
                    connection.setReadTimeout(timeoutSeconds);

                    InputStream response = connection.getInputStream();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                    String stream;
                    while ((stream = streamReader.readLine()) != null)
                        result += stream;
                }

//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("fundTransferTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onFundTransferCompletion(result);
            }
        }
    }

    public static class FundTransferHistoryTask extends AsyncTask<String, Void, String> {

        public interface FundTransferHistoryListener{
            void onReceivedFundTransferHistory(String result);
        }

        private FundTransferHistoryListener mListener;
        private ProgressDialog pd;
        public FundTransferHistoryTask(FundTransferHistoryListener listener){
            mListener = listener;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... param) {

            String result="";

            try{

                if (param.length > 3) {
                    String subscriberId = param[0];
                    String password = param[1];
                    String fromDateString = Uri.encode(param[2]);
                    String toDateString = Uri.encode(param[3]);

                    //softphoneRequestJson.jsp?actionId=getAmountTransferWithPassword&subscriberId=gowthami&from=2012-9-7%2000:00:00&to=2012-9-11%2000:00:00&password=gowthami&userId=gowthami
                    String parameters = "subscriberId=" + subscriberId + "&from=" + fromDateString + "&to=" + toDateString + "&password=" + password + "&userId=" + subscriberId;
                    String url = "http://"+DialerSettings.USER_REGISTRATION_URL+"/softphoneRequestJson.jsp?actionId=getAmountTransferWithPassword&" + parameters;
                    URL myUrl =  new URL(url);
                    URLConnection connection = myUrl.openConnection();
                    int timeoutSeconds = 5000;
                    connection.setConnectTimeout(timeoutSeconds);
                    connection.setReadTimeout(timeoutSeconds);

                    InputStream response = connection.getInputStream();

                    BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

                    String stream;
                    while ((stream = streamReader.readLine()) != null)
                        result += stream;
                }

//               Log.e("https-auth", "HTTPS result: " + result);

            } catch (Exception e) {
                Log.e("paymentHistoryTask", e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if( mListener!=null) {
                mListener.onReceivedFundTransferHistory(result);
            }
        }
    }

//    public static class CountryCodeFromIP extends AsyncTask<String, Void, String> {
//
//        public interface CountryCodeListener {
//            void onCountryCodeFetch(String result);
//        }
//
//        private CountryCodeListener mListener;
//
//        public CountryCodeFromIP(CountryCodeListener listener){
//            mListener = listener;
//        }
//
//        @Override
//        protected void onPreExecute() {
//        }
//
//        @Override
//        protected String doInBackground(String... param) {
//            try{
//
//                String url = "http://ip-api.com/json";
//                URL myUrl =  new URL(url);
//                URLConnection connection = myUrl.openConnection();
//                InputStream response = connection.getInputStream();
//                BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
//
//                String stream, result="";
//                while ((stream = streamReader.readLine()) != null)
//                    result += stream;
//
//                JSONObject jsonObject = new JSONObject(result);
//                return jsonObject.getString("countryCode");
//
//            } catch (Exception e) {
//                Log.e("CountryCodeFromIP", e.toString());
//            }
//            return "0";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            if( mListener!=null) {
//                mListener.onCountryCodeFetch(result);
//            }
//        }
//    }
//
//    private static String COUNTRY_CODE = "0";
//    public static String getCountryCodeFromIp(){
//
//        try {
//            Thread t = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try{
//
//                        String url = "http://ip-api.com/json";
//                        URL myUrl =  new URL(url);
//                        URLConnection connection = myUrl.openConnection();
//                        InputStream response = connection.getInputStream();
//                        BufferedReader streamReader = new BufferedReader(new InputStreamReader(response, "UTF-8"));
//
//                        String stream, result="";
//                        while ((stream = streamReader.readLine()) != null)
//                            result += stream;
//
//                        JSONObject jsonObject = new JSONObject(result);
//                        COUNTRY_CODE = jsonObject.getString("countryCode");
//
//                    } catch (Exception e) {
//                        Log.e("CountryCodeFromIP", e.toString());
//                    }
//                }
//            },"CountryCodeThread");
//            t.start();
//            t.join(CONNECTION_TIMEOUT);
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        String mcc = COUNTRY_CODE;
//        COUNTRY_CODE = "0";
//        return mcc;
//    }

}
