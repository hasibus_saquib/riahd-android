package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Masud Rashid on 5/12/2016.
 */
public class CksUtil {

    public static byte[] getByteArraryForIp(String[] ipList) {
        byte[] dnsByteList = new byte[ipList.length * 4];
        int iLength = 0;
        for (String anIpList : ipList) {
            int ipInt = PhoneInformation.getIntFromIpAddress(anIpList);
            byte[] ipInByte = PhoneInformation.getReverseByteFromInt(ipInt);
            System.arraycopy(ipInByte, 0, dnsByteList, iLength, ipInByte.length);
            iLength += ipInByte.length;
        }
        return dnsByteList;
    }
    public static byte[] getByteArraryForDomainList(){
        byte[] dnsByteList = new byte[ApplicationValidator.DNS_IP_LIST.length * 4];

        int iLength = 0;
        for(int i = 0; i< ApplicationValidator.DNS_IP_LIST.length; i++){
//			int ipInt = PhoneInformation.getIntFromIpAddress(DNS_IP_LIST[i]);
//			byte[] ipInByte = PhoneInformation.getByteFromInt(ipInt);
            byte[] ipInByte = PhoneInformation.getByteFromInt(ApplicationValidator.DNS_IP_LIST[i]);
            System.arraycopy(ipInByte, 0, dnsByteList, iLength, ipInByte.length);
            iLength += ipInByte.length;
        }
        return dnsByteList;
    }

    public static String getInternalDetailsFormatted(Context context) {
        String ret = "";
        String temp;
        CksNativeLibrary cksNative = CksNativeLibrary.getInstance();

        ret += "\nApplication version : " + PhoneInformation.getAppVersion(context) + "\n";
        ret += "\nComponent version : " + cksNative.GetComponentVersion() + "\n";
        ret += "\nDialer ID : " + DialerSettings.DIALER_ID + "\n";
        ret += "\nAuth Protocol version : " + cksNative.GetAuthProtocolVersion() + "\n";
        ret += "\nVPN Protocol version : " + cksNative.GetVpnProtocolVersion() + "\n";

        String auhtList = "";
        for(int i = 0; i< ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST.length; i++){
            auhtList += (i+1) + ". " + PhoneInformation.decodeDnsServerUrl(ApplicationValidator.ENCRYPTION_KEY, ApplicationValidator.ENCRYPTED_AUTH_DOMAIN_LIST[i]) + "\n";
        }
        ret += "\nAuth Domains : \n" + auhtList + "\n";

//		String reportDomainList = PhoneInformation.decodeDnsServerUrl(ENCRYPTION_KEY, ENCRYPTED_REPORT_DOMAIN) + ", ";
//		ret += "\nReport domain list : " + reportDomainList + "\n";

        String dnsList = "";
        for(int i = 0; i< ApplicationValidator.DNS_IP_LIST.length; i++){
            dnsList += (i+1) + ". " + PhoneInformation.getIpAddressFromIntReverseByte(ApplicationValidator.DNS_IP_LIST[i]) + "\n";
        }
        ret += "\nDNS IP list : \n" + dnsList + "\n";

        return ret;
    }

    public static String getRuntimeDetailsFormatted(Context context) {
        String ret = "";
        String temp;
        CksNativeLibrary cksNative = CksNativeLibrary.getInstance();

        int opCode = PreferenceSetting.getInt(context, DialerSettings.PREF_OPCODE, 0);
        ret += "\nOpCode : " + opCode + "\n";

        String username = PreferenceSetting.getString(context, DialerSettings.PREF_USERNAME, "");
        ret += "\nUsername : " + username + "\n";

        String countryCode = "0";
        String teleOperatorCode = "0";
        if( StateManager.TEST_MCC_MNC_ENABLED)
        {
            countryCode = StateManager.COUNTRY_CODE;
            teleOperatorCode = StateManager.TELE_OPERATOR_CODE;
        }
        else
        {
            countryCode = PhoneInformation.getCountryCode(context);
            teleOperatorCode = PhoneInformation.getTeleOperatorCode(context);
        }
        ret += "\nMCC : " + countryCode + "\n";
        ret += "\nMNC : " + teleOperatorCode + "\n";

        String ipListEncoded = PreferenceSetting.getString(context,DialerSettings.PREF_AUTH_IP_LIST, "");
        List<String> ipList = PhoneInformation.getIpAddressListFromByteArray(Base64.decode(ipListEncoded, Base64.DEFAULT));
        String formatted = "";
        for( int i=0; i<ipList.size(); i++){
            formatted += (i+1) + ". " + ipList.get(i) + "\n";
        }
        ret += "\nAuth IP list : \n" + formatted + "\n";

        ipListEncoded = PreferenceSetting.getString(context,DialerSettings.PREF_VPN_IP_LIST, "");
        ipList = PhoneInformation.getIpAddressListFromByteArray(Base64.decode(ipListEncoded, Base64.DEFAULT));
        formatted = "";
        for( int i=0; i<ipList.size(); i++){
            formatted += (i+1) + ". " + ipList.get(i) + "\n";
        }
        ret += "\nVPN IP list : \n" + formatted + "\n";

        ipListEncoded = PreferenceSetting.getString(context,DialerSettings.PREF_DNS_IP_LIST, "");
        ipList = PhoneInformation.getIpAddressListFromByteArray(Base64.decode(ipListEncoded, Base64.DEFAULT));
        formatted = "";
        for( int i=0; i<ipList.size(); i++){
            formatted += (i+1) + ". " + ipList.get(i) + "\n";
        }
        ret += "\nDNS IP list : \n" + formatted + "\n";

//		ipListEncoded = PreferenceSetting.getString(context,DialerSettings.PREF_REPORT_IP_LIST, "");
//		ipList = PhoneInformation.getIpAddressListFromByteArray(Base64.decode(ipListEncoded, Base64.DEFAULT));
//		formatted = "";
//		for( int i=0; i<ipList.size(); i++){
//			formatted += (i+1) + ". " + ipList.get(i) + "\n";
//		}
//		ret += "\nReport IP list : \n" + ipList + "\n";

        ret += "\nOperating System : Android " + Build.VERSION.RELEASE + "\n";

        ret += "\nDevice model : " + Build.MODEL + "\n";

//		ret += "\nPublic IP : " +  + "\n";

        return ret;
    }

    public static String sendRequest(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        //con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        String response="";

        while ((inputLine = in.readLine()) != null) {
            response += inputLine;
        }
        in.close();

        return response;
    }

    public static List<String> getIpListFromJsonResponse(String response, List<String> ipList) throws Exception {

        if (response != null && !response.isEmpty()) {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("Answer");
            for (int i = 0; i < jsonArray.length(); i++) {
                ipList.add(jsonArray.getJSONObject(i).getString("data"));
            }
        }
        return ipList;
    }

    public static boolean isSKDGreaterEqual18()
    {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean isSKDGreaterEqual22()
    {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }
}
