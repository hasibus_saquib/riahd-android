package com.ci.connectdialer.core.ckslib;

public interface BalanceListener {

	public void onBalanceUrlResponse(String url);
	
	public void onBalanceURLRequestTimeout();
	
}
