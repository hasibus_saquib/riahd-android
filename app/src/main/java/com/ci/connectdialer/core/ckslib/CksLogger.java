package com.ci.connectdialer.core.ckslib;

import android.util.Log;

import com.signal.CkslibraryEnum;

public class CksLogger {
	
	public static final boolean DEBUG = false;
	
	public static void printNative(CkslibraryEnum state, String methodName, String log) {
		if(DEBUG && null != log) {
			Log.v(methodName, log);
			addToLog(state, log, methodName);
		}
	}
	
	public static void printError(CkslibraryEnum state, String methodName, String log) {
		if(DEBUG && null != log) {
			Log.e(methodName, log);
			addToLog(state, log, methodName);
		}
	}
	
	public static void printInfo(CkslibraryEnum state, String methodName, String log) {
		if(DEBUG && null != log) {
			Log.i(methodName, log);
			addToLog(state, log, methodName);
		}
	}

	public static void printDebug(CkslibraryEnum state, String methodName, String log) {
		if(DEBUG && null != log) {
			Log.d(methodName, log);
			addToLog(state, log, methodName);
		}
	}
	
	/* *********************
	 *  Android State Logger
	 * *********************
	 *  */
//	private static StringBuilder mLogBuilder = new StringBuilder(100);
	
	public static synchronized void addToLog(CkslibraryEnum state, String info, String methodName) {
//		if(null != mLogBuilder) {
//			mLogBuilder.append(methodName+ "::" +state.name()+ "::  >>> " +info+ "\n");
//		}
	}
	
	public static String getLog() {
//		return mLogBuilder.toString();
		return null;
	}
	
	public static void clearLog() {
//		if(null != mLogBuilder) {
//			mLogBuilder.setLength(0);
//		}
	}	
}
