package com.ci.connectdialer.core.utilities;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Log;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PhoneInformation {

	public static final String DIALER_TYPE = "2";
	public static final String DIALER_VERSION = "3.0";

	public static String getCellId(Context context) {
		if (context == null) {
			return "0";
		}
		int cellId = -1;
		try {

			TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			GsmCellLocation location = null;
			if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
				location = (GsmCellLocation) telephony.getCellLocation();
				if (location != null) {
					cellId = location.getCid();
				}
			}
		} catch (Exception e) {
		}

		if (cellId == -1 || cellId == 0)
			cellId = 0;
		return "" + cellId;
	}

	public static String getIMEI(Context context) {
		if (context == null) {
			return "0";
		}
		String deviceId = null;

		try {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			deviceId = telephonyManager.getDeviceId();
		} catch (Exception e) {
		}

		if (deviceId == null)
			deviceId = "0";
		return deviceId;
	}

	public static String getMccAndMnc(Context context) {
		String strRet = "";
		int iMcc = 0;
		int iMnc = 1;

		if (isDeviceConnectedToWifi(context)) {
			iMnc = 0;
		}

		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);

		try {
			if(CksUtil.isSKDGreaterEqual22())
            {
                SubscriptionManager subManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                final List<SubscriptionInfo> subInfoList = subManager.getActiveSubscriptionInfoList();
                for (SubscriptionInfo subInfo : subInfoList)
                {
                    iMcc = subInfo.getMcc();
                    if(iMnc != 0) {
                        iMnc = subInfo.getMnc();
                        break;
                    }
                }
            }
            else if(CksUtil.isSKDGreaterEqual18()) {
                final List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
                for (CellInfo cellInfo : allCellInfo) {
                    if (cellInfo instanceof CellInfoGsm) {
                        CellIdentityGsm cellIdentity = ((CellInfoGsm) cellInfo).getCellIdentity();
                        iMcc = cellIdentity.getMcc();
                        if (iMnc != 0) {
                            iMnc = cellIdentity.getMnc();
                            break;
                        }
                        //TODO Use cellIdentity to check MCC/MNC code, for instance.
                    } else if (cellInfo instanceof CellInfoLte) {
                        CellIdentityLte cellIdentity = ((CellInfoLte) cellInfo).getCellIdentity();
                        iMcc = cellIdentity.getMcc();
                        if (iMnc != 0) {
                            iMnc = cellIdentity.getMnc();
                            break;
                        }
                    } else if (cellInfo instanceof CellInfoCdma) {
                        CellIdentityCdma cellIdentity = ((CellInfoCdma) cellInfo).getCellIdentity();
                        iMcc = cellIdentity.getBasestationId();
                        if (iMnc != 0) {
                            iMnc = cellIdentity.getNetworkId();
                            break;
                        }
                    } else if (cellInfo instanceof CellInfoWcdma) {
                        CellIdentityWcdma cellIdentity = ((CellInfoWcdma) cellInfo).getCellIdentity();
                        iMcc = cellIdentity.getMcc();
                        if (iMnc != 0) {
                            iMnc = cellIdentity.getMnc();
                            break;
                        }
                    }
                }
            }
            else
            {
                String networkOperator = telephonyManager.getNetworkOperator();
                if (networkOperator != null) {
                    iMcc = Integer.parseInt(networkOperator.substring(0, 3));
                    if(iMnc != 0)
                        iMnc = Integer.parseInt(networkOperator.substring(3));
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		strRet = iMcc + "," + iMnc;
		return strRet;
	}

	public static String getCountryCode(Context context) {

		String countryCode = "0";

		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);

		try {
			String networkOperator = telephonyManager.getNetworkOperator();
			if (networkOperator != null) {
				countryCode = networkOperator.substring(0, 3);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countryCode;
	}

	public static String getTeleOperatorCode(Context context) {

		String sOperatorCode = "1";
		if (isDeviceConnectedToWifi(context)) {
			return "0";
		}

		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		try {
			String networkOperator = telephonyManager.getNetworkOperator();

			if (networkOperator != null) {
				sOperatorCode = networkOperator.substring(3);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sOperatorCode;
	}

	public static int[] getVersion(Context context) {
		int vrsn[] = new int[2];
		if(context == null){
			vrsn[0] = 0; // major
			vrsn[1] = 0; // minor
		}
		try {
			String versionName = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionName;
			String[] build = versionName.split("\\.");
			vrsn[0] = Integer.parseInt(build[0]);
			vrsn[1] = Integer.parseInt(build[1]);
		} catch (Exception e) {
			vrsn[0] = 0; // major
			vrsn[1] = 0; // minor
		}
		return vrsn;
	}

	public static boolean isNetworkAvailable(Context context) {
		if(context == null){
			return false;
		}
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetworkInfo = connectivityManager
					.getActiveNetworkInfo();
			if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public static void checkVersionInfo(Context context) {
		if(context == null){
			return;
		}

		String version = "";
		String lastVersion = "";
		try {
			version = context.getPackageManager().getPackageInfo( context.getPackageName(), 0).versionName;
			lastVersion = PreferenceSetting.getString(context, DialerSettings.DIALER_VERSION, "");

		} catch (Exception e) {
			Log.e("",e.getMessage());
		}
		if (!version.equals(lastVersion)) {
			PreferenceSetting.putInt(context, DialerSettings.PREF_CACHE_OPCODE, 0);
			PreferenceSetting.putLong(context, DialerSettings.LAST_AUTHENTICATION_TIME, 0L);
			PreferenceSetting.putString(context, DialerSettings.DIALER_VERSION, version);
		}

	}

	public static boolean isDeviceConnectedToWifi(Context context) {
		final ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo wifiNetInfo = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetInfo != null && wifiNetInfo.isAvailable()
				&& wifiNetInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public static String getCertificateSHA1Fingerprint(Context mContext) {
		if (mContext == null) {
			return null;
		}
		PackageManager pm = mContext.getPackageManager();
		String packageName = mContext.getPackageName();
		int flags = PackageManager.GET_SIGNATURES;
		PackageInfo packageInfo = null;
		try {
			packageInfo = pm.getPackageInfo(packageName, flags);
		} catch (PackageManager.NameNotFoundException e) {
			Log.e("",e.getMessage());
		}
		Signature[] signatures = packageInfo.signatures;
		byte[] cert = signatures[0].toByteArray();
		InputStream input = new ByteArrayInputStream(cert);
		CertificateFactory cf = null;
		try {
			cf = CertificateFactory.getInstance("X509");
		} catch (CertificateException e) {
			Log.e("",e.getMessage());
		}
		X509Certificate c = null;
		try {
			c = (X509Certificate) cf.generateCertificate(input);
		} catch (CertificateException e) {
			Log.e("",e.getMessage());
		}
		String hexString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			byte[] publicKey = md.digest(c.getEncoded());
			hexString = byte2HexFormatted(publicKey);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (CertificateEncodingException e) {
			Log.e("",e.getMessage());
		}
		return hexString;
	}

	@SuppressLint("DefaultLocale")
	public static String byte2HexFormatted(byte[] arr) {
		StringBuilder str = new StringBuilder(arr.length * 2);
		for (int i = 0; i < arr.length; i++) {
			String h = Integer.toHexString(arr[i]);
			int l = h.length();
			if (l == 1)
				h = "0" + h;
			if (l > 2)
				h = h.substring(l - 2, l);
			str.append(h.toUpperCase());
			if (i < (arr.length - 1))
				str.append(':');
		}
		return str.toString();
	}

	/*
	 * New methods related to retrieving contact name, id and photo START
	 * */

	private static long getContactIDFromNumberNew(Context context, String contactNumber)
	{
		contactNumber = Uri.encode(contactNumber);
		long phoneContactID = Math.abs(new Random().nextInt());
		Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,contactNumber),new String[] {ContactsContract.PhoneLookup._ID}, null, null, null);
		int osVersion = Build.VERSION.SDK_INT;
		if(osVersion < 17)
			contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI,contactNumber),new String[] {ContactsContract.CommonDataKinds.Phone._ID}, null, null, null);
		while(contactLookupCursor.moveToNext()){
			phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
		}
		contactLookupCursor.close();

		return phoneContactID;
	}

	public static Bitmap retrieveContactPhotoNew(Context context, String contactNumber, long cntId) {

		Bitmap photo = null;
		long contactID = getContactIDFromNumberNew(context, contactNumber);
		if(cntId >= 0)
			contactID = cntId;
		try {
			InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
					ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID), true);

			if (inputStream != null) {
				photo = BitmapFactory.decodeStream(inputStream);
				inputStream.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return photo;
	}

	public static String retrieveContactNameNew(Context context, String contactNumber, long cntId) {

		String contactName = "";
		long contactID = getContactIDFromNumberNew(context, contactNumber);
		if(cntId >= 0)
			contactID = cntId;
		// querying contact data store
		Cursor cursor = context.getContentResolver().query(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID), null, null, null, null);

		if (cursor.moveToFirst()) {

			// DISPLAY_NAME = The display name for the contact.
			// HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.
			contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		}

		cursor.close();
		return contactName;
	}

	/*
	 * New methods related to retrieving contact name, id and photo END
	 * */
	/**
	 * Gets the ip address from domain.
	 *
	 * @param domainName
	 *            the domain name
	 * @return the ip address from domain
	 */
	public static String getIpAddressFromDomain(String domainName) {
		try {
			InetAddress giriAddress = java.net.InetAddress
					.getByName(domainName);
			return giriAddress.getHostAddress();
		} catch (Exception e) {
			Log.e("",e.getMessage());
		}
		return "";
	}

	/**
	 * Gets the ip address from int.
	 *
	 * @param ipAddress
	 *            the ip address
	 * @return the ip address from int
	 */
	public static String getIpAddressFromInt(int ipAddress) {
		byte[] bytes = BigInteger.valueOf(ipAddress).toByteArray();
		InetAddress address;
		try {
			address = InetAddress.getByAddress(bytes);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			Log.e("",e.getMessage());
		}
		return "";
	}

	public static String getIpAddressFromIntReverseByte(int ipAddress) {
		byte[] bytes = BigInteger.valueOf(ipAddress).toByteArray();
		InetAddress address;
		try {
			byte[] revBytes = new byte[4];
			for( int i=0, r=bytes.length-1; i<bytes.length;i++, r--)
				revBytes[i] = bytes[r];

			for( int i = bytes.length; i < revBytes.length; i++)
				revBytes[i] = 0;

			address = InetAddress.getByAddress(revBytes);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			Log.e("",e.getMessage());
		}
		return "";
	}

	public static String getIpAddressFromInt(byte[] bytes) {
		InetAddress address;
		try {
			address = InetAddress.getByAddress(bytes);
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			Log.e("",e.getMessage());
		}
		return "";
	}

	public static List<String> getIpAddressListFromByteArray(byte[] bytes) {

		List<String> ips = new ArrayList<String>();
		try {
			for( int start = 0; start < bytes.length; start += 4) {
				byte[] range = Arrays.copyOfRange(bytes, start, start+4);
				ips.add(InetAddress.getByAddress(range).getHostAddress());
			}
		} catch (UnknownHostException e) {
			Log.e("",e.getMessage());
		}
		return ips;
	}

	public static int getIntFromIpAddress(String address) {
		int iIpAddress = 0;
		int result = 0;

		try {
			InetAddress inetAddress = InetAddress.getByName(address);

			byte[] bytes = inetAddress.getAddress();
			for (byte b : bytes) {
				result = result << 8 | (b & 0xFF);
			}
		} catch (UnknownHostException e) {
		}
//		Log.e("TEST", "getIntFromIpAddress : "+ result);
		return result;

	}

	public static byte[] getByteFromInt(int i) {
		byte[] result = new byte[4];
		result[3] = (byte) (i >> 24);
		result[2] = (byte) (i >> 16);
		result[1] = (byte) (i >> 8);
		result[0] = (byte) (i /* >> 0 */);
		return result;
	}

	public static byte[] getReverseByteFromInt(int i) {
		byte[] result = new byte[4];
		result[0] = (byte) (i >> 24);
		result[1] = (byte) (i >> 16);
		result[2] = (byte) (i >> 8);
		result[3] = (byte) (i /* >> 0 */);
		return result;
	}

	public static byte[] getByteFromShort(int i) {
		byte[] result = new byte[2];
		result[1] = (byte) (i >> 8);
		result[0] = (byte) (i /* >> 0 */);
		return result;
	}

	public static byte[] getReverseByteFromShort(int i) {
		byte[] result = new byte[2];
		result[0] = (byte) (i >> 8);
		result[1] = (byte) (i /* >> 0 */);
		return result;
	}

	/**
	 * Gets the app version.
	 *
	 * @param context
	 *            the context
	 * @return the app version
	 */
	public static String getAppVersion(Context context) {
		if (context == null) {
			return ApplicationValidator.APPLICATION_VERSION;
		}
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			Log.e("",e.getMessage());
		}
		return (pInfo != null ? pInfo.versionName : "0");
	}

	public static int getAppVersionCode(Context context) {
		if (context == null) {
			return ApplicationValidator.APPLICATION_VERSION_CODE;
		}
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			Log.e("",e.getMessage());
		}
		return (pInfo != null ? pInfo.versionCode : 0);
	}


	public static String getAppVersionWithComponentVersion(Context context) {
		String versionName = getAppVersion(context);
		CksNativeLibrary lib = CksNativeLibrary.getInstance();
		if (lib != null) {

			int version = lib.GetComponentVersion();
			versionName = versionName + "."+ version;
		}
		return versionName;
	}

	/**
	 * Gets the device os version.
	 *
	 * @return the device os version
	 */
	public static String getDeviceOSVersion() {
		return android.os.Build.VERSION.RELEASE;
	}

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		}
		return capitalize(manufacturer) + " " + model;
	}

	private static String capitalize(String str) {
		if (TextUtils.isEmpty(str)) {
			return str;
		}
		char[] arr = str.toCharArray();
		boolean capitalizeNext = true;
		String phrase = "";
		for (char c : arr) {
			if (capitalizeNext && Character.isLetter(c)) {
				phrase += Character.toUpperCase(c);
				capitalizeNext = false;
				continue;
			} else if (Character.isWhitespace(c)) {
				capitalizeNext = true;
			}
			phrase += c;
		}
		return phrase;
	}

	public static String randomString(int len) {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	public static String decodeDnsServerUrl(String key, String value) {
		char[] resolved = new char[value.length()];
		char[] keyArray = key.toCharArray();

		int i = 0, k = 0;
		for (char c : value.toCharArray()) {
			resolved[i++] = (char) (c ^ (keyArray[k] - '0'));
			k = ++k < key.length() ? k : 0;
		}

		return new String(resolved);
	}

	public static String getAppName(Context context){
		return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
	}

	public static byte[] toPortByte(List<Integer> portList) {
		byte[] result = new byte[portList.size()*2];
		for(int i=0;i<portList.size();i++){
			short port = portList.get(i).shortValue();
			System.arraycopy( getByteFromShort(port), 0, result, i*2, 2);
		}
		return result;
	}
}
