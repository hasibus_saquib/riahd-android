package com.ci.connectdialer.core.utilities;

import android.app.Application;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;

public class PDApplication extends Application {

	@Override
	public void onCreate() {
		Thread.setDefaultUncaughtExceptionHandler(new PDExceptionHandler());
		super.onCreate();
	}

	private class PDExceptionHandler implements UncaughtExceptionHandler {

		@Override
		public void uncaughtException(Thread t, Throwable e) {
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			e.printStackTrace(printWriter);
			printWriter.close();
			String bugDetail = result.toString();
			bugDetail = bugDetail.replaceAll("\n", "@@");

			PreferenceSetting.putString(getApplicationContext(),
					DialerSettings.KEY_BUG_DETAILS, bugDetail);
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	}
}
