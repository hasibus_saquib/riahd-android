package com.ci.connectdialer.core.ckslib;

import android.util.Log;

import com.ci.connectdialer.core.media.Packet;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateManager;
import com.signal.Siglib;
import com.signal.CkslibraryEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class CksNativeLibrary extends Siglib {
	
	private static volatile CksNativeLibrary lib = null;
	private AuthResponseListener authlistener;
	private BalanceListener balanceListener;
	private StatusListener statusListener;
	
	private CkslibraryEnum oldState = CkslibraryEnum.EAccessPoint;
	
	public static final BlockingQueue<Packet> rtpQueue = new ArrayBlockingQueue<>(100);
	
	private CksNativeLibrary() {
		super();
		indexList = new ArrayList<>(BUFFER_SIZE);
		packetList = new ArrayList<>(BUFFER_SIZE);
	}

	public static CksNativeLibrary getInstance() {
		if(lib == null) {
			synchronized (CksNativeLibrary.class) {
				if(lib == null) {
					lib = new CksNativeLibrary();
					if(!lib.loaded){
						return null;
					} else {
						boolean isAuthDomainResolvod = PreferenceSetting.getBool(
								CksTabService.getContext(), DialerSettings.PREF_AUTH_DOMAIN_RESOLVED,false);
						lib.SetAuthDomainResolved(isAuthDomainResolvod);
					}
				}
			}
		}
		
		return lib;
	}

	public void addAuthResponseListener(AuthResponseListener responseListener) {
		this.authlistener = responseListener;
	}

	
	public void addBalanceListener(BalanceListener balanceListener) {
		this.balanceListener = balanceListener;
	}
	
	public void addStatusListener(StatusListener statusListener) {
		this.statusListener = statusListener;
	}
	
	/**
	 * @author DIPU
	 * Methods native callbacks
	 */	
	@Override
	public void ReadyToAuthenticate() {
		if( null != authlistener ){
			authlistener.onReady();
		} else{
			CksLogger.printError(oldState, "CksNativeLibrary:ReadyToAuthenticate", "Failed");
		}
	}
	
	@Override
	public void ReAuthenticate() {
		if( null != authlistener ) {
			authlistener.reAuthenticate();
		}
		else
			CksLogger.printError(oldState, "CksNativeLibrary:ReAuthenticate", "Failed");
	}
	
	@Override
	public void AuthenticationTimeout() {
		if( null != authlistener ) {
			UpdateDialerState(CkslibraryEnum.EReconnecting.getStatus(), "Search Timeout");
			authlistener.authenticationTimeOut();
		}
		else
			CksLogger.printError(oldState, "CksNativeLibrary:AuthenticationTimeOut", "Failed");
	}
	
	@Override
	public void AuthenticationVoid() {
		if( null != authlistener ) {
			UpdateDialerState(CkslibraryEnum.EReconnecting.getStatus(), "Not Found");
			authlistener.authenticationVoid();
		}
	}

	
	
	@Override
	public void UseCashedAuthSettings() {
		if( null != authlistener ){
			authlistener.UseCachedAuthSettings();
		} else{
			CksLogger.printError(oldState, "CksNativeLibrary:UseCashedAuthSettings", "Failed");
		}
	}

	@Override
	public void StoreServerAddress(byte[] pAuthIps, int iNumberOfServerIp) {

		if( null != authlistener )
			authlistener.StoreServerAddress(pAuthIps, iNumberOfServerIp);
	}
	
	
	@Override
	public void UpdateDialerState(int State, String strInfo){

		CkslibraryEnum currentState = CkslibraryEnum.findById(State);
		if(currentState == oldState && currentState != CkslibraryEnum.EAuthTrying) {
			return;
		}
		
		oldState = currentState;

		if( null != statusListener) {
			if(strInfo == null || strInfo.equals("")) {
				statusListener.onState(currentState, "");
			}
			else {
				statusListener.onState(currentState, strInfo);
			}
		}
	}
	
	@Override
	public void UpdateInfo(String strInfo, int strLength){
		String debug = "UpdateInfo: " + strInfo + "\nLength: " + strLength;
		Log.d("UpdateInfo", debug);
	}

	@Override
	public void IncommingCallFrom(String caller) {
	}	
	
	
	public CkslibraryEnum getState() {
		return oldState;
	}

	private static final int BUFFER_SIZE = 5;
	private static final int MAX_RTP_PACKET_ID = 65335;
	private static final int RTP_THRESHOLD = 200;
	private List<Integer> indexList;
	private List<Packet> packetList;
	private int mLastQueuedIndex;

	public void initRtpQueue() {
		rtpQueue.clear();
		indexList.clear();
		packetList.clear();
		mLastQueuedIndex = -1;
		DialerSettings.lastReceivedRtpIndex = mLastQueuedIndex;
	}

	private static boolean isSequential(int last, int index){
		return (last > MAX_RTP_PACKET_ID && index < RTP_THRESHOLD) ||
				(last < index && (last == -1 || last >= RTP_THRESHOLD || index <= MAX_RTP_PACKET_ID));
	}

	private List<Packet> orderedPacket(Packet packet, int index){
		List<Packet> retPackets = new ArrayList<>();

		if(isSequential(mLastQueuedIndex, index))
		{
			int cur = indexList.size() - 1;
			for (; cur >= 0; cur--) {

				if(isSequential(indexList.get(cur), index)) break;
			}
			indexList.add(cur + 1, index);
			packetList.add(cur + 1, packet);
		}

//		Log.e("orderedPacket", index + " " + indexList.toString() + " last: "+mLastQueuedIndex);

		// condition:( first_time || serial_maintained || buffer_full )
		while( mLastQueuedIndex == -1 ||
				(indexList.size() > 0 && (mLastQueuedIndex + 1) % MAX_RTP_PACKET_ID == indexList.get(0)) ||
				packetList.size() == BUFFER_SIZE ) {

			int retIndex = indexList.remove(0);
			retPackets.add(packetList.remove(0));
			mLastQueuedIndex = retIndex;
			DialerSettings.lastReceivedRtpIndex = mLastQueuedIndex;
		}
		return retPackets;
	}

	@Override
	public void receivedRtp(byte[] rtpData, int rtpLength, int rtpIndex){
//		Log.d("rtp", "received: " + rtpIndex);
		try {
			Packet packet = new Packet(rtpData);

			if( !StateManager.BufferAndSerializeRtpEnabled) {
				rtpQueue.put(packet);
			}
			else {
				for( Packet pac : orderedPacket(packet, rtpIndex)){
					rtpQueue.put(pac);
				}
			}
		}
		catch(Exception e) {
			Log.e("receivedRtp exception",Log.getStackTraceString(e));
			initRtpQueue();
		}
	}

	@Override
	public void BufferAndSerializeRtp(boolean val){
		StateManager.BufferAndSerializeRtpEnabled = val;
	}

	
	@Override
	public void receivedBalance(String strBalance) {
		if( null != statusListener && strBalance != null)
			statusListener.onState(CkslibraryEnum.JBalanceReceived, strBalance);
		else
			CksLogger.printError(oldState, "CksNativeLibrary:receivedBalance", "Failed");
	}
	
	@Override
	public void receivedBalanceURL(String strUrl) {
		if(null != balanceListener)
			balanceListener.onBalanceUrlResponse(strUrl);
	}
	
	@Override
	public void balanceURLRequestTimeout() {
		if(null != balanceListener)
			balanceListener.onBalanceURLRequestTimeout();
	}	
	
	@Override
	public void onExit() {
		super.onExit();
		lib = null;
	}

	@Override
	public void UpdateDialerInformation(String strBrandName, String strIvr, String strFooter, long lUpdateTime) {

		DialerSettings.saveUpdatedDialerInformation(CksTabService.getContext(),
				strBrandName, strIvr, strFooter, lUpdateTime);

		if (null != authlistener) {
			authlistener.UpdateDialerInformation();
		}
	}

	@Override
	public void usingNewAuthSettings(
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls,
			byte[] pVpnPortListTls, boolean bQuickRegistration, boolean bUseVpn,
			boolean bAllowSipIncoming, boolean bUsePhoneNo, boolean bEnableOpcodeEdit,
			boolean bUseBalanceServer, String strBrandName, String strIvr, String strFooter,
			long iUpdateTime, int iNumberOfDnsIp, byte[] pDnsIpList, int iNumberOfAuthIp,
			byte[] pAuthIpList, int iNumberOfReportIp, byte[] pReportIpList, boolean bReportSend) {

		DialerSettings.saveNewAuthSettings(
				CksTabService.getContext(),
				iNumberOfVpnIp, pVpnIpList, iNumberOfVpnPortUdp, pVpnPortListUdp,
				iNumberOfVpnPortTcp, pVpnPortListTcp, iNumberOfVpnPortTls, pVpnPortListTls,
				bQuickRegistration, bUseVpn, bAllowSipIncoming, bUsePhoneNo, bEnableOpcodeEdit,
				bUseBalanceServer, strBrandName, strIvr, strFooter, iUpdateTime, iNumberOfDnsIp,
				pDnsIpList, iNumberOfAuthIp, pAuthIpList, iNumberOfReportIp, pReportIpList, bReportSend);

		if (null != authlistener) {
			authlistener.usingNewAuthSettings(iNumberOfAuthIp, pAuthIpList);
		}
	}

	@Override
	public void ReRegister(
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls, byte[] pVpnPortListTls) {

		DialerSettings.saveReRegisterValues(
				CksTabService.getContext(),
				iNumberOfVpnIp, pVpnIpList, iNumberOfVpnPortUdp, pVpnPortListUdp,
				iNumberOfVpnPortTcp, pVpnPortListTcp, iNumberOfVpnPortTls, pVpnPortListTls);

		if (null != authlistener) {
			authlistener.ReRegister();
		}
	}
	
	@Override
	public void StoreReportAddress(byte[] pReportIps, int iNumberOfReportIp) {

		DialerSettings.saveReportAddressValues(CksTabService.getContext(), pReportIps, iNumberOfReportIp);

		if (null != authlistener) {
			authlistener.StoreReportAddress(pReportIps, iNumberOfReportIp);
		}
	}
	
	@Override
	public void RegistrationCompleted(byte[] pDnsIps, int iNumberOfDnsIp,
			byte[] pAuthIpList, int iNumberOfAuthIp, byte[] pReportIps,
			int iNumberOfReportIp, boolean bReportSend, int iSuccessProtocol, boolean bIgnoreSilence,
			String profileId, boolean forceUpdate, String mcc, String mnc) {

		oldState = CkslibraryEnum.ERegistered;

		try {
			DialerSettings.saveRegistrationCompletedValues(
                    CksTabService.getContext(),
                    pDnsIps, iNumberOfDnsIp, pAuthIpList, iNumberOfAuthIp,
                    pReportIps, iNumberOfReportIp, bReportSend, iSuccessProtocol,
					bIgnoreSilence, profileId, forceUpdate);

			if (null != authlistener) {
                authlistener.RegistrationCompleted(pAuthIpList, iNumberOfAuthIp, mcc, mnc);
            }

			if( null != statusListener) {
                statusListener.onState(CkslibraryEnum.ERegistered, "");
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
