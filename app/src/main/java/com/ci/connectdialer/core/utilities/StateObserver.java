package com.ci.connectdialer.core.utilities;

import com.signal.CkslibraryEnum;

public interface StateObserver {

	void updateStateOnUi(CkslibraryEnum state, String info);
}
