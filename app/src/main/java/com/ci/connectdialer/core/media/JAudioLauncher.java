package com.ci.connectdialer.core.media;


import android.util.Log;

import com.android.webrtc.audio.MobileAEC;
import com.codecs.Codec;
import com.codecs.G729;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;

/** Audio launcher based on javax.sound */
public class JAudioLauncher implements MediaLauncher {

	int dir;

	RtpStreamSenderNew sender = null;
	RtpStreamReceiverNew receiver = null;

	public static MobileAEC mobileAEC = null;
	public static long t_render = 0;
	public static long t_analize = 0;
	public static long t_process = 0;
	public static long t_capture = 0;
	public static long t_player_delay = 0;

	/** Costructs the audio launcher */
	public JAudioLauncher(RtpStreamSenderNew rtp_sender,
			RtpStreamReceiverNew rtp_receiver) {
		sender = rtp_sender;
		receiver = rtp_receiver;
	}

	/** Costructs the audio launcher */
	public JAudioLauncher(CksNativeLibrary cksLib, int sample_rate, int frame_size) {

		Codec codec = new G729();
		try {
			dir = 0;
			// sender
			if (dir >= 0) {
				sender = new RtpStreamSenderNew(true, codec, sample_rate, frame_size, cksLib);
				// sender.setSyncAdj(2);
			}

			// receiver
			if (dir <= 0) {
				receiver = new RtpStreamReceiverNew(codec, cksLib);
			}

			codec.init();
			if (mobileAEC == null) {
				mobileAEC = new MobileAEC(MobileAEC.SamplingFrequency.FS_8000Hz);
				mobileAEC.prepare();
			}

		} catch (Exception e) {
			Log.e("",e.getMessage());
		}
	}

	/** Starts media application */
	public boolean startMedia(int dir) {

		// audio player < 1
		// audio sender > 1

		try {
			if (dir > 1 && sender != null && !sender.isRunning()) {
				sender.powerOn();
			}

			if (dir < 1 && receiver != null && !receiver.isRunning()) {
				receiver.powerOn();
			}
		} catch (IllegalThreadStateException e) {
		}

		return true;
	}

	/** Stops media application */
	public boolean stopMedia() {
		if (sender != null) {
			sender.halt();
			sender = null;
		}
		if (receiver != null) {
			receiver.halt();
			receiver = null;
		}

		return true;
	}

	public boolean muteMedia() {

		return false;
	}

	public boolean speaker(boolean speakerOn) {
		if (receiver != null)
			return receiver.speaker(speakerOn);

		return false;
	}

	@Override
	public boolean sendDTMF(char c) {
		return false;
	}

	@Override
	public boolean isRecorderRunning() {
		if (sender != null) {
			return sender.isRunning();
		}
		return false;
	}

	@Override
	public boolean isPlayerRunning() {
		if (receiver != null) {
			return receiver.isRunning();
		}
		return false;
	}

}
