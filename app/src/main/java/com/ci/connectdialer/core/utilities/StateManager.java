package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.CallLog;
import android.provider.Settings;
import android.text.TextUtils;

import com.ci.connectdialer.core.auth.AuthenticationManager;
import com.ci.connectdialer.core.auth.BalanceManagerNew;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.ckslib.StatusListener;
import com.ci.connectdialer.core.db.CallInformation;
import com.ci.connectdialer.core.db.DBAdapter;
import com.ci.connectdialer.core.media.JAudioLauncher;
import com.ci.connectdialer.core.media.MediaLauncher;
import com.ci.connectdialer.core.media.RtpStreamReceiverNew;
import com.signal.CkslibraryEnum;

import java.util.Calendar;

public class StateManager implements StatusListener {

	// Cks Native Library
	public static CksNativeLibrary mCksLibrary = null;
	private AuthenticationManager mAuthManager = null;
	private BalanceManagerNew mBalanceManagerNew = null;

	// Dialer State Related Variables
	public static CkslibraryEnum mState = CkslibraryEnum.EAccessPoint;
	public static String mStateInfo = "";
	public static boolean mInCall = false;
	public static boolean mIncoming = false;
	public static String mBalance = "";

	// Call Related Variables
	public static final int SAMPLE_RATE = 8000;
	public static final int FRAME_SIZE = 160;
	public static boolean CALL_ACTIVE = false;

	public static CallInformation mCallInfo;
	public static Ringtone oRingtone;
	public MediaLauncher mMediaLauncher;
	private StateObserver mStateObserver;

	// In Call Activity Variables
	public static boolean MICROPHONE_ON;
	public static boolean SPEAKER_ON;
	public static boolean BLUETOOTH_ON;
	public static final int MIC_GAIN_NORMAL = 6;
	public static final int MIC_GAIN_MEDIAM = 12;
	public static final int MIC_GAIN_HIGH = 18;
	public static int CURRENT_MIC_GAIN = 6;
	public static float MIC_GAIN_FACTOR = 2.0f;

	public static float SPEAKER_GAIN_FACTOR = 1.0f;
	public static int CURRENT_SPEAKER_GAIN = 0;
	public static int SPEAKER_GAIN_PRODUCT = 6;

	// [Masud]
	public static boolean CUSTOM_AUTH_ENABLED = false;
	public static boolean CUSTOM_DNS_ENABLED = false;
	public static boolean CACHE_CLEARED = false;
	public static boolean TEST_MCC_MNC_ENABLED = false;
	public static String COUNTRY_CODE = "0";
	public static String TELE_OPERATOR_CODE = "0";
	//	public static boolean TEST_DOMAIN_RESOLVE_SUCCESS = false;
	public static boolean STAR_HASH_HOT_KEY_ENABLE = false;
	public static boolean BufferAndSerializeRtpEnabled = false;
	// faltu implementation. Will fix it later.
	public static void reset() {
		CUSTOM_AUTH_ENABLED = false;
		CUSTOM_DNS_ENABLED = false;
		CACHE_CLEARED = false;
		clearMccMnc();
//		TEST_DOMAIN_RESOLVE_SUCCESS = false;
		STAR_HASH_HOT_KEY_ENABLE = false;
		BufferAndSerializeRtpEnabled = false;
	}

	public StateManager() {
		mCksLibrary = CksNativeLibrary.getInstance();
		if (null != mCksLibrary) {
			mCksLibrary.addStatusListener(this);

			mAuthManager = new AuthenticationManager(mCksLibrary);
			mCksLibrary.addAuthResponseListener(mAuthManager);

			mBalanceManagerNew = new BalanceManagerNew();
			mCksLibrary.addBalanceListener(mBalanceManagerNew);
		}
	}

	public void connect() {
		if (null != mBalanceManagerNew){
			mBalanceManagerNew.dispose();
			mBalance = "";
		}
		mCksLibrary.receivedBalance("");
		if (null != mAuthManager)
			mAuthManager.connect();
	}

	public void reconnect() {
		if (null != mBalanceManagerNew){
			mBalanceManagerNew.dispose();
		}
		mCksLibrary.receivedBalance("");
		if (null != mAuthManager)
			mAuthManager.reconnect();
	}

	public void connectionEstablished() {
		if (mCksLibrary != null)
			mCksLibrary.ConnectionStablished();
	}

	public void connectionLost() {
		if (mCksLibrary != null) {
			if(StateManager.mState == CkslibraryEnum.ERegistered)
				mCksLibrary.UpdateDialerState(CkslibraryEnum.EConnectionError.getStatus(), "You are offline!");
		}
	}

	public void disconnect() {

		//sendMsg(CkslibraryEnum.EUnregistered, null );

		if (mAuthManager != null) {
			mAuthManager.stop();
			mAuthManager = null;
		}

		if (mBalanceManagerNew != null) {
			mBalanceManagerNew.dispose();
			mBalanceManagerNew = null;
		}

		if (mCksLibrary != null) {
			mCksLibrary.onExit();
			mCksLibrary = null;
		}

		mState = CkslibraryEnum.EAccessPoint;
		mStateInfo = "";
		mInCall = false;
		mIncoming = false;
		mBalance = "";
		mCallInfo = null;
		mStateObserver = null;
	}

	@Override
	public synchronized void onState(CkslibraryEnum status, String strInfo) {
		Context context = CksTabService.getContext();

		if (status != CkslibraryEnum.JBalanceReceived) {
			mState = status;
			mStateInfo = strInfo;
		}
		long baseTime = -1;
		switch (status) {

			case EAuthenticating:
				sendMsg(status, strInfo);
				break;

			case ECalling:
				mInCall = true;

				initMedia();
				startCallScreen();
				break;

			case EInvited:
				startMedia(2);
				startMedia(-2);

				break;

			case ERinging:
				mInCall = true;

				startCallScreen();
//			startMedia(2);
//			startMedia(-2);

				break;

			case ECallReceiving:
				mInCall = true;
				mIncoming = true;

				if (context != null) {
					AudioManager am = (AudioManager) context
							.getSystemService(Context.AUDIO_SERVICE);
					if(am != null){
						am.setMode(AudioManager.MODE_NORMAL);
						if (am.getStreamVolume(AudioManager.STREAM_RING) > 0) {
							String sUriSipRingtone = Settings.System.DEFAULT_RINGTONE_URI
									.toString();

							if (!TextUtils.isEmpty(sUriSipRingtone)) {
								oRingtone = null;
								oRingtone = RingtoneManager.getRingtone(context,
										Uri.parse(sUriSipRingtone));
								if (oRingtone != null) {
									oRingtone.play();
								}
							}
						}
					}
				}

				initCallInfo(-2, strInfo, -1);
				initMedia();
				startCallScreen();
				break;

			case ECallEstablished:
				if (!mInCall)
					break;

				stopRingtone();

				CALL_ACTIVE = true;
				CksNativeLibrary.getInstance().initRtpQueue();

				if(mCallInfo != null){
					mCallInfo.setCallaccepttime(Calendar.getInstance()
							.getTimeInMillis());
					mCallInfo.base = SystemClock.elapsedRealtime();
					if (mIncoming)
						mCallInfo.setState(CallLog.Calls.INCOMING_TYPE);
					baseTime = mCallInfo.base;
				}

				RtpStreamReceiverNew.timeout = 1;

				if (null != mMediaLauncher && !mMediaLauncher.isRecorderRunning())
					startMedia(2);
				if (null != mMediaLauncher && !mMediaLauncher.isPlayerRunning())
					startMedia(-2);

				startCallScreen();
				break;

			case ERegistering:
				sendMsg(status, strInfo);
				break;

			case ERegistered:
				sendMsg(status, strInfo);

				if (mInCall) {
					mInCall = false;
					mIncoming = false;

					CALL_ACTIVE = false;

					stopRingtone();
					stopMedia();

					if (mCallInfo != null && mCallInfo.getCallaccepttime() > 0) {
						long duration = Calendar.getInstance().getTimeInMillis()
								- mCallInfo.getCallaccepttime();
						mCallInfo.setDuration(duration);
					}

					if(context != null){
                        try {
                            DBAdapter dbAdapter = new DBAdapter(context);
                            dbAdapter.open();
                            dbAdapter.addCallInfoToDB(mCallInfo);
                            dbAdapter.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

					CallRecorder.getInstance().saveRecordedCall(context);
					startCallScreen();
				}

//			if (null != mBalanceManagerNew){
//				mBalanceManagerNew.requestBalance();
//			}

				break;

			case EUnregistered:
				sendMsg(status, strInfo);
				break;

			case JBalanceReceived:
				if (null != mBalanceManagerNew){
					mBalanceManagerNew.dispose();
				}

				if (strInfo != null && strInfo.length() <= 20) {
					sendMsg(status, strInfo);
					mBalance = strInfo;
				}
				break;

			case EConnectionError:
				sendMsg(status, strInfo);

				connectionError();
				break;

			case EAuthenticationError:
				sendMsg(status, strInfo);
				break;

			case EAuthTrying:
				if (null != mAuthManager) {
					sendMsg(status, strInfo);
				}
				break;

			case EReconnecting:
				sendMsg(status, strInfo);
				break;

			default:
				break;
		}

		if(context != null && status != null){
			Intent intent = (new Intent(context, CksTabService.class));
			intent.putExtra("state", status.ordinal());
			intent.putExtra("base", baseTime);
			context.startService(intent);
		}
	}

	public synchronized void sendMsg(CkslibraryEnum state, String msg) {

		if (null != mStateObserver)
			mStateObserver.updateStateOnUi(state, msg);
	}

	public void setStateObserver(StateObserver observer) {
		mStateObserver = observer;
	}

	public void callTo(final String name, final String number, final long contactID) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (mCksLibrary != null) {
					initCallInfo(2, number, contactID);
					mCksLibrary.CallToNumber(number);
				}
			}
		}).start();
	}

	public static void callEnd() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (mCksLibrary != null) {
					mCksLibrary.CancelCall();
				}
			}
		}).start();
	}

	public static void callAccept() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				stopRingtone();
				if (mCksLibrary != null) {
					mCksLibrary.AcceptCall();
				}
			}
		}).start();
	}

	public static void stopRingtone() {
		if (StateManager.oRingtone != null) {
			StateManager.oRingtone.stop();
			StateManager.oRingtone = null;
		}
	}

	public void initCallInfo(int dir, String number, long contactID) {
		mCallInfo = null;
		mCallInfo = new CallInformation();
		mCallInfo.base = 0;
		mCallInfo.setName("");
		mCallInfo.setPhoneNo(number == null ? "" : number);
		mCallInfo.setDuration(0L);
		mCallInfo.setTime(Calendar.getInstance().getTimeInMillis());
		mCallInfo.setCallaccepttime(-1);
		mCallInfo.setContactID(contactID);
		if (dir > 1)
			mCallInfo.setState(CallLog.Calls.OUTGOING_TYPE);
		else
			mCallInfo.setState(CallLog.Calls.MISSED_TYPE);

		if (number == null || number.equals(""))
			return;
		Context context = CksTabService.getContext();
		mCallInfo.setName(PhoneInformation.retrieveContactNameNew(context, number, contactID));
	}

	public void initMedia() {
		if (null != mCksLibrary)
			mMediaLauncher = new JAudioLauncher(mCksLibrary, SAMPLE_RATE, FRAME_SIZE);
	}

	public void startMedia(int dir) {
		// audio player < 1
		// audio sender > 1

		if (null != mMediaLauncher) {
			mMediaLauncher.startMedia(dir);
		}
	}

	public void stopMedia() {
		if (mMediaLauncher != null) {
			mMediaLauncher.stopMedia();
			mMediaLauncher = null;
		}
	}

	public boolean speaker(boolean enable) {
		if (mMediaLauncher != null) {
			return mMediaLauncher.speaker(enable);
		}
		return false;
	}

	public void startCallScreen() {

		if( mInCallListener !=null)
			mInCallListener.startCallScreen();
	}

	private static InCallListener mInCallListener;
	public static void setCallScreenListener( InCallListener pListener){
		mInCallListener = pListener;
	}
	public interface InCallListener {
		void startCallScreen();
	}


	public void connectionError() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(5000);

					connect();
				} catch (Exception e) {
				}
			}
		}).start();
	}

	public static void setMccMnc(String mcc, String mnc){

		TEST_MCC_MNC_ENABLED = true;
		COUNTRY_CODE = mcc;
		TELE_OPERATOR_CODE = mnc;
	}

	public static void clearMccMnc() {

		COUNTRY_CODE = "0";
		TELE_OPERATOR_CODE = "0";
		TEST_MCC_MNC_ENABLED = false;
	}
}
