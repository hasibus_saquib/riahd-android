package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Masud Rashid on 3/12/2016.
 */
public class Util {

    public static String loadHashedImageData(Context context, int resourceId) {
        String data = "";
        try {
            byte[] payload = toByteArray(context.getResources().openRawResource(resourceId));
            data = getMd5FromByte(payload);
        } catch (Exception e) {
            Log.e("HashedImageData", e.getMessage());
        }
        return data;
    }

    public static byte[] toByteArray(InputStream input) throws IOException, IndexOutOfBoundsException {

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int n;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }

    public static void hideSystemKeyboard(View view) {

        final InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getMd5FromByte(byte[] bitmapBytes) {
        String s;
        try {
            s = new String(bitmapBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }

        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
        }

        m.update(s.getBytes(), 0, s.length());
        return calcHash(m);
    }

    private static String calcHash(MessageDigest m) {
        return new BigInteger(1, m.digest()).toString(16);
    }

    public static boolean highDensityAutoCheck(Context context){
        boolean autoCheck = false;
        int density = context.getResources().getDisplayMetrics().densityDpi;
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
            case DisplayMetrics.DENSITY_MEDIUM:
            case DisplayMetrics.DENSITY_HIGH:
            case DisplayMetrics.DENSITY_XHIGH:
            case DisplayMetrics.DENSITY_XXHIGH:
                break;
            //For DENSITY_XXXHIGH DENSITY_TV devices do not check images
            case DisplayMetrics.DENSITY_XXXHIGH:
            case DisplayMetrics.DENSITY_TV:
            default:
                autoCheck = true;
                break;
        }
        return autoCheck;
    }

}
