package com.ci.connectdialer.core.utilities;

/**
 * Created by Masud Rashid on 4/1/2017.
 */
public class ContactRow {

    String conactName;
    String contactPhotoUrl;
    String lookUpKey;


    public String getConactName() {
        return conactName;
    }

    public void setConactName(String conactName) {
        this.conactName = conactName;
    }

    public String getContactImageUri() {
        return contactPhotoUrl;
    }

    public void setContactPhotoUrl(String contactPhotoUrl) {
        this.contactPhotoUrl = contactPhotoUrl;
    }

    public String getLookUpKey() {
        return lookUpKey;
    }

    public void setLookUpKey(String lookUpKey) {
        this.lookUpKey = lookUpKey;
    }



}
