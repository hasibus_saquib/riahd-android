package com.ci.connectdialer.core.utilities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import com.signal.CkslibraryEnum;

public class CksTabService extends Service {

	private Receiver m_receiver;
	private PowerManager.WakeLock mWakeLock;
	private WifiManager.WifiLock mWifiLock;
	private static Context mContext = null;
	private static NotificationUtil mNotificationUtil;

	@Override
	public void onCreate() {
		super.onCreate();

		mContext = getApplicationContext();
		mNotificationUtil = getNotificationUtil();

		if (m_receiver == null) {
			IntentFilter intentfilter = new IntentFilter();
			intentfilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			intentfilter.addAction(Receiver.ACTION_DATA_STATE_CHANGED);
			intentfilter.addAction(Receiver.ACTION_PHONE_STATE_CHANGED);
			// intentfilter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED);

			try{
				registerReceiver(m_receiver = new Receiver(), intentfilter);

				if (mWakeLock == null && mWifiLock == null) {
					PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
					WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
					mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "connect.Receiver");
					mWakeLock.acquire();
					mWifiLock = wm.createWifiLock(3, "connect.Receiver");
					mWifiLock.acquire();
				}
			} catch(Exception e){
				Log.e("", e.getMessage());
			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null && intent.getExtras() != null && mNotificationUtil != null) {
			int state = intent.getExtras().getInt("state");
			long baseTime = intent.getExtras().getLong("base");
			mNotificationUtil.showNotification(CkslibraryEnum.getStatusMessage(state), baseTime);
		}
		return START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public void onDestroy() {
		super.onDestroy();
		if (m_receiver != null) {
			unregisterReceiver(m_receiver);
			m_receiver = null;
		}

		if (mWakeLock != null && mWifiLock != null) {
			mWakeLock.release();
			mWifiLock.release();
		}

		mNotificationUtil.cancelNotification();
		mContext = null;
	}

	private static NotificationUtil getNotificationUtil(){
		if(mNotificationUtil == null && mContext != null){
			mNotificationUtil = new NotificationUtil((NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE));
		}
		return mNotificationUtil;
	}

	public static Context getContext() {
		return mContext;
	}

	@Override
	public void onTaskRemoved(Intent rootIntent){
		mNotificationUtil.cancelNotification();

		super.onTaskRemoved(rootIntent);
	}
}
