package com.ci.connectdialer.core.utilities;

/**
 * Created by Masud Rashid on 25/11/2016.
 */
public interface IPermissionMgr {

    void startApplication();
    void exitApplication();
}
