package com.ci.connectdialer.core.ckslib;

import com.signal.CkslibraryEnum;

public interface StatusListener {

	void onState(CkslibraryEnum status, String strInfo);
	
}
