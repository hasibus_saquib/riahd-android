package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.signal.CkslibraryEnum;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Masud Rashid on 4/1/2017.
 */
public class CallRecorder {

    public boolean isCallRecordRnabled = false;
    public int STORAGE_TYPE = 0;
    private String STORAGE_DIRECTORY;

    private String FILE_INCOMING_PCM_TEMP = "incoming.pcm";
    private String FILE_OUTGOING_PCM_TEMP = "outgoing.pcm";
    private String FILE_INCOMING_WAV = "incoming.wav";
    private String FILE_OUTGOING_WAV = "outgoing.wav";

    private String contactString="";

//    public boolean CALL_RECORD_DONE = false;

    private static CallRecorder instance=null;
    public static CallRecorder getInstance(){
        if(instance==null){
            instance = new CallRecorder();
        }
        return instance;
    }

    public String getStorageDirectory(Context context, int type) throws NullPointerException{

        File storageDirectory;

        if(type==0){
            String EXTERNAL_STORAGE_DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/";
            STORAGE_DIRECTORY = EXTERNAL_STORAGE_DIRECTORY;
        }
        else if(type==1){
            String tempDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            String EXTERNAL_STORAGE_PUBLIC_DIRECTORY =  tempDir.substring(0,tempDir.lastIndexOf("/")+1);
            STORAGE_DIRECTORY = EXTERNAL_STORAGE_PUBLIC_DIRECTORY;
        }

        storageDirectory = new File(STORAGE_DIRECTORY);
        if(!storageDirectory.exists()) return null;

        return STORAGE_DIRECTORY;

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private BlockingQueue<Short> bufferOutgoing = new LinkedBlockingQueue<Short>();

    public void bufferOutgoingData(Context context, short[] data) {
        if(isCallRecordRnabled && StateManager.CALL_ACTIVE) {
            for( short s:data){
                bufferOutgoing.add(s);
            }
        }
    }

    private BlockingQueue<Short> bufferIncoming = new LinkedBlockingQueue<>();

    public void bufferIncomingData(Context context, short[] data) {
        if(isCallRecordRnabled && (StateManager.CALL_ACTIVE || StateManager.mState == CkslibraryEnum.ERinging)) {
            for( short s:data){
                bufferIncoming.add(s);
            }
        }
    }

    public void saveIncomingData(Context context, short[] data) {
        if(isCallRecordRnabled && (StateManager.CALL_ACTIVE || StateManager.mState == CkslibraryEnum.ERinging)) {
            saveVoiceData(context, data, FILE_INCOMING_PCM_TEMP);
        }
    }

    public void saveOutgoingData(Context context, short[] data) {
        if(isCallRecordRnabled && (StateManager.CALL_ACTIVE || StateManager.mState == CkslibraryEnum.ERinging)) {
            saveVoiceData(context, data, FILE_OUTGOING_PCM_TEMP);
        }
    }

    public void saveIncomingAndOutgoingData(Context context, short[] dataOutgoing) {
        if(isCallRecordRnabled && (StateManager.CALL_ACTIVE || StateManager.mState == CkslibraryEnum.ERinging)) {

            if(!StateManager.CALL_ACTIVE)
            {
                for(int j = 0; j < dataOutgoing.length; j++)
                    dataOutgoing[j] = 0;
            }

            short[] dataIncomming = new short[dataOutgoing.length];
            int i=0;
            for(; i<dataOutgoing.length && !bufferIncoming.isEmpty();i++){
                dataIncomming[i] = bufferIncoming.poll();
            }
            for(; i<dataOutgoing.length;i++){
                dataIncomming[i] = 0;
            }

            saveVoiceData(context, dataIncomming, FILE_INCOMING_PCM_TEMP);
            saveVoiceData(context, dataOutgoing, FILE_OUTGOING_PCM_TEMP);
        }
    }

    private void saveVoiceData(Context context, short[] data, String filename) {

        if (context != null) {

            FileOutputStream outputStream;

            try {
//                outputStream = new FileOutputStream(EXTERNAL_STORAGE_DIRECTORY +filename, true);
                outputStream = context.openFileOutput(filename, Context.MODE_APPEND);
                outputStream.write(getBytes(data));
                outputStream.close();
            }
            catch (OutOfMemoryError e) {
                Toast.makeText(context, "Not enough memory space to record the call", Toast.LENGTH_SHORT).show();
                setCallRecordRnabled(false);
            }
            catch (Exception e) {
                Log.e("saveVoiceData", e.getStackTrace().toString());
            }
        }
    }

    public void handleCallRecordAction(Context context) {
        isCallRecordRnabled = !isCallRecordRnabled;

//        if(!CallRecorder.isCallRecordRnabled){
//            CALL_RECORD_DONE = saveRecordedCall(context);
//        }
//        else{
//            CALL_RECORD_DONE = false;
//        }
    }

    private boolean mRecordedCallSaved = false;

    public void saveRecordedCall(Context pContext) {

        final Context context = pContext;

        new Thread(new Runnable() {
            @Override
            public synchronized void run() {

                bufferIncoming.clear();

                if(context==null) return ;

                try{
//                String directory = EXTERNAL_STORAGE_DIRECTORY;
                    String directory = context.getFilesDir().toString();

                    File incoming_pcm = new File(directory, FILE_INCOMING_PCM_TEMP);
                    File outgoing_pcm = new File(directory, FILE_OUTGOING_PCM_TEMP);
                    if( !incoming_pcm.exists() || !outgoing_pcm.exists()) return;
                    File incoming_wav = new File(directory, FILE_INCOMING_WAV);
                    File outgoing_wav = new File(directory, FILE_OUTGOING_WAV);

                    rawToWave(incoming_pcm, incoming_wav);
                    rawToWave(outgoing_pcm, outgoing_wav);
                    incoming_pcm.delete();
                    outgoing_pcm.delete();

                    File mixed_wave = getOutputFile(context);
                    if( mixed_wave==null){
                        Toast.makeText(context, "File Error: "+mixed_wave, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        mixWaveFiles(incoming_wav, outgoing_wav, mixed_wave);
                    }

                    incoming_wav.delete();
                    outgoing_wav.delete();

                    mRecordedCallSaved = true;
                } catch (IOException e) {
                    Log.e("saveRecordedCall", e.getMessage());
                } catch (OutOfMemoryError memoryError) {
                    Log.e("saveRecordedCall", memoryError.getMessage());
                    Toast.makeText(context, "Not enough memory space to record the call", Toast.LENGTH_SHORT).show();
                }
            }
        }, "saveRecordedCall").start();
    }

    private File getOutputFile(Context context) {

        String storagePath = getStorageDirectory(context,STORAGE_TYPE);
        if( storagePath==null) return null;

        File appDirectory = new File(storagePath+PhoneInformation.getAppName(context));

        if( !appDirectory.exists()) appDirectory.mkdir();
        String dateDir = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        File dateDirectory = new File(appDirectory.getAbsolutePath()+"/"+dateDir);
        if( !dateDirectory.exists()) dateDirectory.mkdir();
        String fileName = contactString + "_" + new SimpleDateFormat("HHmmss").format(new Date());
        File outFile = new File(dateDirectory, fileName+".wav");

        return outFile;
    }

    private byte[] readFromFile(File rawFile) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
            input.close();
        }
        catch (Exception ex) {
            Log.e("rawToWave", ex.getMessage());
        }
        finally {
            if (input != null) {
                input.close();
            }
        }

        return rawData;
    }

    private void rawToWave(final File rawFile, final File waveFile) throws IOException {

        byte[] rawData = readFromFile(rawFile);

        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            addWaveHeader(output, rawData.length);
            short[] shorts = getShortsOrdered(rawData); // conversion big endian -> little endian
            output.write(getBytes(shorts));

        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private void addWaveHeader(final DataOutputStream output, int dataLength) throws IOException {

        // WAVE header
        // see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
        writeString(output, "RIFF"); // chunk id
        writeInt(output, 36 + dataLength); // chunk size
        writeString(output, "WAVE"); // format
        writeString(output, "fmt "); // subchunk 1 id
        writeInt(output, 16); // subchunk 1 size
        writeShort(output, (short) 1); // audio format (1 = PCM)
        writeShort(output, (short) 1); // number of channels
        writeInt(output, StateManager.SAMPLE_RATE); // sample rate
        writeInt(output, StateManager.SAMPLE_RATE*2); // byte rate
        writeShort(output, (short) 2); // block align
        writeShort(output, (short) 16); // bits per sample
        writeString(output, "data"); // subchunk 2 id
        writeInt(output, dataLength); // subchunk 2 size
    }

    public short[] getAsShortArray(File waveFile){
        ByteBuffer myByteBuffer = ByteBuffer.allocate((int) waveFile.length());
        myByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        FileChannel in = null;

        try {
            in = new FileInputStream(waveFile).getChannel();
            in.read(myByteBuffer);
            myByteBuffer.flip();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte [] bb = new byte[myByteBuffer.remaining()];
        myByteBuffer.get(bb);

        short[] audioData = new short[bb.length/2];
        // to turn bytes to shorts as either big endian or little endian.
        ByteBuffer.wrap(bb).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(audioData);
        return audioData;
    }

    private byte[] getBytes(short[] shorts){
        ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
        for (short s : shorts) {
            bytes.putShort(s);
        }
        return bytes.array();
    }

    private byte[] getBytesOrdered(short[] shortData) {

        byte[] bytes = new byte[shortData.length * 2];
        ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(shortData);
        return bytes;
    }

    private short[] getShortsOrdered(byte[] byteData) {

        short[] shorts = new short[byteData.length / 2];
        ByteBuffer.wrap(byteData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
        return shorts;
    }

    private void writeToFile(short[] bytes, File mixed_wave) {
        try {

            ByteBuffer myByteBuffer = ByteBuffer.allocate(bytes.length * 2);
            myByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

            ShortBuffer myShortBuffer = myByteBuffer.asShortBuffer();
            myShortBuffer.put(bytes);

            FileChannel out = new FileOutputStream(mixed_wave).getChannel();
            out.write(myByteBuffer);
            out.close();
        } catch (Exception e) {
            Log.e("writeToFile", e.getMessage());
        }
    }

    private short[] mixWaveAudioData(short[] smallData, short[] bigData) {

        try {
            // find the max:
            float max = 0;
            for (int i = 22; i < smallData.length; i++) {
                if (Math.abs(smallData[i] + bigData[i]) > max)
                    max = Math.abs(smallData[i] + bigData[i]);
            }

            // now find the result, with scaling:
            for (int i = 22; i < smallData.length; i++) {

                int c = Math.round(Short.MAX_VALUE * (smallData[i] + bigData[i]) / max);

                if (c > Short.MAX_VALUE) c = Short.MAX_VALUE;
                if (c < Short.MIN_VALUE) c = Short.MIN_VALUE;

                bigData[i] = (short) c;
            }
        }catch (Exception ex){
            Log.e("mixWaveAudioData", ex.getMessage());
        }
        return bigData;
    }

    private void mixWaveFiles(File incoming_wav, File outgoing_wav, File mixed_wave) {

        Log.e("mixing", "mixing started");
        short[] audioData1 = getAsShortArray(incoming_wav);
        short[] audioData2 = getAsShortArray(outgoing_wav);
        short[] audioDataR ;
        Log.e("mixing", "got short array");

        if( audioData1.length < audioData2.length)
            audioDataR = mixWaveAudioData(audioData1, audioData2);
        else
            audioDataR = mixWaveAudioData(audioData2, audioData1);

        Log.e("mixing", "mixed audio data");
        byte[] bytes = getBytesOrdered(audioDataR);
        Log.e("mixing", "got ordered bytes");
        writeToFile(audioDataR, mixed_wave);
        Log.e("mixing", "file written");
    }

    private void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }


    public void setContact(String name, String number) {
        contactString = name.length()==0 ? number : name.substring(0,name.length()>8?8:name.length()) + "_" + number;

    }

    public boolean isCallRecordRnabled() {
        return isCallRecordRnabled;
    }

    public void setCallRecordRnabled(boolean callRecordRnabled) {
        isCallRecordRnabled = callRecordRnabled;
    }
}
