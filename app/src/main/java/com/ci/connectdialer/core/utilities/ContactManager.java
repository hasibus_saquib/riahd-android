package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.content.CursorLoader;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Created by Masud Rashid on 3/12/2016.
 * Incomplete class - TODO
 */
public class ContactManager {

    public interface IContactManager{

    }
    private IContactManager mContactMgrListener;

    public ContactManager(IContactManager pListener){
        mContactMgrListener = pListener;
    }


    public static CursorLoader getContactCursorLoader(Context context, String search_string) {

        Uri CONTACT_URI = ContactsContract.Contacts.CONTENT_URI;

        String[] PROJECTION = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.LOOKUP_KEY,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
//                    ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        String SELECTION =
                "(" + ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?"
                        + " OR "  + ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?)"
                        + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + " = ?"
//            + " AND " + ContactsContract.Contacts.Data.MIMETYPE + " = '"
//                    + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'"
                ;

        // Define a sort order
        String SORT_ORDER = "upper(" + ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + ") ASC ";
        // Defines the array to hold values that replace the ?
        String[] SELECTION_ARGS = new String[3]; //{SEARCH_STRING, SEARCH_STRING, HAS_PHONE_NUMBER};

        SELECTION_ARGS[0] = search_string + "%";
        SELECTION_ARGS[1] = "% " + search_string + "%";
        String hasPhoneNo = "1";
        SELECTION_ARGS[2] = hasPhoneNo;

        return new CursorLoader(
                context,
                CONTACT_URI,
                PROJECTION,
                SELECTION,
                SELECTION_ARGS,
                SORT_ORDER
        );
    }

    public static CursorLoader getContactDetailsCursorLoader(Context context, Uri contactUri, String lookupKey) {

        Uri CONTACT_URI = ContactsContract.Data.CONTENT_URI;
//        Uri CONTACT_URI = contactUri;

        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        String SELECTION =
                ContactsContract.Data.LOOKUP_KEY + " = ?"
//                + " AND " + ContactsContract.Contacts.HAS_PHONE_NUMBER + " = ?"
                        + " AND " + ContactsContract.Contacts.Data.MIMETYPE + " = '"
                        + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'"
                ;

//        // Define a sort order
//        String SORT_ORDER = ContactsContract.CommonDataKinds.Phone.NUMBER + " ASC ";

        // Defines the array to hold values that replace the ?
        String[] SELECTION_ARGS = new String[1]; //{SEARCH_STRING, SEARCH_STRING, HAS_PHONE_NUMBER};
//
        SELECTION_ARGS[0] = lookupKey;
//        String hasPhoneNo = "1";
//        SELECTION_ARGS[2] = hasPhoneNo;

        return new CursorLoader(
                context,
                CONTACT_URI,
                PROJECTION,
                SELECTION,
                SELECTION_ARGS,
                null  //SORT_ORDER
        );
    }

}
