package com.ci.connectdialer.core.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

/**
 * Created by Masud Rashid on 28/11/2016.
 */
public class NotificationUtil {

    public interface NotificationListener{
        Notification getNotification(String message, long baseTime);
        int getNfId();
        PendingIntent getNotificationIntent();
    }

    private static NotificationListener mNotificationListener;

    public static void setNotificationListener( NotificationListener listener){
        mNotificationListener = listener;
    }

    private NotificationManager mNotificationManager;

    public NotificationUtil(NotificationManager systemService) {
        mNotificationManager = systemService;
    }

    public void showNotification(String notificationMessage, long baseTime) {
        if (notificationMessage == null || notificationMessage.length() == 0 || mNotificationListener==null) {
            return;
        }

        Notification notification = mNotificationListener.getNotification(notificationMessage,baseTime);
        if (notification != null) {
//            if (mNotificationManager == null) {
//                mNotificationManager = (NotificationManager) CksTabService.getContext().getSystemService(NOTIFICATION_SERVICE);
//            }
            mNotificationManager.notify(mNotificationListener.getNfId(), notification);
        }
    }

    public void cancelNotification() {
        if(mNotificationManager != null){
            mNotificationManager.cancel(mNotificationListener.getNfId());
        }
    }
}
