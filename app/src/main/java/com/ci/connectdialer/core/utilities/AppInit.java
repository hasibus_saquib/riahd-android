package com.ci.connectdialer.core.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;

/**
 * Created by Masud Rashid on 1/12/2016.
 */
public class AppInit implements AsyncTasks.AppUpdateCheck.UpdateAppListener
{
    private Context mContext;
    private static IAppInit listener;
    private ISplashAnimation splashAnimation;
    private AsyncTasks.AppUpdateCheck appUpdateCheck;

    private static final int SPLASH_SCREEN_OUT = 5000; // 5 seconds

    private static final int SPLASH_START = 0;
    private static final int SPLASH_STOP = 1;
    private static final int NONSPLASH_START = 4;
    private static final int NONSPLASH_STOP = 5;
    private static final int APP_UPDATE = 2;
    private static final int APP_LIB = 3;

    public AppInit(Context context, IAppInit pListener){
        mContext = context;
        listener = pListener;
        appUpdateCheck = new AsyncTasks.AppUpdateCheck(mContext, AppInit.this);
    }

    public interface IAppInit{
        boolean extendSplashTime();
        void afterSplashStop();
        void exitApp();
    }

    public interface ISplashAnimation{

        void initSplashAnimation();

        void startAnimations();

        boolean isSplashRunning();

        void stopAnimations();
    }

    public void setSplashAnimation(ISplashAnimation pSplashAnimation){
        splashAnimation = pSplashAnimation;
    }

    public void startAppInit() {

        boolean splash = ApplicationValidator.SPLASH_ENABLED;

        if(splash) {
            splashAnimation.initSplashAnimation();
        }

        Message message = Message.obtain();
        message.what = splash ? SPLASH_START : NONSPLASH_START;
        handler.sendMessageDelayed(message, splash ? 500 : 100);

        Message message2 = Message.obtain();
        message2.what = APP_LIB;
        handler.sendMessageDelayed(message2, splash ? 1000 : 2000);

        Message message3 = Message.obtain();
        message3.what = splash ? SPLASH_STOP : NONSPLASH_STOP;
        handler.sendMessageDelayed(message3, splash ? SPLASH_SCREEN_OUT : 100);

        appUpdateCheck.execute();
    }

    public void onAppUpdateChecked(String result){
        if(result == null) return;

        Message msg = Message.obtain();
        msg.what = APP_UPDATE;
        msg.obj = result;
        handler.sendMessageDelayed(msg, SPLASH_SCREEN_OUT + 500 );
    }

    private void waitMoreOnSplash(int status) {
        handler.removeMessages(status);
        Message message3 = Message.obtain();
        message3.what = SPLASH_STOP;
        handler.sendMessageDelayed(message3, SPLASH_SCREEN_OUT/2);
    }

    private final Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int status = msg.what;
            String info = "";

            if(null != msg.obj) info = (String) msg.obj;

            switch (status) {

                case SPLASH_START:
                    splashAnimation.startAnimations();
                    // don't break here
                case NONSPLASH_START:
                    break;
                case SPLASH_STOP:
                    if( listener.extendSplashTime()) {
                        waitMoreOnSplash(status);
                        return;
                    }
                    splashAnimation.stopAnimations();
                    // don't break here
                case NONSPLASH_STOP:
                    listener.afterSplashStop();
                    if(mPlayStoreAppVersion!=null) {
                        showUpdateAvailable(mContext, mPlayStoreAppVersion);
                    }
                    break;

                case APP_LIB:
                    CksNativeLibrary.getInstance();
                    break;

                case APP_UPDATE:
                    mPlayStoreAppVersion = info;
                    if( !splashAnimation.isSplashRunning() ) {
                        showUpdateAvailable(mContext, mPlayStoreAppVersion);
                    }
                    break;

                default:
                    break;
            }

            super.removeMessages(status);
        }
    };

    public void onPauseParentActivity() {
        handler.removeCallbacksAndMessages(null);
        splashAnimation.stopAnimations();
    }

    public void onResumeParentActivity() {
    }

    private String mPlayStoreAppVersion = null;

    public static boolean showUpdateAvailable(Context context, String playStoreVersion) {

        if(((Activity)context).isFinishing()) return false;

        // check if playStoreVersion is newer
        if (DialerSettings.isVersionLowerThan(context, playStoreVersion)) {

            boolean forceUpdate = PreferenceSetting.getBool(CksTabService.getContext(),
                    DialerSettings.PREF_FORCE_UPDATE, false);

            //check if forceUpdate applicable or AlertDialog to be shown for first time
            if (forceUpdate || DialerSettings.isUpdateAlertToShow(context, playStoreVersion)) {
                showUpdateAlertDialog(context, playStoreVersion, forceUpdate);
            }
            //not force update and dialog already shown for new version
            else {
                Toast.makeText(context, "Update Available!", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return false;
    }

    public static void showUpdateAlertDialog(final Context context, String playStoreVersion, boolean forceUpdate) {

        PreferenceSetting.putString(context, DialerSettings.DIALER_VERSION_UPDATE, playStoreVersion);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                .setTitle("Update Available")
                .setMessage("Latest version of "+ ApplicationValidator.APPLICTION_NAME +" is: "+ playStoreVersion)
                .setPositiveButton("Download", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDownloadAppUpdate(context);
                    }
                });
        //show exit button if forceUpdate
        if(forceUpdate){
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.exitApp();
                }
            });
        }
        //show cancel button if not forceUpdate
        else if (!forceUpdate) {
            alertDialogBuilder.setCancelable(true);
            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        alertDialogBuilder.create().show();
    }

    public static void startDownloadAppUpdate(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
        } catch(Exception e) {
            Log.e("", e.getMessage());
        }
        listener.exitApp();
    }

    public static void startDownloadAppConnect(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.connect.dialer")));
        } catch(Exception e) {
            Log.e("", e.getMessage());
        }
    }

}
