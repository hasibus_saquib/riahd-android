package com.ci.connectdialer.core.media;//package com.cks.dialer.core.media;
//
//import java.util.Arrays;
//import java.util.concurrent.TimeUnit;
//
//import android.content.ContentResolver;
//import android.content.Context;
//import android.media.AudioFormat;
//import android.media.AudioManager;
//import android.media.AudioTrack;
//import android.media.ToneGenerator;
//import android.net.wifi.WifiManager;
//import android.os.Build;
//import android.os.PowerManager;
//import android.view.KeyEvent;
//
//import com.cks.dialer.core.platinum.CksTabService;
//import com.cks.dialer.core.platinum.StateManager;
//import com.cks.dialer.core.ckslib.CksNativeLibrary;
//import com.codecs.Codec;
//import com.component.CkslibraryEnum;
//
//public class RtpStreamReceiver extends Thread {
//
//	public static boolean DEBUG = true;
//
//	Codec p_type;
//
////	static String codec = "";
//
//	public static final int BUFFER_SIZE = 1024 * 2;
//	public static final int SO_TIMEOUT = 1000;
//
////	RtpSocket rtp_socket = null;
//
//	boolean running;
//
//	AudioManager am;
//	ContentResolver cr;
//
////	public static int speakermode = 2;
//	public static boolean bluetoothmode;
//	int samplePushed = 0;
//
//	private CksNativeLibrary cksLib;
//
//	public RtpStreamReceiver( Codec payload_type, CksNativeLibrary lib) {
//
//		p_type = payload_type;
//		cksLib = lib;
//	}
//
//	public boolean isRunning() {
//		return running;
//	}
//
//	public void powerOn() throws IllegalThreadStateException {
//		running = true;
//		start();
//	}
//
//	public void halt() {
//		running = false;
//	}
//
//	public boolean speaker(boolean speakerOn) {
//		Context context = CksTabService.getContext();
//		if(context != null){
//			AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//
//			if(speakerOn)
//				am.setSpeakerphoneOn(true);
//			else
//				am.setSpeakerphoneOn(false);
//
//			return am.isSpeakerphoneOn();
//		}
//		return false;
//	}
//
////	static ToneGenerator ringbackPlayer;
//
//	static int stream() {
////		if (android.os.Build.VERSION.SDK_INT > 10 ) {
////
////			if(speakermode == AudioManager.MODE_IN_COMMUNICATION || speakermode == AudioManager.MODE_IN_CALL)
////				return AudioManager.STREAM_VOICE_CALL;
////			else
////				return AudioManager.STREAM_MUSIC;
////		}
////		else {
////
////			if(speakermode == AudioManager.MODE_IN_CALL)
////				return AudioManager.STREAM_VOICE_CALL;
////			else
////				return AudioManager.STREAM_MUSIC;
////		}
//		return AudioManager.STREAM_VOICE_CALL;
//	}
//
//
//	public static void adjust(int keyCode, boolean down) {
//		Context context = CksTabService.getContext();
//		if(context != null){
//			AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//	        mAudioManager.adjustStreamVolume(stream(),
//	                keyCode == KeyEvent.KEYCODE_VOLUME_UP ? AudioManager.ADJUST_RAISE : AudioManager.ADJUST_LOWER,
//	                AudioManager.FLAG_SHOW_UI);
//		}
//	}
//
//	static boolean samsung;
//	static boolean lenovo;
//
//	public static void initMode() {
//		samsung = Build.MODEL.contains("SAMSUNG") || Build.MODEL.contains("SPH-") ||
//				Build.MODEL.contains("SGH-") || Build.MODEL.contains("GT-");
//
//		lenovo = Build.MODEL.contains("Lenovo") || Build.MODEL.contains("lenovo");
//	}
//
//	public static int getMode() {
//		Context context = CksTabService.getContext();
//		if (context != null) {
//			AudioManager am = (AudioManager) context
//					.getSystemService(Context.AUDIO_SERVICE);
//			return am.isSpeakerphoneOn() ? AudioManager.MODE_NORMAL
//					: AudioManager.MODE_IN_CALL;
//		}
//		return -1;
//	}
//
//	public static void setMode(int mode) {
//		initMode();
//		Context context = CksTabService.getContext();
//		if (context != null) {
//			AudioManager am = (AudioManager) context
//					.getSystemService(Context.AUDIO_SERVICE);
//			if (am.isSpeakerphoneOn())
//				am.setSpeakerphoneOn(false);
//
//			if (android.os.Build.VERSION.SDK_INT > 10) {
//				am.setMode(AudioManager.MODE_IN_COMMUNICATION);
//			} else {
//				am.setMode(AudioManager.MODE_IN_CALL);
//			}
//		}
//
//
//	}
//
//	void empty() {
//		seq = 0;
//	}
//
//	Packet rtp_packet;
//	AudioTrack track;
//	ToneGenerator tg;
//
//    public static float good, late, lost, loss, loss2;
//    double avgheadroom,devheadroom;
//    int avgcnt;
//    public static int timeout;
//    int seq;
//
//	int maxjitter,minjitter,minjitteradjust;
//	int cnt,cnt2,user,luser,luser2,lserver;
//	public static int jitter,mu=1;
//	float maxVolume;
//
//	void setCodec() {
//		synchronized (this) {
//			AudioTrack oldtrack;
//
//			//p_type.init();
//
////			maxjitter = AudioTrack.getMinBufferSize(p_type.samp_rate(),
////					AudioFormat.CHANNEL_CONFIGURATION_MONO,
////					AudioFormat.ENCODING_PCM_16BIT);
//
//			maxjitter = AudioTrack
//					.getMinBufferSize(p_type.samp_rate(),
//							AudioFormat.CHANNEL_IN_LEFT,
//							AudioFormat.ENCODING_PCM_16BIT);
//
//			if (maxjitter < 2*2*BUFFER_SIZE*6*mu)
//				maxjitter = 2*2*BUFFER_SIZE*6*mu;
//
//			maxjitter /= 2*2;
//
////			if(maxjitter < BUFFER_SIZE*6*mu)
////				maxjitter = BUFFER_SIZE*6*mu;
////
////			maxjitter = maxjitter * 3;
//
//			oldtrack = track;
//			maxVolume = AudioTrack.getMaxVolume();
//			track = new AudioTrack(AudioManager.STREAM_VOICE_CALL, p_type.samp_rate(),
//					AudioFormat.CHANNEL_IN_LEFT,
//					AudioFormat.ENCODING_PCM_16BIT, maxjitter,
//					AudioTrack.MODE_STREAM);
//			track.setStereoVolume(maxVolume, maxVolume);
////			track = new AudioTrack(stream(), p_type.samp_rate(), AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT,
////					maxjitter, AudioTrack.MODE_STREAM);
//
////			maxjitter /= 2;
//			minjitter = minjitteradjust = 500*mu;
//			jitter = 875*mu;
//			devheadroom = Math.pow(jitter/5, 2);
//			timeout = 1;
//			luser = luser2 = -8000*mu;
//			cnt = cnt2 = user = lserver = 0;
//
//			if (oldtrack != null) {
//				oldtrack.stop();
//				oldtrack.release();
//			}
//		}
//	}
//
//	synchronized void write(short a[],int b,int c) {
//
//		int offset = b;
//		int totalLength = c;
//
//		int length = 160;
//
//		if(a == null || a.length < (b+c))
//			return;
//
//		try {
//
//			for(int i=offset; i<totalLength; i+=length) {
//
//				int stop = (i+length) <= (offset+totalLength) ? (i+length) : (offset+totalLength);
//				short output[] = Arrays.copyOfRange(a, i, stop);
//
////				if(RtpStreamSenderNew.echo && RtpStreamSenderNew.mobileAec != null) {
////					JAudioLauncher.t_analize = System.currentTimeMillis();
////					RtpStreamSenderNew.mobileAec.farendBuffer(a, length);
////
////					user += track.write(output, 0, length);
////
////					JAudioLauncher.t_render = System.currentTimeMillis();
////					JAudioLauncher.t_player_delay = (JAudioLauncher.t_render+(user - track.getPlaybackHeadPosition())/16) - JAudioLauncher.t_analize;
////				}
////				else {
////					user += track.write(output, 0, length);
////				}
//				user += track.write(output, 0, length);
//			}
//		}
//		catch(ArrayIndexOutOfBoundsException e) {
//			//Log.e("ArrayIndexOutOfBound","Track Write");
//		}
//		catch(Exception e) {
//			//Log.e("Exception",e.getMessage());
//		}
//	}
//
//	PowerManager.WakeLock pwl,pwl2;
//	WifiManager.WifiLock wwl;
//	static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;
//	boolean lockLast,lockFirst;
//	boolean keepon = false;
//	short linm[] = new short[BUFFER_SIZE];
//
//	void lock(boolean lock) {
//		try {
//			if (lock) {
//				boolean lockNew = keepon;
//				if (lockFirst || lockLast != lockNew) {
//					lockLast = lockNew;
//					lock(false);
//					lockFirst = false;
//					if (pwl == null) {
//						Context context = CksTabService.getContext();
//						if (context != null) {
//							PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//							pwl = pm.newWakeLock(lockNew?(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP):PROXIMITY_SCREEN_OFF_WAKE_LOCK, "Sipdroid.Receiver");
//							pwl.acquire();
//						}
//					}
//				}
//			} else {
//				lockFirst = true;
//				if (pwl != null) {
//					pwl.release();
//					pwl = null;
//				}
//			}
//		} catch (Exception e) {
//		}
//		if (lock) {
//			if (pwl2 == null) {
//				Context context = CksTabService.getContext();
//				if (context != null) {
//					PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//					WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//					pwl2 = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Sipdroid.Receiver");
//					pwl2.acquire();
//					wwl = wm.createWifiLock(3,"Sipdroid.Receiver");
//					wwl.acquire();
//				}
//			}
//		} else if (pwl2 != null) {
//			pwl2.release();
//			pwl2 = null;
//			wwl.release();
//		}
//	}
//
//    void newjitter(boolean inc) {
//        if (good == 0 || lost/good > 0.01)
//                return;
//        int newjitter = (int)Math.sqrt(devheadroom)*5 + (inc?minjitteradjust:0);
//        if (newjitter < minjitter)
//                newjitter = minjitter;
//        if (newjitter > maxjitter)
//                newjitter = maxjitter;
//        if (!inc && (Math.abs(jitter-newjitter) < minjitteradjust || newjitter >= jitter))
//                return;
//        if (inc && newjitter <= jitter)
//                return;
//        jitter = newjitter;
//        late = 0;
//        avgcnt = 0;
//        luser2 = user;
//    }
//
//	public void run() {
//		println("started");
//
//		//if (rtp_socket == null) {
//		//	println("socket null");
//		//	return;
//		//}
//
//		//byte[] buffer = new byte[BUFFER_SIZE];
//		//rtp_packet = new RtpPacket(buffer, 0);
//		//rtp_packet = new Packet(buffer);
//
//		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
//
//		setCodec();
//		Context context = CksTabService.getContext();
//		if (context != null) {
//			am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//		}
//
//		short lin[] = new short[BUFFER_SIZE];
//		//short lin2[] = new short[BUFFER_SIZE];
//		int server, headroom, todo, len = 0, m = 1, expseq, getseq, vm = 1, gap, gseq;
//
//		try {
//			tg = new ToneGenerator(stream(),(int)(ToneGenerator.MAX_VOLUME));
//		}
//		catch(Exception e) {
//			tg = null;
//		}
//		boolean reset = false;
//
//		System.gc();
//		//empty();
//		//track.play();
//
//		//running = true;
//		CksNativeLibrary.rtpQueue.clear();
//		int ringTimeout = 0;
//		int queueTimeout = 1000; //ms
//		lockFirst = true;
//		while (running) {
//			lock(true);
//
//			try {
////				rtp_socket.receive(rtp_packet);
//				rtp_packet = CksNativeLibrary.rtpQueue.poll(queueTimeout, TimeUnit.MILLISECONDS);
//				if(rtp_packet == null) {
//					if(StateManager.mState != null && StateManager.mState == CkslibraryEnum.ERinging) {
//						ringTimeout += queueTimeout;
//
//						if(ringTimeout >= 5000 && tg != null) {
//							try {
//								tg.startTone(ToneGenerator.TONE_SUP_RINGTONE);
//							}
//							catch(Exception err) {
//								tg.stopTone();
//								tg.release();
//								tg = null;
//							}
//							ringTimeout = 0;
//						}
//					}
//					continue;
//				}
//
//				if (timeout != 0) {
//					if(tg != null) {
//						tg.stopTone();
//						tg.release();
//						tg = null;
//					}
//
//					if(StateManager.mState != null && StateManager.mState == CkslibraryEnum.ECallEstablished && !reset) {
//						track.pause();
//						track.flush();
//						track.play();
//						reset = true;
//					}
//
//					if(track != null && track.getState() != AudioTrack.STATE_INITIALIZED) {
//						CksNativeLibrary.getInstance().CancelCall();
//
//						continue;
//					}
//
////                    track.pause();
////                    for (int i = maxjitter*4; i > 0; i -= BUFFER_SIZE)
////                        write(lin2, 0, i>BUFFER_SIZE? BUFFER_SIZE : i);
////
////                    cnt += maxjitter*2;
//
//                    if(track.getPlayState() != AudioTrack.PLAYSTATE_PLAYING)
//                    	track.play();
//
////					empty();
//				}
//				timeout = 0;
//
//				if(am != null) {
//					if(StateManager.SPEAKER_ON != am.isSpeakerphoneOn()) {
//						if(StateManager.SPEAKER_ON)
//							am.setSpeakerphoneOn(true);
//						else
//							am.setSpeakerphoneOn(false);
//					}
//				}
//			}
//			catch (Exception e) {
//				Log.e("",e.getMessage());
//			}
////				if ((StateManager.mState == CkslibraryEnum.ERinging) && (null != tg) && (timeout == 3)) {
////					try {
////						tg.startTone(ToneGenerator.TONE_SUP_RINGTONE);
////					}
////					catch(Exception err) {
////						tg.stopTone();
////						tg = null;
////					}
////				}
////
////				rtp_socket.getDatagramSocket().disconnect();
////				if (++timeout > 60) {
////					Log.e("Call Cancel", "Cancelling..");
////					CksNativeLibrary.getInstance().CancelCall();
////					break;
////				}
////			}
////			catch (Exception e) {
////				Log.e("",e.getMessage());
////			}
////
////			if (running && timeout == 0 && (StateManager.mState == CkslibraryEnum.ECallEstablished)) {
////				gseq = getSeq();
////				if(seq == gseq) {
////					m++;
////					continue;
////				}
////				gap = (gseq - seq) & 0xff;
////				if(gap > 240)
////					continue;
////				server = track.getPlaybackHeadPosition();
////				headroom = user - server;
////				if(headroom > 2*jitter)
////					cnt += len;
////				else
////					cnt = 0;
////
////				if(lserver == server)
////					cnt2++;
////				else
////					cnt2 = 0;
////
////				if(cnt <= 500 || cnt2 >=2 || headroom-jitter < len) {
////					len = p_type.decode(rtp_packet.getRtp(), lin, rtp_packet.getLength());
////				}
////
////				if(cnt == 0)
////					avgheadroom = avgheadroom * 0.99 + (double) headroom * 0.01;
////				if(avgcnt++ > 300)
////					devheadroom = devheadroom * 0.999 + Math.pow(Math.abs(headroom - avgheadroom), 2) * 0.001;
////
////				if(headroom < 250) {
////					late++;
////					avgcnt += 10;
////					if(avgcnt > 400)
////						newjitter(true);
////					todo = jitter - headroom;
////					write(lin2, 0, todo>BUFFER_SIZE?BUFFER_SIZE:todo);
////				}
////
////				if(cnt > 500 && cnt2 < 2) {
////					todo = headroom - jitter;
////					if (todo < len) {
////						write(lin, todo, len-todo);
////					}
////				}
////				else {
////					//cksFile.writeToFile(String.format("Writing: %d", len));
////					write(lin, 0, len);
////				}
////
////				if(seq != 0) {
////					getseq = gseq & 0xff;
////					expseq = ++seq & 0xff;
////
////					vm = m;
////					gap = (getseq - expseq) & 0xff;
////					if(gap > 0) {
////						if(gap > 100) gap = 1;
////						loss += gap;
////						lost += gap;
////						good += gap - 1;
////						loss2++;
////					}
////				}
////				m = 1;
////				seq = gseq;
////
////				if(user >= luser + 8000 && (StateManager.mInCall)) {
////					luser = user;
////					if(user >= luser2 + 160000)
////						newjitter(false);
////				}
////				lserver = server;
////			}
////			else if (running && timeout == 0 && (StateManager.mState != CkslibraryEnum.ECallEstablished)) {
//
//			if(running && timeout == 0) {
//				len = p_type.decode(rtp_packet.getRtp(), lin, rtp_packet.getLength());
//				write(lin, 0, len);
//			}
//		}
//
//		lock(false);
//
//		if(track != null && track.getState() == AudioTrack.STATE_INITIALIZED) {
//			track.stop();
//			track.release();
//		}
//		else if(track != null && track.getState() != AudioTrack.STATE_INITIALIZED) {
//			track.release();
//		}
//		track = null;
//
//		if(null != tg) {
//			tg.stopTone();
//			tg.release();
//		}
//
////		rtp_socket = null;
//
//		println("terminated");
//
//	}
//
//	private static void println(String str) {
//
//	}
//
//	public int mSeq = 0;
//	public int getSeq() {
//		return mSeq++;
//	}
//}
