package com.ci.connectdialer.core.auth;

import android.util.Log;

import com.ci.connectdialer.core.ckslib.BalanceListener;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BalanceManagerNew implements BalanceListener {
//	private boolean DEBUG = false;
	
	private int mTimeout;
	
	private Timer mBalanceUrlTimer;
	private Timer mBalanceConTimer;
	
	public BalanceManagerNew() {
		mTimeout = 0;
	}
	
	public synchronized void requestBalance() {
		dispose();
		
		boolean useBalanceServer = PreferenceSetting.getBool(CksTabService.getContext(),
				DialerSettings.DIALER_USE_BALANCE_SERVER,
				DialerSettings.DEFAULT_DIALER_USE_BALANCE_SERVER);
		
		if(useBalanceServer) {
			mBalanceConTimer = new Timer();
			mBalanceConTimer.scheduleAtFixedRate(new FetchBalance(), 0, 2000);
		}
		else {
			mBalanceUrlTimer = new Timer();
			mBalanceUrlTimer.scheduleAtFixedRate(new FetchBalanceUrl(), 0, 2000);
		}
	}
	
	public synchronized void dispose() {
		mTimeout = 0;
		
		if(null != mBalanceUrlTimer) {
			mBalanceUrlTimer.cancel();
			mBalanceUrlTimer.purge();
			mBalanceUrlTimer = null;
		}
		
		if(null != mBalanceConTimer) {
			mBalanceConTimer.cancel();
			mBalanceConTimer.purge();
			mBalanceConTimer = null;
		}
	}
	
	@Override
	public synchronized void onBalanceUrlResponse(String url) {
		dispose();
		if(url.matches("\\A\\p{ASCII}*\\z")) {
			PreferenceSetting.putString(CksTabService.getContext(), DialerSettings.DIALER_BALANCE_URL, url);
			
			new BalanceFetcherThread().start();
		}
		else {			
			mBalanceUrlTimer = new Timer();
			mBalanceUrlTimer.scheduleAtFixedRate(new FetchBalanceUrl(), 0, 2000);
		}
	}

	@Override
	public void onBalanceURLRequestTimeout() {
	}

	private class FetchBalanceUrl extends TimerTask {
		
		@Override
		public void run() {
			CksNativeLibrary.getInstance().RequestBalanceUrl();
			mTimeout++;

			if(mTimeout >= 2 && mBalanceUrlTimer != null) {
				dispose();
			}
		}
	};
	
	private class FetchBalance extends TimerTask {
		
		@Override
		public void run() {
			CksNativeLibrary.getInstance().ConnectBalanceServer();
			mTimeout++;
			if(mTimeout >= 2 && mBalanceConTimer != null) {
				dispose();
			}
		}
	};
	
	private class BalanceFetcherThread extends Thread {
		
		@Override
		public void run() {
			String response = null;

			String url = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.DIALER_BALANCE_URL, DialerSettings.DEFAULT_DIALER_BALANCE_URL);
			String username = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_USERNAME, "");
			
			if(null != url && url.contains("<pin>") && !username.equals("")) {
				url = url.replace("<pin>", username);

	            try {
	            	DefaultHttpClient httpClient = new DefaultHttpClient();
	                HttpGet httpGet = new HttpGet(url);
	                
	            	HttpResponse httpResponse = httpClient.execute(httpGet);
	            	response = EntityUtils.toString(httpResponse.getEntity());
	            	
	            	if(response != null && response.length() < 20) {
	            		
		            	Matcher matcher = Pattern.compile( "[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?" ).matcher(response);
						if(matcher.find()) {
							response = matcher.group();
						}
						double balance = Double.parseDouble(response);
						DecimalFormat df = new DecimalFormat("0.00");
						response = df.format(balance);

						CksNativeLibrary.getInstance().receivedBalance(response);
	            	}
	            } catch(Exception e) {
					Log.e("", e.getMessage());
				}
			}
		}
	}
	
}
