package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.database.SQLException;
import android.os.AsyncTask;

import com.ci.connectdialer.core.db.CallInformation;
import com.ci.connectdialer.core.db.DBAdapter;
import com.ci.connectdialer.ui.CalllogAdapter;

import java.util.List;
import java.util.Vector;

/**
 * Created by Masud Rashid on 11/1/2017.
 */
public class CallLogMgr {

    public static class CallLogTask extends AsyncTask<Integer, Void, Vector<CallInformation>> {
        private Context mContext;
        private DBAdapter mDBAdapter;
        private CalllogAdapter calllogAdapter;

        public CallLogTask(Context context, CalllogAdapter adapter) {
            mContext = context;
            calllogAdapter = adapter;
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();

            mDBAdapter = new DBAdapter(mContext);
            try {
                mDBAdapter.open();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Vector<CallInformation> doInBackground(Integer... params) {
            int callogType = params[0];

            mDBAdapter.removeCallLogExpiredEntries();
            switch (callogType) {
                case 0: // Fetch All Log
                    return mDBAdapter.getCallList();

                case 1: // Fetch Dialed Log
                    return mDBAdapter.getDialCallList();

                case 2: // Fetch Missed Log
                    return mDBAdapter.getMissedCallList();

                default:
                    break;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Vector<CallInformation> vector) {
            if(null != vector && null != calllogAdapter) {
                calllogAdapter.setCallInformation(vector);
                calllogAdapter.notifyDataSetChanged();
            }

            mDBAdapter.close();
        }
    }

    public static class ClearCallLogTask extends AsyncTask<Integer, Void, Void> {

        private Context mContext;
        private DBAdapter mDBAdapter;
        private CalllogAdapter calllogAdapter;
        private List<Long> mCallIdListToClear;

        public ClearCallLogTask(Context context, CalllogAdapter adapter) {
            mContext = context;
            calllogAdapter = adapter;
        }

        public void setCallIdListToClear(List<Long> ids){
            mCallIdListToClear = ids;
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();

            mDBAdapter = new DBAdapter(mContext);
            try {
                mDBAdapter.open();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int type = params[0];

            switch (type) {
                case 0:
                    mDBAdapter.removeAllCallLogEntries();
                    break;

                case 1:
                    mDBAdapter.removeDialedCallLogEntries();
                    break;

                case 2:
                    mDBAdapter.removeReceivedCallLogEntries();
                    break;

                case 3:
                    mDBAdapter.removeMissedCallLogEntries();
                    break;

                case 4:
                    mDBAdapter.removeSelectedCallLogEntries(mCallIdListToClear);
                    break;

                default:
                    break;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mDBAdapter.close();
            new CallLogTask(mContext, calllogAdapter).execute(0);
        }
    }

    public static class FrequentCallsTask extends AsyncTask<Integer, Void, CallInformation[]> {
        private Context mContext;
        private DBAdapter mDBAdapter;
        private FrequentCallsListener freqListener;

        public FrequentCallsTask(Context context, FrequentCallsListener listener) {
            mContext = context;
            freqListener = listener;
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();

            mDBAdapter = new DBAdapter(mContext);
            try {
                mDBAdapter.open();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected CallInformation[] doInBackground(Integer... params) {

            try {
                mDBAdapter.removeCallLogExpiredEntries();
                return mDBAdapter.getFrequentCalls(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
                this.cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(CallInformation[] calls) {
            try {
                mDBAdapter.close();
                freqListener.updateFrequentCalls(calls);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface FrequentCallsListener{

        void updateFrequentCalls(CallInformation[] calls);
    }

}
