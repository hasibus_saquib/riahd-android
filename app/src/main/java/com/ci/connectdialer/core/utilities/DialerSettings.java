package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.Vector;


public class DialerSettings {

//	public static String SERVER_1 = "server1";
//	public static String SERVER_2 = "server2";
//
//	public static String LOCAL_IP = "localip";
//	public static String LOCAL_PORT = "localport";
//	public static String LOCAL_RTP_PORT = "localrtpport";
//
//	public static String SIP_SERVER_IP = "sipserverip";
//	public static String SIP_SERVER_PORT = "sipserverport";
//
//	public static String VPN_IP = "vpnip";
//	public static String VPN_HANDSHAKE_PORT = "vpnhandshakeport";
//	public static String VPN_RTP_PORT = "vpnrtpport";
//	public static String VPN_RTP_BUNDLE_SIZE = "rtpbundlesize";
//
//	public static String PTIME = "ptime";
//	public static String ENCRYPTION_KEY = "encryptkey";
	
	public static String BALANCE_IP = "balanceip";
	public static String BALANCE_PORT = "balanceport";

	public static String PREF_ACCOUNT = "accountName";
	public static String PREF_ACCOUNT_KEY_SET = "accountkeySet";
	public static String ACCOUNT_KEY_PREFIX = "account@";

	public static String PREF_OPCODE = "opcode";
	public static String PREF_CACHE_OPCODE = "cacheopcode";
	public static String PREF_USERNAME = "username";
	public static String PREF_PASSWORD = "password";
	public static String PREF_PHONENO = "phoneno";
	public static String PREF_ECHO_CANCEL = "echo_cancellation";
	public static String PREF_AUDIO_MODE = "audiooption";
	public static String PREF_NUMBER_VPN_IP = "iNumberOfVpnIp";
	public static String PREF_VPN_IP_LIST = "vpn_ip_list";
	public static String PREF_NUMBER_DNS_IP = "iNumberOfDnsIp";
	public static String PREF_DNS_IP_LIST = "dns_ip_list";
	public static String PREF_NUMBER_REPORT_IP = "iNumberOfReportIp";
	public static String PREF_REPORT_IP_LIST = "report_ip_list";
	public static String PREF_NUMBER_VPN_PORT_UDP = "iNumberOfVpnPortUdp";
	public static String PREF_VPN_PORT_LIST_UDP = "vpn_port_list_udp";
	public static String PREF_NUMBER_VPN_PORT_TCP = "iNumberOfVpnPortTcp";
	public static String PREF_VPN_PORT_LIST_TCP = "vpn_port_list_tcp";
	public static String PREF_NUMBER_VPN_PORT_TLS = "iNumberOfVpnPortTls";
	public static String PREF_VPN_PORT_LIST_TLS = "vpn_port_list_tls";
	public static String PREF_RTP_DATA = "rtp_data";
	public static String PREF_QUICK_REQ = "quick_reg";
	public static String PREF_USE_VPN = "use_vpn";
	public static String PREF_USE_PHONE_NO = "use_phone_no";
	public static String PREF_ALLOW_SIP_INCOMING = "allow_sip_incoming";
	public static String PREF_COUNT_REAUTHENTICATING = "PREF_COUNT_REAUTHENTICATING";
	public static String PREF_REPORT_SETTINGS = "bReportSend";
	public static String PREF_DNS_SAVED_FOR_VERSION = "dns_received_for_version";
	public static String PREF_SUCCESS_PROTOCOL = "pref_success_protocol";
	public static String PREF_IGNORE_SILENCE = "pref_ignore_silence";

	public static String PREF_AUTH_IP_LIST = "auth_ip_list";
	public static String PREF_NUMBER_OF_AUTH_IP = "number_of_auth_ip";
	public static String PREF_AUTH_DOMAIN_RESOLVED = "auth_domain_resolved";
	public static String PREF_SAVED_AUTH_INDEX = "saved_auth_index";

	public static String PREF_CURRENT_BALANCE = "current_balance";
	public static String PREF_FORCE_UPDATE = "force_update";

	public static String DIALER_NAME = "dialername";
	public static String DIALER_FOOTER = "dialerfooter";
	public static String DIALER_IVR = "dialerivr";
	public static String DIALER_OPCODE_EDITABLE = "dialeropcodeedit";
	public static String DIALER_USE_BALANCE_SERVER = "bUseBalanceServer";
	public static String DIALER_BALANCE_URL = "dialerbalanceurl";
	public static String DIALER_CONTACT_NAME = "dialerContactName";
	public static String DIALER_CONTACT_NUMBER = "dialerContactNumber";
	public static String DIALER_CONTACT_ID = "dialerContactId";
	public static String DIALER_PROFILE_ID ;

	public static String LAST_AUTHENTICATION_TIME = "authtime";
	public static String APP_ON = "on";
	public static String APP_SETTINGS = "noprofile";

	public final static String DIALER_VERSION = "dialerversion";
	public final static String DIALER_VERSION_UPDATE = "lastdialerversionupdate";

	public final static String MIC_GAIN_IN_DB = "mic_gain_in_db";
	public final static String SPEAKER_GAIN_IN_DB = "speaker_gain_in_db";

	//Default Value
	public static boolean DEFAULT_DIALER_OPCODE_EDITABLE = false;
	public static boolean DEFAULT_ECHO_OPTION = false;
	public static boolean DEFAULT_AUDIO_OPTION = true;
	public static boolean DEFAULT_DIALER_USE_BALANCE_SERVER = false;
	public static String DEFAULT_DIALER_BALANCE_URL = null;

	//Push notification key
	public static String PUSH_NOTIFICATION_TITLE = "push_notification_title";
	//this field must be present in push notification
	public static String PUSH_NOTIFICATION_MSG_TITLE = "push_notification_msg_title";
	public static String PUSH_NOTIFICATION_MSG = "push_notification_msg";
	public static String PUSH_NOTIFICATION_IMG_URL = "push_notification_img_url";
	public static String PUSH_NOTIFICATION_IMG_DOMAIN_FRONTING_URL = "push_notification_img_domain_fronting_url";
	public static final String PUSH_NOTIFICATION_TOPIC_GLOBAL = "global";

	//Cut from PreferenceSettings.java
	public static final String KEY_ENABLE_LOOP_BACK = "enable_loop_back";
	public static final String KEY_RECEIVER_ENABLED = "enable_receiver";
	public static final String KEY_BUG_DETAILS = "bug_details";
	public static final String CUSTOM_AUTH_IP = "auth_ip_1";
	public static final String CUSTOM_DNS_IP = "dns_ip_custom";

	public static final String USER_REGISTRATION_URL = "185.106.240.201:8080";

	public static boolean ECHO_CANCELLATION_ENABLED = false;

	public static boolean COUNTRY_RESTRICTION_ON = false;

	public static int lastReceivedRtpIndex = -1;

	public enum ConnectivityType {
		Connectivity_None,
		Connectivity_Limited,
		Connectivity_Selected,
		Connectivity_Full
	};

	public static final int PLATINUM_OPCODE = 67089;
	public static boolean SHOULD_SET_PLATINUM_OPCODE = true;
	public static boolean SHOW_OPCODE_FIELD = false;
	public static final int DIALER_ID = 1009;

	public static String CURRENT_BALANCE = "current_balance";
	public static boolean NETWORK_DETECTED = false;
	
	public static boolean isSettingsSaved(Context context) {
		if (context == null)
			return false;
		return PreferenceSetting.getBool(context, DialerSettings.APP_SETTINGS, false);
	}
	
	public static boolean isUpdateAlertToShow(Context context, String playStoreVersion) {

		if (context == null)
			return false;

		String lastAlertedVersion = PreferenceSetting.getString(context,DIALER_VERSION_UPDATE, null);
		if(lastAlertedVersion != null && !lastAlertedVersion.isEmpty()) {
			if(lastAlertedVersion.equalsIgnoreCase(playStoreVersion)) {
				return false;
			}
		}

		return true;
	}

	public static String getPlayStoreVersion(Context context) {
		if(context == null)
			return null;

		String playVersion = PreferenceSetting.getString(context,DIALER_VERSION_UPDATE, null);

		return playVersion;
	}

	public static boolean isVersionLowerThanPlayStore(Context context){

		if(context == null)
			return false;

		String playVersion = PreferenceSetting.getString(context,DIALER_VERSION_UPDATE, null);
		return isVersionLowerThan(context, playVersion);
	}

	public static boolean isVersionLowerThan(Context context, String paramVersion){

		if( paramVersion == null ) return false;

		final String[] part = paramVersion.split("\\.");

		if( part.length < 2 && part[0].length() < 1 && part[1].length() < 1) return false;

		final int major = Integer.parseInt(part[0]);
		final int minor = Integer.parseInt(part[1]);

		final int[] thisVersion = PhoneInformation.getVersion(context);
		return (major > thisVersion[0]) || ((major >= thisVersion[0]) && (minor > thisVersion[1]));
	}

	public static void clearAllSettingsExceptAccount(Context context) {

		if(context == null){
			return;
		}

		Account currentAccount = getCurrentAccount(context);
		clearPreference(context);
		String accountKey = ACCOUNT_KEY_PREFIX + currentAccount.getAccountName();
		PreferenceSetting.putString(context, accountKey, currentAccount.toString());
		saveAsCurrentAccount(context, currentAccount);
	}

	public static void clearPreference(Context context) {
		if(context == null){
			return;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.clear();
		prefsEditor.commit();
	}

	public static void saveContactInfo(Context context, String name, String number, long contactID) {
		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		editor.putString(DIALER_CONTACT_NAME, name);
		editor.putString(DIALER_CONTACT_NUMBER, number);
		editor.putLong(DIALER_CONTACT_ID, contactID);
		editor.apply();
	}

	public static void saveNewAuthSettings(
			Context context,
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls,
			byte[] pVpnPortListTls, boolean bQuickRegistration, boolean bUseVpn,
			boolean bAllowSipIncoming, boolean bUsePhoneNo, boolean bEnableOpcodeEdit,
			boolean bUseBalanceServer, String strBrandName, String strIvr, String strFooter,
			long iUpdateTime, int iNumberOfDnsIp, byte[] pDnsIpList, int iNumberOfAuthIp,
			byte[] pAuthIpList, int iNumberOfReportIp, byte[] pReportIpList, boolean bReportSend) {

		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putInt(PREF_NUMBER_VPN_IP, iNumberOfVpnIp);
		String vpnIpListString = Base64.encodeToString(pVpnIpList, Base64.DEFAULT);
		editor.putString(PREF_VPN_IP_LIST, vpnIpListString);
		editor.putInt(PREF_NUMBER_VPN_PORT_UDP, iNumberOfVpnPortUdp);
		String vpnPortListStringUdp = Base64.encodeToString(pVpnPortListUdp, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_UDP, vpnPortListStringUdp);
		editor.putInt(PREF_NUMBER_VPN_PORT_TCP, iNumberOfVpnPortTcp);
		String vpnPortListStringTcp = Base64.encodeToString(pVpnPortListTcp, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_TCP, vpnPortListStringTcp);
		editor.putInt(PREF_NUMBER_VPN_PORT_TLS, iNumberOfVpnPortTls);
		String vpnPortListStringTls = Base64.encodeToString(pVpnPortListTls, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_TLS, vpnPortListStringTls);
		editor.putBoolean(PREF_QUICK_REQ, bQuickRegistration);
		editor.putBoolean(PREF_USE_VPN, bUseVpn);
		editor.putBoolean(PREF_ALLOW_SIP_INCOMING, bAllowSipIncoming);
		editor.putBoolean(PREF_USE_PHONE_NO, bUsePhoneNo);
		editor.putBoolean(DIALER_OPCODE_EDITABLE, bEnableOpcodeEdit);
		editor.putBoolean(DIALER_USE_BALANCE_SERVER, bUseBalanceServer);
		editor.putString(DIALER_NAME, strBrandName);
		editor.putString(DIALER_IVR, strIvr);
		editor.putString(DIALER_FOOTER, strFooter);
		editor.putLong(LAST_AUTHENTICATION_TIME, iUpdateTime);
		editor.putInt(PREF_NUMBER_DNS_IP, iNumberOfDnsIp);
		String dnsIpLIst = Base64.encodeToString(pDnsIpList, Base64.DEFAULT);
		editor.putString(PREF_DNS_IP_LIST, dnsIpLIst);
		editor.putInt(PREF_NUMBER_REPORT_IP, iNumberOfReportIp);
		String reportIpList = Base64.encodeToString(pReportIpList, Base64.DEFAULT);
		editor.putString(PREF_REPORT_IP_LIST, reportIpList);

		editor.putInt(PREF_NUMBER_OF_AUTH_IP, iNumberOfAuthIp);
		String authIpList = Base64.encodeToString(pAuthIpList, Base64.DEFAULT);
		editor.putString(PREF_AUTH_IP_LIST, authIpList);
		editor.putBoolean(PREF_REPORT_SETTINGS, bReportSend);

		editor.putString(PREF_DNS_SAVED_FOR_VERSION, PhoneInformation.getAppVersion(context));

		editor.commit();
	}

	public static void saveReRegisterValues(
			Context context,
			int iNumberOfVpnIp, byte[] pVpnIpList, int iNumberOfVpnPortUdp, byte[] pVpnPortListUdp,
			int iNumberOfVpnPortTcp, byte[] pVpnPortListTcp, int iNumberOfVpnPortTls, byte[] pVpnPortListTls) {

		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putInt(PREF_NUMBER_VPN_IP, iNumberOfVpnIp);
		String vpnIpListString = Base64.encodeToString(pVpnIpList, Base64.DEFAULT);
		editor.putString(PREF_VPN_IP_LIST, vpnIpListString);
		String vpnPortListStringUdp = Base64.encodeToString(pVpnPortListUdp, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_UDP, vpnPortListStringUdp);
		String vpnPortListStringTcp = Base64.encodeToString(pVpnPortListTcp, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_TCP, vpnPortListStringTcp);
		String vpnPortListStringTls = Base64.encodeToString(pVpnPortListTls, Base64.DEFAULT);
		editor.putString(PREF_VPN_PORT_LIST_TLS, vpnPortListStringTls);

		editor.commit();
	}

	public static void saveUpdatedDialerInformation(
			Context context, String strBrandName, String strIvr, String strFooter, long lUpdateTime) {

		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putString(DIALER_NAME, strBrandName);
		editor.putString(DIALER_IVR, strIvr);
		editor.putString(DIALER_FOOTER, strFooter);
		editor.putLong(LAST_AUTHENTICATION_TIME, lUpdateTime);

		editor.commit();
	}

	public static void saveReportAddressValues(
			Context context, byte[] pReportIps, int iNumberOfReportIp) {

		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putInt(PREF_NUMBER_REPORT_IP, iNumberOfReportIp);
		String reportIpListString = Base64.encodeToString(pReportIps, Base64.DEFAULT);
		editor.putString(PREF_REPORT_IP_LIST, reportIpListString);

		editor.commit();
	}

	public static void saveRegistrationCompletedValues(
			Context context,
			byte[] pDnsIps, int iNumberOfDnsIp, byte[] pAuthIpList, int iNumberOfAuthIp,
			byte[] pReportIpList, int iNumberOfReportIp, boolean bReportSend, int iSuccessProtocol,
			boolean bIgnoreSilence, String profileId, boolean forceUpdate) {

		if(context == null)
			return;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putInt(PREF_NUMBER_DNS_IP, iNumberOfDnsIp);
		String dnsIpLIst = Base64.encodeToString(pDnsIps, Base64.DEFAULT);
		editor.putString(PREF_DNS_IP_LIST, dnsIpLIst);

		editor.putInt(PREF_NUMBER_OF_AUTH_IP, iNumberOfAuthIp);
		String authIpList = Base64.encodeToString(pAuthIpList, Base64.DEFAULT);
		editor.putString(PREF_AUTH_IP_LIST, authIpList);

		editor.putInt(PREF_NUMBER_REPORT_IP, iNumberOfReportIp);
		String reportIpLIst = Base64.encodeToString(pReportIpList, Base64.DEFAULT);
		editor.putString(PREF_REPORT_IP_LIST, reportIpLIst);

		editor.putBoolean(PREF_REPORT_SETTINGS, bReportSend);
		editor.putString(PREF_DNS_SAVED_FOR_VERSION, PhoneInformation.getAppVersion(context));

		editor.putInt(PREF_SUCCESS_PROTOCOL, iSuccessProtocol);

		editor.putBoolean(PREF_IGNORE_SILENCE, bIgnoreSilence);

		DIALER_PROFILE_ID = profileId;

		editor.putBoolean(PREF_FORCE_UPDATE, forceUpdate);


		editor.commit();
	}

	public static Vector<Account> getSavedAccountList(Context context) {

		Vector<Account> accountVector = new Vector<>();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		Set<String> accountKeys = prefs.getStringSet(PREF_ACCOUNT_KEY_SET, null);

		if( accountKeys != null){
			for( String key : accountKeys) {

				String accountItem = prefs.getString(key, "");
				Account account = Account.getFromString(accountItem);
				accountVector.add(account);
			}
			Collections.sort(accountVector, new Comparator<Account>() {
				@Override
				public int compare(Account left, Account right) {
					return left.getAccountName().compareTo(right.getAccountName());
				}
			});
		}
		return accountVector;
	}

	public static void removeCustomFromAccounts(Context context) {
		if(context == null)
			return;

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();
		Set<String> accountKeys = prefs.getStringSet(PREF_ACCOUNT_KEY_SET, null);

		if( accountKeys != null){
			for( String key : accountKeys) {

				String accountItem = prefs.getString(key, "");
				Account account = Account.resetCustomSettings(accountItem);
				String editedAccountItem = account.toString();
				editor.putString(key, editedAccountItem);
			}
			editor.commit();
		}
	}

	public static String getNewAccountName(Context context) {

		return "Account-1";

		/*if(context == null)
			return "Account-1";

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		Set<String> accountKeys = prefs.getStringSet(PREF_ACCOUNT_KEY_SET, null);

		int max = 0;
		try {
			if (accountKeys != null && accountKeys.size() > 0) {
				for (String key : accountKeys) {
					int number = Integer.parseInt(key.split("-")[1]);
					max = number > max ? number : max;
				}
			}
		}catch (Exception e){
			Log.e("getNewAccountName", e.getMessage());
		}
		return "Account-" + (max+1);*/
	}

	public static Account getSavedAccount(Context context, String accountName) {
		if(context == null)
			return new Account();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		String accountStringItem = prefs.getString(ACCOUNT_KEY_PREFIX+accountName, "");
		Account account = Account.getFromString(accountStringItem);
		return account;
	}

	public static Account getCurrentAccount(Context context) {
		Account account = new Account();

		if(context == null)
			return account;

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		account.setOpCodeEditable(prefs.getBoolean(DIALER_OPCODE_EDITABLE, false));
		account.setAccountName(prefs.getString(PREF_ACCOUNT, ""));
		account.setOpCode(prefs.getInt(PREF_OPCODE, 0));
		account.setUsername(prefs.getString(PREF_USERNAME, ""));
		account.setPassword(prefs.getString(PREF_PASSWORD, ""));
		account.setPhoneNumber(prefs.getString(PREF_PHONENO, ""));
		account.setCustomAuthIp(prefs.getString(CUSTOM_AUTH_IP, ""));
		account.setCustomDnsIp(prefs.getString(CUSTOM_DNS_IP, ""));

		return account;
	}

	public static boolean removeCurrentAccount(Context context) {

		if(context == null)
			return false;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.remove(APP_SETTINGS);
		editor.remove(DIALER_OPCODE_EDITABLE);
		editor.remove(PREF_ACCOUNT);
		editor.remove(PREF_OPCODE);
		editor.remove(PREF_USERNAME);
		editor.remove(PREF_PASSWORD);
		editor.remove(PREF_PHONENO);
		editor.remove(CUSTOM_AUTH_IP);
		editor.remove(CUSTOM_DNS_IP);

		editor.commit();

		return true;
	}

	public static boolean saveAsCurrentAccount(Context context, Account account){

		if(context == null)
			return false;

		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();

		editor.putString(PREF_ACCOUNT, account.getAccountName());
		editor.putBoolean(DIALER_OPCODE_EDITABLE, account.isOpCodeEditable());
		editor.putInt(PREF_OPCODE, account.getOpCode());
		editor.putString(PREF_USERNAME, account.getUsername());
		editor.putString(PREF_PASSWORD, account.getPassword());
		editor.putString(PREF_PHONENO, account.getPhoneNumber());
		editor.putString(CUSTOM_AUTH_IP, account.getCustomAuthIp());
		editor.putString(CUSTOM_DNS_IP, account.getCustomDnsIp());

		editor.putBoolean(APP_SETTINGS, true);
		editor.putInt(PREF_COUNT_REAUTHENTICATING, 0);

		editor.commit();

		return true;
	}

	public static boolean isEchoCancelationOn(Context context) {
		if (context == null)
			return false;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		ECHO_CANCELLATION_ENABLED = prefs.getBoolean(PREF_ECHO_CANCEL, false);
		return ECHO_CANCELLATION_ENABLED;
	}

	public static void setEchoCancelationOn(Context context, boolean value) {
		if (context == null)
			return;
		ECHO_CANCELLATION_ENABLED = value;
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		editor.putBoolean(PREF_ECHO_CANCEL, value);
		editor.commit();
		return;
	}

	public static void setCurrentBalance(Context context, String strBalance) {
		if (context == null)
			return;
		CURRENT_BALANCE = strBalance;
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		editor.putString(PREF_CURRENT_BALANCE, strBalance);
		editor.commit();
		return;
	}

	public static String getCurrentBalance(Context context) {
		if (context == null)
			return "";
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		CURRENT_BALANCE = prefs.getString(PREF_CURRENT_BALANCE, "");
		return CURRENT_BALANCE;
	}
}
