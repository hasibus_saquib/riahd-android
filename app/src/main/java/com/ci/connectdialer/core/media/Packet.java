package com.ci.connectdialer.core.media;

public class Packet {
	
	private byte[] rtp;
	private int length;
	
	public Packet(byte[] buffer) {
		this.rtp = buffer;
		this.length = buffer.length;
	}
	
	public byte[] getRtp() {
		return rtp;
	}
	
	public void setRtp(byte[] rtp) {
		this.rtp = rtp;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
}
