package com.ci.connectdialer.core.utilities;

/**
 * Created by Erfan on 2/13/17.
 */

public interface OptionsInterface {
    void shareRecordedCall(int position);
    void playRecordedCall(int position);
    void deleteRecordedCall(int position);
}
