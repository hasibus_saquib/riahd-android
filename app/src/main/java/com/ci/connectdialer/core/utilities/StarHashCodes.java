package com.ci.connectdialer.core.utilities;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.db.DBAdapter;

/**
 * Created by user on 19/9/2016.
 */
public class StarHashCodes implements AsyncTasks.GoogleDnsDomainResolve.IGoogleDNSDomainResolve {

    private static final String HASH_CODE_KEY_VALUE_SEPARATOR = " --> ";

    private static final String[] KEYS = {
            "*#00*1#",
            "*#00*2#",
            "*#02*1#",
            "*#02*0#",
            "*#03#",
            "*#05*0#",
            "*#05*3#",
            "*#05*4#",
            "*#05*5#",
            "*#06*0#",
            "*#06*1*mcc*mnc#",
            "*#06*2#",
            "*#09#",
            "*#0101#",
            "*#0102#",
            "*#0103#",
            "*#10#",
    };

    private static final String[] VALUE_DETAILS = {
            "Vkmq&lnv`qlgj%dgqbkju",
            "Vkmq&wulqjoc&aevdjnu",
            "@mcdj` njlrdgfk",
            "Ajqgdie\"ilmvddci",
            "Vkmq&duvm/\"pvk ckg\"bhv ku",
            "Ajqgdie\"fvqrih cpwj&ou kkswr",
            "@mcdj` appvik%awqk\"ov%iluvv",
            "@mcdj` appvik%dlv#kv&lnrpw",
            "Fvqrih fjncoh%rgvlnpc",
            "Ppg&hdtksf\"kef $%nle",
            "Vfv&epsvjn\"kef $%nle",
            "Hbic&Weepogt&Aicifp",
            "Vkmq&QX\"dmf&T] tdowcu",
            "Foggt%ccfkg",
            "Foggt%aaflwhr",
            "Foggt%aaflwhr",
            "Wfqcr%ani#jgum ajggu",
    };

//    private static void loadStarHashCodes() {
//
//        try {
//            Resources res = getResources();
//            InputStream in_s = res.openRawResource(R.raw.hashcode_keys);
//
//            byte[] b = new byte[in_s.available()];
//            in_s.read(b);
//            txtHelp.setText(new String(b));
//        } catch (Exception e) {
//            // Log.e("",e.getMessage());
//            txtHelp.setText("Error: can't show help.");
//        }
//    }

    private static StarHashCodes INSTANCE;
    public static StarHashCodes getInstance() {
        if( INSTANCE == null ) {
            INSTANCE = new StarHashCodes();
        }
        return  INSTANCE;
    }

    private StarHashCodes()
    {
        //loadStarHashCodes();
    }

    private static Context context = null;

    public boolean checkAndProcessStarHashKey(Activity pActivity, String starHash){

        try {
            context = pActivity;
            boolean hKeyEnabled = StateManager.STAR_HASH_HOT_KEY_ENABLE;

            String mccMnc="";
            if(starHash.startsWith("*#06*1*")){
                mccMnc = starHash.substring(7);
                starHash = "*#06*1*";
            }
            
            switch (starHash) {
                case "*#256699#" :  onEnableHotKey();
                    return true;
                case "*#256690#" :  onDisableHotKey();
                    return true;
                case "*#000000#" :
                    if(hKeyEnabled) showAllStarHashCodes();
                    return true;
                case "*#0101#" :
                    if(hKeyEnabled) onAllCacheDataClear();
                    return true;
                case "*#0102#" :
                    if(hKeyEnabled) onAllCacheAndAccountClear();
                    return true;
                case "*#0103#" :
                    if(hKeyEnabled) onAllCachesAccountAndCallLogClear();
                    return true;
                case "*#02#" :
                    if(hKeyEnabled) showLoopBackState();
                    return true;
                case "*#020#" :
                    if(hKeyEnabled) onDisableLoopBack();
                    return true;
                case "*#021#" :
                    if(hKeyEnabled) onEnableLoopBack();
                    return true;
                case "*#03#" :
                    if(hKeyEnabled) showServerInformations();
                    return true;
                case "*#06*1*" :
                    if(hKeyEnabled) setMccMncAndConnect(starHash+mccMnc);
                    return true;
                case "*#06*1#" :
                    if(hKeyEnabled) setMccMncWithAlertAndConnect();
                    return true;
                case "*#06*0#" :
                    if(hKeyEnabled) clearMccMncAndConnect();
                    return true;
                case "*#06*2#" :
                    if (hKeyEnabled) {
                        DialerSettings.SHOULD_SET_PLATINUM_OPCODE = false;
                        DialerSettings.SHOW_OPCODE_FIELD = true;
                    }
                    return true;
                case "*#05*3#" :
                    if(hKeyEnabled) enableCustomAuthInput();
                    return true;
                case "*#05*4#" :
                    if(hKeyEnabled) enableCustomDNSInput();
                    return true;
                case "*#05*0#" :
                    if(hKeyEnabled) disableCustomAuthDNSInput();
                    return true;
                case "*#05*5#" :
                    if(hKeyEnabled) showGoogleDNSAuthDomainInput();
                    return true;
                case "*#00*1#" :
                    if(hKeyEnabled) showInternalDetails();
                    return true;
                case "*#00*2#" :
                    if(hKeyEnabled) showRuntimeDetails();
                    return true;
                default:
                    return false;
            }
        } catch (Exception e) {
            Log.e("StarHashCode", e.getMessage());
        }
        return false;
    }

    //case "*#05*0#"
    private void disableCustomAuthDNSInput() {
        StateManager.CUSTOM_AUTH_ENABLED = false;
        StateManager.CUSTOM_DNS_ENABLED = false;
        Toast.makeText(context, "Test Auth/DNS Disabled", Toast.LENGTH_LONG).show();
    }

    //case "*#05*4#"
    private void enableCustomDNSInput() {
        StateManager.CUSTOM_DNS_ENABLED = true;
        Toast.makeText(context, "Custom DNS Enabled", Toast.LENGTH_LONG).show();
    }

    //case "*#05*3#"
    private void enableCustomAuthInput() {
        StateManager.CUSTOM_AUTH_ENABLED = true;
        Toast.makeText(context, "Custom Auth Enabled", Toast.LENGTH_LONG).show();
    }

    //case "*#06*1#"
    private void setMccMncWithAlertAndConnect() {

        final EditText editMcc = new EditText(context);
        editMcc.setInputType(InputType.TYPE_CLASS_NUMBER);
        editMcc.setHint("Mcc");
        final EditText editMnc = new EditText(context);
        editMnc.setInputType(InputType.TYPE_CLASS_NUMBER);
        editMnc.setHint("Mnc");

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(editMcc);
        layout.addView(editMnc);

        Alert.showOkView(context, "Set Mcc-Mnc value", layout, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                StateManager.setMccMnc(editMcc.getText().toString(), editMnc.getText().toString());
                Receiver.getStateManager().connect();
                Toast.makeText(context, "Reconnecting", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //case "*#06*1*"
    private void setMccMncAndConnect(String starHashCode) {

        if(starHashCode.length()<=7){
            return ;
        }
        starHashCode = starHashCode.substring(7);
        starHashCode = starHashCode.replace("*", "-");
        starHashCode = starHashCode.substring(0, starHashCode.length() - 1);
        String[] data = starHashCode.split("-");

        if(data.length < 1) return ;

        StateManager.setMccMnc(data[0], data[1]);
        Toast.makeText(context, "Reconnecting", Toast.LENGTH_SHORT).show();
        Receiver.getStateManager().connect();
    }

    //case "*#06*0#"
    private void clearMccMncAndConnect() {
        StateManager.clearMccMnc();
        Toast.makeText(context, "Mcc/Mnc data cleared", Toast.LENGTH_SHORT).show();
        Receiver.getStateManager().connect();
    }

    public String getAllStarHashFormatted() throws Exception {
        String ret = "";
        CksNativeLibrary cksNative = CksNativeLibrary.getInstance();
        for( int i = 0; i < KEYS.length; i++) {

            String value = cksNative.EncryptDecryptAndReturnValue(VALUE_DETAILS[i], ApplicationValidator.HASH_CODE_ENCRYPTION_KEY);
            ret += "\n" + (i+1) + ". " + KEYS[i] + HASH_CODE_KEY_VALUE_SEPARATOR + value + "\n" ;

        }
        //Log.d("hashcode", ret);
        return ret;
    }

    //case "*#0101#"
    public void onAllCacheDataClear() throws Exception {
        if(!((Activity) context).isFinishing()){

            Alert.showOkCancel(context, null, "Do you want to clear all cached data?",
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Toast.makeText(context, "All settings cleared", Toast.LENGTH_LONG).show();
                    DialerSettings.clearAllSettingsExceptAccount(context);
                    StateManager.CACHE_CLEARED = true;
                    Receiver.getStateManager().reconnect();
                }
            },null);
        }
    }

    //case "*#0102#"
    public void onAllCacheAndAccountClear() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showPositiveNegative(context, null, "Do you want to clear account settings?",
                    "Delete", "cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            DialerSettings.clearPreference(context);
                            mStarHashCodeListener.afterAllCacheClear();

                        }
                    }, null);
        }
    }

    //case "*#0103#"
    public void onAllCachesAccountAndCallLogClear() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showPositiveNegative(context, null, "Do you want to clear all data?",
                    "Delete", "cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            DialerSettings.clearPreference(context);
                            mStarHashCodeListener.afterAllCacheClear();
                            DBAdapter dbAdapter = new DBAdapter(context);
                            dbAdapter.open();
                            dbAdapter.removeAllCallLogEntries();
                            dbAdapter.close();
                        }
                    }, null);
        }
    }

    //case "*#000777#"
    public void onEnableHotKey() throws Exception {
        if (!((Activity) context).isFinishing()) {

            StateManager.STAR_HASH_HOT_KEY_ENABLE = true;
            Toast.makeText(context, "Hot key Enabled", Toast.LENGTH_SHORT).show();
        }
    }

    //case "*#000770#"
    public void onDisableHotKey() throws Exception {
        if (!((Activity) context).isFinishing()) {

            StateManager.STAR_HASH_HOT_KEY_ENABLE = false;
            Toast.makeText(context, "Hot key Disabled", Toast.LENGTH_SHORT).show();
        }
    }

    //case "*#02#"
    public void showLoopBackState() throws Exception {
        if (!((Activity) context).isFinishing()) {

            final boolean loop_back_state = PreferenceSetting.getBool(
                    context, DialerSettings.KEY_ENABLE_LOOP_BACK, false);
            String msg = "Loop Back is currently " + (loop_back_state ? "enable." : "disable.");

            Alert.showOk(context, msg);
        }
    }

    //case "*#021#"
    public void onEnableLoopBack() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showOkCancel(context, null, "Want to Enable Loop Back?",
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PreferenceSetting.putBool(context, DialerSettings.KEY_ENABLE_LOOP_BACK,true);
                }
            }, null);
        }
    }

    //case "*#020#"
    public void onDisableLoopBack() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showOkCancel(context, null, "Want to Disable Loop Back?",
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PreferenceSetting.putBool(context, DialerSettings.KEY_ENABLE_LOOP_BACK,false);
                }
            }, null);
        }
    }

    //case "*#000000#"
    public void showAllStarHashCodes() throws Exception {

        if (!((Activity) context).isFinishing()) {

            Alert.showOkScroll(context, "List of all hashcodes",
                    StarHashCodes.getInstance().getAllStarHashFormatted(), null);
        }
    }

    //case "*#05*5#"
    public void showGoogleDNSAuthDomainInput() throws Exception {
        if (!((Activity) context).isFinishing()) {

            final EditText inputText = new EditText(context);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    context)
                    .setTitle("Enter Domain")
                    .setView(inputText)
                    .setCancelable(true)
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startGoogleDNSAuthDomainResolve(inputText.getText().toString());

                                }
                            });
            alertDialogBuilder.create().show();
        }
    }

    private void startGoogleDNSAuthDomainResolve(String domain){

        new AsyncTasks.GoogleDnsDomainResolve(context, this).execute(domain);
    }

    public void showGoogleDNSAuthDomainResponse( String response) {
        if (!((Activity) context).isFinishing()) {

            Alert.showOkScroll(context, "Google DNS Auth Domain Response", response, null);
        }
    }

    //case "*#00*1#"
    public void showInternalDetails() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showOkScroll(context, "Internal Details",
                    CksUtil.getInternalDetailsFormatted(context), null);
        }
    }

    //case "*#00*2#"
    public void showRuntimeDetails() throws Exception {
        if (!((Activity) context).isFinishing()) {

            Alert.showOkScroll(context, "Runtime Details",
                    CksUtil.getRuntimeDetailsFormatted(context), null);
        }
    }

    //case "*#03#"
    public void showServerInformations() {

        new AsyncTasks.ShowServerInfo(context).execute();
    }

    public interface StarHashCodeListener{
        void afterAllCacheClear();
    }

    private StarHashCodeListener mStarHashCodeListener;

    public void setStarHashCodeListener(StarHashCodeListener pListener){
        mStarHashCodeListener = pListener;
    }
}
