package com.ci.connectdialer.core.media;

import android.annotation.SuppressLint;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AutomaticGainControl;
import android.media.audiofx.NoiseSuppressor;

public class AudioFixer_SDK16 {

	private AcousticEchoCanceler aec = null;
	private NoiseSuppressor noise = null;
	private AutomaticGainControl agc = null;

	@SuppressLint("NewApi")
	public void fixAudioRecord(AudioRecord audioRecord) {
		if (android.os.Build.VERSION.SDK_INT >= 16 && audioRecord != null) {
			if (AcousticEchoCanceler.isAvailable()) {
				aec = AcousticEchoCanceler.create(audioRecord.getAudioSessionId());
				if (aec != null && !aec.getEnabled()) {
					aec.setEnabled(true);
				}
			}
			if (NoiseSuppressor.isAvailable()) {
				noise = NoiseSuppressor.create(audioRecord.getAudioSessionId());
				if (noise != null && !noise.getEnabled()) {
					noise.setEnabled(true);
				}
			}
			if (AutomaticGainControl.isAvailable()) {
				agc = AutomaticGainControl.create(audioRecord.getAudioSessionId());
				if (agc != null) {
					agc.setEnabled(true);
				}
			}
		}
	}

	@SuppressLint("NewApi")
	public void fixAudioTrack(AudioTrack audioTrack) {
		if (android.os.Build.VERSION.SDK_INT >= 16 && audioTrack != null) {
			if (AcousticEchoCanceler.isAvailable()) {
				aec = AcousticEchoCanceler.create(audioTrack.getAudioSessionId());
				if (aec != null && !aec.getEnabled()) {
					aec.setEnabled(true);
				}
			}
			if (NoiseSuppressor.isAvailable()) {
				noise = NoiseSuppressor.create(audioTrack.getAudioSessionId());
				if (noise != null && !noise.getEnabled()) {
					noise.setEnabled(true);
				}
			}
			if (AutomaticGainControl.isAvailable()) {
				agc = AutomaticGainControl.create(audioTrack.getAudioSessionId());
				if (agc != null) {
					agc.setEnabled(true);
				}
			}
		}
	}

	@SuppressLint("NewApi")
	public void dispose() {
		if (null != aec) {
			aec.setEnabled(false);
			aec.release();
			aec = null;
		}
		if (null != noise) {
			noise.setEnabled(false);
			noise.release();
			noise = null;
		}
		if (null != agc) {
			agc.setEnabled(false);
			agc.release();
			agc = null;
		}
	}
}
