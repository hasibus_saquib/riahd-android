package com.ci.connectdialer.core.auth;//package com.cks.dialer.core.auth;
//
//import java.text.DecimalFormat;
//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.util.EntityUtils;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.cks.dialer.core.platinum.CksTabService;
//import com.cks.dialer.core.platinum.DialerSettings;
//import com.cks.dialer.core.platinum.PreferenceSetting;
//import com.cks.dialer.core.ckslib.BalanceListener;
//import com.cks.dialer.core.ckslib.CksNativeLibrary;
//
//public class BalanceManager implements BalanceListener {
//
//	private Timer mBalanceTimer;
//	private static int mTimeout = 0;
//	private final TimerTask mBalanceTask = new TimerTask() {
//
//		@Override
//		public void run() {
//			CksNativeLibrary.getInstance().ConnectBalanceServer();
//			mTimeout++;
//
//			if(mTimeout >= 2 && mBalanceTimer != null) {
//				disposeBalanceServerRequest();
//			}
//		}
//	};
//
//	public BalanceManager() {
//		mTimeout = 0;
//	}
//
//	public synchronized void reqBalance() {
//		boolean useBalanceServer = PreferenceSetting.getBool(CksTabService.getContext(),
//				DialerSettings.DIALER_USE_BALANCE_SERVER,
//				DialerSettings.DEFAULT_DIALER_USE_BALANCE_SERVER);
//
//		if(useBalanceServer)
//			reqBalanceServer();
//		else
//			new BalanceTask(BalanceTask.REQ_BALANCE_URL).start();
//	}
//
//	private void reqBalanceServer() {
//		disposeBalanceServerRequest();
//
//		mBalanceTimer = new Timer();
//		mBalanceTimer.scheduleAtFixedRate(mBalanceTask, 0, 3000);
//	}
//
//	public void disposeBalanceServerRequest() {
//		if(null != mBalanceTimer) {
//			mBalanceTimer.cancel();
//			mBalanceTimer.purge();
//			mBalanceTimer = null;
//			mTimeout = 0;
//		}
//	}
//
//
//	/* BalanceListener Callbacks */
//	@Override
//	public void onBalanceUrlResponse(String url) {
//		if(url.matches("\\A\\p{ASCII}*\\z")) {
//			PreferenceSetting.putString(CksTabService.getContext(), DialerSettings.DIALER_BALANCE_URL, url);
//			new BalanceTask(BalanceTask.FETCH_BALANCE).start();
//		} else {
//			new BalanceTask(BalanceTask.REQ_BALANCE_URL);
//		}
//	}
//
//	@Override
//	public void onBalanceURLRequestTimeout() {
//		reqBalanceServer();
//	}
//
//	private class BalanceTask extends Thread {
//		public static final int REQ_BALANCE_URL = 0;
//		public static final int FETCH_BALANCE = 1;
//
//		private int taskType = -1;
//
//		public BalanceTask(int param) {
//			taskType = param;
//		}
//
//		@Override
//		public void run() {
//			String result = null;
//
//			switch (taskType) {
//			case REQ_BALANCE_URL:
//				CksNativeLibrary.getInstance().RequestBalanceUrl();
//
//				break;
//
//			case FETCH_BALANCE:
//				result = fetchBalance();
//				break;
//
//			default:
//				break;
//			}
//
//			if(result != null) {
//				try {
//					Matcher matcher = Pattern.compile( "[-+]?\\d*\\.?\\d+([eE][-+]?\\d+)?" ).matcher(result);
//					if(matcher.find()) {
//						result = matcher.group();
//					}
//
//					double balance = Double.parseDouble(result);
//					DecimalFormat df = new DecimalFormat("#.00");
//					result = df.format(balance);
//
//					CksNativeLibrary.getInstance().receivedBalance(result);
//				}
//				catch(Exception e) {
//					Log.e("Balance Parse", "Error");
//				}
//				result = null;
//			}
//		}
//
//		public String fetchBalance() {
//			Context context = CksTabService.getContext();
//			String url = PreferenceSetting.getString(context, DialerSettings.DIALER_BALANCE_URL, DialerSettings.DEFAULT_DIALER_BALANCE_URL);
//			String username = PreferenceSetting.getString(context, DialerSettings.PREF_USERNAME, "");
//
//			if(null != url && url.contains("<pin>") && !username.equals("")) {
//				url = url.replace("<pin>", username);
//
//	            try {
//	            	DefaultHttpClient httpClient = new DefaultHttpClient();
//	                HttpGet httpGet = new HttpGet(url);
//
//	            	HttpResponse httpResponse = httpClient.execute(httpGet);
//	            	final String response = EntityUtils.toString(httpResponse.getEntity());
//
//	            	if(response != null)
//	            		return response;
//	            }
//	            catch(Exception e) {}
//			}
//			return null;
//		}
//	}
//
//}
