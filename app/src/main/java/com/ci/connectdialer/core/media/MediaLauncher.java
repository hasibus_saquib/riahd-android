package com.ci.connectdialer.core.media;

/** Interface for classes that start media application such as for audio or video */
public interface MediaLauncher {
	/** Starts media application */
	public boolean startMedia(int dir);

	/** Stops media application */
	public boolean stopMedia();
	
	public boolean muteMedia();
	public boolean speaker(boolean speakerOn);
//	public void bluetoothMedia();

	public boolean sendDTMF(char c);
	
	public boolean isRecorderRunning();
	
	public boolean isPlayerRunning();
}