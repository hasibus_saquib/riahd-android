package com.ci.connectdialer.core.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.util.Log;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.auth.AuthenticationManager;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.signal.CkslibraryEnum;

public class Receiver extends BroadcastReceiver {

	public final static String ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE";
	public final static String ACTION_DATA_STATE_CHANGED = "android.intent.action.ANY_DATA_STATE";
	public final static String ACTION_SCO_AUDIO_STATE_CHANGED = "android.media.SCO_AUDIO_STATE_CHANGED";
	public final static String EXTRA_SCO_AUDIO_STATE = "android.media.extra.SCO_AUDIO_STATE";
	
	public static StateManager mStateManager;

	public static synchronized void unregister() {
		if (mStateManager != null) {
			mStateManager.disconnect();
			mStateManager = null;
		}
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {

		try {
			mStateManager = getStateManager();

			String intentAction = intent.getAction();

			if (intentAction.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
				boolean mNoConnection = !PhoneInformation.isNetworkAvailable(CksTabService.getContext());
				
				if (!mNoConnection && null != mStateManager && !StateManager.mInCall
						&& (StateManager.mState != null && StateManager.mState != CkslibraryEnum.EAuthenticating
						&& StateManager.mState != CkslibraryEnum.ERegistering
						&& StateManager.mState != CkslibraryEnum.ERegistered)) {
					if (context != null) {
						if (PreferenceSetting.getBool(context, DialerSettings.KEY_RECEIVER_ENABLED, false)) {
							mStateManager.connect();
						} else {
							AuthenticationManager.getAuthenticationListener().afterAuthenticationInit();
							PreferenceSetting.putBool(context, DialerSettings.KEY_RECEIVER_ENABLED, true);
						}
					} else {
						mStateManager.connect();
					}
				} else {
					mStateManager.connectionLost();
					AuthenticationManager.getAuthenticationListener().afterAuthenticationInit();
				}
			} else if (intentAction.equals(ACTION_DATA_STATE_CHANGED)) {
			} else if (intentAction.equals(ACTION_PHONE_STATE_CHANGED) &&
					!intent.getBooleanExtra(ApplicationValidator.APPLICTION_NAME, false)) {
				String phnState = intent.getStringExtra("state");
				if (phnState.equals("RINGING")) {
					if (StateManager.mInCall)
						CksNativeLibrary.getInstance().SetHold(true); // Do HOLD true
				} else if (phnState.equals("IDLE")) {
					if (StateManager.mInCall) {
						CksNativeLibrary.getInstance().SetHold(false); // Do HOLD false
					}
				} else if (phnState.equals("OFFHOOK")) {
					if (StateManager.mInCall)
						CksNativeLibrary.getInstance().CancelCall(); // Cancel CALL
				}
			} else if (intent.getAction().equals(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED)) {
				int mBtooth = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, -1);

				switch (mBtooth) {
					case AudioManager.SCO_AUDIO_STATE_CONNECTED:
						//startBtooth();
						break;
					case AudioManager.SCO_AUDIO_STATE_DISCONNECTED:
						//stopBtooth();
						break;

					default:
						break;
				}
			}
		} catch (Exception ex){
			Log.e("onReceive", ex.toString());
		}
	}
	
	public static synchronized StateManager getStateManager() {
		if (mStateManager == null) {
			mStateManager = new StateManager();
		}
		return mStateManager;
	}

}
