package com.ci.connectdialer.core.ckslib;

public interface AuthResponseListener {
	
	void onReady();
	
	void UseCachedAuthSettings();
	
	void StoreServerAddress(byte[] pAuthIps, int iNumberOfServerIp);
	
	void reAuthenticate();
	
	void authenticationTimeOut();
	
	void authenticationVoid();

	void UpdateDialerInformation();

	void usingNewAuthSettings(int iNumberOfAuthIp, byte[] pAuthIpList);

	void ReRegister();

	void StoreReportAddress(byte[] pReportIps, int iNumberOfReportIp);

	void RegistrationCompleted(byte[] pAuthIpList, int iNumberOfAuthIp, String mcc, String mnc);
}
