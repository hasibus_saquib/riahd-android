package com.ci.connectdialer.core.utilities;

/**
 * Created by Erfan on 3/27/17.
 */

public interface DialpadVisibilityInterface {
    void SetDialpadVisibilityFromInterface(int visibility);
    void AnimateDialpadOnY(float distanceY);
}

