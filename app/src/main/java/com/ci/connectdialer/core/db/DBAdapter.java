package com.ci.connectdialer.core.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.CallLog;
import android.provider.CallLog.Calls;

import java.util.List;
import java.util.Vector;

public class DBAdapter {

	private static final String DATABASE_NAME = "com.cks.tab";
	private static final String TABLE = "calllog";
	
	private static final int DATABASE_VERSION = 3;
	private final static String TABLE_CALLLOGS_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE + " (" + CallLog.Calls._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + CallLog.Calls.CACHED_NAME + " TEXT," + CallLog.Calls.DATE + " INTEGER," + CallLog.Calls.NUMBER + " TEXT," + CallLog.Calls.TYPE + " INTEGER," + CallLog.Calls.DURATION + " INTEGER, " + "contact_id LONG);";
	private final static String TABLE_CALLLOGS_UPCATE = "ALTER TABLE "+ TABLE + " ADD COLUMN contact_id LONG";
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase db;

	private boolean opened = false;

	public DBAdapter(Context context) {
		if(context != null){
			databaseHelper = new DatabaseHelper(context);
		}
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);

		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(TABLE_CALLLOGS_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(TABLE_CALLLOGS_UPCATE);
		}
	}

	public DBAdapter open() throws SQLException {
		if(databaseHelper != null){
			db = databaseHelper.getWritableDatabase();
			opened = true;
		} else {
			db = null;
			opened = false;
		}
		return this;
	}

	public void close() {
		databaseHelper.close();
		opened = false;
	}

	public boolean isOpen() {
		return opened;
	}

	public static final int ID_COLUMN_INDEX = 0;
	public static final int CALLER_NAME_COLUMN_INDEX = 1;
	public static final int DATE_COLUMN_INDEX = 2;
	public static final int NUMBER_COLUMN_INDEX = 3;
	public static final int CALL_TYPE_COLUMN_INDEX = 4;

	public void addCallInfoToDB(CallInformation callInfo) {

		if(db == null || callInfo == null){
			return;
		}
		ContentValues cv = new ContentValues();
		try {
			cv.put(Calls.CACHED_NAME, callInfo.getName());
			cv.put(Calls.NUMBER, callInfo.getPhoneNo());
			cv.put(Calls.TYPE, callInfo.getState());
			cv.put(Calls.DATE, callInfo.getTime());
			cv.put(Calls.DURATION, callInfo.getDuration());
			cv.put("contact_id", callInfo.getContactID());
		} catch (Exception e) {
			e.printStackTrace();
		}
		db.insert(TABLE, null, cv);
	}

    public Vector<CallInformation> getCallList() {
    	if(db == null){
			return null;
		}
		Vector<CallInformation> callInfoVector = new Vector<CallInformation>();
		CallInformation callInfo;
		Cursor contactCursor;
		String sql = "SELECT * FROM "+TABLE+" ORDER BY "+ CallLog.Calls.DEFAULT_SORT_ORDER;
		int count = 0;
		contactCursor = db.rawQuery(sql, null);
		if (contactCursor.moveToFirst()) {
			do {
				callInfo = new CallInformation();
				try {
					callInfo.setId(contactCursor.getLong(contactCursor.getColumnIndex(Calls._ID)));
					callInfo.setName(contactCursor.getString(contactCursor.getColumnIndex(Calls.CACHED_NAME)));
					callInfo.setPhoneNo(contactCursor.getString(contactCursor.getColumnIndex(Calls.NUMBER)));
					callInfo.setTime(contactCursor.getLong(contactCursor.getColumnIndex(Calls.DATE)));
					callInfo.setState(contactCursor.getInt(contactCursor.getColumnIndex(Calls.TYPE)));
					callInfo.setDuration(contactCursor.getInt(contactCursor.getColumnIndex(Calls.DURATION)));
					callInfo.setContactID(contactCursor.getLong(contactCursor.getColumnIndex("contact_id")));
				} catch (Exception e) {
					e.printStackTrace();
					callInfo.setContactID(-1);
				}
				callInfoVector.add(callInfo);
				count++;

				if(count==50)
					break;
			} while (contactCursor.moveToNext());
		}
		contactCursor.close();
		return callInfoVector;
	}

	public Vector<CallInformation> getDialCallList() {
		if(db == null){
			return null;
		}
		Vector<CallInformation> callInfoVector = new Vector<CallInformation>();
		CallInformation callInfo;
		Cursor contactCursor;
		String sql = "SELECT * FROM "+TABLE+" WHERE "+ CallLog.Calls.DURATION+" > 0 AND "+ CallLog.Calls.TYPE+" == 2 ORDER BY "+ CallLog.Calls.DEFAULT_SORT_ORDER;
		contactCursor = db.rawQuery(sql, null);
		int count = 0;
		if (contactCursor.moveToFirst()) {
			do {
				if(contactCursor.getInt(contactCursor.getColumnIndex(CallLog.Calls.TYPE)) == Calls.OUTGOING_TYPE  ){
					callInfo = new CallInformation();
					try {
						callInfo.setState(contactCursor.getInt(contactCursor.getColumnIndex(Calls.TYPE)));
						callInfo.setId(contactCursor.getLong(contactCursor.getColumnIndex(Calls._ID)));
						callInfo.setName(contactCursor.getString(contactCursor.getColumnIndex(Calls.CACHED_NAME)));
						callInfo.setPhoneNo(contactCursor.getString(contactCursor.getColumnIndex(Calls.NUMBER)));
						callInfo.setTime(contactCursor.getLong(contactCursor.getColumnIndex(Calls.DATE)));
						callInfo.setDuration(contactCursor.getInt(contactCursor.getColumnIndex(Calls.DURATION)));
						callInfo.setContactID(contactCursor.getLong(contactCursor.getColumnIndex("contact_id")));
					} catch (Exception e) {
						e.printStackTrace();
						callInfo.setContactID(-1);
					}
					callInfoVector.add(callInfo);
					count++;

					if(count==50)
						break;
				}
			} while (contactCursor.moveToNext());
		}
		contactCursor.close();
		return callInfoVector;
	}

	public Vector<CallInformation> getMissedCallList() {
		if(db == null){
			return null;
		}
		Vector<CallInformation> callInfoVector = new Vector<CallInformation>();
		CallInformation callInfo;
		Cursor contactCursor;
		String sql = "SELECT * FROM "+TABLE+" WHERE "+ CallLog.Calls.TYPE+" == 3 ORDER BY "+ CallLog.Calls.DEFAULT_SORT_ORDER;
		contactCursor = db.rawQuery(sql, null);
		int count = 0;
		if (contactCursor.moveToFirst()) {
			do {
				if(contactCursor.getInt(contactCursor.getColumnIndex(CallLog.Calls.TYPE)) == Calls.MISSED_TYPE  ){
					callInfo = new CallInformation();
					try {
						callInfo.setState(contactCursor.getInt(contactCursor.getColumnIndex(Calls.TYPE)));
						callInfo.setId(contactCursor.getLong(contactCursor.getColumnIndex(Calls._ID)));
						callInfo.setName(contactCursor.getString(contactCursor.getColumnIndex(Calls.CACHED_NAME)));
						callInfo.setPhoneNo(contactCursor.getString(contactCursor.getColumnIndex(Calls.NUMBER)));
						callInfo.setTime(contactCursor.getLong(contactCursor.getColumnIndex(Calls.DATE)));
						callInfo.setDuration(contactCursor.getInt(contactCursor.getColumnIndex(Calls.DURATION)));
						callInfo.setContactID(contactCursor.getLong(contactCursor.getColumnIndex("contact_id")));
					} catch (Exception e) {
						e.printStackTrace();
						callInfo.setContactID(-1);
					}
					callInfoVector.add(callInfo);
					count++;

					if(count==50)
						break;
				}
			} while (contactCursor.moveToNext());
		}
		contactCursor.close();
		return callInfoVector;
	}

	public CallInformation[] getFrequentCalls(int limit) {
		if(db == null){
			return null;
		}
		CallInformation[] callInfos = new CallInformation[limit];

		String sql = "SELECT * FROM "+TABLE+" WHERE "+ Calls.NUMBER+" != '' GROUP BY "
				+ Calls.NUMBER+" ORDER BY COUNT(*) desc limit "+ limit;

		int count = 0;
		Cursor contactCursor = db.rawQuery(sql, null);
		if (contactCursor.moveToFirst()) {
			do {
				CallInformation callInfo = new CallInformation();
				try {
					callInfo.setState(contactCursor.getInt(contactCursor.getColumnIndex(Calls.TYPE)));
					callInfo.setId(contactCursor.getLong(contactCursor.getColumnIndex(Calls._ID)));
					callInfo.setName(contactCursor.getString(contactCursor.getColumnIndex(Calls.CACHED_NAME)));
					callInfo.setPhoneNo(contactCursor.getString(contactCursor.getColumnIndex(Calls.NUMBER)));
					callInfo.setTime(contactCursor.getLong(contactCursor.getColumnIndex(Calls.DATE)));
					callInfo.setDuration(contactCursor.getInt(contactCursor.getColumnIndex(Calls.DURATION)));
					callInfo.setContactID(contactCursor.getLong(contactCursor.getColumnIndex("contact_id")));
				} catch (Exception e) {
					e.printStackTrace();
					callInfo.setContactID(-1);
				}
				callInfos[count++] = callInfo;
			} while (contactCursor.moveToNext() && count <= limit );
		}
		contactCursor.close();
		return callInfos;
	}

	public void removeCallLogExpiredEntries() {
		if(db == null){
			return;
		}
		db.delete(TABLE, CallLog.Calls._ID + " IN " + "(SELECT " + CallLog.Calls._ID + " FROM " + TABLE + " ORDER BY " + CallLog.Calls.DEFAULT_SORT_ORDER + " LIMIT -1 OFFSET 500)", null);
	}

	public void removeAllCallLogEntries() {
		if(db == null){
			return;
		}
		db.delete(TABLE, null, null);
	}

	public void removeDialedCallLogEntries() {
		if(db == null){
			return;
		}
		db.delete(TABLE, CallLog.Calls._ID + " IN " + "(SELECT " + CallLog.Calls._ID + " FROM " + TABLE + " WHERE "+ CallLog.Calls.TYPE+" == 2 AND "+ CallLog.Calls.DURATION+" > 0 "+"ORDER BY " + CallLog.Calls.DEFAULT_SORT_ORDER + " )", null);
	}

	public void removeReceivedCallLogEntries() {
		if(db == null){
			return;
		}
		db.delete(TABLE, CallLog.Calls._ID + " IN " + "(SELECT " + CallLog.Calls._ID + " FROM " + TABLE + " WHERE "+ CallLog.Calls.TYPE+" == 1 ORDER BY " + CallLog.Calls.DEFAULT_SORT_ORDER + " )", null);
	}

	public void removeMissedCallLogEntries() {
		if(db == null){
			return;
		}
		db.delete(TABLE, CallLog.Calls._ID + " IN " + "(SELECT " + CallLog.Calls._ID + " FROM " + TABLE + " WHERE "+ CallLog.Calls.TYPE+" == 3 ORDER BY " + CallLog.Calls.DEFAULT_SORT_ORDER + " )", null);
	}

	public void removeSelectedCallLogEntries(List<Long> idList) {
		if(db == null || idList == null || idList.size() == 1){
			return;
		}

		String ids = idList.toString().replace('[', ' ').replace(']', ' ');
		db.delete(TABLE, CallLog.Calls._ID + " IN " + "("+ ids +")", null);
	}
}
