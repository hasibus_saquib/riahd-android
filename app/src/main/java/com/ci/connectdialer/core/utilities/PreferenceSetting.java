package com.ci.connectdialer.core.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

/**
 * The Class PreferenceSetting.
 */
public class PreferenceSetting {

	/**
	 * Gets the string.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param defValue
	 *            the def value
	 * @return the string
	 */
	public static String getString(Context context, String key, String defValue) {
		if(context == null){
			return defValue;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getString(key, defValue);
	}

	/**
	 * Gets the int.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param defValue
	 *            the def value
	 * @return the int
	 */
	public static int getInt(Context context, String key, int defValue) {
		if(context == null){
			return defValue;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getInt(key, defValue);
	}

	/**
	 * Checks if is key exist.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @return true, if is key exist
	 */
	public static boolean isKeyExist(Context context, String key) {
		if(context == null){
			return false;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.contains(key);
	}

	/**
	 * Put string.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void putString(Context context, String key, String value) {
		if(context == null){
			return;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putString(key, value);
		prefsEditor.commit();
	}

	/**
	 * Put int.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void putInt(Context context, String key, int value) {
		if(context == null){
			return;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putInt(key, value);
		prefsEditor.commit();
	}

	/**
	 * Put bool.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void putBool(Context context, String key, boolean value) {
		if(context == null){
			return;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(key, value);
		prefsEditor.commit();
	}

	/**
	 * Gets the bool.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param defValue
	 *            the def value
	 * @return the bool
	 */
	public static boolean getBool(Context context, String key, boolean defValue) {
		if(context == null){
			return defValue;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(key, defValue);
	}

	/**
	 * Put long.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void putLong(Context context, String key, long value) {
		if(context == null){
			return;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putLong(key, value);
		prefsEditor.commit();
	}

	/**
	 * Gets the long.
	 * 
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param defValue
	 *            the def value
	 * @return the long
	 */
	public static long getLong(Context context, String key, long defValue) {
		if(context == null){
			return defValue;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getLong(key, defValue);
	}

	/**
	 * Put String to the String Set
	 *
	 * @param context
	 *            the context
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void addStringToSet(Context context, String key, String value) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();

		Set<String> valueSet = prefs.getStringSet(key, null);
		Set<String> newKeySet = new HashSet<>();
		if( valueSet!=null ) {
			newKeySet.addAll(valueSet);
		}
		newKeySet.add(value);

		editor.putStringSet(key, newKeySet);
		editor.commit();
	}

	public static void removeStringFromSet(Context context, String key, String value) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();

		Set<String> valueSet = prefs.getStringSet(key, null);
		Set<String> newKeySet = new HashSet<>();
		if( valueSet!=null ) {
			newKeySet.addAll(valueSet);
		}
		newKeySet.remove(value);

		editor.putStringSet(key, newKeySet);
		editor.commit();
	}

	public static Set<String> getStringSet(Context context, String key, Set<String> defValues){
		if( context == null){
			return defValues;
		}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getStringSet(key, defValues);
	}


}
