package com.ci.connectdialer.core.auth;

import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.ckslib.AuthResponseListener;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.CksUtil;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateManager;
import com.signal.CkslibraryEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthenticationManager implements AuthResponseListener,
		AsyncTasks.DnsDomainResolvTask.DnsDomainResolveListener,
		AsyncTasks.DomainFrontingTask.DomainFrontingListener
{

	private static AuthenticationListener mAuthenticationListener;
	public static void setAuthenticationListener( AuthenticationListener pListener){
		mAuthenticationListener = pListener;
	}
	public static AuthenticationListener getAuthenticationListener(){
		return mAuthenticationListener;
	}
	public interface AuthenticationListener{
		void afterAuthenticationInit();
		void showMccInvalidDialog();
	}


	private CksNativeLibrary mCksNative;
	private int mOpcode;
	public static String mCountryCode = "0";
	public static String mTeleOperatorCode = "0";
	private boolean mReAuth = false;
	private Thread mReAuthThread = null;
	private int mNumberOfAuthIp = 0;
	private boolean bDnsDomainResolved = false;

	public AuthenticationManager(CksNativeLibrary nativeLib) {
		mCksNative = nativeLib;
		if (mCksNative != null) {
			mCksNative.addAuthResponseListener(this);
		}
		mNumberOfAuthIp = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_OF_AUTH_IP, 0);

		if( mNumberOfAuthIp > 0 ) bDnsDomainResolved = true;
		else bDnsDomainResolved = false;
	}

	private DialerSettings.ConnectivityType connectivityType = DialerSettings.ConnectivityType.Connectivity_None;

	private boolean DOMAIN_FRONTING_CHECKED = false;
	public static boolean AUTHENTICATION_HAPPENED = false;

	public static boolean AUTHENTICATION_BY_HTTPS = false;

	public void detectNetwork() {
		if (!DialerSettings.NETWORK_DETECTED) {
			mCountryCode = "0";
			mTeleOperatorCode = "0";
			Log.d("Authentication", "detecting network");
			mCksNative.UpdateDialerState(CkslibraryEnum.EAuthenticating.getStatus(), "");
			connectivityType = AsyncTasks.httpsConnectionCheckSync();
			Log.i("Authentication", "Connectivity Type: "+ connectivityType.toString());
			DialerSettings.NETWORK_DETECTED = true;
		}
	}

	public void resolveAuthDomain() {
		if (!bDnsDomainResolved && connectivityType == DialerSettings.ConnectivityType.Connectivity_Full) {
			mCksNative.UpdateDialerState(CkslibraryEnum.EAuthenticating.getStatus(), "");
			List<String> iplist = AsyncTasks.getAuthIpListFromDomains();
			if (iplist.size() > 0) {
				parseDnsResponse(iplist);
			}
		}
	}

	private boolean initAuthentication() {

		mOpcode = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_OPCODE, 0);
		int cachedOpcode = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, 0);

		if ( StateManager.TEST_MCC_MNC_ENABLED) {

			mCountryCode = StateManager.COUNTRY_CODE;
			mTeleOperatorCode = StateManager.TELE_OPERATOR_CODE;
			AUTHENTICATION_HAPPENED = false;
		} else {
			mCountryCode = PhoneInformation.getCountryCode(CksTabService.getContext());
			mTeleOperatorCode = PhoneInformation.getTeleOperatorCode(CksTabService.getContext());
		}

		if (mCountryCode.equalsIgnoreCase("424") && DialerSettings.COUNTRY_RESTRICTION_ON)
        {
			DialerSettings.COUNTRY_RESTRICTION_ON = false;
			if( mAuthenticationListener!=null){
				mAuthenticationListener.showMccInvalidDialog();
			}
        }

		Log.d("Authentication", "final mcc mnc: " + mCountryCode + ", " + mTeleOperatorCode);

		if( (mReAuth || mOpcode != cachedOpcode) && !DOMAIN_FRONTING_CHECKED && connectivityType == DialerSettings.ConnectivityType.Connectivity_Full
				&& (!StateManager.CUSTOM_AUTH_ENABLED && !StateManager.CUSTOM_DNS_ENABLED)) {

			AUTHENTICATION_BY_HTTPS = false;
			mCksNative.UpdateDialerState(CkslibraryEnum.EAuthenticating.getStatus(), "");

			int parentOpcode = DialerSettings.PLATINUM_OPCODE;
			if (!DialerSettings.SHOULD_SET_PLATINUM_OPCODE) parentOpcode = 0;
			String authProtocolVersion = mCksNative.GetAuthProtocolVersion();
			int vpnProtocolVersion = mCksNative.GetVpnProtocolVersion();
			int os = Integer.parseInt(PhoneInformation.DIALER_TYPE);

			JSONObject jsonOb = new JSONObject();
			try {
				jsonOb.put("opc", mOpcode);
				jsonOb.put("popc", parentOpcode);
				jsonOb.put("apv", authProtocolVersion);
				jsonOb.put("vpv", vpnProtocolVersion);
				jsonOb.put("mcc", mCountryCode);
				jsonOb.put("mnc", mTeleOperatorCode);
				jsonOb.put("os", os);

			} catch (JSONException e) {
				Log.e("Authentication", e.toString());
			}
			parseHttAuthResponse(AsyncTasks.doAuthenticationByDomainFronting(jsonOb.toString()));
		}

		if( mAuthenticationListener!=null){
			mAuthenticationListener.afterAuthenticationInit();
		}
		return true;
	}

	private void setCredentials(){
//		int copcode = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, 0);
//		long lastupdate = PreferenceSetting.getLong(CksTabService.getContext(),
//				DialerSettings.LAST_AUTHENTICATION_TIME, 0L);
//		if (mReAuth) {
//			copcode = 0;
//			lastupdate = 0L;
//			mReAuth = false;
//		} else {
//			int iNumberOfVpnIp = PreferenceSetting.getInt(CksTabService.getContext(),
//					DialerSettings.PREF_NUMBER_VPN_IP, 0);
//				if (iNumberOfVpnIp <= 0) {
//					copcode = 0;
//					lastupdate = 0L;
//				}
//		}

		int copcode = 0;
		long lastupdate = 0L;
		int iNumberOfVpnIp = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_VPN_IP, 0);

		if( !mReAuth && iNumberOfVpnIp > 0 ) {

			copcode = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, 0);
			lastupdate = PreferenceSetting.getLong(CksTabService.getContext(),
					DialerSettings.LAST_AUTHENTICATION_TIME, 0L);
		}
		mReAuth = false;

		String username = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_USERNAME, "");
		String password = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_PASSWORD, "");
		String phone = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_PHONENO, "");

		if (username == null || username.length() == 0 || password == null || password.length() == 0) {
			return;
		}

		int[] vrsn = PhoneInformation.getVersion(CksTabService.getContext());
		int iSuccessProtocol = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_SUCCESS_PROTOCOL, 0);

		copcode = StateManager.CUSTOM_DNS_ENABLED ? 0 : copcode ;

		String imeiAndPackageName = PhoneInformation.getIMEI(CksTabService.getContext())+"-"+CksTabService.getContext().getPackageName();

		mCksNative.setCredentials(DialerSettings.DIALER_ID,
				PhoneInformation.DIALER_TYPE, Build.VERSION.RELEASE,
				Build.MODEL, imeiAndPackageName,
				mCountryCode, mTeleOperatorCode, //"424", "02",
				mOpcode, username, password, phone, copcode, lastupdate,
				vrsn[0], vrsn[1],
				PhoneInformation.getAppVersionWithComponentVersion(CksTabService.getContext()),
				connectivityType.ordinal(), AUTHENTICATION_HAPPENED, iSuccessProtocol, StateManager.TEST_MCC_MNC_ENABLED,
				false);
		if (DialerSettings.SHOULD_SET_PLATINUM_OPCODE) {
			mCksNative.setPlatinumOpcode(DialerSettings.PLATINUM_OPCODE);
		}
	}

	private void setAddresses(){
		int numberOfAuthIp = mNumberOfAuthIp;
		//		int iNumberOfAuthServerIps = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_OF_AUTH_IP, 0);
		byte[] listOfAuthIp = new byte[0];

		if (StateManager.CUSTOM_AUTH_ENABLED) {

			String authIp1 = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.CUSTOM_AUTH_IP, "");
			String[] ipList = { authIp1 };
			if (authIp1 != null && authIp1.length() > 0) {
				listOfAuthIp = CksUtil.getByteArraryForIp(ipList);
				numberOfAuthIp = 1;
			}
		}
		else {
			String authIpListString = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_AUTH_IP_LIST, "");
			if (numberOfAuthIp > 0 && authIpListString != null && authIpListString.length() > 0) {
				listOfAuthIp = Base64.decode(authIpListString, Base64.DEFAULT);
			}
		}

		int numberOfDnsIp = 0;
		byte[] dnsIpList = new byte[0];

		if( StateManager.CUSTOM_DNS_ENABLED) {
			numberOfAuthIp = 0;

			String dnsIp = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.CUSTOM_DNS_IP, "");
			String[] ipList = { dnsIp };
			if (dnsIp != null && dnsIp.length() > 0) {
				dnsIpList = CksUtil.getByteArraryForIp(ipList);
				numberOfDnsIp = 1;
			}
		}
		else {
			numberOfDnsIp = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_DNS_IP, ApplicationValidator.DNS_IP_LIST.length);
			String dnsIpListString = PreferenceSetting.getString(CksTabService.getContext(),  DialerSettings.PREF_DNS_IP_LIST, "");
			boolean bVersionMatched = PhoneInformation.getAppVersion(CksTabService.getContext()).equals(PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.PREF_DNS_SAVED_FOR_VERSION, ""));
			if (dnsIpListString != null && dnsIpListString.length() > 0 && bVersionMatched) {
				dnsIpList = Base64.decode(dnsIpListString, Base64.DEFAULT);
			} else {
				numberOfDnsIp = ApplicationValidator.DNS_IP_LIST.length;
				dnsIpList = CksUtil.getByteArraryForDomainList();
			}
		}



		int numberOfReportIp = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_REPORT_IP, 0);
		byte[] reportIpList = new byte[0];
		String reportIpListString = PreferenceSetting.getString(CksTabService.getContext(),  DialerSettings.PREF_REPORT_IP_LIST, "");
		if (reportIpListString != null && reportIpListString.length() > 0) {
			reportIpList = Base64.decode(reportIpListString, Base64.DEFAULT);
		}

		if(StateManager.CACHE_CLEARED) {
			numberOfAuthIp = 0;
			StateManager.CACHE_CLEARED = false;
		}

		mCksNative.SetAddresses(numberOfAuthIp, listOfAuthIp, numberOfDnsIp, dnsIpList, numberOfReportIp, reportIpList);
		//mCksNative.SetAddresses(mNumberOfAuthIp, listOfAuthIp, 0, dnsIpList, numberOfReportIp, reportIpList);
	}

	private void startAuthentication () {

		detectNetwork();

		resolveAuthDomain();

		initAuthentication();

		setCredentials();

		setAddresses();

		mCksNative.connectAuthServer();

	}

	private void start(final int sleeptime) {
		if(mCksNative == null) {
			return;
		}

		stopReconnectThread();

		if (!PhoneInformation.isNetworkAvailable(CksTabService.getContext())) {
			mCksNative.UpdateDialerState(CkslibraryEnum.EConnectionError.getStatus(), "You are offline!");
			return;
		}

		if( !DialerSettings.isSettingsSaved(CksTabService.getContext())){
			return;
		}

		mReAuthThread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					if (sleeptime > 0) {
						Thread.sleep(sleeptime);
					}
					startAuthentication();
				} catch (Exception e) {
					Log.e("",e.toString());
				}
			}
		});
		mReAuthThread.start();

//		detectNetwork();
//
//		if(!initAuthentication()){
//			return;
//		}
//
//		setCredentials();
//
//		setAddresses();
//
//		boolean reportSettings = PreferenceSetting.getBool(CksTabService.getContext(),
//				DialerSettings.PREF_REPORT_SETTINGS, true);
//		mCksNative.setReportSettings(reportSettings);
//
//		mCksNative.connectAuthServer();
//
//		if(CksTabService.getContext() != null){
//			if (PhoneInformation.isNetworkAvailable(CksTabService.getContext())) {
//				String bugDetails = PreferenceSetting.getString(CksTabService.getContext(),
//						DialerSettings.KEY_BUG_DETAILS, "");
//				if (bugDetails != null && bugDetails.length() > 0) {
//					PreferenceSetting.putString(CksTabService.getContext(),	DialerSettings.KEY_BUG_DETAILS, "");
//					mCksNative.ReportException(bugDetails);
//				}
//			}
//		}
	}

	public void stop() {
		if (mCksNative != null) {
			mCksNative.addAuthResponseListener(null);
		}
	}

	public static boolean validateIpAddress(String ip){

		String IPADDRESS_PATTERN =
				"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
		Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
		Matcher matcher = pattern.matcher(ip);
		return matcher.matches();
	}

	public void createReAuthenticationTask() {
		mReAuth = true;
		DOMAIN_FRONTING_CHECKED = false;
		start(4000);
//		if (mReAuthThread != null) {
//			mReAuthThread.interrupt();
//			mReAuthThread = null;
//		}
//		mReAuthThread = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					mReAuth = true;
//					DOMAIN_FRONTING_CHECKED = false;
//					Thread.sleep(4000);
//					start();
//				} catch (Exception e) {
//					Log.e("",e.toString());
//				}
//			}
//		});
//		mReAuthThread.start();
	}

	public void connect() {
		stopReconnectThread();
		start(0);
	}

	public void reconnect() {
		mNumberOfAuthIp = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_OF_AUTH_IP, 0);
		bDnsDomainResolved = mNumberOfAuthIp > 0;
		mReAuth = true;
		DOMAIN_FRONTING_CHECKED = false;
		start(0);
	}

	private void stopReconnectThread(){
		try {
			if (mReAuthThread != null) {
				mReAuthThread.interrupt();
				mReAuthThread = null;
			}
		} catch (Exception e) {
			Log.e("",e.toString());
		}
	}

	// *********  AuthResponseListener implemented methods start  ************ //

	@Override
	public void onReady() {
	}

	@Override
	public void UseCachedAuthSettings() {
		if (mCksNative == null) {
			return;
		}
		int iNumberOfVpnIp = PreferenceSetting.getInt(
				CksTabService.getContext(), DialerSettings.PREF_NUMBER_VPN_IP, 0);
		String vpnIpListString = PreferenceSetting.getString(
				CksTabService.getContext(), DialerSettings.PREF_VPN_IP_LIST, "");
		byte[] vpnIpList = Base64.decode(vpnIpListString, Base64.DEFAULT);

		// UDP PORT LIST, NUMBER OF PORT
		int iNumberOfVpnPortUdp = PreferenceSetting.getInt(
				CksTabService.getContext(), DialerSettings.PREF_NUMBER_VPN_PORT_UDP, 0);
		String vpnPortListStringUdp = PreferenceSetting.getString(
				CksTabService.getContext(), DialerSettings.PREF_VPN_PORT_LIST_UDP, "");
		byte[] vpnPortListUdp = Base64.decode(vpnPortListStringUdp, Base64.DEFAULT);

		// TCP PORT LIST, NUMBER OF PORT
		int iNumberOfVpnPortTcp = PreferenceSetting.getInt(
				CksTabService.getContext(), DialerSettings.PREF_NUMBER_VPN_PORT_TCP, 0);
		String vpnPortListStringTcp = PreferenceSetting.getString(
				CksTabService.getContext(), DialerSettings.PREF_VPN_PORT_LIST_TCP, "");
		byte[] vpnPortListTcp = Base64.decode(vpnPortListStringTcp, Base64.DEFAULT);

		// TLS PORT LIST, NUMBER OF PORT
		int iNumberOfVpnPortTls = PreferenceSetting.getInt(
				CksTabService.getContext(), DialerSettings.PREF_NUMBER_VPN_PORT_TLS, 0);
		String vpnPortListStringTls = PreferenceSetting.getString(
				CksTabService.getContext(), DialerSettings.PREF_VPN_PORT_LIST_TLS, "");
		byte[] vpnPortListTls = Base64.decode(vpnPortListStringTls, Base64.DEFAULT);


		boolean bQuickRegistration = PreferenceSetting.getBool(CksTabService.getContext(), DialerSettings.PREF_QUICK_REQ, false);
		boolean useVpn = PreferenceSetting.getBool(CksTabService.getContext(), DialerSettings.PREF_USE_VPN, false);
		boolean usePhoneNo = PreferenceSetting.getBool(
				CksTabService.getContext(), DialerSettings.PREF_USE_PHONE_NO, false);
		boolean allowSipIncoming = PreferenceSetting.getBool(
				CksTabService.getContext(), DialerSettings.PREF_ALLOW_SIP_INCOMING, false);

		mCksNative.setCachedAttributes(
				iNumberOfVpnIp, vpnIpList, iNumberOfVpnPortUdp, vpnPortListUdp,
				iNumberOfVpnPortTcp, vpnPortListTcp, iNumberOfVpnPortTls, vpnPortListTls,
				bQuickRegistration, useVpn, allowSipIncoming, usePhoneNo);
	}

	@Override
	public void StoreServerAddress(byte[] pAuthIps, int iNumberOfAuthIp) {

		PreferenceSetting.putInt(CksTabService.getContext(),
				DialerSettings.PREF_NUMBER_OF_AUTH_IP, iNumberOfAuthIp);
		mNumberOfAuthIp = iNumberOfAuthIp;
		String authIpList = Base64.encodeToString(pAuthIps, Base64.DEFAULT);
		PreferenceSetting.putString(CksTabService.getContext(),	DialerSettings.PREF_AUTH_IP_LIST, authIpList);

		if (iNumberOfAuthIp > 0	&& (pAuthIps != null && pAuthIps.length > 0)) {
			PreferenceSetting.putBool(CksTabService.getContext(), DialerSettings.PREF_AUTH_DOMAIN_RESOLVED, true);
			mCksNative.SetAuthDomainResolved(true);
		}
	}

	@Override
	public void reAuthenticate() {
		createReAuthenticationTask();
	}

	@Override
	public void authenticationTimeOut() {

		mNumberOfAuthIp = 0;
		bDnsDomainResolved = false;
		createReAuthenticationTask();
	}

	@Override
	public void authenticationVoid() {

		mNumberOfAuthIp = 0;
		bDnsDomainResolved = false;
		createReAuthenticationTask();
	}

	@Override
	public void UpdateDialerInformation() {

		mCksNative.UpdateDialerState(CkslibraryEnum.ERegistered.getStatus(), "");
	}

	@Override
	public void usingNewAuthSettings(int iNumberOfAuthIp, byte[] pAuthIpList) {

		PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, mOpcode);
		mNumberOfAuthIp = iNumberOfAuthIp;
		AUTHENTICATION_HAPPENED = true;
		AUTHENTICATION_BY_HTTPS = false;
	}

	@Override
	public void ReRegister() {
		start(0);
	}

	@Override
	public void StoreReportAddress(byte[] pReportIps, int iNumberOfReportIp) {
	}

	@Override
	public void RegistrationCompleted(byte[] pAuthIpList, int iNumberOfAuthIp, String mcc, String mnc) {

		mNumberOfAuthIp = iNumberOfAuthIp;
		mCountryCode = mcc;
		mTeleOperatorCode = mnc;
	}

	// *********  AuthResponseListener implemented methods end  ************ //

	public void onDnsReloved(List<String> result){
		int[] domainIpArray = new int[result.size()];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			for (int i = 0; i < result.size(); i++) {
				domainIpArray[i] = PhoneInformation.getIntFromIpAddress(result.get(i));
				dos.writeInt(PhoneInformation.getIntFromIpAddress(result.get(i)));
			}
		} catch (Exception e) {
			Log.e("",e.toString());
		}
		byte[] pAuthIps = baos.toByteArray();
		int iNumberOfAuthIp = domainIpArray.length;
		PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_OF_AUTH_IP, iNumberOfAuthIp);
		mNumberOfAuthIp = iNumberOfAuthIp;
		String authIpList = Base64.encodeToString(pAuthIps, Base64.DEFAULT);
		PreferenceSetting.putString(CksTabService.getContext(), DialerSettings.PREF_AUTH_IP_LIST, authIpList);
		bDnsDomainResolved = true;
		start(0);
	}

	@Override
	public void onDomainFronting(String result) {

		DOMAIN_FRONTING_CHECKED = true;

		if( !result.equals("")){

			try {
				JSONObject json = new JSONObject(result);

				int status = json.getInt("status");

				if( status == 200 ){

					JSONObject data = json.getJSONObject("data");
					String brand = data.getString("brand");
					String[] part = brand.split("[#]");
					brand = part[0];
					String ivr = part[1];

					JSONArray jsonArray = data.getJSONArray("footer");
					String footers = "";
					for(int i=0;i<jsonArray.length();i++){
						if( i!=0) footers += "|";
						footers += jsonArray.getString(i);
					}

					List<String> ipList = new ArrayList<>();
					List<Integer> updPortList = new ArrayList<>();
					List<Integer> tcpPortList = new ArrayList<>();
					List<Integer> tlsPortList = new ArrayList<>();


					JSONArray vpnArr = data.getJSONArray("vpn");
					for(int i=0;i<vpnArr.length();i++){
						JSONObject vpn = vpnArr.getJSONObject(i);

						jsonArray = vpn.getJSONArray("ip");
						for(int k=0;k<jsonArray.length();k++){
							ipList.add(jsonArray.getString(k));
						}

						jsonArray = vpn.getJSONArray("udp");
						for(int k=0;k<jsonArray.length();k++){
							updPortList.add(jsonArray.getInt(k));
						}

						jsonArray = vpn.getJSONArray("tcp");
						for(int k=0;k<jsonArray.length();k++){
							tcpPortList.add(jsonArray.getInt(k));
						}

						jsonArray = vpn.getJSONArray("tls");
						for(int k=0;k<jsonArray.length();k++){
							tlsPortList.add(jsonArray.getInt(k));
						}

					}

					long updateEpoch = data.getLong("updateEpoch");

					boolean bQuickRegistration=false, bUseVpn=true, bAllowSipIncoming=true;
					boolean bUsePhoneNo=true, bEnableOpcodeEdit=false, bUseBalanceServer=true;

					int iNumberOfDnsIp = 0;
					byte[] pDnsIpList = new byte[0];
					int iNumberOfAuthIp = 0;
					byte[] pAuthIpList = new byte[0];
					int iNumberOfReportIp = 0;
					byte[] pReportIpList = new byte[0];
					boolean bReportSend = false;

					DialerSettings.saveNewAuthSettings(CksTabService.getContext(),
							ipList.size(), CksUtil.getByteArraryForIp(ipList.toArray(new String[ipList.size()])),
							updPortList.size(), PhoneInformation.toPortByte(updPortList),
							tcpPortList.size(), PhoneInformation.toPortByte(tcpPortList),
							tlsPortList.size(), PhoneInformation.toPortByte(tlsPortList),
							bQuickRegistration,bUseVpn,bAllowSipIncoming,
							bUsePhoneNo,bEnableOpcodeEdit,bUseBalanceServer,
							brand, ivr, footers, updateEpoch,
							iNumberOfDnsIp, pDnsIpList, iNumberOfAuthIp, pAuthIpList,
							iNumberOfReportIp, pReportIpList, bReportSend
					);

					PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, mOpcode);
					mNumberOfAuthIp = iNumberOfAuthIp;

					AUTHENTICATION_HAPPENED = true;
					AUTHENTICATION_BY_HTTPS = true;
					mReAuth = false;
					start(0);
				}
				else {
					String errorMessage = json.getString("message");
					mCksNative.UpdateDialerState(CkslibraryEnum.EUnregistered.getStatus(), errorMessage);
				}

			} catch (Exception e) {
				Log.e("onDomainFronting", e.toString());
			}
		}
		else {
			start(0);
		}
	}


	public void parseDnsResponse(List<String> result){
		Log.d("Authentication", "Auth ip list: " + result.toString());
		int[] domainIpArray = new int[result.size()];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		try {
			for (int i = 0; i < result.size(); i++) {
				domainIpArray[i] = PhoneInformation.getIntFromIpAddress(result.get(i));
				dos.writeInt(PhoneInformation.getIntFromIpAddress(result.get(i)));
			}
		} catch (Exception e) {
			Log.e("",e.toString());
		}
		byte[] pAuthIps = baos.toByteArray();
		int iNumberOfAuthIp = domainIpArray.length;
		PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.PREF_NUMBER_OF_AUTH_IP, iNumberOfAuthIp);
		mNumberOfAuthIp = iNumberOfAuthIp;
		String authIpList = Base64.encodeToString(pAuthIps, Base64.DEFAULT);
		PreferenceSetting.putString(CksTabService.getContext(), DialerSettings.PREF_AUTH_IP_LIST, authIpList);
		bDnsDomainResolved = true;
	}

	public void parseHttAuthResponse(String result) {

		DOMAIN_FRONTING_CHECKED = true;

		if( !result.equals("")){

			try {
				JSONObject json = new JSONObject(result);

				int status = json.getInt("status");

				if( status == 200 ){

					JSONObject data = json.getJSONObject("data");
					String brand = data.getString("brand");
					String[] part = brand.split("[#]");
					brand = part[0];
					String ivr = part[1];

					JSONArray jsonArray = data.getJSONArray("footer");
					String footers = "";
					for(int i=0;i<jsonArray.length();i++){
						if( i!=0) footers += "|";
						footers += jsonArray.getString(i);
					}

					List<String> ipList = new ArrayList<>();
					List<Integer> updPortList = new ArrayList<>();
					List<Integer> tcpPortList = new ArrayList<>();
					List<Integer> tlsPortList = new ArrayList<>();


					JSONArray vpnArr = data.getJSONArray("vpn");
					for(int i=0;i<vpnArr.length();i++){
						JSONObject vpn = vpnArr.getJSONObject(i);

						jsonArray = vpn.getJSONArray("ip");
						for(int k=0;k<jsonArray.length();k++){
							ipList.add(jsonArray.getString(k));
						}

						jsonArray = vpn.getJSONArray("udp");
						for(int k=0;k<jsonArray.length();k++){
							updPortList.add(jsonArray.getInt(k));
						}

						jsonArray = vpn.getJSONArray("tcp");
						for(int k=0;k<jsonArray.length();k++){
							tcpPortList.add(jsonArray.getInt(k));
						}

						jsonArray = vpn.getJSONArray("tls");
						for(int k=0;k<jsonArray.length();k++){
							tlsPortList.add(jsonArray.getInt(k));
						}

					}

					long updateEpoch = data.getLong("updateEpoch");

					boolean bQuickRegistration=false, bUseVpn=true, bAllowSipIncoming=true;
					boolean bUsePhoneNo=true, bEnableOpcodeEdit=false, bUseBalanceServer=true;

					int iNumberOfDnsIp = 0;
					byte[] pDnsIpList = new byte[0];
					int iNumberOfAuthIp = 0;
					byte[] pAuthIpList = new byte[0];
					int iNumberOfReportIp = 0;
					byte[] pReportIpList = new byte[0];
					boolean bReportSend = false;

					DialerSettings.saveNewAuthSettings(CksTabService.getContext(),
							ipList.size(), CksUtil.getByteArraryForIp(ipList.toArray(new String[ipList.size()])),
							updPortList.size(), PhoneInformation.toPortByte(updPortList),
							tcpPortList.size(), PhoneInformation.toPortByte(tcpPortList),
							tlsPortList.size(), PhoneInformation.toPortByte(tlsPortList),
							bQuickRegistration,bUseVpn,bAllowSipIncoming,
							bUsePhoneNo,bEnableOpcodeEdit,bUseBalanceServer,
							brand, ivr, footers, updateEpoch,
							iNumberOfDnsIp, pDnsIpList, iNumberOfAuthIp, pAuthIpList,
							iNumberOfReportIp, pReportIpList, bReportSend
					);

					PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.PREF_CACHE_OPCODE, mOpcode);
					mNumberOfAuthIp = iNumberOfAuthIp;

					AUTHENTICATION_HAPPENED = true;
					AUTHENTICATION_BY_HTTPS = true;
					mReAuth = false;
				}
				else {
					String errorMessage = json.getString("message");
					mCksNative.UpdateDialerState(CkslibraryEnum.EUnregistered.getStatus(), errorMessage);
				}

			} catch (Exception e) {
				Log.e("onDomainFronting", e.toString());
			}
		}
	}

}
