package com.ci.connectdialer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.ui.NotificationActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "CD-FirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log.d(TAG, "From: " + remoteMessage.getFrom());
        //Log.d(TAG, "NotificationActivity Message Body: " + remoteMessage.getNotification().getBody());

        //generate notification
        sendNotification( remoteMessage );
    }

    //This method is only generating push notification
    private void sendNotification(RemoteMessage remoteMessage) {

        String pushNotificationTitle = remoteMessage.getData().get(DialerSettings.PUSH_NOTIFICATION_TITLE);
        String pushNotificationMsgTitle = remoteMessage.getData().get(DialerSettings.PUSH_NOTIFICATION_MSG_TITLE);
        String pushNotificationMsg = remoteMessage.getData().get(DialerSettings.PUSH_NOTIFICATION_MSG);
        String pushNotificationImgUrl = remoteMessage.getData().get(DialerSettings.PUSH_NOTIFICATION_IMG_URL);
        String pushNotificationImgDomainFrontingUrl = remoteMessage.getData().get(DialerSettings.PUSH_NOTIFICATION_IMG_DOMAIN_FRONTING_URL);

        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra(DialerSettings.PUSH_NOTIFICATION_TITLE, pushNotificationTitle);
        intent.putExtra(DialerSettings.PUSH_NOTIFICATION_MSG, pushNotificationMsg);
        intent.putExtra(DialerSettings.PUSH_NOTIFICATION_MSG_TITLE, pushNotificationMsgTitle);
        intent.putExtra(DialerSettings.PUSH_NOTIFICATION_IMG_URL, pushNotificationImgUrl);
        intent.putExtra(DialerSettings.PUSH_NOTIFICATION_IMG_DOMAIN_FRONTING_URL, pushNotificationImgDomainFrontingUrl);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(pushNotificationTitle)
                .setContentText(pushNotificationMsgTitle)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationID = (int)System.currentTimeMillis();
        notificationManager.notify(notificationID, notificationBuilder.build());




    }
}