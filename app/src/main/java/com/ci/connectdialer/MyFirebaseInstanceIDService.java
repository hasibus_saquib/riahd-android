package com.ci.connectdialer;

import android.util.Log;

import com.ci.connectdialer.core.utilities.DialerSettings;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Mahim-Ul Asad on 7/14/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //generate token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, refreshedToken);

        //subscribe to token
        FirebaseMessaging.getInstance().subscribeToTopic(DialerSettings.PUSH_NOTIFICATION_TOPIC_GLOBAL);
    }

}