package com.ci.connectdialer;

import android.content.Context;

import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.Util;

/**
 * Use this class for application's all hard-coded values
 */
public class ApplicationValidator {

	public static final String APPLICTION_NAME = "Ria HD";
	public static final String APPLICATION_VERSION = "2.2.0";
	public static final int APPLICATION_VERSION_CODE = 3;

	public static final String ENCRYPTION_KEY = "75389640";
	public static final String HASH_CODE_ENCRYPTION_KEY = "53266502";

	public static final String[] ENCRYPTED_AUTH_DOMAIN_LIST = {
			"d4b}}n*chkmmjr`ififz'ekm", "d7b}}n*chkmmjr`ififz'ekm" };

	public static final int[] DNS_IP_LIST = { -1898348981, -1948680629, -1915126197, -1982235061,
			-1931903413, -1999012277, -1965457845, -1881571765, 2127865964, 2043979884, 2027202668,
			2060757100, 2111088748, 2077534316, 2144643180, 2094311532, -694107731, -677330515,
			-794771027, -727662163, -761216595, -710884947, -744439379, -777993811 };

	public static final String ENCRYPTED_REPORT_DOMAIN = "rdxfww*ce`f}pmonahbhk-gim";

	public static final boolean SPLASH_ENABLED = false;

	private static final String[] IMAGE_HASH = {
		"21fb789325cd080b84459f15e4b14c24",
		"752d3e02d014efa1303617676313e90b",
		"39fcdd89636d73be26820371927f4793",
		"72e733e199cb282ab9ef6aa94b43e1e8",
		"22b59437ea4d73df3c516a0244c535d0",
		"a1a2f515e63f43e5050f58e16a186d88"
	};

	public static boolean isApplicationValid(Context context, int appNameResourceId, int imageResourceId) {

		String imageData = Util.loadHashedImageData(context, imageResourceId);
		String applicationVersion = PhoneInformation.getAppVersion(context);
		int versionCode = PhoneInformation.getAppVersionCode(context);

		//Check Application Name
		String appName = context.getString(appNameResourceId);
		if (appName != null && !appName.equals(ApplicationValidator.APPLICTION_NAME)) {
			return false;
		}

		//Check Application Version Name
		if (applicationVersion != null && !applicationVersion.equals(APPLICATION_VERSION)) {
			return false;
		}
		
		//Check Application Version Code
		if (versionCode != APPLICATION_VERSION_CODE) {
			return false;
		}

//		String ldpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_ldpi);
//		String mdpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_mdpi);
//		String hdpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_hdpi);
//		String xhdpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_xhdpi);
//		String xxhdpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_xxhdpi);
//		String xxxhdpi = Util.loadHashedImageData(context, R.drawable.ic_launcher_xxxhdpi);

		boolean imageMatched = false;
		for (String imHash : IMAGE_HASH) {
			if (imageData.equals(imHash)) {
				imageMatched = true;
			}
		}

		if ( !(Util.highDensityAutoCheck(context) || imageMatched)) {
			return false;
		}

		return true;
	}
}
