package com.ci.connectdialer.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Erfan on 12/02/17.
 */
public class CallRecordsModel {

    String fileName;
    Date dateCreated;


    public CallRecordsModel(String fileName, Date dateCreated) {
        this.fileName=fileName;
        this.dateCreated=dateCreated;
    }


    public String getFileName() {
        return fileName;
    }


    public String getDateCreated() {

        return new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss").format(dateCreated);
    }

    public Date getDateCreatedAsDate() {

        return dateCreated;
    }
}
