package com.ci.connectdialer.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Account;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Erfan on 5/5/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AsyncTasks.GetCredentialsTask.GetCredentialsListener, AsyncTasks.SendOTPTask.SendOTPListener {

    private EditText otpEditText;
    private String currentAccountName;
    private ProgressDialog progressDialog;

    private SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                otpEditText.setText(code);//set code in edit text
                //then you can send verification code to server
            }
        });
        //smsVerifyCatcher.setPhoneNumberFilter("ZELLAS");

        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);

        Button resendOtp = (Button) findViewById(R.id.resendOtpButton);
        resendOtp.setOnClickListener(this);

        Button changeNumber = (Button) findViewById(R.id.editNumberButton);
        changeNumber.setOnClickListener(this);

        otpEditText = (EditText) findViewById(R.id.editTextOtp);

        currentAccountName = getIntent().getStringExtra("account_name");
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void processLoginAction()
    {
        if(isValid()) {
            String phoneNumber = getIntent().getStringExtra("phoneNumber");
            String otp = otpEditText.getText().toString();
            progressDialog = ProgressDialog.show(LoginActivity.this, "", "Please Wait", false);
            new AsyncTasks.GetCredentialsTask(this).execute(phoneNumber, otp);
        }
    }

    private void processResendOTPAction()
    {
        progressDialog = ProgressDialog.show(LoginActivity.this, "", "Please Wait", false);
        String phoneNumber = getIntent().getStringExtra("phoneNumber");
        new  AsyncTasks.SendOTPTask(this).execute(phoneNumber);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.loginButton:
                processLoginAction();
                break;

            case R.id.resendOtpButton:
                processResendOTPAction();
                break;

            case R.id.editNumberButton:
            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    private Account GetAccountObject(JSONObject jsonObject)
    {
        Account account = new Account();
        try {
            account.setOpCodeEditable(false);
            account.setOpCode(DialerSettings.PLATINUM_OPCODE);
            account.setUsername(jsonObject.getString("subscriberId"));
            account.setPassword(jsonObject.getString("password"));
            account.setPhoneNumber(jsonObject.getString("msisdn"));
            account.setAccountName(currentAccountName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return account;
    }

    private boolean saveAccountListItemSettings(JSONObject jsonObject) {

        //Todo: get Account from accountName -> DialerSettings
        //Todo: add/update Account -> DialerSettings
        String accountKey = DialerSettings.ACCOUNT_KEY_PREFIX + currentAccountName;//accountNameText.getText().toString();
        Account accountItem = GetAccountObject(jsonObject);

        PreferenceSetting.putString(LoginActivity.this, accountKey, accountItem.toString());
        PreferenceSetting.addStringToSet(LoginActivity.this, DialerSettings.PREF_ACCOUNT_KEY_SET, accountKey);
        DialerSettings.saveAsCurrentAccount(LoginActivity.this, accountItem);

        return true;
    }

    @Override
    public void onBackPressed() {


        final Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        superBackPress();
    }

    @Override
    public void onGetCredentials(String result) {

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                int statusCode = json.getInt("statusCode");
                String status = json.getString("status");
                if( statusCode == 1001 || status.equals("success")){
                    Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    saveAccountListItemSettings(json);
                    gotoMainScreen();
                }
                else {
                    Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Log.e("onSendOTP", e.toString());
            }
        }
        else {
            Toast.makeText(this, "Error sending OTP. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    private void gotoMainScreen() {
        final Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
//        onBackPressed();
        superBackPress();
    }
    public void superBackPress() {
        super.onBackPressed();
    }

    public boolean isValid() {

        String otp = otpEditText.getText().toString();
        if(otp.toString().length() < 1)
        {
            Alert.showOk(LoginActivity.this, "Error", "Please enter otp");
            return false;
        }
        return true;
    }

    @Override
    public void onSendOTP(String result) {

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                int statusCode = json.getInt("statusCode");
                String status = json.getString("status");
                if( statusCode == 1000 || status.equals("success")){
                    Alert.showOk(LoginActivity.this, "Success", json.getString("message"));
                }
                else {
                    Alert.showOk(LoginActivity.this, "Error", json.getString("message"));
                }

            } catch (Exception e) {
                Log.e("onSendOTP", e.toString());
            }
        }
        else {
            Alert.showOk(LoginActivity.this, "Error", "Error sending OTP. Please try again later.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
