package com.ci.connectdialer.ui;

/**
 * Created by Erfan on 2/17/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.AudioWife;
import com.ci.connectdialer.R;

import java.io.File;

public class AudioPlayerActivity extends Activity implements View.OnClickListener,  SensorEventListener {
    private static final String TAG = "AudioPlayer";

//    private MediaPlayer mediaPlayer = null;
//    private MediaController mediaController;
    private String audioFile;
    private Uri fileUri;
    private int iCurrentPosition = 0;
    private Handler handler = new Handler();

//    private SensorManager mSensorManager;
//    private Sensor mProximity;
//    private static final int SENSOR_SENSITIVITY = 0;

//    private boolean bIsNear = false;

    public ImageView mPlayMedia;
    public ImageView mPauseMedia;
    public SeekBar mMediaSeekBar;
    public TextView mRunTime;
    public TextView mTotalTime;

//    private PowerManager.WakeLock powerLock;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.audio_player);

//        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        mPlayMedia = (ImageView) findViewById(R.id.play);
        mPauseMedia = (ImageView) findViewById(R.id.pause);
        mMediaSeekBar = (SeekBar) findViewById(R.id.media_seekbar);
        mRunTime = (TextView) findViewById(R.id.playback_time);
        mTotalTime = (TextView) findViewById(R.id.total_time);

        Intent intent = getIntent();
        String filePath = intent.getExtras().getString("filePath");
        String fileName = intent.getExtras().getString("fileName");

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar_share);
        backToolbarIv.setOnClickListener(this);

        TextView titleTv = (TextView) findViewById(R.id.toolbartitle);
        titleTv.setText("Record File");

        ImageView shareToolbarIv = (ImageView) findViewById(R.id.share_toolbar);
        shareToolbarIv.setVisibility(View.VISIBLE);
        shareToolbarIv.setOnClickListener(this);

        audioFile = filePath + "/" + fileName;

        String filenameTrimmed = fileName.replace(".wav", "");

        String dateCreated = filePath.substring(filePath.lastIndexOf('/') + 1);

        TextView fileIdTV = ((TextView) findViewById(R.id.tv_fileid));
        fileIdTV.setText("File: " + filenameTrimmed);

        TextView dateTV = ((TextView) findViewById(R.id.tv_date_created));
        dateTV.setText("Date: " + dateCreated);

        fileUri = Uri.fromFile(new File(filePath + File.separator + fileName));
        AudioWife.getInstance()
                .init(getApplicationContext(), fileUri)
                .setPlayView(mPlayMedia)
                .setPauseView(mPauseMedia)
                .setSeekBar(mMediaSeekBar)
                .setRuntimeView(mRunTime)
                .setTotalTimeView(mTotalTime);

        AudioWife.getInstance().play();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_toolbar_share:
                onBackPressed();
                break;
            case R.id.share_toolbar:
                shareRecordedCall();
            default:
                break;
        }

    }
    @Override
    public void onBackPressed() {
        AudioWife.getInstance().release();
        super.onBackPressed();
    }

    public void shareRecordedCall() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri fileUri = Uri.parse(audioFile);
        sharingIntent.setType("audio/wav");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        startActivity(Intent.createChooser(sharingIntent, "Share audio using"));
    }

    @Override
    protected void onStart() {
        super.onStart();

//        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        powerLock = pm.newWakeLock(
//                PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK,
//                "PlDialer.PWLock");
//        powerLock.acquire();
    }

    @Override
    protected void onStop() {
//        if(null != powerLock && powerLock.isHeld()) {
//            powerLock.release();
//            powerLock = null;
//        }

//        try {
//            AudioWife.getInstance().pause();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause()
    {
//        mSensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
//        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
//            if (event.values[0] == SENSOR_SENSITIVITY) {
//                //near
//                if(bIsNear || !AudioWife.getInstance().IsPlaying())
//                    return;
//                bIsNear = true;
//
//                AudioWife.getInstance()
//                        .reInit(getApplicationContext(), fileUri, false)
//                        .setPlayView(mPlayMedia)
//                        .setPauseView(mPauseMedia)
//                        .setSeekBar(mMediaSeekBar)
//                        .setRuntimeView(mRunTime)
//                        .setTotalTimeView(mTotalTime);
//                AudioWife.getInstance().play();
//            } else {
//                //far
//                if(!bIsNear || !AudioWife.getInstance().IsPlaying())
//                    return;
//                bIsNear = false;
//                AudioWife.getInstance()
//                        .reInit(getApplicationContext(), fileUri, true)
//                        .setPlayView(mPlayMedia)
//                        .setPauseView(mPauseMedia)
//                        .setSeekBar(mMediaSeekBar)
//                        .setRuntimeView(mRunTime)
//                        .setTotalTimeView(mTotalTime);
//                AudioWife.getInstance().play();
//            }
//        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
