package com.ci.connectdialer.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Receiver;

import org.json.JSONObject;

/**
 * Created by Erfan on 5/11/17.
 */

public class FundTransferActivity extends AppCompatActivity implements View.OnClickListener, AsyncTasks.FundTransferTask.FundTransferListener {

    EditText editTextToUserId;
    EditText editTextTransferAmount;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fund_transfer);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(this);

        TextView titleTextView = (TextView) findViewById(R.id.titleEditAccount);
        titleTextView.setText("Fund Transfer");

        Button topRightButton = (Button) findViewById(R.id.saveAccountTv);
        topRightButton.setText("Transfer");
        topRightButton.setOnClickListener(this);

        editTextToUserId = (EditText) findViewById(R.id.editTextToUserId);
        editTextToUserId.setSingleLine();
        editTextTransferAmount = (EditText) findViewById(R.id.editTextTransferAmount);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveAccountTv:
                processFundTransfer();
                break;

            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isValid() {

        if(editTextToUserId.getText().toString().length() < 1)
        {
            Alert.showOk(FundTransferActivity.this, "Error", "Please enter user id");
            return false;
        }
        if(editTextTransferAmount.getText().toString().length() < 1)
        {
            Alert.showOk(FundTransferActivity.this, "Error", "Please enter the amount you want to transfer");
            return false;
        }
        return true;
    }

    private void processFundTransfer() {
        if(isValid())
        {
            String toUserId = "OTT" + editTextToUserId.getText().toString();
            String transferAmount = editTextTransferAmount.getText().toString();
            String username = PreferenceSetting.getString(FundTransferActivity.this, DialerSettings.PREF_USERNAME, "");
            String password = PreferenceSetting.getString(FundTransferActivity.this, DialerSettings.PREF_PASSWORD, "");

            progressDialog = ProgressDialog.show(FundTransferActivity.this, "", "Please wait", false);
            new AsyncTasks.FundTransferTask(this).execute(username, password, toUserId, transferAmount);
        }
    }

    @Override
    public void onFundTransferCompletion(String result) {

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                String status = json.getString("status");
                if(status.equals("success")){
                    Receiver.getStateManager().reconnect();
                    String message = json.getString("message") + " to " + json.getString("Transferred_To") + ".\nYour current Balance is " + json.getString("Updated_Balance") + ".";
                    //Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    Alert.showOk(FundTransferActivity.this, FundTransferActivity.this.getString(R.string.app_name), message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onBackPressed();
                                }
                            }
                    );
                }
                else {
                    //Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    Alert.showOk(FundTransferActivity.this, "Error", json.getString("message"));
                }

            } catch (Exception e) {
                Log.e("onSendOTP", e.toString());
            }
        }
        else {
            //Toast.makeText(this, "Error sending OTP. Please try again later.", Toast.LENGTH_LONG).show();
            Alert.showOk(FundTransferActivity.this, "Error", "Error redeeming voucher. Please try again later.");
        }
    }
}
