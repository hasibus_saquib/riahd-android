package com.ci.connectdialer.ui;

/**
 * Created by Masud Rashid on 23/11/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.PushNotification;
import com.ci.connectdialer.R;
import com.ci.connectdialer.core.auth.AuthenticationManager;
import com.ci.connectdialer.core.ckslib.CksLogger;
import com.ci.connectdialer.core.ckslib.CksNativeLibrary;
import com.ci.connectdialer.core.utilities.Account;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AppInit;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.DialpadVisibilityInterface;
import com.ci.connectdialer.core.utilities.MyGestureListener;
import com.ci.connectdialer.core.utilities.NotificationUtil;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Receiver;
import com.ci.connectdialer.core.utilities.StarHashCodes;
import com.ci.connectdialer.core.utilities.StateManager;
import com.ci.connectdialer.core.utilities.StateObserver;
import com.signal.CkslibraryEnum;

import static com.signal.CkslibraryEnum.ERegistered;

public class MainActivity extends AppCompatActivity implements
        AppInit.IAppInit,
//        IPermissionMgr,
        StarHashCodes.StarHashCodeListener,
        StateManager.InCallListener,
        NotificationUtil.NotificationListener,
        AuthenticationManager.AuthenticationListener,
        AsyncTasks.AppUpdateCheck.UpdateAppListener,
        Dialpad.OnDialpadVisibilityListener,
        View.OnClickListener,
        DialpadVisibilityInterface
{

//    private AppInit appInit;
//    private PermissionManager permissionMgr;
    private DrawerLayout mDrawerLayout;
//    private ActionBarDrawerToggle mDrawerToggle;
    private ImageView mDrawerToggleMenu;
    private NavigationView mNavigationView;
    private TabLayout mTabLayout;
    private DialPadManager dialPadManager;
    private Dialpad mDialpad;
    private FloatingActionButton fab;
    private AppInit appInit;

    private boolean isActivityRunning = false;
    private boolean backFromCallScreen = false;

    private static final int PAGE_COUNT = 2;
    private static final String[] mTabsTitle = {"RECENT", "CONTACTS"};
    public static final int TAB_CALLS=0, TAB_CONTACTS=1;

    private GestureDetector gesturedetector = null;

    private boolean bIsFirstLaunch = true;
    private boolean manuallyExitApp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.d("MainActivity", "MainActivity.java onCreate()");

        appInit = new AppInit(getApplicationContext(), this);
        Log.e("ApplicationStarted", "Initial");
        if( multipleAppInstance()) return;
        Log.e("ApplicationStarted", "Single Instance");
        if (!ApplicationValidator.isApplicationValid(this, R.string.app_name, R.drawable.ic_launcher)) {
            showApplicationInvalidDialog();
            return;
        }
        AuthenticationManager.AUTHENTICATION_BY_HTTPS = false;
        AuthenticationManager.AUTHENTICATION_HAPPENED = false;
        DialerSettings.COUNTRY_RESTRICTION_ON = false;
		DialerSettings.NETWORK_DETECTED = false;
//        if (BuildConfig.DEBUG)
//            DialerSettings.COUNTRY_RESTRICTION_ON = false;
        startApp();
        //checkPushNotification();
        PushNotification.displayNotification(getApplicationContext());
    }

    private boolean multipleAppInstance() {
        Log.d("multiple instance", "!isTaskRoot: "+!isTaskRoot()+", getIntent: "+getIntent());
        if (!isTaskRoot() && getIntent() != null) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                Log.d("Multiple instance", "has multiple instance");
                //Intent intent1 = new Intent(this, BringToFrontActivity.class);
                //startActivity(intent1);
                finish();
                return true;
            }
            Log.d("multiple instance", "if statement is not true");
        }
        return false;
    }

//    private void initPermission() {
//        permissionMgr = new PermissionManager(this);
//        permissionMgr.checkPermissionAndStartApp(this);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        permissionMgr.onRequestPermissionsResult( this, requestCode, permissions, grantResults);
//    }

    public void startApp(){

//        appInit.startAppInit();

        addSomeListeners();

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        PhoneInformation.checkVersionInfo(getApplicationContext());

        setContentView(R.layout.activity_main);
        setupDrawerItems();
        setUpTabs();
        setUpDialpad();
        selectTab(TAB_CALLS);
        CksNativeLibrary.getInstance();
        changeOnUpdateAvailable();
        new AsyncTasks.AppUpdateCheck(this,this).execute();
        startService(new Intent(getApplicationContext(), CksTabService.class));
    }

    private void setUpDialpad() {
        dialPadManager = new DialPadManager(this);
        dialPadManager.setupDialpad();

        mDialpad = dialPadManager.getDialPad();
        mDialpad.setOnDialpadVisibilityListener(this);

        MyGestureListener myListener = new MyGestureListener();
        myListener.SetListener(this);
        gesturedetector = new GestureDetector(this, myListener);

        mDialpad.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                gesturedetector.onTouchEvent(event);
                return true;
            }
        });



        fab = (FloatingActionButton) findViewById(R.id.fab_main_pad);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialpad.setVisibility(View.VISIBLE);
            }
        });
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);

        return gesturedetector.onTouchEvent(ev);
    }

    public void toggleDialpadVisibility(){

        if( mDialpad==null) return;

        if( mDialpad.getVisibility()==View.GONE){
            mDialpad.setVisibility(View.VISIBLE);
        }
        else if(mDialpad.getVisibility()==View.VISIBLE){
            mDialpad.setVisibility(View.GONE);
        }
    }

    public void setDialpadVisibility(int value){

        if( mDialpad==null) return;
        mDialpad.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDialpadVisibility(int visibility) {
        if( visibility == View.VISIBLE){
            fab.setVisibility(View.GONE);
            mTabLayout.setTabTextColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));
            mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        else if( visibility == View.GONE){
            fab.setVisibility(View.VISIBLE);
            mTabLayout.setTabTextColors(getResources().getColor(R.color.colorPrimary), Color.WHITE);
            mTabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        }
    }

    @Override
    public void exitApp() {

        DialerSettings.COUNTRY_RESTRICTION_ON = false;
		DialerSettings.NETWORK_DETECTED = false;
        PreferenceSetting.putString(MainActivity.this, DialerSettings.CUSTOM_AUTH_IP, "");
        PreferenceSetting.putString(MainActivity.this, DialerSettings.CUSTOM_DNS_IP, "");
        DialerSettings.removeCustomFromAccounts(MainActivity.this);
        StateManager.reset();
        stopService(new Intent(getApplicationContext(), CksTabService.class));
        Receiver.unregister();
        CksLogger.clearLog();
        super.onBackPressed();
        isActivityRunning = false;
        manuallyExitApp = true;
        finish();
    }

    private void addSomeListeners() {

        StarHashCodes.getInstance().setStarHashCodeListener(this);
        StateManager.setCallScreenListener(this);
        NotificationUtil.setNotificationListener(this);
        AuthenticationManager.setAuthenticationListener(this);
    }

    public void selectTab(int tab){
        mTabLayout.getTabAt(tab).select();
    }

    public void selectContact( String contactName, String contactNumber, long contactID) {
        contactName = contactName.replaceAll("[|]", " " );
        contactNumber = contactNumber.replaceAll("[^\\d*#,]",""); //only accept phone type input
        Receiver.getStateManager().sendMsg(CkslibraryEnum.JContact, contactName+"|"+ contactNumber+"|"+contactID);
//        selectTab(TAB_CALLS);
    }

    private StateObserver mStateUiObserver;
    private int countFragmentCreate = 0;

    public void onFragmentActivityCreate(int tab) {
        countFragmentCreate++;

        if( countFragmentCreate == PAGE_COUNT) {
            mStateUiObserver = new StateUiObserver(this);
            Receiver.getStateManager().setStateObserver(mStateUiObserver);
        }
    }
    public StateObserver getStateUiObserver(){
        return mStateUiObserver;
    }

    @Override
    protected void onStart() {
        super.onStart();
//        Log.d("MainActivity", "MainActivity.java onStart()");
        updateLeftNavigationHeader(DialerSettings.getCurrentAccount(this));
        backFromCallScreen = false;
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.d("MainActivity", "MainActivity.java onResume()");
        isActivityRunning = true;
//        appInit.onResumeParentActivity();

        boolean bStatus = checkAccountSetup();
        if(bIsFirstLaunch)
        {
            bIsFirstLaunch = false;
            StartSplashActivity(bStatus);
        }
        if(StateManager.mState == ERegistered && !CksNativeLibrary.getInstance().IsReady()){
            Receiver.getStateManager().connect();
        }

        if(StateManager.mInCall) {
            startCallScreen();
        }

    }

    private void StartSplashActivity(boolean bIsNewInstallation) {
        Intent splashIntent = new Intent(MainActivity.this, SplashActivityNew.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        splashIntent.putExtra("SplashTimeout", bIsNewInstallation);
        startActivity(splashIntent);
    }

    private boolean alreadyAskedAboutAccount = false;

    private boolean checkAccountSetup() {

        if( alreadyAskedAboutAccount || DialerSettings.isSettingsSaved(MainActivity.this)) return false;

        alreadyAskedAboutAccount = true;

        newAccountActivity();

        return true;
    }


    @Override
    public void onPause() {
        super.onPause();
//        Log.d("MainActivity", "MainActivity.java onPause()");
        isActivityRunning = false;
//        appInit.onPauseParentActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.d("MainActivity", "MainActivity.java onDestroy()");
        if( mStateUiObserver!=null) ((StateUiObserver)mStateUiObserver).onDestroy();
        if( dialPadManager!=null) dialPadManager.onDestroyView();
        if (manuallyExitApp) {
            android.os.Process.killProcess(android.os.Process.myPid());
        }

    }

    private boolean authenticationInitCompleted = false;
    @Override
    public void afterAuthenticationInit() {
        authenticationInitCompleted = true;
    }

    @Override
    public boolean extendSplashTime() {
        return !authenticationInitCompleted;
    }

    @Override
    public void afterSplashStop() {
    }

    public void showApplicationInvalidDialog(){

        Alert.showOk(this, "Internal Error", "Please contact with your provider", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exitApp();
            }
        });
    }

    public void showMccInvalidDialog(){
//        final Handler handler = new Handler(Looper.getMainLooper());
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                Alert.showPositiveNegative(MainActivity.this, "Use Connect Dialer Speed", "To use in this country please install Connect Dialer Speed.", "Download", "Cancel",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                AppInit.startDownloadAppConnectSpeed(MainActivity.this);
//                                exitApp();
//                            }
//                        },
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
////                                exitApp();
//                            }
//                        }
//                );
//            }
//        }, 1000);
//

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this)
//                .setTitle(R.string.app_name)
//                .setIcon(R.drawable.ic_inapp_icon)
//                .setCancelable(false)
//                .setMessage("To use in this country please install Connect Dialer.")
//                .setCancelable(true)
//                .setPositiveButton("Exit",new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,int id) {
//                        exitApp();
//                    }
//                })
//                .setNegativeButton("Download",new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,int id) {
//                        AppInit.startDownloadAppConnect(MainActivity.this);
//                        exitApp();
//                    }
//                });
//        alertDialogBuilder.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        if( mDialpad.getVisibility()==View.VISIBLE){
            mDialpad.setVisibility(View.GONE);
            return;
        }

        onExitDialog(MainActivity.this);
    }

    public void onExitDialog(Context context) {
        try{
            if(!((Activity)context).isFinishing()){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
                        .setTitle(R.string.app_name)
                        .setIcon(R.drawable.ic_launcher)
                        .setMessage("Want to exit?")
                        .setCancelable(true)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                exitApp();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                alertDialogBuilder.show();

            }
        } catch(Exception e) {
            Log.e("", e.toString());
        }
    }

    public void afterAllCacheClear() {
        newAccountActivity();
    }

    private static final int ACCOUNT_NEW = 9936;
    private static final int ACCOUNT_UPDATE = 9937;
    private static final int FROM_CALL_SCREEN = 9938;


    private void updateAccountActivity() {
        Intent intent = new Intent(this, EditAccountActivity.class);
        intent.putExtra("new_account", false);
        intent.putExtra("account_name", DialerSettings.getCurrentAccount(this).getAccountName());
        startActivityForResult(intent, ACCOUNT_UPDATE);
    }

    private void newAccountActivity() {

//        Intent intent = new Intent(this, SignUpActivity.class);
//        startActivityForResult(intent, ACCOUNT_NEW);
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivityForResult(intent, ACCOUNT_NEW);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == ACCOUNT_NEW || requestCode == ACCOUNT_UPDATE){
            String resultData = data.getStringExtra("result");
            if( resultCode == Activity.RESULT_OK && resultData != null) {
                if( resultData.equals("connect")){
                    connectAccount();
                }
                else if( resultData.equals("reconnect")){
                    reconnectAccount();
                }
                else if( resultData.equals("disconnect")){
                    disconnectAccount();
                }
                updateLeftNavigationHeader(DialerSettings.getCurrentAccount(this));
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                if( resultData.equals("no_account")) {
                    exitApp();
                }
            }
        }
        else if(requestCode == FROM_CALL_SCREEN){
            backFromCallScreen = true;
        }
    }

    private void disconnectAccount(){
        Receiver.unregister();
    }
    private void connectAccount() {

        Receiver.getStateManager().setStateObserver(getStateUiObserver());
        Receiver.getStateManager().connect();
    }
    private void reconnectAccount() {

        Receiver.getStateManager().setStateObserver(getStateUiObserver());
        Receiver.getStateManager().reconnect();
    }

    @Override
    public void startCallScreen() {
        Context context = CksTabService.getContext();
        if (context != null) {
            Intent intent = new Intent();
            intent.setClass(context, IncallActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(intent, FROM_CALL_SCREEN);
        }
    }

    @Override
    public PendingIntent getNotificationIntent() {
//        Log.d("Notification", "MainActivity.java getNotification() ");
        Intent intent = new Intent(this, MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //return null;
        return PendingIntent.getActivity(CksTabService.getContext(), 0, intent, 0);
    }

    @Override
    public Notification getNotification(String message, long baseTime) {
        try {
//            RemoteViews remoteViews = new RemoteViews(
//                    CksTabService.getContext().getPackageName(), R.layout.layout_notification);
//
//            if (baseTime > 0) {
//                remoteViews.setViewVisibility(R.id.call_time_noti, View.VISIBLE);
//                remoteViews.setChronometer(R.id.call_time_noti, baseTime, null, true);
//            } else {
//                remoteViews.setViewVisibility(R.id.call_time_noti, View.INVISIBLE);
//            }
//
//            remoteViews.setTextViewText(R.id.dialer_state_noti, message);

//            Log.d("notification", "MainActivity.java getNotification() ");
            NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(this);
            notiBuilder.setSmallIcon(R.drawable.ic_action_ic_launcher)
                    .setContentIntent(getNotificationIntent())
//                    .setContent(remoteViews)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message);
            Notification notificaiton = notiBuilder.build();
            notificaiton.flags = Notification.FLAG_NO_CLEAR;
            return notificaiton;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int getNfId() {
        return R.string.service_running;
    }

    private void setupDrawerItems() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                String username = PreferenceSetting.getString(MainActivity.this, DialerSettings.PREF_PHONENO, "");
                String balance = DialerSettings.getCurrentBalance(MainActivity.this);
                updateMyAccountInfo(username, balance);
            }
        });
        mNavigationView = (NavigationView) findViewById(R.id.nav_view) ;

        //TextView appNameTv = (TextView) hView.findViewById(R.id.drawerAppNameTv);
        //Typeface type = Typeface.createFromAsset(getAssets(),"MTCORSVA.ttf");
        //appNameTv.setTypeface(type);

        //appNameTv.setOnClickListener(this);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.nav_reconnect) {
                    Receiver.getStateManager().reconnect();
                }
                else if (menuItem.getItemId() == R.id.nav_ivr) {
                    if(!isFinishing()){
                        String ivr = PreferenceSetting.getString(MainActivity.this, DialerSettings.DIALER_IVR, "");
                        if(!ivr.equals(""))
                            Receiver.getStateManager().callTo("IVR", ivr, -1);
                        else
                            Alert.showOk(MainActivity.this, "No IVR assigned");
                    }
                }
                /*else if (menuItem.getItemId() == R.id.nav_account_settings) {
                    if( !DialerSettings.isSettingsSaved(MainActivity.this)){
                        newAccountActivity();
                    }
                    else{
                        updateAccountActivity();
                    }
                }*/
                else if (menuItem.getItemId() == R.id.nav_account_payments) {
                    showPaymentsActivity();
                }
                else if (menuItem.getItemId() == R.id.nav_my_rate) {
                    String url = "http://riahd.net/rates.php";
                    Uri webpage = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                    if (intent.resolveActivity(getPackageManager()) != null){
                        startActivity(intent);
                    }
                }
                else if (menuItem.getItemId() == R.id.nav_account_settings) {
                    showSettingsActivity();
                }
                else if (menuItem.getItemId() == R.id.nav_about) {
                    showAboutDialog();
                }
                else if (menuItem.getItemId() == R.id.nav_update) {
                    AppInit.startDownloadAppUpdate(MainActivity.this);
                }
                else if (menuItem.getItemId() == R.id.nav_call_records) {
                    openFolder();
                }
                else if (menuItem.getItemId() == R.id.nav_exit) {
                    onExitDialog(MainActivity.this);
                }
                return true;
            }
        });

         // Setup Drawer Toggle of the Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
//        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
//        mDrawerToggle.syncState();
        getSupportActionBar().setTitle("");

//        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggleMenu = (ImageView) findViewById(R.id.drawerToggleMenu);
        mDrawerToggleMenu.setOnClickListener(this);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
//        return true;
//    }

    private void showPaymentsActivity() {
        Intent intent = new Intent(this, PaymentsActivity.class);
        startActivity(intent);
    }

    private void showSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void showAboutDialog() {
        View view = getLayoutInflater().inflate(R.layout.about_view, null);

        TextView versionTv = (TextView) view.findViewById(R.id.aboutVersion);
        String version = String.format(getString(R.string.versionAbout),
                PhoneInformation.getAppVersion(MainActivity.this));
        versionTv.setText(version);

        ImageView fb = (ImageView) view.findViewById(R.id.about_fb);
        ImageView twitter = (ImageView) view.findViewById(R.id.about_twitter);
        ImageView gplus = (ImageView) view.findViewById(R.id.about_gplus);

        // disabled click action temporarily as click urls not yet defined
//        fb.setOnClickListener(MainActivity.this);
//        twitter.setOnClickListener(MainActivity.this);
//        gplus.setOnClickListener(MainActivity.this);

        Alert.showView(MainActivity.this, null, view);
    }

    private int hiddenClick = 0;
    public void forceCrash() {
        hiddenClick++;
        if( hiddenClick==5){
            throw new RuntimeException("This is a force crash");
        }
    }

    private void openFolder()
    {
        Intent intent = new Intent(this, FileManagerActivity.class);
        startActivity(intent);

        /*
        File directory = new File(Environment.getExternalStorageDirectory() + "/Dual Dialer/");
        if (!directory.exists()) {
            directory.mkdir();
        }
        Uri uri = Uri.parse(directory.getPath());

        Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setDataAndType(Uri.fromFile(directory), "resource/folder");
        intent.setDataAndType(uri, "resource/folder");
        startActivity(Intent.createChooser(intent, "Open Directory"));
        */
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.about_fb :
                openUrl(getString(R.string.fb_url));
                break;
            case R.id.about_twitter :
                openUrl(getString(R.string.twitter_url));
                break;
            case R.id.about_gplus :
                openUrl(getString(R.string.gplus_url));
                break;
            case R.id.drawerToggleMenu :
                if( !mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                break;
//            case R.id.drawerAppNameTv :
//                break;
            default:
                break;
        }
    }

    public final void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    public void updateMyAccountInfo(String username, String balance) {
        View hView = mNavigationView.getHeaderView(0);

        TextView usernameTV = (TextView) hView.findViewById(R.id.usernameTV);
        usernameTV.setText(username);
        TextView balanceTV = (TextView) hView.findViewById(R.id.balanceTV);
        balanceTV.setText(balance);
    }
    public void updateLeftNavigationHeader(Account account) {

//        if( mNavigationView == null ) return;
//        View hView =  mNavigationView.getHeaderView(0);
//        TextView opcodeTv = (TextView) hView.findViewById(R.id.opcodeDrawerTv);
//        TextView usernameTv = (TextView) hView.findViewById(R.id.usernameDrawerTv);
//
//        if( account.getOpCode() != 0 ) {
//            opcodeTv.setText("Opcode : " + account.getOpCode());
//            usernameTv.setVisibility(View.VISIBLE);
//            usernameTv.setText("Username : " + account.getUsername());
//        }
//        else{
//            opcodeTv.setText("No Active Account");
//            usernameTv.setVisibility(View.GONE);
//        }
    }



    private void setUpTabs() {

        // Setup the viewPager
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        final MyPagerAdapter pagerAdapter = new MyPagerAdapter(getFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout_top);
        mTabLayout.setupWithViewPager(viewPager);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mDialpad.setVisibility(View.GONE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                mDialpad.setVisibility(View.GONE);
            }
        });

        mTabLayout.setTabTextColors(getResources().getColor(R.color.colorPrimary), Color.WHITE);
    }

    @Override
    public void onAppUpdateChecked(String result) {

        //check country validity
//        String country = PhoneInformation.getCountryCode(this);
//        if(country.equalsIgnoreCase("0"))
//            country = AsyncTasks.getCountryCodeFromIp();
//        if ((!country.equalsIgnoreCase("424") && !country.equalsIgnoreCase("AE")) && COUNTRY_RESTRICTION)
//        {
//            showMccInvalidDialog();
//        }

        if( AppInit.showUpdateAvailable(MainActivity.this, result)){
            changeOnUpdateAvailable();
        }
    }

    private void changeOnUpdateAvailable(){

        if( DialerSettings.isVersionLowerThanPlayStore(this)) {
            if (mDrawerToggleMenu != null) {
                mDrawerToggleMenu.setImageResource(R.drawable.ic_menu_update_white_36dp);
            }
            String playStoreVersion = DialerSettings.getPlayStoreVersion(this);
            int red = Color.parseColor("#B71C1C");
            Menu navMenu = mNavigationView.getMenu();
            MenuItem appUpdateItem = navMenu.findItem(R.id.nav_update);
            SpannableString s = new SpannableString("Update App (" + playStoreVersion + ")");
            s.setSpan(new ForegroundColorSpan(red), 0, s.length(), 0);
            appUpdateItem.setTitle(s);
            appUpdateItem.getIcon().setColorFilter(red, PorterDuff.Mode.SRC_IN);
            appUpdateItem.setVisible(true);
        }
    }

    @Override
    public void SetDialpadVisibilityFromInterface(int visibility) {
        if(mDialpad == null)
            return;

        mDialpad.setVisibility(visibility);
    }

    @Override
    public void AnimateDialpadOnY(float distanceY) {
        if(mDialpad == null)
            return;

        mDialpad.animate().translationY(distanceY);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            Fragment fragment = null;
            switch (pos) {
                // or use getInstance
                case TAB_CONTACTS : fragment = new ContactsFragment(); break;
                case TAB_CALLS    : fragment = new CallsFragment(); break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }
}
