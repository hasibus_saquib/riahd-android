package com.ci.connectdialer.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class SplashActivityNew extends AppCompatActivity
{

    public int splashTimeout = 10000;
    private boolean bAlreadyFinised = false;
    static SplashActivityNew splashActivity = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        splashActivity = this;

        TextView splashTv = (TextView) findViewById(R.id.splashTv);
        Typeface type = Typeface.createFromAsset(getAssets(),"MTCORSVA.ttf");
        splashTv.setTypeface(type);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        switch(displayMetrics.densityDpi){
            case DisplayMetrics.DENSITY_LOW:
                splashTv.setTextSize(28);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                splashTv.setTextSize(30);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                splashTv.setTextSize(34);
                break;
        }

        ProgressBar v = (ProgressBar) findViewById(R.id.splashProgress);
        v.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        Intent intent = getIntent();
        if(intent.getBooleanExtra("SplashTimeout", false))
            splashTimeout = 1000;

        finishSplashActivity();
    }

    public static SplashActivityNew getInstance(){
        return splashActivity;
    }

    public void finishSplashActivity() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                finish();
            }
        }, splashTimeout);
    }
}
