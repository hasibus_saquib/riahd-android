package com.ci.connectdialer.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.CallRecorder;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Receiver;
import com.ci.connectdialer.core.utilities.StarHashCodes;
import com.ci.connectdialer.core.utilities.StateManager;
import com.ci.connectdialer.core.utilities.Util;
import com.signal.CkslibraryEnum;

/**
 * Created by Masud Rashid on 8/12/2016.
 */
public class DialPadManager implements Dialpad.OnDialpadEventListener {

    private Context mContext;

    private ToneGenerator toneGenerator;
    private TextView dialerContact;
    private EditText dialerNumber;
    private Dialpad dialpad;

    private LinearLayout addButtonLayout;
    private LinearLayout deleteButtonLayout;

    public DialPadManager(Context context) {
        mContext = context;
    }

    public void setupDialpad() {

        dialpad = (Dialpad) ((Activity) mContext).findViewById(R.id.dialerFragmentKeypad);
        dialpad.setOnDialpadEventListener(this);

        addButtonLayout = (LinearLayout) ((Activity) mContext).findViewById(R.id.btnAddContact);
        deleteButtonLayout = (LinearLayout) ((Activity) mContext).findViewById(R.id.btnDel);

        dialerContact = (TextView) ((Activity) mContext).findViewById(R.id.dialerContact);
        dialerNumber = (EditText) ((Activity) mContext).findViewById(R.id.EditTextPhoneNumber);
        dialerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(dialerNumber.length() > 0)
                {
                    addButtonLayout.setVisibility(View.VISIBLE);
                    deleteButtonLayout.setVisibility(View.VISIBLE);
//                    dialerNumber.setFocusable(true);
                }
                else
                {
                    addButtonLayout.setVisibility(View.GONE);
                    deleteButtonLayout.setVisibility(View.GONE);
//                    dialerNumber.setFocusable(false);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        if (toneGenerator != null)
            return;

        try {
            toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, ToneGenerator.MAX_VOLUME/2);
        } catch (Exception e) {
            toneGenerator = null;
        }
        Util.hideSystemKeyboard(dialerNumber);
    }

    Dialpad getDialPad(){
        return dialpad;
    }

    @Override
    public void onClick(int keyCode, int keyTone) {

        if (null != toneGenerator && keyTone != -1)
            toneGenerator.startTone(keyTone, 100);

        switch (keyCode) {
            case Dialpad.ButtonEvent_ADDCONTACT:
                String contact = dialerNumber.getText().toString().trim();

                if (!contact.equals("")) {
                    Intent intent = new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT, Uri.parse("tel:" + contact));
                    intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);
                    mContext.startActivity(intent);
                }
                break;

            case Dialpad.ButtonEvent_CALL:
                String text = dialerNumber.getText().toString();
                String textName = dialerContact.getText().toString();
                //dialerContact.setText("");

                StarHashCodes starHashCodes = StarHashCodes.getInstance();

                if (starHashCodes.checkAndProcessStarHashKey((Activity) mContext, text)) {
                    dialerNumber.setText("");
                    return;
                }

                if (!text.equals("") && StateManager.mState != null && StateManager.mState == CkslibraryEnum.ERegistered) {

                    long lCntId = -1;
                    if (textName.equals("")) {
                        Context context = CksTabService.getContext();
                        textName = PhoneInformation.retrieveContactNameNew(context, text, lCntId);
                        dialerContact.setText(textName);
                    }
                    else
                        lCntId = PreferenceSetting.getLong(CksTabService.getContext(), DialerSettings.DIALER_CONTACT_ID, -1);


                    text = text.replaceAll("[^\\d]", "");
                    Receiver.getStateManager().callTo(textName, text, lCntId);
                    DialerSettings.saveContactInfo(mContext, textName, text, lCntId);
                    CallRecorder.getInstance().setContact(textName,text);

                }
                else if (text.equals("") && StateManager.mState != null && StateManager.mState == CkslibraryEnum.ERegistered) {
                    restoreContactInfo();
                }

                break;

            case Dialpad.ButtonEvent_DELETE:
                dialerContact.setText("");
                onKeyPressed(KeyEvent.KEYCODE_DEL);
                break;

            case Dialpad.ButtonEvent_DELETE_LONG:
                dialerNumber.getText().clear();
                dialerContact.setText("");

                break;

            case Dialpad.ButtonEvent_PAD:
                dialpad.setVisibility(View.GONE);
                break;

            default:
                onKeyPressed(keyCode);
                break;
        }
    }

    public void restoreContactInfo() {
        String cName = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.DIALER_CONTACT_NAME, "");
        String cNumber = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.DIALER_CONTACT_NUMBER, "");
        dialerContact.setText(cName);
        dialerNumber.setText(cNumber);
    }

    private void onKeyPressed(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_PLUS && !dialerNumber.getText().toString().contains("+")) {
            if (dialerNumber.getText().toString().equals("") || (dialerNumber.getSelectionStart() == 0)) {
                KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
                dialerNumber.onKeyDown(keyCode, event);
            }
            return;
        }
        dialerContact.setText("");
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        dialerNumber.onKeyDown(keyCode, event);
    }

    public void onResume() {

    }

    public void onDestroyView() {
//        DialerSettings.saveContactInfo(mContext, dialerContact.getText().toString(), dialerNumber.getText().toString());

        if(null != toneGenerator) {
            toneGenerator.release();
            toneGenerator = null;
        }
    }

    @Override
    public void onCheckedChanged(View view, boolean value) {
        if( view.getId()==R.id.callRecordSwitch){
            CallRecorder.getInstance().setCallRecordRnabled(value);
        }
    }
}
