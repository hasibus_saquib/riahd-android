package com.ci.connectdialer.ui;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Erfan on 5/11/17.
 */

public class FundTransferHistoryModel {

    Date fundTransferDate;
    String fromAccount = "";
    String toAccount = "";
    String transferedAmount = "";
    String transferType = "";


    public FundTransferHistoryModel(JSONObject recordObject) {
        try {
            String strDate = recordObject.getString("Transferred-Date").replaceAll("[-: ]", "");
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            this.fundTransferDate = format.parse(strDate);
            this.fromAccount = recordObject.getString("From-Account");
            this.toAccount = recordObject.getString("To-Account");
            this.transferedAmount = recordObject.getString("Amount");
            this.transferType = recordObject.getString("Type");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Date getFundTransferDate() {
        return fundTransferDate;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public String getTransferedAmount() {
        return transferedAmount;
    }

    public String getTransferType() { return transferType; }
}