package com.ci.connectdialer.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.OptionsInterface;
import com.ci.connectdialer.R;

import java.util.ArrayList;


/**
 * Created by Erfan on 12/02/17.
 */
public class CallRecordsAdapter extends ArrayAdapter<CallRecordsModel> implements View.OnClickListener{

    private OptionsInterface optionsInterface;
    private ArrayList<CallRecordsModel> dataSet;
    private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtDate;
        LinearLayout options;
    }



    public CallRecordsAdapter(ArrayList<CallRecordsModel> data, Context context, OptionsInterface optionsInterface) {
        super(context, R.layout.call_records_row_item, data);
        this.dataSet = data;
        this.mContext = context;
        this.optionsInterface = optionsInterface;
    }


    @Override
    public void onClick(View v) {


        final int position=(Integer) v.getTag();
        Object object= getItem(position);
        CallRecordsModel dataModel=(CallRecordsModel)object;

        switch (v.getId())
        {
            case R.id.item_options_cr:

//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                Uri screenshotUri = Uri.parse(path + File.separator + dataModel.getFileName());
//                sharingIntent.setType("audio/wav");
//                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
//                startActivity(Intent.createChooser(sharingIntent, "Share audio using"));

//                if(mContext instanceof OptionsInterface){
//                    ((OptionsInterface)mContext).shareRecordedCall(position);
//                }
//                optionsInterface.shareRecordedCall(position);
//                Snackbar.make(v, "Create date " +dataModel.getDateCreated(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();

                final Context theContext = v.getRootView().getContext();

                PopupMenu popup = new PopupMenu(mContext, v);
                popup.getMenuInflater().inflate(R.menu.call_record_row_menu,
                        popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.playCallRecord:
                                optionsInterface.playRecordedCall(position);
                                break;
                            case R.id.shareCallRecordFile:
                                optionsInterface.shareRecordedCall(position);
                                break;
                            case R.id.delCallRecordFile:
                                AlertDialog.Builder builder = new AlertDialog.Builder(theContext);
                                builder.setTitle("Do you really want to delete this file?")
                                        .setIconAttribute(android.R.attr.alertDialogIcon)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                // Yes-code
                                                optionsInterface.deleteRecordedCall(position);
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        })
                                        .show();
                                break;
                            default:
                                break;
                        }

                        return true;
                    }
                });


                break;

        }


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CallRecordsModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.call_records_row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.fileName);
            viewHolder.txtDate = (TextView) convertView.findViewById(R.id.dateCreated);
            viewHolder.options = (LinearLayout) convertView.findViewById(R.id.item_options_cr);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        //Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        lastPosition = position;


        String fullfilename = dataModel.getFileName();
        String filename = fullfilename.replace(".wav", "");
        viewHolder.txtName.setText(filename);
        viewHolder.txtDate.setText(dataModel.getDateCreated());
        viewHolder.options.setOnClickListener(this);
        viewHolder.options.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }


}
