package com.ci.connectdialer.ui;

import android.app.Activity;
import android.os.Bundle;

public class BringToFrontActivity extends Activity {
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		finish();
	}

}
