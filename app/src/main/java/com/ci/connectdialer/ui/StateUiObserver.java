package com.ci.connectdialer.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateObserver;
import com.ci.connectdialer.core.utilities.Util;
import com.ci.connectdialer.R;
import com.signal.CkslibraryEnum;

/**
 * Created by Masud Rashid on 6/12/2016.
 */
public class StateUiObserver implements StateObserver{

    private Toolbar toolbar;
    private TextView dialerState;
    //private ImageView dialerStateLight;
    private TextView dialerName;
    private TextView dialerBalance;
    private TextView dialerFooter;
//    private Animation slidingTopIn;
//    private Animation slidingTopOut;

    private Dialpad dialpad;
    private TextView dialerContact;
    private EditText dialerNumber;
    private long contactID;

    private static final int STATE_ANIMATE_INTERVAL = 700;
    private int stateCount = 1;
    private static final int stateMaxCount = 3;
    private String stateText = "";

    private Context mContext;
    public StateUiObserver(Context context){
        mContext = context;

        setupUiObserver();
        mHandler.sendEmptyMessage(-1); // Default
    }

    private void setupUiObserver() {

        toolbar = (Toolbar) ((Activity)mContext).findViewById(R.id.tool_bar);
        dialerState = (TextView) ((Activity)mContext).findViewById(R.id.dialerState);
        //dialerStateLight = (ImageView) ((Activity)mContext).findViewById(R.id.dialerStateLight);
        dialerName = (TextView) ((Activity)mContext).findViewById(R.id.dialerName);
        dialerBalance = (TextView) ((Activity)mContext).findViewById(R.id.dialerBalance);
        dialerFooter = (TextView) ((Activity)mContext).findViewById(R.id.dialerFooter);
        dialerFooter.setSelected(true);

        dialpad = (Dialpad) ((Activity) mContext).findViewById(R.id.dialerFragmentKeypad);

        dialerContact = (TextView) ((Activity)mContext).findViewById(R.id.dialerContact);
        dialerNumber = (EditText) ((Activity)mContext).findViewById(R.id.EditTextPhoneNumber);
        dialerNumber.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                Util.hideSystemKeyboard(dialerNumber);
                return true;
            }
        });
        dialerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


//        if(slidingTopIn != null && slidingTopOut != null)
//            return;
//
//        slidingTopIn = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_up);
//        slidingTopOut = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_out);
        ((Activity)mContext).getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void showHeaderFooter() {
        if (mContext == null)
            return;
        String header = PreferenceSetting.getString(CksTabService.getContext(), DialerSettings.DIALER_NAME, "");
        String footers = PreferenceSetting.getString(CksTabService.getContext(),DialerSettings.DIALER_FOOTER, "");
        footer = footers.split("\\|");
        footerHandler.removeCallbacks(footerRunnable);
        dialerName.setText(header);
        footerHandler.postDelayed(footerRunnable, 1000);
    }

    private void removeHeaderFooter() {
        dialerName.setText("");
        //dialerError.setText("");
        footerHandler.removeCallbacks(footerRunnable);
        dialerFooter.setText("");
        dialerBalance.setText("");
    }

    int footerNum = 0;
    String[] footer;

    final Handler footerHandler = new Handler();
    final Runnable footerRunnable = new Runnable() {

        @Override
        public void run() {

            if(footerNum >= footer.length)
                footerNum = 0;

            dialerFooter.setSelected(true);
            dialerBalance.setSelected(true);
            if (footer.length > 1 || dialerFooter.getText().length() == 0) {
                dialerFooter.setText(footer[footerNum]);
            }

            footerNum++;

            footerHandler.postDelayed(this, 5000);

        }
    };

    @Override
    public void updateStateOnUi(CkslibraryEnum state, String info) {
        if(state != null){
            Message message = Message.obtain();
            message.what = state.getStatus();
            message.obj = info;

//            if(state == CkslibraryEnum.JContact || state == CkslibraryEnum.EAuthenticating
//                    || state == CkslibraryEnum.EAuthTrying || state == CkslibraryEnum.EUnregistered)
                mHandler.sendMessageDelayed(message, 100);
//            else
//                mHandler.sendMessageDelayed(message, 1000);
        }
    }

    public void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
//        dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
        showHeaderFooter();
    }

    @SuppressLint("HandlerLeak")
    public final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int status = msg.what;

            String statusDetail = (String) msg.obj;
            if(null != statusDetail && !statusDetail.isEmpty()
                    && status != CkslibraryEnum.JBalanceReceived.getStatus()
                    && status != CkslibraryEnum.JContact.getStatus()
                    && status != CkslibraryEnum.ERegistered.getStatus()) {

                onErrorStart(statusDetail);
            }

//            Log.e("StateUiObserver", CkslibraryEnum.getStatusMessage(status));

            //toolbar.setBackgroundResource(R.color.colorPrimaryDark);
            toolbar.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            switch (CkslibraryEnum.findById(status)) {

                case EAuthTrying:
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("Searching");
//                    dialerStateLight.setImageResource(R.drawable.offline);
                    removeHeaderFooter();
                    break;
                case EAuthenticating:

                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("Searching");
                    removeHeaderFooter();
//                    dialerStateLight.setImageResource(R.drawable.offline);

//                    removeHeaderFooter(); //disabled by Masud [30-09-2016]
//                    onErrorStop();
                    break;

                case EAuthenticationError:
//                    dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("INVALID");
//                    dialerStateLight.setImageResource(R.drawable.offline);
                    dialerName.setText(R.string.app_name);
//                    dialerBalance.setText("");
                    toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    footerHandler.removeCallbacks(footerRunnable);
                    break;

                case ERegistering:
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("Connecting");
//                    dialerStateLight.setImageResource(R.drawable.waiting);
//                    onErrorStop();
                    showHeaderFooter();
                    break;
                case ERegistered:
//                    dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("Connected");
//                    dialerStateLight.setImageResource(R.drawable.online);
                    showHeaderFooter();
                    try {
                        SplashActivityNew.getInstance().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(statusDetail != null && statusDetail.length() > 0)
                        Alert.showWarning(mContext, "Alert!", statusDetail);
                    break;
                case EUnregistered:
//                    dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("INVALID");
//                    dialerStateLight.setImageResource(R.drawable.offline);
                    footerHandler.removeCallbacks(footerRunnable);
//                    dialerBalance.setText("");
                    toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    break;

                case EConnectionError:
//                    dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("INVALID");
//                    dialerStateLight.setImageResource(R.drawable.offline);
//                    dialerBalance.setText("");
                    toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    footerHandler.removeCallbacks(footerRunnable);
//                    if(!PhoneInformation.isNetworkAvailable(mContext)) {
                        //updateStateOnUi(CkslibraryEnum.EConnectionError, "You are offline!");
//                    }
                    break;

                case EReconnecting:
                    dialerState.setTextColor(Color.WHITE);
                    dialerState.setText("Searching");
                    break;

                case JBalanceReceived:

                    if(dialerState.getText().equals(R.string.invalid))
                        toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    if (statusDetail.equals(""))
                        dialerBalance.setText("");
                    else
                        dialerBalance.setText(String.format(mContext.getString(R.string.balance), statusDetail));
                    break;

                case EAccessPoint:
                    break;


                case JContact:
                    if(dialerState.getText().equals(R.string.invalid))
                        toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    if(null != statusDetail) {
                        Util.hideSystemKeyboard(dialerNumber);
                        String[] contact = statusDetail.split("[|]");

                        dialerContact.setText("");
                        dialerNumber.getText().clear();

                        if(contact.length < 2)
                            break;

                        if(!contact[0].equals(""))
                            dialerContact.setText(contact[0]);

                        if(!contact[1].equals("")) {
                            dialerNumber.setText(contact[1]);
                        }

                        if(!contact[2].equals("")) {
                            try { contactID = Integer.parseInt(contact[2]); }
                            catch(NumberFormatException nfe) { nfe.printStackTrace(); }
                            DialerSettings.saveContactInfo(mContext,contact[0], contact[1], contactID);
                        }

                        dialerNumber.requestFocus();
                        dialerNumber.setSelection(dialerNumber.getText().length());
                        dialpad = (Dialpad) ((Activity) mContext).findViewById(R.id.dialerFragmentKeypad);
                        dialpad.setVisibility(View.VISIBLE);

                    }
                    break;

                default:
                    if(!PhoneInformation.isNetworkAvailable(mContext)) {
                        updateStateOnUi(CkslibraryEnum.EConnectionError, "You are offline!");
                        toolbar.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWarning));
                    }
                    break;
            }
        }

    };

    public void onErrorStart(String statusDetail) {

//        Toast.makeText(mContext, statusDetail, Toast.LENGTH_LONG).show();
        dialerFooter.setSelected(true);
//        dialerFooter.setText(statusDetail);
        dialerFooter.setText("");
        dialerBalance.setText(statusDetail);
        dialerFooter.setSelected(true);
    }

//    public void onErrorStop() {
//        if(dialerErrorLayout.getVisibility() == View.VISIBLE) {
//            slidingTopOut.setAnimationListener(new Animation.AnimationListener() {
//
//                @Override
//                public void onAnimationStart(Animation animation) {}
//
//                @Override
//                public void onAnimationRepeat(Animation animation) {}
//
//                @Override
//                public void onAnimationEnd(Animation animation) {
//                    dialerErrorLayout.setVisibility(View.GONE);
//                    dialerError.setText("");
//                }
//            });
//
//            dialerErrorLayout.startAnimation(slidingTopOut);
//        }
//    }
//
//    private void setDialerStateCounting(final String stateText){
//        this.stateText = stateText;
//        stateCount = 1;
//        dialerStateShowingHandler.removeCallbacks(dialerStateShowingRunnable);
//        dialerStateShowingHandler.postDelayed(dialerStateShowingRunnable, 0);
//    }

//    final Handler dialerStateShowingHandler = new Handler();
//    final Runnable dialerStateShowingRunnable = new Runnable() {
//
//        @Override
//        public void run() {
//            if(stateCount == stateMaxCount){
//                stateCount = 1;
//            } else {
//                stateCount ++;
//            }
//            StringBuilder sb = new StringBuilder();
//            sb.append(stateText);
//            for (int i = 0; i < stateCount; i++) {
//                sb.append(".");
//            }
//
//            try {
//                dialerState.setText(sb.toString());
//            } catch (Exception e) {
//                Log.e("", e.getMessage());
//            }
//            dialerStateShowingHandler.postDelayed(this, STATE_ANIMATE_INTERVAL);
//        }
//    };

//    public void restoreView() {
//        if (StateManager.mState != null) {
////            if (StateManager.mState != CkslibraryEnum.JBalanceReceived
////                    || StateManager.mState != CkslibraryEnum.JContact) {
////                if (StateManager.mStateInfo != null && !StateManager.mStateInfo.equals("")) {
//////                    dialerError.setText(StateManager.mStateInfo);
////
////                    onErrorStart(StateManager.mStateInfo);
////                }
////            }
//
//            if (StateManager.mState == CkslibraryEnum.EConnectionError
//                    || StateManager.mState == CkslibraryEnum.EUnregistered) {
//                dialerStateShowingHandler
//                        .removeCallbacks(dialerStateShowingRunnable);
//                dialerState.setTextColor(Color.RED);
//                dialerState.setText("Unregistered");
//                dialerStateLight.setImageResource(R.drawable.offline);
//            } else if (StateManager.mState == CkslibraryEnum.EAuthenticating
//                    || StateManager.mState == CkslibraryEnum.EAuthTrying) {
//                removeHeaderFooter();
//                dialerState.setTextColor(Color.WHITE);
//                setDialerStateCounting("Authenticating");
//                dialerStateLight.setImageResource(R.drawable.offline);
//            } else if (StateManager.mState == CkslibraryEnum.ERegistering) {
//                dialerState.setTextColor(Color.WHITE);
//                setDialerStateCounting("Registering");
//                dialerStateLight.setImageResource(R.drawable.waiting);
//                showHeaderFooter();
//            } else if (StateManager.mState == CkslibraryEnum.ERegistered) {
//                dialerStateShowingHandler
//                        .removeCallbacks(dialerStateShowingRunnable);
//                dialerState.setTextColor(Color.WHITE);
//                dialerState.setText("Registered");
//                dialerStateLight.setImageResource(R.drawable.online);
//                showHeaderFooter();
//
//                if (StateManager.mBalance != null && !StateManager.mBalance.equals(""))
//                    dialerBalance.setText(String.format(mContext.getString(R.string.balance), StateManager.mBalance));
//            }
//        }
////		restoreContactInfo();
//    }

}
