package com.ci.connectdialer.ui;

import java.util.Date;

/**
 * Created by Erfan on 12/02/17.
 */
public class FileManagerModel {

    String fileName;
    Date dateCreated;
    int itemCount;


    public FileManagerModel(String fileName, Date dateCreated, int itemCount) {
        this.fileName = fileName;
        this.dateCreated = dateCreated;
        this.itemCount = itemCount;
    }


    public String getFileName() {
        return fileName;
    }

    public String getItemCountString() {

        String strItemText = " items";
        if(this.itemCount == 1)
            strItemText = " item";
        String strItemCount = this.itemCount + strItemText;
        return strItemCount;
    }

    public String getDateCreated() {
        return dateCreated.toString();
    }

    public Date getDateCreatedAsDate() {
        return dateCreated;
    }
}
