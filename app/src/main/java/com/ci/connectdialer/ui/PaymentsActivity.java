package com.ci.connectdialer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.R;

/**
 * Created by Erfan on 5/11/17.
 */

public class PaymentsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(this);

        TextView titleTextView = (TextView) findViewById(R.id.titleEditAccount);
        titleTextView.setText("Payments");

        Button topRightButton = (Button) findViewById(R.id.saveAccountTv);
        topRightButton.setVisibility(View.GONE);

        TextView rechargeButton = (TextView) findViewById(R.id.rechargeButtonTV);
        rechargeButton.setOnClickListener(this);

        TextView paymentHistory = (TextView) findViewById(R.id.paymentHistoryButtonTV);
        paymentHistory.setOnClickListener(this);

        TextView fundTransferButton = (TextView) findViewById(R.id.fundTransferButtonTV);
        fundTransferButton.setOnClickListener(this);

        TextView fundTransferHistoryButton = (TextView) findViewById(R.id.fundTransferHistoryButtonTV);
        fundTransferHistoryButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rechargeButtonTV:
                showRedeemVoucher();
               break;

            case R.id.paymentHistoryButtonTV:
                showPaymentHistory();
                break;

            case R.id.fundTransferButtonTV:
                showFundTransfer();
                break;

            case R.id.fundTransferHistoryButtonTV:
                showFundTransferHistory();
                break;

            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void showRedeemVoucher() {
        Intent intent = new Intent(this, RedeemActivity.class);
        startActivity(intent);
    }

    public void showPaymentHistory() {
        Intent intent = new Intent(this, PaymentHistoryActivity.class);
        startActivity(intent);
    }

    public void showFundTransfer() {
        Intent intent = new Intent(this, FundTransferActivity.class);
        startActivity(intent);
    }

    public void showFundTransferHistory() {
        Intent intent = new Intent(this, FundTransferHistoryActivity.class);
        startActivity(intent);
    }
}
