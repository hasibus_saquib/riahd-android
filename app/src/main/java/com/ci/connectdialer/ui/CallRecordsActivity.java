package com.ci.connectdialer.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ci.connectdialer.core.utilities.OptionsInterface;
import com.ci.connectdialer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Erfan on 2/12/17.
 */

public class CallRecordsActivity extends AppCompatActivity
        implements View.OnClickListener, OptionsInterface {
    ArrayList<CallRecordsModel> dataModels;
    ListView listView;
    private static CallRecordsAdapter adapter;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        Intent intent = getIntent();
        String filePath = intent.getExtras().getString("filePath");
        String fileName = intent.getExtras().getString("fileName");

        path = filePath + "/" + fileName;
        TextView titleTv = (TextView) findViewById(R.id.toolbartitle);
        titleTv.setText(fileName);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar_share);
        backToolbarIv.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.list);
        listView.setBackgroundColor(Color.parseColor("#ECEFF1"));

        // Read all files sorted into the values-array
        dataModels = new ArrayList<>();
        File dir = new File(path);
        if (!dir.canRead()) {
            setTitle("Directory inaccessible");
        }
        else {
            File[] files = dir.listFiles();
            Arrays.sort(files);
            for(File file : files)
            {
                Date fileDate = new Date(file.lastModified());
                dataModels.add(new CallRecordsModel(file.getName(), fileDate));
            }
        }

        Collections.sort(dataModels, new Comparator<CallRecordsModel>() {
            public int compare(CallRecordsModel o1, CallRecordsModel o2) {
                return o2.getDateCreatedAsDate().compareTo(o1.getDateCreatedAsDate());
            }
        });

        adapter= new CallRecordsAdapter(dataModels, getApplicationContext(), this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CallRecordsModel dataModel= dataModels.get(position);

                //playCallRecords(path, dataModel.getFileName());
                gotoAudioPlayer(path, dataModel.getFileName());
            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_toolbar_share:
                onBackPressed();
                break;
            default:
                break;
        }

    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public void shareRecordedCall(int index) {
        CallRecordsModel model = dataModels.get(index);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri fileUri = Uri.parse(path + File.separator + model.getFileName());
        sharingIntent.setType("audio/wav");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        startActivity(Intent.createChooser(sharingIntent, "Share audio using"));
    }

    public void playRecordedCall(int position)
    {
        CallRecordsModel dataModel= dataModels.get(position);
        //playCallRecords(path, dataModel.getFileName());
        gotoAudioPlayer(path, dataModel.getFileName());
    }

    public void deleteRecordedCall(int position)
    {
        CallRecordsModel dataModel= dataModels.get(position);
        String filename = dataModel.getFileName();
        File file = new File(path, dataModel.getFileName());

        if( file.exists()){
            adapter.remove(adapter.getItem(position));
			file.delete();
            adapter.notifyDataSetChanged();
        }
    }

    private void gotoAudioPlayer(String filePath, String fileName) {
        Intent intent = new Intent(this, AudioPlayerActivity.class);
        intent.putExtra("filePath", filePath);
        intent.putExtra("fileName", fileName);
        startActivity(intent);
    }

    private void playCallRecords(String path, String fileName){

        // Audio playing works
        try {
            File directory = new File(path + File.separator + fileName);
            String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(directory).toString());
            String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(directory), mimetype);
            startActivity(intent);
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }
}
