package com.ci.connectdialer.ui;

/**
 * Created by Masud Rashid on 22/11/2016.
 */

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.ContactManager;
import com.ci.connectdialer.core.utilities.Util;
import com.ci.connectdialer.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ContactsFragment extends Fragment implements
        ContactManager.IContactManager,
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener, AbsListView.OnScrollListener, ListView.OnTouchListener {

    private ContactManager mContactManger;

    // An adapter that binds the result Cursor to the ListView
    private static SimpleCursorAdapter mCursorAdapter;
//    private ContactListAdapter mContactListAdapter;

    // Defines a variable for the search string
    private String SEARCH_STRING = "";

    // The column indexes for PROJECTION
    private static final int CONTACT_ID_INDEX = 0;
    private static final int LOOKUP_KEY_INDEX = 1;
    private static final int DISPLAY_NAME_INDEX = 2;
    private static final int PHOTO_THUMBNAIL_INDEX = 3;

    private static final int CONTACT_QUERY_ID = 9923;
    private static final int CONTACT_DETAILS_QUERY_ID = 9925;

    private EditText findContacts;

    private ListView contactsListView;
    private int mLastFirstVisibleItem;
    private boolean mScrollUp = false;

    LinearLayout indexLayout;

    Map<String, Integer> mapIndex;

    private GestureDetector mGestureDetector;

    // Empty public constructor, required by the system
    public ContactsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity()).onFragmentActivityCreate(MainActivity.TAB_CONTACTS);

        mGestureDetector = new GestureDetector(getActivity(), new SideIndexGestureListener());

        mContactManger = new ContactManager(this);

        //Init Contact List Loader
        getLoaderManager().initLoader(CONTACT_QUERY_ID, null, this);

        mCursorAdapter = getContactCursorAdapter(getActivity());
//        mContactListAdapter = new ContactListAdapter(getActivity());

        indexLayout = (LinearLayout) getActivity().findViewById(R.id.side_index);

        contactsListView = (ListView) getActivity().findViewById(R.id.contactList);
        contactsListView.setAdapter(mCursorAdapter);
//        contactsListView.setAdapter(mContactListAdapter);
        contactsListView.setOnItemClickListener(this);
        contactsListView.setOnScrollListener(this);

        findContacts = (EditText) getActivity().findViewById(R.id.find_contacts);
        //next 4 line is needed to work under API level 23
        Drawable drawable = getResources().getDrawable(R.drawable.ic_search_black_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));
        findContacts.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        findContacts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                SEARCH_STRING = charSequence.toString();
                getLoaderManager().restartLoader(CONTACT_QUERY_ID, null, ContactsFragment.this);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    indexLayout.setVisibility(View.INVISIBLE);
                    findContacts.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    indexLayout.setVisibility(View.VISIBLE);
                    Drawable drawable = getResources().getDrawable(R.drawable.ic_search_black_24dp);
                    drawable = DrawableCompat.wrap(drawable);
                    //DrawableCompat.setTint(drawable, Color.parseColor("#5df2d6"));
                    DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));

                    findContacts.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
            }
        });


//        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab_contacts);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Contacts.Intents.Insert.ACTION, Contacts.People.CONTENT_URI);
//                getActivity().startActivity(intent);
//            }
//        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            // ...
        }
        if (findContacts != null) {
            findContacts.clearFocus();
            Util.hideSystemKeyboard(findContacts);
        }

    }

    public static SimpleCursorAdapter getContactCursorAdapter(Context context) {

        // Defines an array that contains column names to move from the Cursor to the ListView.
        String mFromColumn[] = {
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                ContactsContract.Contacts._ID,
//             ContactsContract.CommonDataKinds.Phone.NUMBER,
        };
    /*
     * Defines an array that contains resource ids for the layout views
     * that get the Cursor column contents. The id is pre-defined in
     * the Android framework, so it is prefaced with "android.R.id"
     */
        int mToResourceId[] = {
                R.id.contact_name,
                R.id.contact_image,
//            R.id.contact_number,
        };

        ContactRowCursorAdapter cursorAdapter = new ContactRowCursorAdapter(
                context, R.layout.contacts_list_item, null, mFromColumn, mToResourceId, 0);

        return cursorAdapter;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {

        switch (loaderId) {

            case CONTACT_QUERY_ID:
                return mContactManger.getContactCursorLoader(getActivity(), SEARCH_STRING);

            case CONTACT_DETAILS_QUERY_ID:
                return mContactManger.getContactDetailsCursorLoader(getActivity(), mSelectedContactUri, mSelectedContactLookupKey);
        }
        return null;
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        switch (loader.getId()) {

            case CONTACT_QUERY_ID:
//                mContactListAdapter.setCursor(cursor);
                mCursorAdapter.swapCursor(cursor);
                break;

            case CONTACT_DETAILS_QUERY_ID:

                showContactDetailsAlert(cursor, mSelectedContactName, mSelectedContactId);
                break;
        }
        getIndexList();
        displayIndex();
    }

    private void showContactDetailsAlert(Cursor cursor, final String contactName, final long cntId) {

        List<String> numberList = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                numberList.add(cursor.getString(0));
            }
        } catch (Exception ex) {
            Log.e("showContactDetailsAlert", ex.getMessage());
        }

        //Todo: why this wrong cursor loader id bug?
        // Fixme
        if (numberList.size() < 1) return;

        if (numberList.size() == 1) {
            ((MainActivity) getActivity()).selectContact(contactName, numberList.get(0), cntId);
            return;
        }

        final String[] numbers = numberList.toArray(new String[0]);

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(contactName)
                .setItems(numbers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        ((MainActivity) getActivity()).selectContact(contactName, numbers[i], cntId);
                    }
                })
                .create();

        alertDialog.show();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        switch (loader.getId()) {

            case CONTACT_QUERY_ID:

                mCursorAdapter.swapCursor(null);
                break;

            //Fixme: wrong cursor loader id bug
            case CONTACT_DETAILS_QUERY_ID:
                break;
        }
    }

    private long mSelectedContactId;
    private String mSelectedContactLookupKey;
    private Uri mSelectedContactUri;
    private String mSelectedContactName;

    private boolean detailsLoderFirstTime = true;

    @Override
    public void onItemClick(AdapterView<?> parent, View item, int position, long rowID) {

        // Get the Cursor
//        Cursor cursor = ((SimpleCursorAdapter)parent.getAdapter()).getCursor();
        Cursor cursor = mCursorAdapter.getCursor();
//        Cursor cursor = mContactListAdapter.getCursor();
        // Move to the selected contact
        cursor.moveToPosition(position);

        // Get the _ID value
        mSelectedContactId = cursor.getLong(CONTACT_ID_INDEX);
        //Get the selected LOOKUP KEY
        mSelectedContactLookupKey = cursor.getString(LOOKUP_KEY_INDEX);
        // Create the contact's content Uri
        //mSelectedContactUri = ContactsContract.Contacts.getLookupUri(mSelectedContactId, mSelectedContactLookupKey);
        mSelectedContactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, mSelectedContactId);

        mSelectedContactName = cursor.getString(DISPLAY_NAME_INDEX);
//        String contactNumber = cursor.getString(Phone_NUMBER_INDEX);

        /*
         * You can use mSelectedContactUri as the content URI for retrieving
         * the details for a contact.
         */

        //Init Contact Details Loader
        if (detailsLoderFirstTime) {
            getLoaderManager().initLoader(CONTACT_DETAILS_QUERY_ID, null, this);
            detailsLoderFirstTime = false;
        } else {
            getLoaderManager().restartLoader(CONTACT_DETAILS_QUERY_ID, null, this);
        }

//        Receiver.getStateManager().sendMsg(CkslibraryEnum.JContact, contactName+"-"+contactNumber);
//        ((MainActivity)getActivity()).selectTab(MainActivity.TAB_CALLS);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        final ListView lw = contactsListView;

        if (view.getId() == lw.getId()) {
            final int currentFirstVisibleItem = lw.getFirstVisiblePosition();

            if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                if (!mScrollUp) {
                    mScrollUp = true;
//                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_top);
//                    findContacts.startAnimation(animation);
//                    findContacts.setVisibility(View.GONE);
                }
            } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                if (mScrollUp) {
                    mScrollUp = false;
//                    findContacts.setVisibility(View.VISIBLE);
//                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
//                    findContacts.startAnimation(animation);
                }
            }

            mLastFirstVisibleItem = currentFirstVisibleItem;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (mGestureDetector.onTouchEvent(event)) {
            return true;
        } else {
            return false;
        }

    }

//    @Override
//    public void onClick(View v) {
//        TextView selectedIndex = (TextView) v;
//        contactsListView.setSelection(mapIndex.get(selectedIndex.getText()));
//
//    }
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        TextView selectedIndex = (TextView) v;
//        contactsListView.setSelection(mapIndex.get(selectedIndex.getText()));
//        return false;
//    }

    public static class ContactRowCursorAdapter extends SimpleCursorAdapter{

        public ContactRowCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            ImageView iv = (ImageView) view.findViewById(R.id.contact_image);
            TextView tv = (TextView) view.findViewById(R.id.contact_name);

            String imageUri = cursor.getString(PHOTO_THUMBNAIL_INDEX);
            if (imageUri == null) {
                iv.setImageResource(R.drawable.ic_person_black_36dp);
                iv.setColorFilter(ResourcesCompat.getColor(context.getResources(), R.color.defaultContactColor, null));
//                iv.setColorFilter(context.getResources().getColor(R.color.colorPrimaryLight));
//                iv.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            } else {
                iv.setImageURI(Uri.parse(imageUri));
                iv.setColorFilter(Color.TRANSPARENT);
            }

            tv.setText(cursor.getString(DISPLAY_NAME_INDEX));
        }
    }

    private void getIndexList() {
        ArrayList<String> namelist = new ArrayList<>();
        for (int i = 0; i < contactsListView.getCount(); i++) {
            Cursor cursor = mCursorAdapter.getCursor();
            cursor.moveToPosition(i);
            namelist.add(i, cursor.getString(DISPLAY_NAME_INDEX));
        }
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < namelist.size(); i++) {
            String contactName = namelist.get(i).toUpperCase();
            String index = contactName.substring(0, 1);
            //Log.d("contact indexbar", "getIndexList() ContactsFragment.java index: "+index);
            //not letter
            if (!Character.isLetter(index.charAt(0))) {
                index = "#";
            }
            //unicode characters
            else if (!(index.charAt(0)>='A' && index.charAt(0)<='Z')){
                index = "*";
            }
            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }


    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            // we know already coordinates of first touch
            // we know as well a scroll distance
            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            // when the user scrolls within our side index
            // we can show for every position in it a proper
            // item in the country list
            if (sideIndexX >= 0 && sideIndexY >= 0) {
                displayListItem();
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }

    private int sideIndexHeight;
    private float sideIndexX;
    private float sideIndexY;
    int indexListSize;
    List<String> indexList;

    private void displayIndex() {

        indexLayout.removeAllViews();
        TextView textView;
        indexList = new ArrayList<String>(mapIndex.keySet());

        indexListSize = indexList.size();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math.floor(indexLayout.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

//        for (String index : indexList) {
//            textView = (TextView) getActivity().getLayoutInflater().inflate(
//                    R.layout.side_index_item, null);
//            textView.setText(index);
//            textView.setOnTouchListener(this);
//            indexLayout.addView(textView);
//        }


        TextView tmpTV;
        for (double i = 1; i <= indexListSize; i = i + delta) {
            String tmpLetter = indexList.get((int) i - 1);

            tmpTV = new TextView(getActivity());
            tmpTV.setText(tmpLetter);
            tmpTV.setGravity(Gravity.CENTER);
            tmpTV.setTextSize(12);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            tmpTV.setLayoutParams(params);
            indexLayout.addView(tmpTV);
        }

        sideIndexHeight = indexLayout.getHeight();

        indexLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return true;
            }
        });
    }

    public void displayListItem() {
//        LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);
        sideIndexHeight = indexLayout.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition>=0 && itemPosition < indexList.size()) {
            String indexItem = indexList.get(itemPosition);
            //Text bubble
            Log.d("index clicked", "contact indexbar clicked at: "+indexItem);
            final FrameLayout layoutTextBubble = (FrameLayout) getActivity().findViewById(R.id.layout_text_bubble);
            TextView textBubble = (TextView) getActivity().findViewById(R.id.text_bubble);
            textBubble.setText(indexItem);
            layoutTextBubble.setVisibility(View.VISIBLE);
            layoutTextBubble.postDelayed(new Runnable() {
                @Override
                public void run() {
                    layoutTextBubble.setVisibility(View.GONE);
                }
            }, 500);

            //start contact list from the position
            contactsListView.setSelection(mapIndex.get(indexItem));
        }
    }
}
