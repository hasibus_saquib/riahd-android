package com.ci.connectdialer.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ci.connectdialer.ApplicationValidator;
import com.ci.connectdialer.core.auth.AuthenticationManager;
import com.ci.connectdialer.core.utilities.Account;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.StateManager;
import com.ci.connectdialer.R;

import java.util.Random;

public class EditAccountActivity extends AppCompatActivity implements View.OnClickListener{

    private boolean bNewAccount;
    private String currentAccountName;
    private String resultIntentData="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_account);

        TextView appnameAboveAccount = (TextView) findViewById(R.id.appnameAboveAccount);
        Typeface type = Typeface.createFromAsset(getAssets(),"MTCORSVA.ttf");
        appnameAboveAccount.setTypeface(type);

        bNewAccount = getIntent().getBooleanExtra("new_account", false);
        currentAccountName = getIntent().getStringExtra("account_name");
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupFragment();
    }

    @Override
    public void onResume() {
        super.onResume();

        Account acc = null;
        boolean echoCancellationOn = false;
        if( bNewAccount ) {
            acc = new Account();
            acc.setAccountName(currentAccountName);
        }
        else {
            acc = DialerSettings.getSavedAccount(EditAccountActivity.this,currentAccountName);
            echoCancellationOn = DialerSettings.isEchoCancelationOn(EditAccountActivity.this);
        }
        updateListItem(acc, echoCancellationOn);
    }

    private LinearLayout opcodeLayout;
    private LinearLayout usernameLayout;
    private LinearLayout passwordLayout;
//    private EditText accountNameText;
    private EditText opcodeEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
//    private EditText phoneEditText;

    private LinearLayout customAuthLayout;
    private LinearLayout customDnsLayout;
    private EditText customAuthEditText;
    private EditText customDnsEditText;
    SwitchCompat echoCancelSwitch;

    private void setupFragment() {

        opcodeLayout = (LinearLayout) findViewById(R.id.opcodeLayout);
        usernameLayout = (LinearLayout) findViewById(R.id.usernameLayout);
        passwordLayout = (LinearLayout) findViewById(R.id.passwordLayout);

//        accountNameText = (EditText) findViewById(R.id.accountTitle);
        opcodeEditText = (EditText) findViewById(R.id.opcode);
        usernameEditText = (EditText) findViewById(R.id.username);
        passwordEditText = (EditText) findViewById(R.id.password);
//        phoneEditText = (EditText) findViewById(R.id.phone);

        Button saveBtn = (Button) findViewById(R.id.saveAccountTv);
        saveBtn.setOnClickListener(this);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(EditAccountActivity.this);
        TextView titleTv = (TextView) findViewById(R.id.titleEditAccount);

//        TextView loginAccountBtn = (TextView) findViewById(R.id.loginAccountBtn);
//        loginAccountBtn.setOnClickListener(EditAccountActivity.this);

        echoCancelSwitch = (SwitchCompat) findViewById(R.id.echoCancelationSwitch);

        opcodeLayout.setOnClickListener(this);
        usernameLayout.setOnClickListener(this);
        passwordLayout.setOnClickListener(this);

        if( bNewAccount) {
            titleTv.setText("Add Account");
//            accountNameText.setText(currentAccountName);
        }
        else {

            titleTv.setText("Settings");

//            accountNameText.setEnabled(false); //Todo: check if it works


        }

        customAuthLayout = (LinearLayout) findViewById(R.id.custom_auth_container);
        customDnsLayout = (LinearLayout) findViewById(R.id.custom_dns_container);
        customAuthEditText = (EditText) findViewById(R.id.authET);
        customDnsEditText = (EditText) findViewById(R.id.dnsET);

    }

    //ToDo: update from account item
    private void updateListItem(Account acc, boolean echoCancellationOn) {

        if(!DialerSettings.SHOW_OPCODE_FIELD) {

            if(acc.isOpCodeEditable()) {
                opcodeLayout.setVisibility(View.VISIBLE);

                if(acc.getOpCode() == DialerSettings.PLATINUM_OPCODE)
                    opcodeEditText.setText("");
                else
                    opcodeEditText.setText(acc.getOpCode() == 0 ? "" : Integer.toString(acc.getOpCode()));
            }
            else {
                opcodeLayout.setVisibility(View.GONE);
            }
        }
        else {
            opcodeEditText.setText(acc.getOpCode() == 0 ? "" : Integer.toString(acc.getOpCode()));
        }

//        accountNameText.setText(acc.getAccountName());
        usernameEditText.setText(acc.getUsername());
        passwordEditText.setText(acc.getPassword());
//        phoneEditText.setText(acc.getPhoneNumber());

        echoCancelSwitch.setChecked(echoCancellationOn);

        if(StateManager.CUSTOM_AUTH_ENABLED){
            customAuthLayout.setVisibility(View.VISIBLE);
            customAuthEditText.setText(acc.getCustomAuthIp());
        } else {
            customAuthLayout.setVisibility(View.GONE);
        }

        if(StateManager.CUSTOM_DNS_ENABLED){
            customDnsLayout.setVisibility(View.VISIBLE);
            customDnsEditText.setText(acc.getCustomDnsIp());
        } else {
            customDnsLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        switch (v.getId()) {
            case R.id.saveAccountTv:
                if(isValid()) {
                    if( bNewAccount || isItemChanged()){
                        if( saveAccountListItemSettings())
                            Toast.makeText(EditAccountActivity.this, "Account Settings Saved", Toast.LENGTH_SHORT).show();
                    }
                    DialerSettings.setEchoCancelationOn(EditAccountActivity.this, echoCancelSwitch.isChecked());
                    onBackPressed();
                }
                break;

            case R.id.opcodeLayout:
                EditText editText = (EditText) findViewById(R.id.opcode);
                editText.requestFocus();
                editText.setSelection(editText.getText().length());
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.usernameLayout:
                EditText editTextusername = (EditText) findViewById(R.id.username);
                editTextusername.requestFocus();
                editTextusername.setSelection(editTextusername.getText().length());
                imm.showSoftInput(editTextusername, InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.passwordLayout:
                EditText editTextpassword = (EditText) findViewById(R.id.password);
                editTextpassword.requestFocus();
                editTextpassword.setSelection(editTextpassword.getText().length());
                imm.showSoftInput(editTextpassword, InputMethodManager.SHOW_IMPLICIT);
                break;

//            case R.id.delAccountBtn:
//                Alert.showPositiveNegative(this, null, "Are you sure to delete?", "Delete", "No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        if( deleteAccountListItemSettings()){
//                            Toast.makeText(EditAccountActivity.this, "Account Removed", Toast.LENGTH_SHORT).show();
//                            onBackPressed();
//                        }
//                    }
//                }, null);
//                break;
            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        final Intent returnIntent = new Intent();

        if( resultIntentData.equals("")) {
            if (!DialerSettings.isSettingsSaved(EditAccountActivity.this)) {
                Alert.showPositiveNegative(EditAccountActivity.this, "Account Setup", "You did not provide any account information. Do you want to exit?", "Yes", "No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                resultIntentData = "no_account";
                                returnIntent.putExtra("result", resultIntentData);
                                setResult(Activity.RESULT_CANCELED, returnIntent);
                                superBackPress();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        }
                );
            }
            else {
                returnIntent.putExtra("result", resultIntentData);
                setResult(Activity.RESULT_CANCELED, returnIntent);
                super.onBackPressed();
            }
        }
        else{
            returnIntent.putExtra("result", resultIntentData);
            setResult(Activity.RESULT_OK, returnIntent);
            super.onBackPressed();
        }

    }

    public void superBackPress() {
        super.onBackPressed();
    }

    public boolean isValid() {

        if((opcodeLayout.getVisibility() == View.VISIBLE)) {

            if(opcodeEditText.getText().toString().length() < 1) {
                Alert.showOk(EditAccountActivity.this, "Error", "Please enter Opcode");
                return false;
            }
            else {
                if(!isNumeric( opcodeEditText.getText().toString() )) {
                    Alert.showOk(EditAccountActivity.this, "Error", "Opcode must be numeric");
                    return false;
                }
            }

        }

//        if(accountNameText.getText().toString().length() < 1) {
//            Alert.showOk(this, "Error", "Please enter Account Name");
//            return false;
//        }

        if(usernameEditText.getText().toString().trim().length() < 1) {
            Alert.showOk(EditAccountActivity.this, "Error", "Please enter Username");
            return false;
        }

        if(passwordEditText.getText().toString().trim().length() < 1) {
            Alert.showOk(EditAccountActivity.this, "Error", "Please enter Password");
            return false;
        }

//        if(phoneEditText.getText().toString().length() > 1) {
//            if(!isNumeric( phoneEditText.getText().toString() )) {
//                Alert.showOk(this, "Error", "Phone number must be numeric");
//                return false;
//            }
//        }

        if( StateManager.CUSTOM_AUTH_ENABLED) {
            String ip_new = customAuthEditText.getText().toString().trim();
            if (!AuthenticationManager.validateIpAddress(ip_new)) {
                Alert.showOk(EditAccountActivity.this, "Error", "Please Enter valid IP Address");
                return false;
            }
        }

        if( StateManager.CUSTOM_DNS_ENABLED) {
            String ip_new = customDnsEditText.getText().toString().trim();
            if (!AuthenticationManager.validateIpAddress(ip_new)) {
                Alert.showOk(EditAccountActivity.this, "Error", "Please Enter valid IP Address");
                return false;
            }
        }

//        //Todo: check if Account exists from accountName -> DialerSettings

        return true;
    }

    private Account getAccountFromView(){

        String accountName = currentAccountName; //accountNameText.getText().toString();
        boolean opcodeEditable = opcodeLayout.getVisibility()==View.VISIBLE;
        int opCode;

        if( DialerSettings.SHOW_OPCODE_FIELD || opcodeEditable)
            opCode = Integer.parseInt(opcodeEditText.getText().toString());
        else
            opCode = DialerSettings.PLATINUM_OPCODE;

        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String phnNumber = "";//= phoneEditText.getText().toString();

        if(phnNumber.length() < 1) {
            if(isNumeric(username)) {
                phnNumber = username;
            }
            else {
                phnNumber = getRandom(9);
            }
        }
//        phoneEditText.setText(phnNumber);

        String auth_ip_new = "";
        if(StateManager.CUSTOM_AUTH_ENABLED){
            auth_ip_new = customAuthEditText.getText().toString().trim();
        }

        String dns_ip_new = "";
        if(StateManager.CUSTOM_DNS_ENABLED){
            dns_ip_new = customDnsEditText.getText().toString().trim();
        }

        Account account = new Account();
        account.setOpCodeEditable(opcodeEditable);
        account.setOpCode(opCode);
        account.setUsername(username);
        account.setPassword(password);
        account.setPhoneNumber(phnNumber);
        account.setCustomAuthIp(auth_ip_new);
        account.setCustomDnsIp(dns_ip_new);
        account.setAccountName(accountName);

        return account;
    }
    //Todo: remove this checking for add Account
    private boolean isItemChanged() {

        //Todo: get Account from accountName -> DialerSettings
        String accountKey = DialerSettings.ACCOUNT_KEY_PREFIX + currentAccountName;//accountNameText.getText().toString();
        String newItem = PreferenceSetting.getString(EditAccountActivity.this, accountKey, "");

        String accountItem = getAccountFromView().toString();

        //Todo: add equality check in Account class
        return !accountItem.equals(newItem);
    }

    private boolean saveAccountListItemSettings() {

        //Todo: get Account from accountName -> DialerSettings
        //Todo: add/update Account -> DialerSettings
        String accountKey = DialerSettings.ACCOUNT_KEY_PREFIX + currentAccountName;//accountNameText.getText().toString();
        Account accountItem = getAccountFromView();

        PreferenceSetting.putString(EditAccountActivity.this, accountKey, accountItem.toString());
        PreferenceSetting.addStringToSet(EditAccountActivity.this, DialerSettings.PREF_ACCOUNT_KEY_SET, accountKey);

        String savedAccountName = PreferenceSetting.getString(EditAccountActivity.this,DialerSettings.PREF_ACCOUNT, "");
        if( savedAccountName.equals("") || /* if no account saved */
                savedAccountName.equals(currentAccountName)) /* if updated account is saved account */
        {
            int savedOpCode = PreferenceSetting.getInt(EditAccountActivity.this,DialerSettings.PREF_OPCODE, 0);

            if (savedOpCode != accountItem.getOpCode() || isNewCustomAuth() || isNewCustomDns())
                resultIntentData = "reconnect";
            else
                resultIntentData = "connect";

//            resultIntentData = savedOpCode == accountItem.getOpCode() ? "connect" : "reconnect";
            DialerSettings.saveAsCurrentAccount(EditAccountActivity.this, accountItem);
        }

        return true;
    }

    private boolean deleteAccountListItemSettings() {

        //Todo: get Account from accountName -> DialerSettings
        //Todo: add/update Account -> DialerSettings
        String accountKey = DialerSettings.ACCOUNT_KEY_PREFIX + currentAccountName;//accountNameText.getText().toString();

        PreferenceSetting.putString(EditAccountActivity.this, accountKey, null);
        PreferenceSetting.removeStringFromSet(EditAccountActivity.this, DialerSettings.PREF_ACCOUNT_KEY_SET, accountKey);

        if( PreferenceSetting.getString(EditAccountActivity.this,DialerSettings.PREF_ACCOUNT, "").equals(currentAccountName)){
            DialerSettings.removeCurrentAccount(EditAccountActivity.this);
            resultIntentData = "disconnect";
        }

        return true;
    }

    public boolean isNewCustomAuth() {
        if(StateManager.CUSTOM_AUTH_ENABLED){
            String ip_old = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.CUSTOM_AUTH_IP, "");
            String ip_new =	customAuthEditText.getText().toString();
            if (!ip_new.equals(ip_old)) {
                return true;
            }
        }
        return false;
    }

    public boolean isNewCustomDns() {
        if(StateManager.CUSTOM_DNS_ENABLED){
            String ip_old = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.CUSTOM_DNS_IP, "");
            String ip_new =	customDnsEditText.getText().toString();
            if (!ip_new.equals(ip_old)) {
                return true;
            }
        }
        return false;
    }

    public boolean isNewCurrent() {

        if(StateManager.CUSTOM_AUTH_ENABLED){
            String ip_old = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.CUSTOM_AUTH_IP, "");
            String ip_new =	customAuthEditText.getText().toString();
            if (!ip_new.equals(ip_old)) {
                return true;
            }
        }
        if(StateManager.CUSTOM_DNS_ENABLED){
            String ip_old = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.CUSTOM_DNS_IP, "");
            String ip_new =	customDnsEditText.getText().toString();
            if (!ip_new.equals(ip_old)) {
                return true;
            }
        }

        int opcode = PreferenceSetting.getInt(EditAccountActivity.this,DialerSettings.PREF_OPCODE, 0);
        String username = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.PREF_USERNAME, "");
        String password = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.PREF_PASSWORD, "");
        String phone = PreferenceSetting.getString(EditAccountActivity.this, DialerSettings.PREF_PHONENO, "");

        if(opcodeLayout.getVisibility() == View.VISIBLE) {
            if((Integer.parseInt(opcodeEditText.getText().toString()) == opcode) &&
                    usernameEditText.getText().toString().equals(username) &&
                    passwordEditText.getText().toString().equals(password)
//                    && phoneEditText.getText().toString().equals(phone)
                    ){

                return false;
            }
            else {
                return true;
            }
        }
        else {
            if(	usernameEditText.getText().toString().trim().equals(username) &&
                    passwordEditText.getText().toString().trim().equals(password)
//                    && phoneEditText.getText().toString().equals(phone)
                    ) {

                return false;
            }
            else {
                return true;
            }
        }
    }

    public boolean saveCurrentSettings() {

        Account account = getAccountFromView();
        DialerSettings.saveAsCurrentAccount(EditAccountActivity.this, account);

        return true;
    }

    private static char[] VALID_CHARACTERS = "0123456879".toCharArray();

    private String getRandom(int numChars) {
        Random rand = new Random();
        char[] buff = new char[numChars];
        for (int i = 0; i < numChars; ++i) {
            buff[i] = VALID_CHARACTERS[rand.nextInt(VALID_CHARACTERS.length)];
        }
        return new String(buff);
    }

    public static boolean isNumeric(String str) {
        String regx = "[0-9]+";
        if (str.matches(regx)) {
            return true;
        }
        return false;
    }

}
