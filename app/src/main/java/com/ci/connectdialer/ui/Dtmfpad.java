package com.ci.connectdialer.ui;

import android.content.Context;
import android.media.ToneGenerator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.StateManager;

import java.util.HashMap;
import java.util.Map;

public class Dtmfpad extends LinearLayout implements OnClickListener {

	private Context mContext;
	private EditText mDtmfEditText;
	
	public Dtmfpad(Context context) {
		super(context);
		
		init(context);
	}
	
	public Dtmfpad(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init(context);
	}

	private void init(Context context) {
		
		mContext = context;
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.dtmf_layout, this, true);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		mDtmfEditText = (EditText) findViewById(R.id.dtmfEditText);
		for(int id: KEYPAD_MAP.keySet()) {
			
			final View dtmfKey = findViewById(id);
			
			TextView digit = (TextView) dtmfKey.findViewById(R.id.dtmf_number_digit);
			TextView letter = (TextView) dtmfKey.findViewById(R.id.dtmf_number_letter);
			
			digit.setText( mContext.getString(KEYPAD_MAP.get(id)[2]) );
			if(KEYPAD_MAP.get(id)[3] != -1)
				letter.setText( mContext.getString(KEYPAD_MAP.get(id)[3]) );
			
			dtmfKey.setOnClickListener(this);
		}

		LinearLayout keypadHide = (LinearLayout) findViewById(R.id.btnPadHideDtmf);
		keypadHide.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {

		if( v.getId()==R.id.btnPadHideDtmf){
			setVisibility(GONE);
			return;
		}

		simulateKeyPress(v.getId());
	}
	
	public void simulateKeyPress(int id) {
		if(mDtmfEditText.getText().toString().length() >= 10)
			mDtmfEditText.getText().clear();
		
		int[] value = KEYPAD_MAP.get(id);

		KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, value[1]);
		mDtmfEditText.onKeyDown(value[1], event);
		
		if (StateManager.mCksLibrary != null) {
			switch (value[1]) {
			case KeyEvent.KEYCODE_1:
				StateManager.mCksLibrary.SendDtmfSipInfo(49);
				break;
			case KeyEvent.KEYCODE_2:
				StateManager.mCksLibrary.SendDtmfSipInfo(50);
				break;
			case KeyEvent.KEYCODE_3:
				StateManager.mCksLibrary.SendDtmfSipInfo(51);
				break;
			case KeyEvent.KEYCODE_4:
				StateManager.mCksLibrary.SendDtmfSipInfo(52);
				break;
			case KeyEvent.KEYCODE_5:
				StateManager.mCksLibrary.SendDtmfSipInfo(53);
				break;
			case KeyEvent.KEYCODE_6:
				StateManager.mCksLibrary.SendDtmfSipInfo(54);
				break;
			case KeyEvent.KEYCODE_7:
				StateManager.mCksLibrary.SendDtmfSipInfo(55);
				break;
			case KeyEvent.KEYCODE_8:
				StateManager.mCksLibrary.SendDtmfSipInfo(56);
				break;
			case KeyEvent.KEYCODE_9:
				StateManager.mCksLibrary.SendDtmfSipInfo(57);
				break;
			case KeyEvent.KEYCODE_STAR:
				StateManager.mCksLibrary.SendDtmfSipInfo(42);
				break;
			case KeyEvent.KEYCODE_0:
				StateManager.mCksLibrary.SendDtmfSipInfo(48);
				break;
			case KeyEvent.KEYCODE_POUND:
				StateManager.mCksLibrary.SendDtmfSipInfo(35);
				break;

			default:
				break;
			}
		}
	}
	
	private static final Map<Integer, int[]> KEYPAD_MAP = new HashMap<Integer, int[]>() {
		private static final long serialVersionUID = -6640726691906396774L;
		{
			put(R.id.dtmfzero, new int[] { ToneGenerator.TONE_DTMF_0, KeyEvent.KEYCODE_0, R.string.dialpad_zero_digit, R.string.dialpad_zero_letter });
			put(R.id.dtmfone, new int[] { ToneGenerator.TONE_DTMF_1, KeyEvent.KEYCODE_1, R.string.dialpad_one_digit, R.string.dialpad_one_letter });
			put(R.id.dtmftwo, new int[] { ToneGenerator.TONE_DTMF_2, KeyEvent.KEYCODE_2, R.string.dialpad_two_digit, R.string.dialpad_two_letter });
			put(R.id.dtmfthree, new int[] { ToneGenerator.TONE_DTMF_3, KeyEvent.KEYCODE_3, R.string.dialpad_three_digit, R.string.dialpad_three_letter});
			put(R.id.dtmffour, new int[] { ToneGenerator.TONE_DTMF_4, KeyEvent.KEYCODE_4, R.string.dialpad_four_digit, R.string.dialpad_four_letter});
			put(R.id.dtmffive, new int[] { ToneGenerator.TONE_DTMF_5, KeyEvent.KEYCODE_5, R.string.dialpad_five_digit, R.string.dialpad_five_letter});
			put(R.id.dtmfsix, new int[] { ToneGenerator.TONE_DTMF_6, KeyEvent.KEYCODE_6, R.string.dialpad_six_digit, R.string.dialpad_six_letter});
			put(R.id.dtmfseven, new int[] { ToneGenerator.TONE_DTMF_7, KeyEvent.KEYCODE_7, R.string.dialpad_seven_digit, R.string.dialpad_seven_letter});
			put(R.id.dtmfeight, new int[] { ToneGenerator.TONE_DTMF_8, KeyEvent.KEYCODE_8, R.string.dialpad_eight_digit, R.string.dialpad_eight_letter});
			put(R.id.dtmfnine, new int[] { ToneGenerator.TONE_DTMF_9, KeyEvent.KEYCODE_9, R.string.dialpad_nine_digit, R.string.dialpad_nine_letter});
			put(R.id.dtmfstar, new int[] { ToneGenerator.TONE_DTMF_S, KeyEvent.KEYCODE_STAR, R.string.dialpad_star_digit, -1});
			put(R.id.dtmfpound, new int[] { ToneGenerator.TONE_DTMF_P, KeyEvent.KEYCODE_POUND, R.string.dialpad_pound_digit, -1});
		}
	};

}
