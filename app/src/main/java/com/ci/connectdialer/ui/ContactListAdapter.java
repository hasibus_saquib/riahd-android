package com.ci.connectdialer.ui;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.ContactRow;
import com.ci.connectdialer.R;

import java.util.List;

/**
 * Created by Masud Rashid on 4/1/2017.
 */
public class ContactListAdapter extends BaseAdapter{

    private List<ContactRow> contactRowList;
    private Context mContext;
    private Cursor mCursor;

    public ContactListAdapter(Context context){
        mContext = context;
    }

    @Override
    public int getCount() {
        return contactRowList != null ? contactRowList.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        RowItem rowItem = new RowItem();

        if(contactRowList == null)
            return null;

        if(view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.contacts_list_item, null);

            rowItem.contactName = (TextView) view.findViewById(R.id.contact_name);
            rowItem.contactImage = (ImageView) view.findViewById(R.id.contact_image);
            view.setTag(rowItem);
        }
        else {
            rowItem = (RowItem) view.getTag();
        }

        String name = contactRowList.get(position).getConactName();
        String imageUri = contactRowList.get(position).getContactImageUri();

        rowItem.contactName.setText(name);

        if( imageUri == null) {
            rowItem.contactImage.setImageResource(R.drawable.ic_person_black_36dp);
            rowItem.contactImage.setColorFilter(Color.GRAY);
        }
        else {
            rowItem.contactImage.setImageURI(Uri.parse(imageUri));
            rowItem.contactImage.setColorFilter(Color.TRANSPARENT);
        }

        return view;
    }

    public void setContactRowList(List<ContactRow> contactRowList) {
        this.contactRowList = contactRowList;
    }

    public Cursor getCursor() {
        return mCursor;
    }

    public void setCursor(Cursor mCursor) {
        this.mCursor = mCursor;
    }

    public class RowItem{
        TextView contactName;
        ImageView contactImage;
    }
}
