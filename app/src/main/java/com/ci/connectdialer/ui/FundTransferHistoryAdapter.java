package com.ci.connectdialer.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ci.connectdialer.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Erfan on 5/11/17.
 */

public class FundTransferHistoryAdapter extends ArrayAdapter<FundTransferHistoryModel> {

    private ArrayList<FundTransferHistoryModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView fromAccountTV;
        TextView toAccountTV;
        TextView amountTV;
        TextView transferTypeTV;
        TextView fundTransferSectionHeader;
    }



    public FundTransferHistoryAdapter(ArrayList<FundTransferHistoryModel> data, Context context) {
        super(context, R.layout.fund_transfer_history_list_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FundTransferHistoryModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        FundTransferHistoryAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new FundTransferHistoryAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.fund_transfer_history_list_item, parent, false);
            viewHolder.fundTransferSectionHeader = (TextView) convertView.findViewById(R.id.fundTransferSectionHeader);
            viewHolder.fromAccountTV = (TextView) convertView.findViewById(R.id.fromAccountText);
            viewHolder.toAccountTV= (TextView) convertView.findViewById(R.id.toAccountText);
            viewHolder.amountTV = (TextView) convertView.findViewById(R.id.amountText);
            viewHolder.transferTypeTV = (TextView) convertView.findViewById(R.id.fundTransferTypeText);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (FundTransferHistoryAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        try {
            String strDate = new SimpleDateFormat("d MMM yyyy HH:mm:ss").format(dataModel.getFundTransferDate());
            viewHolder.fundTransferSectionHeader.setText(strDate);
            viewHolder.fromAccountTV.setText(dataModel.getFromAccount());
            viewHolder.toAccountTV.setText(dataModel.getToAccount());
            viewHolder.amountTV.setText(dataModel.getTransferedAmount());
            viewHolder.transferTypeTV.setText(dataModel.getTransferType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Return the completed view to render on screen
        return convertView;
    }


}
