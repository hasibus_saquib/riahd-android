package com.ci.connectdialer.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.ci.connectdialer.core.media.RtpStreamReceiverNew;
import com.ci.connectdialer.core.utilities.CallRecorder;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Receiver;
import com.ci.connectdialer.core.utilities.StateManager;

import com.ci.connectdialer.R;
import com.rey.material.widget.Slider;
import com.signal.CkslibraryEnum;
import jp.wasabeef.blurry.Blurry;
public class IncallActivity extends Activity implements OnClickListener{

	private boolean started;


	private PowerManager.WakeLock powerLock;
	private KeyguardLock keyLock;
	private AudioManager mAudioManager;

	private Dtmfpad dtmfpad;
	private Chronometer incallElapsedTime;
	private TextView incallStateTv;
	private ImageView acceptButton;
	private ImageView recordButton;
	private ImageView rejectButton;

	private ImageView speakerIv;
	private ImageView microphoneIv;
	private ImageView btoothIv;
	private ImageView contactIv;
	private ImageView blarImageView;

	private ImageView powerVoiceIv;
	private TextView micGainLavelTv;

	private TableRow tableRowCallAccept;
	private TableRow tableRowCallEnd;

	private Slider volumeSlider;
	private AlertDialog alertDialog;

	private void init() {
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		dtmfpad = (Dtmfpad) findViewById(R.id.dtmfpad);

		incallElapsedTime = (Chronometer) findViewById(R.id.callElapsed);
		TextView incallName = (TextView) findViewById(R.id.incall_name);
		TextView incallNumber = (TextView) findViewById(R.id.incall_number);
		incallStateTv = (TextView) findViewById(R.id.incall_state);
		contactIv = (ImageView) findViewById(R.id.contact_icon);
		blarImageView = (ImageView) findViewById(R.id.blarimageview);

		ImageView endButton = (ImageView) findViewById(R.id.button_end_call);
		acceptButton = (ImageView) findViewById(R.id.button_accept_call);
		recordButton = (ImageView) findViewById(R.id.button_record_call);
		rejectButton = (ImageView) findViewById(R.id.button_reject_call);

		speakerIv = (ImageView) findViewById(R.id.speaker);
		ImageView dtmf = (ImageView) findViewById(R.id.dtmf);
		microphoneIv = (ImageView) findViewById(R.id.mic);
		btoothIv = (ImageView) findViewById(R.id.btooth);

		powerVoiceIv = (ImageView) findViewById(R.id.power_voice);
		micGainLavelTv = (TextView) findViewById(R.id.text_power_lavel);

		tableRowCallAccept = (TableRow) findViewById(R.id.incall_accept_row);
		tableRowCallEnd = (TableRow) findViewById(R.id.incall_callend_row);

		endButton.setOnClickListener(this);
		acceptButton.setOnClickListener(this);
		rejectButton.setOnClickListener(this);
		recordButton.setOnClickListener(this);
		speakerIv.setOnClickListener(this);
		dtmf.setOnClickListener(this);
		btoothIv.setOnClickListener(this);
		microphoneIv.setOnClickListener(this);
		powerVoiceIv.setOnClickListener(this);
		started = false;
		StateManager.MICROPHONE_ON = false;
		StateManager.SPEAKER_ON = false;
		StateManager.BLUETOOTH_ON = false;

		if(null != StateManager.mCallInfo) {
			String name = StateManager.mCallInfo.getName();
			incallName.setText(name);
			if (name == null || name.length() == 0){
				incallName.setVisibility(View.GONE);
				incallNumber.setTextSize(32);
			}
			incallNumber.setText(StateManager.mCallInfo.getPhoneNo());
			showContactImage();
		}
		StateManager.CURRENT_MIC_GAIN = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.MIC_GAIN_IN_DB, 6);
		StateManager.MIC_GAIN_FACTOR = (float)Math.pow( 10., StateManager.CURRENT_MIC_GAIN / 20. );
		setRecordButtonImage();
		setMicGainIcon();

		LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.volume_slider_dialog, (ViewGroup) findViewById(R.id.your_dialog_root_element));
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(layout);
		alertDialog = builder.create();

		View titleView = inflater.inflate(R.layout.tool_bar_with_share, (ViewGroup) findViewById(R.id.toolbaar_share));
		ImageView backImageView = (ImageView) titleView.findViewById(R.id.back_toolbar_share);
		backImageView.setVisibility(View.GONE);
		ImageView shareImageView = (ImageView) titleView.findViewById(R.id.share_toolbar);
		shareImageView.setVisibility(View.GONE);
		TextView titleTextView = (TextView) titleView.findViewById(R.id.toolbartitle);
		titleTextView.setText("Call Volume");
		alertDialog.setCustomTitle(titleView);
		alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(event.getAction() ==  KeyEvent.ACTION_UP)
					return false;
				int currentVolume = volumeSlider.getValue();

				switch (keyCode) {
					case KeyEvent.KEYCODE_VOLUME_DOWN:
						if(currentVolume > volumeSlider.getMinValue())
							currentVolume--;
						volumeSlider.setValue((float) currentVolume, true);
						return true;

					case KeyEvent.KEYCODE_VOLUME_UP:

						if(currentVolume < volumeSlider.getMaxValue())
							currentVolume++;
						volumeSlider.setValue((float) currentVolume, true);
						return true;

					case KeyEvent.KEYCODE_BACK:
						if(dtmfpad.getVisibility() == View.VISIBLE)
							dtmfpad.setVisibility(View.GONE);
						else if(alertDialog.isShowing())
							alertDialog.dismiss();

						return true;
				}
				return false;
			}
		});

		alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				handler.postDelayed(runnable, 3000);
			}
		});

		alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				handler.removeCallbacks(runnable);
			}
		});


		AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		volumeSlider = (Slider) layout.findViewById(R.id.incall_volumeSlider);
		volumeSlider.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
			@Override
			public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
				handler.removeCallbacks(runnable);
				handler.postDelayed(runnable, 3000);
				RtpStreamReceiverNew.adjustInCallVolume(newValue);
			}
		});
		int currentVolume = audio.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		StateManager.CURRENT_SPEAKER_GAIN = PreferenceSetting.getInt(CksTabService.getContext(), DialerSettings.SPEAKER_GAIN_IN_DB, 0);
		int speakerGain = StateManager.CURRENT_SPEAKER_GAIN / StateManager.SPEAKER_GAIN_PRODUCT;
		int newVolume = currentVolume + speakerGain;
		volumeSlider.setValue((float) newVolume, false);
		audio.setStreamVolume(AudioManager.STREAM_VOICE_CALL, newVolume, 0);
	}

	final Runnable runnable = new Runnable() {
		@Override
		public void run() {
			if (alertDialog.isShowing()) {
				alertDialog.dismiss();
			}
		}
	};

	final Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 0:
					Intent intent = new Intent();
					intent.setClass(IncallActivity.this, BringToFrontActivity.class);

					startActivity(intent);
					finish();
					break;

				case 1:
					if(!started) {
						if(null != StateManager.mCallInfo) {
							incallElapsedTime.setBase(StateManager.mCallInfo.base);
							incallElapsedTime.start();
							started = true;

							incallStateTv.setText(R.string.call_established);
						}
					}
					break;

				default:
					break;
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.incall_layout);

		init();
	}


	@Override
	public void onStart() {
		super.onStart();

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		powerLock = pm.newWakeLock(
				PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP,
				"PlDialer.PWLock");
		powerLock.acquire();

		KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
		keyLock = keyguardManager.newKeyguardLock("PlDialer.KYLock");
		keyLock.disableKeyguard();

		if(incallElapsedTime != null && StateManager.mCallInfo != null && started) {
			incallElapsedTime.setBase(StateManager.mCallInfo.base);
			incallElapsedTime.start();
		}
	}

	private void unlockScreen() {
		Window window = this.getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	}

	@Override
	public void onResume() {
		super.onResume();

		if (StateManager.mState != null) {
			switch (StateManager.mState) {
				case ECalling:
					incallStateTv.setText(R.string.calling);
					break;

				case ERinging:
					incallStateTv.setText(R.string.ringing);
					break;

				case ECallReceiving:
					incallStateTv.setText(R.string.ringing);
//					acceptButton.setVisibility(View.VISIBLE);
					tableRowCallAccept.setVisibility(View.VISIBLE);
					tableRowCallEnd.setVisibility(View.GONE);
					unlockScreen();
					break;

				case ECallEstablished:
					// restoreSpeakerMode();
					if (null != StateManager.mCallInfo) {

						if (!started) {
							incallElapsedTime.setVisibility(View.VISIBLE);
							incallElapsedTime.setBase(StateManager.mCallInfo.base);
							incallElapsedTime.start();

							mAudioManager.setStreamVolume(
									AudioManager.STREAM_VOICE_CALL,
									mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
									0);
							started = true;
						}

						incallStateTv.setText(R.string.call_established);
					}
					break;

				default:
					break;
			}
		}
		if(!StateManager.mInCall) {
			incallStateTv.setTextColor(Color.RED);
			incallStateTv.setText(R.string.call_terminated);
			incallElapsedTime.stop();

			if(StateManager.BLUETOOTH_ON) {
				mAudioManager.stopBluetoothSco();
			}

			handler.sendEmptyMessageDelayed(0, 500);
			StateManager.SPEAKER_ON = false;
			StateManager.BLUETOOTH_ON = false;
		}
	}

	@Override
	public void onStop() {
		super.onStop();

		if(null != powerLock && powerLock.isHeld()) {
			powerLock.release();
			powerLock = null;
		}

		if(null != keyLock) {
			keyLock.reenableKeyguard();
			keyLock = null;
		}

		if(incallElapsedTime != null) {
			incallElapsedTime.stop();
		}

		if (alertDialog.isShowing()) {
			alertDialog.dismiss();
		}
	}

	@Override
	public void onClick(View view) {
		if(dtmfpad.getVisibility() == View.VISIBLE)
			return;

		switch (view.getId()) {
			case R.id.button_accept_call:
//				acceptButton.setVisibility(View.GONE);
				tableRowCallAccept.setVisibility(View.GONE);
				tableRowCallEnd.setVisibility(View.VISIBLE);
				StateManager.callAccept();
				break;

			case R.id.button_end_call:
				StateManager.callEnd();
				break;

			case R.id.button_reject_call:
				StateManager.callEnd();
				break;

			case R.id.speaker:
				if(StateManager.BLUETOOTH_ON)
					return;

				if((StateManager.mState != null) && StateManager.mState == CkslibraryEnum.ECallEstablished
						|| StateManager.mState == CkslibraryEnum.ERinging) {
					if(StateManager.SPEAKER_ON) {
						//Receiver.manager(this).speaker(false);
						speakerIv.setImageResource(R.drawable.ic_volume_up_black_24dp);
					}
					else {
						//Receiver.manager(this).speaker(true);
						speakerIv.setImageResource(R.drawable.ic_volume_off_black_24dp);
					}
					StateManager.SPEAKER_ON = !StateManager.SPEAKER_ON;
				}
				break;

			case R.id.mic:
				if((StateManager.mState != null) && StateManager.mState == CkslibraryEnum.ECallEstablished) {
					if (StateManager.MICROPHONE_ON) {
						if (StateManager.mCksLibrary != null) {
							StateManager.mCksLibrary.SetMute(false);
							microphoneIv.setImageResource(R.drawable.ic_mic_off_black_24dp);
						}
					} else {
						if (StateManager.mCksLibrary != null) {
							StateManager.mCksLibrary.SetMute(true);
							microphoneIv.setImageResource(R.drawable.ic_mic_black_24dp);
						}
					}
					StateManager.MICROPHONE_ON = !StateManager.MICROPHONE_ON;
				}
				break;

			case R.id.dtmf:
				if((StateManager.mState != null) && StateManager.mState == CkslibraryEnum.ECallEstablished)
				{
					if(dtmfpad.getVisibility() == View.GONE)
						dtmfpad.setVisibility(View.VISIBLE);
					else
						dtmfpad.setVisibility(View.GONE);
				}

				break;

			case R.id.btooth:
				if(StateManager.SPEAKER_ON)
					return;

				if((StateManager.mState != null) && StateManager.mState == CkslibraryEnum.ECallEstablished
						|| StateManager.mState == CkslibraryEnum.ERinging)
				{
					if(StateManager.BLUETOOTH_ON) {
						mAudioManager.stopBluetoothSco();
						stopBtooth();
					}
					else {
						mAudioManager.stopBluetoothSco();
						mAudioManager.startBluetoothSco();
						startBtooth();
					}
				}
				break;

			case R.id.button_record_call:
				CallRecorder.getInstance().handleCallRecordAction(this);
				setRecordButtonImage();
				break;

			case R.id.power_voice:
				setPowerVoice();
				break;

			default:
				break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		int currentVolume = volumeSlider.getValue();

		switch (keyCode) {
			case KeyEvent.KEYCODE_VOLUME_DOWN:
				if(alertDialog.isShowing() == false)
					alertDialog.show();

				if(currentVolume > volumeSlider.getMinValue())
					currentVolume--;
				volumeSlider.setValue((float) currentVolume, true);
				RtpStreamReceiverNew.adjustInCallVolume(currentVolume);
				return true;

			case KeyEvent.KEYCODE_VOLUME_UP:
				if(alertDialog.isShowing() == false)
					alertDialog.show();

				if(currentVolume < volumeSlider.getMaxValue())
					currentVolume++;
				volumeSlider.setValue((float) currentVolume, true);
				RtpStreamReceiverNew.adjustInCallVolume(currentVolume);
				return true;

			case KeyEvent.KEYCODE_BACK:
				if(dtmfpad.getVisibility() == View.VISIBLE)
					dtmfpad.setVisibility(View.GONE);

				return true;
		}
		return false;
	}

	public void restoreSpeakerMode() {
		// For Speaker Icon Tweak
		Receiver.getStateManager().speaker(StateManager.SPEAKER_ON);
	}

	public void startBtooth() {
		StateManager.BLUETOOTH_ON = true;
		btoothIv.setImageResource(R.drawable.ic_bluetooth_connected_black_24dp);
	}

	public void stopBtooth() {
		StateManager.BLUETOOTH_ON = false;
		btoothIv.setImageResource(R.drawable.ic_bluetooth_black_24dp);
	}

	public void setMicGainIcon() {
		switch (StateManager.CURRENT_MIC_GAIN) {
			case StateManager.MIC_GAIN_NORMAL:
				powerVoiceIv.setImageResource(R.drawable.ic_mic_gain_0);
				micGainLavelTv.setText("Normal");
				break;
			case StateManager.MIC_GAIN_MEDIAM:
				powerVoiceIv.setImageResource(R.drawable.ic_mic_gain_1);
				micGainLavelTv.setText("Medium");
				break;
			case StateManager.MIC_GAIN_HIGH:
				powerVoiceIv.setImageResource(R.drawable.ic_mic_gain_2);
				micGainLavelTv.setText("High");
				break;
		}
	}

	public void setPowerVoice () {
		if (StateManager.CURRENT_MIC_GAIN == StateManager.MIC_GAIN_NORMAL) {
			StateManager.CURRENT_MIC_GAIN = StateManager.MIC_GAIN_MEDIAM;
			setMicGainIcon();
		}
		else if (StateManager.MIC_GAIN_MEDIAM == StateManager.CURRENT_MIC_GAIN){
			StateManager.CURRENT_MIC_GAIN = StateManager.MIC_GAIN_HIGH;
			setMicGainIcon();
//			int red = Color.parseColor("#B71C1C");
//			powerVoiceIv.setColorFilter(red, PorterDuff.Mode.SRC_IN);
		}
		else if (StateManager.MIC_GAIN_HIGH == StateManager.CURRENT_MIC_GAIN) {
			StateManager.CURRENT_MIC_GAIN = StateManager.MIC_GAIN_NORMAL;
			setMicGainIcon();
//			int white = Color.parseColor("#FFFFFF");
//			powerVoiceIv.setColorFilter(white, PorterDuff.Mode.SRC_IN);
		}

		StateManager.MIC_GAIN_FACTOR = (float)Math.pow( 10., StateManager.CURRENT_MIC_GAIN / 20. );
		PreferenceSetting.putInt(CksTabService.getContext(), DialerSettings.MIC_GAIN_IN_DB, StateManager.CURRENT_MIC_GAIN);
	}

	private void showContactImage() {
		if (StateManager.mCallInfo != null) {
			//long contactId = PhoneInformation.getContactIDFromNumber(StateManager.mCallInfo.getPhoneNo(), getApplicationContext());
			//Bitmap bitmap = PhoneInformation.getContactImageFromId(contactId, getApplicationContext());
			Bitmap bitmap = PhoneInformation.retrieveContactPhotoNew(getApplicationContext(), StateManager.mCallInfo.getPhoneNo(), StateManager.mCallInfo.getContactID());
			if (bitmap != null) {
				contactIv.setImageBitmap(bitmap);
				blarImageView.setImageBitmap(bitmap);
				Blurry.with(IncallActivity.this)
						.radius(5)
						.sampling(2)
//						.color(Color.argb(1, 245, 245, 245))
						.async()
						.from(bitmap)
						.into(blarImageView);
			}
			else{
				contactIv.setImageResource(R.drawable.ic_person_black_48dp);
				contactIv.setColorFilter(Color.WHITE);
//				contactIv.setColorFilter(Color.GRAY);
//				contactIv.setBackgroundColor(Color.LTGRAY);
			}
		}
	}


	void setRecordButtonImage(){
		if(CallRecorder.getInstance().isCallRecordRnabled()){
			recordButton.setImageResource(R.drawable.ic_stop_red_48dp);
		}
		else{
			recordButton.setImageResource(R.drawable.ic_record_rec_red_48dp);
		}
	}
}
