package com.ci.connectdialer.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.OptionsInterface;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.R;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Erfan on 12/02/17.
 */
public class FileManagerAdapter extends ArrayAdapter<FileManagerModel> implements View.OnClickListener{

    private ArrayList<FileManagerModel> dataSet;
    private OptionsInterface optionsInterface;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtItemCount;
        LinearLayout options;
    }



    public FileManagerAdapter(ArrayList<FileManagerModel> data, Context context, OptionsInterface optionsInterface) {
        super(context, R.layout.file_manager_row_item, data);
        this.dataSet = data;
        this.mContext=context;
        this.optionsInterface = optionsInterface;
    }


    @Override
    public void onClick(View v) {


        final int position=(Integer) v.getTag();
        Object object= getItem(position);
        FileManagerModel dataModel=(FileManagerModel)object;

        switch (v.getId())
        {
            case R.id.item_options_fm:

                PopupMenu popup = new PopupMenu(mContext, v);
                popup.getMenuInflater().inflate(R.menu.file_mgr_row_menu,
                        popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.delCallRecordFolder:
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Do you really want to delete this folder?")
                                        .setIconAttribute(android.R.attr.alertDialogIcon)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int id) {
                                                    // Yes-code
                                                    optionsInterface.deleteRecordedCall(position);
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            })
                                            .show();

//                                optionsInterface.deleteRecordedCall(position);
                                break;
                            default:
                                break;
                        }

                        return true;
                    }
                });

                break;
            default:
                break;

        }


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FileManagerModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.file_manager_row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.fileName);
            viewHolder.options = (LinearLayout) convertView.findViewById(R.id.item_options_fm);
            viewHolder.txtItemCount = (TextView) convertView.findViewById(R.id.itemCount);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        String path = Environment.getExternalStorageDirectory().toString() + "/" + PhoneInformation.getAppName(getContext());
        path += "/" + dataModel.getFileName();
        File dir = new File(path);
        ImageView optionForFileManager = (ImageView) convertView.findViewById(R.id.optionForDirectory);

        File[] files = dir.listFiles();
        if(files != null && files.length > 0) {
            viewHolder.options.setOnClickListener(null);
            optionForFileManager.setVisibility(View.GONE);
        }
        else {
            viewHolder.options.setOnClickListener(this);
            optionForFileManager.setVisibility(View.VISIBLE);
        }


        //Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        lastPosition = position;


        viewHolder.txtName.setText(dataModel.getFileName());
        viewHolder.options.setTag(position);
        viewHolder.txtItemCount.setText(dataModel.getItemCountString());
        // Return the completed view to render on screen
        return convertView;
    }


}
