package com.ci.connectdialer.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.CallLog.Calls;
import android.support.v4.content.res.ResourcesCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.core.db.CallInformation;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

public class CalllogAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private Vector<CallInformation> mCallInfoVector;
	private  SparseBooleanArray mSelectedItemsIds;
	private Context mContext;
	
	public CalllogAdapter(Context context, Vector<CallInformation> callInfo) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mCallInfoVector = callInfo;
		mSelectedItemsIds = new  SparseBooleanArray();
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		CallInfoItem callLogItem = new CallInfoItem();
		
		if(mCallInfoVector == null)
			return null;
		
		if(view == null) {
			view = mInflater.inflate(R.layout.call_list_item, null);

			callLogItem.dateSectionHeader = (TextView) view.findViewById(R.id.dateSectionHeader);
			callLogItem.line1View = (TextView) view.findViewById(R.id.line1);
			callLogItem.durationView = (TextView) view.findViewById(R.id.duration);
			callLogItem.numberView = (TextView) view.findViewById(R.id.number);
			callLogItem.dateView = (TextView) view.findViewById(R.id.date);
			callLogItem.iconView = (ImageView) view.findViewById(R.id.call_type_icon);
			callLogItem.contactImage = (ImageView) view.findViewById(R.id.contact_image_history);
			
			view.setTag(callLogItem);
		}
		else {
			callLogItem = (CallInfoItem) view.getTag();
		}
		
		callLogItem.line1View.setText("");
		callLogItem.durationView.setText("");
		callLogItem.numberView.setText("");
		callLogItem.dateView.setText("");
		
		long dateTime = mCallInfoVector.elementAt(position).getTime();
		long duration = mCallInfoVector.elementAt(position).getDuration();
		int type = mCallInfoVector.elementAt(position).getState();
		String name = mCallInfoVector.elementAt(position).getName();
		String number = mCallInfoVector.elementAt(position).getPhoneNo();
		boolean showSectionHeader = mCallInfoVector.elementAt(position).isShowSectionHeader();
		long contactID = mCallInfoVector.elementAt(position).getContactID();

		if(!name.equals(""))
			callLogItem.line1View.setText(name);
		else
			callLogItem.line1View.setText("Unknown");

		if(showSectionHeader)
		{
			callLogItem.dateSectionHeader.setVisibility(View.VISIBLE);
			callLogItem.dateSectionHeader.setText(getDayString(dateTime));
		}
		else callLogItem.dateSectionHeader.setVisibility(View.GONE);
		
		callLogItem.durationView.setText(callDuration(duration));
		callLogItem.dateView.setText(getTimeString(dateTime));
		switch (type) {
		case Calls.INCOMING_TYPE:
			callLogItem.iconView.setImageResource(R.drawable.ic_call_received_black_18dp);
			break;

		case Calls.OUTGOING_TYPE:
			callLogItem.iconView.setImageResource(R.drawable.ic_call_made_black_18dp);
			break;

		case Calls.MISSED_TYPE:
			callLogItem.iconView.setImageResource(R.drawable.ic_call_missed_black_18dp);
			break;
		}
		
//		if (!number.equals(name))
			callLogItem.numberView.setText(number);


		showRecentContactImage(callLogItem.contactImage, number, contactID);
		
		return view;
	}

	private void showRecentContactImage(ImageView contactIv, String number, long cntId) {

		Bitmap bitmap=null;

		if( !number.equals("")) {
			//long contactId = PhoneInformation.getContactIDFromNumber(number, mContext);
			//bitmap = PhoneInformation.getContactImageFromId(contactId, mContext);
			bitmap = PhoneInformation.retrieveContactPhotoNew(mContext, number, cntId);
		}
		if (bitmap != null) {
			contactIv.setImageBitmap(bitmap);
			contactIv.setColorFilter(Color.TRANSPARENT);
		}
		else{
			contactIv.setImageResource(R.drawable.ic_person_black_36dp);
			contactIv.setColorFilter(ResourcesCompat.getColor(mContext.getResources(), R.color.defaultContactColor, null));
		}
	}


	@Override
	public int getCount() {
		if(null != mCallInfoVector)
			return mCallInfoVector.size();
		else
			return 0;
	}

	public CallInformation getCallInformation(int position)
	{
		CallInformation info = null;
		if(mCallInfoVector != null && mCallInfoVector.size() > position)
			info = mCallInfoVector.get(position);
		return info;
	}

	@Override
	public Object getItem(int position) {
		// TODO
		return null;
	}


	@Override
	public long getItemId(int position) {
		// TODO
		return 0;
	}

	public long remove(int position) {
		CallInformation info = mCallInfoVector.remove(position);
		notifyDataSetChanged();
		return info.getId();
	}


	public void  toggleSelection(int position) {
		selectView(position, !mSelectedItemsIds.get(position));
	}

	// Remove selection after unchecked
	public void  removeSelection() {
		mSelectedItemsIds = new  SparseBooleanArray();
		notifyDataSetChanged();
	}

	// Item checked on selection
	public void selectView(int position, boolean value) {
		if (value)
			mSelectedItemsIds.put(position,  value);
		else
			mSelectedItemsIds.delete(position);
		notifyDataSetChanged();
	}

	// Get number of selected item
	public int  getSelectedCount() {
		return mSelectedItemsIds.size();
	}

	public SparseBooleanArray getSelectedIds() {
		return mSelectedItemsIds;
	}

	public void setCallInformation(Vector<CallInformation> callInfo) {

		if( callInfo != null && callInfo.size() > 0 ) {
			int lastDay = getDay(callInfo.get(0).getTime());
			callInfo.get(0).setShowSectionHeader(true);

			for (int i = 1; i < callInfo.size(); i++) {
				int curDay = getDay(callInfo.get(i).getTime());
				callInfo.get(i).setShowSectionHeader(lastDay != curDay);
				lastDay = curDay;
			}
		}

		mCallInfoVector = callInfo;
	}

	private int getDay(long milliseconds) {

		int gmtOffset = TimeZone.getDefault().getRawOffset();
		return (int) ((milliseconds+gmtOffset) / (1000*60*60*24));
	}

	String getDayString(long dateInMillis){
		int today = getDay(System.currentTimeMillis());
		int day = getDay(dateInMillis);

		if( day==today)	return "Today";
		else if(day==today-1) return "Yesterday";
		return new SimpleDateFormat("EEE, MMM dd").format(new Date(dateInMillis));
	}

	String getTimeString(long dateInMillis){
		Date date = new Date(dateInMillis);
		SimpleDateFormat formatter= new SimpleDateFormat("HH:mm a");
		String formatted = formatter.format(date );
		return "at " + formatted;
	}

	private String callDuration(long duration) {
		duration = duration / 1000;
		long hh = duration / 3600;
		long remainder = duration % 3600;
		long mm = remainder / 60;
		long ss = remainder % 60;

		if(hh==0&&mm==0) return ss+" sec";
		if(hh==0) return mm+" min "+ss+" sec";
		return hh+" hr "+mm+" min "+ss+" sec";
	}
	
	public static final class CallInfoItem {
		TextView dateSectionHeader;
		ImageView contactImage;
		TextView line1View;
		TextView durationView;
		TextView numberView;
		TextView dateView;
		ImageView iconView;
	}
}
