package com.ci.connectdialer.ui;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.OptionsInterface;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Erfan on 2/12/17.
 */

public class FileManagerActivity extends AppCompatActivity  implements View.OnClickListener, OptionsInterface {
    ArrayList<FileManagerModel> dataModels;
    ListView listView;
    private static FileManagerAdapter adapter;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        path = Environment.getExternalStorageDirectory().toString() + "/" + PhoneInformation.getAppName(getApplicationContext());
        TextView titleTv = (TextView) findViewById(R.id.toolbartitle);
        titleTv.setText("Call Records");

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar_share);
        backToolbarIv.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.list);
        listView.setBackgroundColor(Color.parseColor("#ECEFF1"));

        // Read all files sorted into the values-array
        dataModels = new ArrayList<>();
        File dir = new File(path);
        if (!dir.canRead()) {
            setTitle("Directory inaccessible");
        }
        else {
            File[] files = dir.listFiles();
            Arrays.sort(files);
            for(File file : files)
            {
                int itemCount = 0;
                File insideDir = new File(file.getAbsolutePath());
                if (!insideDir.canRead()) {
                    setTitle("Directory inaccessible");
                }
                else {
                    itemCount = insideDir.listFiles().length;
                }

                Date fileDate = new Date(file.lastModified());
                dataModels.add(new FileManagerModel(file.getName(), fileDate, itemCount));
            }
        }

        Collections.sort(dataModels, new Comparator<FileManagerModel>() {
            public int compare(FileManagerModel o1, FileManagerModel o2) {
                return o2.getFileName().compareTo(o1.getFileName());
            }
        });

        adapter= new FileManagerAdapter(dataModels, this, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FileManagerModel dataModel= dataModels.get(position);

                //Snackbar.make(view, dataModel.getFileName() + "\n" + dataModel.getDateCreated(), Snackbar.LENGTH_LONG).setAction("No action", null).show();
                gotoCallRecordInDirectory(path, dataModel.getFileName());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_toolbar_share:
                onBackPressed();
                break;
            default:
                break;
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void gotoCallRecordInDirectory(String filePath, String fileName) {
        Intent intent = new Intent(this, CallRecordsActivity.class);
        intent.putExtra("filePath", filePath);
        intent.putExtra("fileName", fileName);
        startActivity(intent);
    }

    public void audioPlayer(String path, String fileName){

        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void shareRecordedCall(int position) {
    }

    @Override
    public void playRecordedCall(int position) {
    }

    @Override
    public void deleteRecordedCall(int position) {

        FileManagerModel dataModel= dataModels.get(position);
        File file = new File(path, dataModel.getFileName());
        if( file.exists()){
            adapter.remove(adapter.getItem(position));
			file.delete();
            adapter.notifyDataSetChanged();
        }
    }

    public void onResume()
    {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

}
