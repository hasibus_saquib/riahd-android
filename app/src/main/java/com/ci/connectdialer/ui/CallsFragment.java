package com.ci.connectdialer.ui;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ci.connectdialer.core.db.CallInformation;
import com.ci.connectdialer.core.utilities.CallLogMgr;
import com.ci.connectdialer.core.utilities.CksTabService;
import com.ci.connectdialer.core.utilities.PhoneInformation;
import com.ci.connectdialer.R;

/**
 * Created by Masud Rashid on 22/11/2016.
 */
public class CallsFragment extends Fragment implements
        CallLogMgr.FrequentCallsListener,
        View.OnClickListener {

    public static final int FREQUENT_LIST_SIZE = 4;
    private CalllogAdapter listAdapter = null;

    LinearLayout freq[] = new LinearLayout[FREQUENT_LIST_SIZE];

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calls,null);
        ((MainActivity)getActivity()).setDialpadVisibility(View.VISIBLE);
//        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity)getActivity()).onFragmentActivityCreate(MainActivity.TAB_CALLS);

        freq[0] = (LinearLayout) getActivity().findViewById(R.id.freq_1st);
        freq[1] = (LinearLayout) getActivity().findViewById(R.id.freq_2nd);
        freq[2] = (LinearLayout) getActivity().findViewById(R.id.freq_3rd);
        freq[3] = (LinearLayout) getActivity().findViewById(R.id.freq_4th);

        for(int i=0; i<FREQUENT_LIST_SIZE; i++) {
            freq[i].setOnClickListener(this);
        }

        final ListView mListView = (ListView) getActivity().findViewById(R.id.logList);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CallInformation callInfo = listAdapter.getCallInformation(position);
                final TextView numberView = (TextView) view.findViewById(R.id.number);
                final TextView nameView = (TextView) view.findViewById(R.id.line1);
                long contactID = -1;
                if(callInfo != null)
                    contactID = callInfo.getContactID();
                String number = numberView.getText().toString().trim();
                String name = nameView.getText().toString().trim();

                if(name.equals(number))
                    name = "";

                ((MainActivity)getActivity()).selectContact(name, number, contactID);
            }
        });

        listAdapter = new CalllogAdapter(getActivity(), null);
        mListView.setAdapter(listAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        new CallLogMgr.CallLogTask(getActivity(), listAdapter).execute(0);
        new CallLogMgr.FrequentCallsTask(getActivity(), this).execute(FREQUENT_LIST_SIZE);
    }

    private CallInformation[] frequentCalls;

    @Override
    public void updateFrequentCalls(CallInformation[] calls) {

        if(!isAdded() || isDetached() || calls == null)
            return;
        frequentCalls = calls;

        for(int i=0;i<FREQUENT_LIST_SIZE;i++) {

            String name = calls[i]==null ? "(None)" : calls[i].getName();
            String number = calls[i]==null ? "" : calls[i].getPhoneNo();
            long contactID = calls[i]==null ? -1 : calls[i].getContactID();
            TextView personTv = ((TextView) freq[i].findViewById(R.id.freq_person_tv));
            ImageView personIv = (ImageView) freq[i].findViewById(R.id.freq_person_iv);

            if(name.length() > 0)
                personTv.setText(name);
            else
                personTv.setText(number);
            showFreqContactImage(personIv, number, contactID);
        }
    }

    private void showFreqContactImage(ImageView contactIv, String number, long cntId) {

        Bitmap bitmap=null;

        Context context = CksTabService.getContext();
        if( !number.equals("")) {
            //long contactId = PhoneInformation.getContactIDFromNumber(number, getActivity());
            //bitmap = PhoneInformation.getContactImageFromId(contactId, getActivity());
            bitmap = PhoneInformation.retrieveContactPhotoNew(context, number, cntId);
        }
        if (bitmap != null) {
            contactIv.setImageBitmap(bitmap);
            contactIv.setColorFilter(Color.TRANSPARENT);
        }
        else{
            contactIv.setImageResource(R.drawable.ic_person_black_48dp);
            contactIv.setColorFilter(ResourcesCompat.getColor(context.getResources(), R.color.defaultContactColor, null));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.freq_1st:
                if(frequentCalls[0]!=null)
                    ((MainActivity)getActivity()).selectContact(frequentCalls[0].getName(), frequentCalls[0].getPhoneNo(), frequentCalls[0].getContactID());
                break;
            case R.id.freq_2nd:
                if(frequentCalls[1]!=null)
                    ((MainActivity)getActivity()).selectContact(frequentCalls[1].getName(), frequentCalls[1].getPhoneNo(), frequentCalls[1].getContactID());
                break;
            case R.id.freq_3rd:
                if(frequentCalls[2]!=null)
                    ((MainActivity)getActivity()).selectContact(frequentCalls[2].getName(), frequentCalls[2].getPhoneNo(), frequentCalls[2].getContactID());
                break;
            case R.id.freq_4th:
                if(frequentCalls[3]!=null)
                    ((MainActivity)getActivity()).selectContact(frequentCalls[3].getName(), frequentCalls[3].getPhoneNo(), frequentCalls[3].getContactID());
                break;
            default:
                break;
        }
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch( item.getItemId() ) {
//
//            case R.id.nav_clear_history:
//                new CallLogMgr.ClearCallLogTask(getActivity(), listAdapter).execute(0);
//                return true;
//
////            case R.id.nav_clear_history_dialed:
////                new ClearCallLogTask(getActivity()).execute(1);
////                return true;
////
////            case R.id.nav_clear_history_received:
////                new ClearCallLogTask(getActivity()).execute(2);
////                return true;
////
////            case R.id.nav_clear_history_missed:
////                new ClearCallLogTask(getActivity()).execute(3);
////                return true;
//        }
//
//        return false;
//    }

}
