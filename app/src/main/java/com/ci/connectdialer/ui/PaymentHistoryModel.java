package com.ci.connectdialer.ui;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Erfan on 5/11/17.
 */

public class PaymentHistoryModel {

    Date paymentDate ;
    String paymentMode = "";
    String paymentAmount = "";
    String balanceAfterPayment = "";


    public PaymentHistoryModel(JSONObject recordObject) {
        try {
            String strDate = recordObject.getString("Fund-added-Time").replaceAll("[-: ]", "");
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            this.paymentDate = format.parse(strDate);
            this.paymentAmount = recordObject.getString("Amount-Added");
            this.balanceAfterPayment = recordObject.getString("Balance-After-Payment");
            this.paymentMode = recordObject.getString("Payment-Mode");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public String getBalanceAfterPayment() {
        return balanceAfterPayment;
    }
}