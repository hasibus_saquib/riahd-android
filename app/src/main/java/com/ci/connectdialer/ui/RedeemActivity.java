package com.ci.connectdialer.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Receiver;

import org.json.JSONObject;

/**
 * Created by Erfan on 5/11/17.
 */

public class RedeemActivity extends AppCompatActivity implements View.OnClickListener, AsyncTasks.RedeemVoucherTask.RedeemVoucherListener {

    EditText editTextPinNumber;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redeem);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(this);

        TextView titleTextView = (TextView) findViewById(R.id.titleEditAccount);
        titleTextView.setText("Redeem Voucher");

        Button topRightButton = (Button) findViewById(R.id.saveAccountTv);
        topRightButton.setText("Submit");
        topRightButton.setOnClickListener(this);

        editTextPinNumber = (EditText) findViewById(R.id.editTextCardPin);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveAccountTv:
                redeemVoucher();
                break;

            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isValid() {

        if(editTextPinNumber.getText().toString().length() < 1)
        {
            Alert.showOk(RedeemActivity.this, "Error", "Please enter your serial number");
            return false;
        }
        return true;
    }

    private void redeemVoucher() {
        if(isValid())
        {
            String pin = editTextPinNumber.getText().toString();
            String username = PreferenceSetting.getString(RedeemActivity.this, DialerSettings.PREF_USERNAME, "");
            String password = PreferenceSetting.getString(RedeemActivity.this, DialerSettings.PREF_PASSWORD, "");

            progressDialog = ProgressDialog.show(RedeemActivity.this, "", "Please wait", false);
            new AsyncTasks.RedeemVoucherTask(this).execute(username, password, pin);
        }
    }

    @Override
    public void onRedeemVoucher(String result) {

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                String status = json.getString("status");
                if(status.equals("success")){
                    Receiver.getStateManager().reconnect();
                    String message = json.getString("message") + ".\nYour current Balance is " + json.getString("NewBalance") + ".";
                    //Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    Alert.showOk(RedeemActivity.this, RedeemActivity.this.getString(R.string.app_name), message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onBackPressed();
                                }
                            }
                    );
                }
                else {
                    //Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    Alert.showOk(RedeemActivity.this, "Error", json.getString("message"));
                }

            } catch (Exception e) {
                Log.e("onSendOTP", e.toString());
            }
        }
        else {
            //Toast.makeText(this, "Error sending OTP. Please try again later.", Toast.LENGTH_LONG).show();
            Alert.showOk(RedeemActivity.this, "Error", "Error redeeming voucher. Please try again later.");
        }
    }
}