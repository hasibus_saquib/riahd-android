package com.ci.connectdialer.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ci.connectdialer.BuildConfig;
import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

/**
 * Created by Erfan on 5/5/17.
 */

public class SignUpActivity extends AppCompatActivity implements AsyncTasks.SendOTPTask.SendOTPListener, View.OnClickListener{
    private CountryCodePicker ccp;
    private EditText phoneNumberEditText;
    private ProgressDialog progressDialog;
    private static final int ACCOUNT_NEW = 9936;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        Button sendOtpButton = (Button) findViewById(R.id.sendOtpButton);
        sendOtpButton.setOnClickListener(this);

        Button haveOtpButton = (Button) findViewById(R.id.haveOtpButton);
        if (BuildConfig.DEBUG) {
            haveOtpButton.setVisibility(View.VISIBLE);
            haveOtpButton.setOnClickListener(this);
        }
        else
            haveOtpButton.setVisibility(View.GONE);

        haveOtpButton.setVisibility(View.GONE);

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCode("US");
        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = "";
        if(tm != null)
            countryCodeValue = tm.getNetworkCountryIso();
        ccp.setDefaultCountryUsingNameCode(countryCodeValue);
        ccp.setCountryForNameCode(countryCodeValue);

        phoneNumberEditText = (EditText) findViewById(R.id.phoneNumber);
    }

    private void processSendOtpAction()
    {
        if(isValid()) {
            String countryCode = ccp.getSelectedCountryCode();
            String phoneNumber = countryCode + phoneNumberEditText.getText().toString();
            phoneNumber = phoneNumber.replaceAll("[^\\d]", "");
            progressDialog = ProgressDialog.show(SignUpActivity.this, "", "Please wait", false);
            new AsyncTasks.SendOTPTask(this).execute(phoneNumber);
        }
    }

    private void processAlreadyHaveOTPAction()
    {
        if(isValid()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_FROM_BACKGROUND);

            String countryCode = ccp.getSelectedCountryCode();
            String phoneNumber = countryCode + phoneNumberEditText.getText().toString();
            intent.putExtra("phoneNumber", phoneNumber);
            intent.putExtra("account_name", DialerSettings.getNewAccountName(this));

            startActivity(intent);
        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.saveAccountTv:
                processSendOtpAction();
                break;
            case R.id.sendOtpButton:
                processSendOtpAction();
                break;
            case R.id.haveOtpButton:
                processAlreadyHaveOTPAction();
                break;
            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {

        Alert.showPositiveNegative(SignUpActivity.this, SignUpActivity.this.getString(R.string.app_name), "Do you want to exit?", "Yes", "No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        superBackPress();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                }
        );
    }

    @Override
    public void onSendOTP(String result) {

        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                int statusCode = json.getInt("statusCode");
                String status = json.getString("status");
                if( statusCode == 1000 || status.equals("success")){
                    Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(this, LoginActivity.class);
                    String countryCode = ccp.getSelectedCountryCode();
                    String phoneNumber = countryCode + phoneNumberEditText.getText().toString();
                    intent.putExtra("phoneNumber", phoneNumber);
                    intent.putExtra("account_name", DialerSettings.getNewAccountName(this));
                    startActivityForResult(intent, ACCOUNT_NEW);
                }
                else {
                    Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Log.e("onSendOTP", e.toString());
            }
        }
        else {
            Toast.makeText(this, "Error sending OTP. Please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == ACCOUNT_NEW){
            if( resultCode == Activity.RESULT_OK){
                registrationDone();
            }
                else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    public void registrationDone() {
        final Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        super.onBackPressed();
    }

    public void superBackPress() {
        final Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "no_account");
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
    }

    public boolean isValid() {

        if(ccp.getSelectedCountryName().toString().length() < 1)
        {
            Alert.showOk(SignUpActivity.this, "Error", "Please select country code");
            return false;
        }

        String phoneNumber = phoneNumberEditText.getText().toString();
        if(phoneNumber.toString().length() < 1)
        {
            Alert.showOk(SignUpActivity.this, "Error", "Please enter phone number");
            return false;
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
