package com.ci.connectdialer.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ci.connectdialer.PushNotification;
import com.ci.connectdialer.core.utilities.IPermissionMgr;
import com.ci.connectdialer.core.utilities.PermissionManager;
import com.ci.connectdialer.R;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity implements IPermissionMgr
{

    private PermissionManager permissionMgr;
    private boolean bAlreadyFinised = false;
    static SplashActivity splashActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //data of push notification should be saved before
        //checking multipleinstance(), as multipleinstance()
        //may return and lower code will not be executed
        PushNotification.saveData(getIntent().getExtras());

        //multiple instance should be checked here instead of MainActivity.java
        //as it is the Launcher Activity now
        if( multipleAppInstance()) {
            //show push notification data (if available)
            //before closing this instance
            PushNotification.displayNotification(getApplicationContext());
            return;
        }

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        splashActivity = this;

        TextView splashTv = (TextView) findViewById(R.id.splashTv);
        Typeface type = Typeface.createFromAsset(getAssets(),"MTCORSVA.ttf");
        splashTv.setTypeface(type);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        switch(displayMetrics.densityDpi){
            case DisplayMetrics.DENSITY_LOW:
                splashTv.setTextSize(28);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                splashTv.setTextSize(30);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                splashTv.setTextSize(34);
                break;
        }



        ProgressBar v = (ProgressBar) findViewById(R.id.splashProgress);
        v.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        initPermission();

    }

    public static SplashActivity getInstance(){
        return splashActivity;
    }

    private void initPermission() {
        permissionMgr = new PermissionManager(this);
        permissionMgr.checkPermissionAndStartApp(this);
    }

    private void startMainActivity() {
        Intent splashIntent = new Intent(SplashActivity.this, MainActivity.class);
        splashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(splashIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        permissionMgr.onRequestPermissionsResult( this, requestCode, permissions, grantResults);
    }


    @Override
    public void startApplication() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                finish();
            }
        }, 500);
        startMainActivity();
    }

    @Override
    public void exitApplication() {
        finish();
        //super.onBackPressed();
    }

    @Override
    public boolean isFinishing() {
        super.isFinishing();
        bAlreadyFinised = true;
        return bAlreadyFinised;
    }

    @Override
    public void finish() {
        if(bAlreadyFinised)
            return;
        super.finish();
    }

    @Override
    public void onDestroy() {
        bAlreadyFinised = true;
        super.onDestroy();
    }

    private boolean multipleAppInstance() {
        Log.d("multiple instance SPA", "!isTaskRoot: "+!isTaskRoot()+", getIntent: "+getIntent());
        if (!isTaskRoot() && getIntent() != null) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                Log.d("Multiple instance SPA", "has multiple instance");
                //Intent intent1 = new Intent(this, BringToFrontActivity.class);
                //startActivity(intent1);
                finish();
                return true;
            }
            Log.d("multiple instance SPA", "if statement is not true");
        }
        return false;
    }
}
