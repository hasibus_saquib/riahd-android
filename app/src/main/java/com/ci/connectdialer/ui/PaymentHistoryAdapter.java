package com.ci.connectdialer.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ci.connectdialer.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Erfan on 5/11/17.
 */

public class PaymentHistoryAdapter extends ArrayAdapter<PaymentHistoryModel> {

    private ArrayList<PaymentHistoryModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView paymentModeTV;
        TextView redeemAmountTV;
        TextView balanceAfterPayment;
        TextView paymentSectionHeader;
    }



    public PaymentHistoryAdapter(ArrayList<PaymentHistoryModel> data, Context context) {
        super(context, R.layout.payment_history_list_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        PaymentHistoryModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        PaymentHistoryAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new PaymentHistoryAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.payment_history_list_item, parent, false);
            viewHolder.paymentSectionHeader = (TextView) convertView.findViewById(R.id.paymentSectionHeader);
            viewHolder.paymentModeTV = (TextView) convertView.findViewById(R.id.paymentModeText);
            viewHolder.redeemAmountTV= (TextView) convertView.findViewById(R.id.redeemAmountText);
            viewHolder.balanceAfterPayment = (TextView) convertView.findViewById(R.id.balanceAfterPaymentText);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PaymentHistoryAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        String strDate = new SimpleDateFormat("d MMM yyyy HH:mm:ss").format(dataModel.getPaymentDate());
        viewHolder.paymentSectionHeader.setText(strDate);
        viewHolder.paymentModeTV.setText(dataModel.getPaymentMode());
        viewHolder.redeemAmountTV.setText(dataModel.getPaymentAmount());
        viewHolder.balanceAfterPayment.setText(dataModel.getBalanceAfterPayment());
        // Return the completed view to render on screen
        return convertView;
    }


}
