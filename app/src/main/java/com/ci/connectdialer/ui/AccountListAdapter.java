package com.ci.connectdialer.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;
import com.ci.connectdialer.core.utilities.Account;
import com.ci.connectdialer.R;

import java.util.Vector;

/**
 * Created by Masud Rashid on 11/12/2016.
 */
public class AccountListAdapter extends BaseAdapter {


    private LayoutInflater mInflater;
    private Vector<Account> accountVector;
    private Context mContext;
    private View.OnClickListener connectClickListener;

    public AccountListAdapter(Context context, Vector<Account> accounts) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        accountVector = accounts;
    }

    public void setAccountList(Vector<Account> accounts) {
        accountVector = accounts;
    }

    public void setConnectClickListener(View.OnClickListener listener) {
        connectClickListener = listener;
    }

    @Override
    public int getCount() {
        if(null != accountVector)
            return accountVector.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        AccountItem accountItem = new AccountItem();

        if(accountVector == null)
            return null;

        if(view == null) {
            view = mInflater.inflate(R.layout.account_list_item, null);

//            accountItem.reConnect = (ImageView) view.findViewById(R.id.item_reconnect);
            accountItem.accountName = (TextView) view.findViewById(R.id.item_account_name);
            accountItem.opCode = (TextView) view.findViewById(R.id.item_opcode);
            accountItem.userName = (TextView) view.findViewById(R.id.item_user_name);
            accountItem.isActive = (ImageView) view.findViewById(R.id.item_is_active);

            accountItem.isActive.setOnClickListener(connectClickListener);

            view.setTag(accountItem);
        }
        else {
            accountItem = (AccountItem) view.getTag();
        }

        String currentAccount = PreferenceSetting.getString(mContext, DialerSettings.PREF_ACCOUNT, "");
        String accountName = accountVector.get(position).getAccountName();
        int opCode = accountVector.get(position).getOpCode();
        String userName = accountVector.get(position).getUsername();

//        accountItem.reConnect.setImageResource(R.drawable.ic_refresh_black_24dp);
        accountItem.accountName.setText("Account-"+(position+1)); //accountItem.accountName.setText(accountName);
        accountItem.opCode.setText("Opcode: " + opCode);
        accountItem.userName.setText("User: " + userName);

        if(currentAccount.equals(accountName)) {
            accountItem.isActive.setImageResource(R.drawable.ic_check_circle_black_36dp);
        }
        else{
            accountItem.isActive.setImageResource(R.drawable.ic_pause_circle_outline_black_36dp);
        }

        return view;
    }

    public static class AccountItem {
//        ImageView reConnect;
        TextView accountName;
        TextView opCode;
        TextView userName;
        ImageView isActive;
    }
}
