package com.ci.connectdialer.ui;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.R;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Mahim-Ul Asad on 7/3/17.
 */

public class NotificationActivity extends AppCompatActivity {

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        TextView activityTitle = (TextView) findViewById(R.id.toolbartitle);
        activityTitle.setText(R.string.title_activity_notification);

        ImageView backButton = (ImageView) findViewById(R.id.back_toolbar_share);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationActivity.super.onBackPressed();
            }
        });

        TextView notificationMsgTitleView = (TextView) findViewById(R.id.notification_msg_title);
        TextView notificationMsgBodyView = (TextView) findViewById(R.id.notification_msg_body);
        ImageView notificationImageView = (ImageView) findViewById(R.id.notification_image);

        Bundle pushNotificationBundle = getIntent().getExtras();
        if (pushNotificationBundle != null ){
            String notificationMsgTitleString = pushNotificationBundle.getString(DialerSettings.PUSH_NOTIFICATION_MSG_TITLE);
            String notificationMsgBodyString = pushNotificationBundle.getString(DialerSettings.PUSH_NOTIFICATION_MSG);

            if (notificationMsgTitleString != null) {
                notificationMsgTitleView.setText(notificationMsgTitleString);
                notificationMsgTitleView.setVisibility(View.VISIBLE);
            }
            if (notificationMsgBodyString != null){
                notificationMsgBodyView.setText(notificationMsgBodyString);
                notificationMsgBodyView.setVisibility(View.VISIBLE);
            }

            String notificationMSgImgUrl = pushNotificationBundle.getString(DialerSettings.PUSH_NOTIFICATION_IMG_URL);
            String notificationMSGImgDomainFrontingUrl =
                    pushNotificationBundle.getString(DialerSettings.PUSH_NOTIFICATION_IMG_DOMAIN_FRONTING_URL);


            if (notificationMSgImgUrl != null){
                new DownloadImageTask(notificationImageView).execute(notificationMSgImgUrl);

                dialog = new ProgressDialog(this); // this = thisActivity
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setMessage("Loading Image. Please wait...");
                dialog.setIndeterminate(true);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

            }
            else if (notificationMSGImgDomainFrontingUrl != null){
                new DownloadImageDomainFrontingTask(notificationImageView).execute(notificationMSGImgDomainFrontingUrl);

                dialog = new ProgressDialog(this); // this = thisActivity
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setMessage("Loading Image. Please wait...");
                dialog.setIndeterminate(true);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

            }

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void,Bitmap > {

        ImageView imageView;

        public DownloadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String notificationMSgImgUrl = params[0];
            Bitmap bitmap = null;
            try {

                bitmap = BitmapFactory.decodeStream((InputStream)new URL(notificationMSgImgUrl).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap!=null){
                imageView.setImageBitmap(bitmap);
                imageView.setVisibility(View.VISIBLE);
            }
            dialog.cancel();
        }
    }



    private class DownloadImageDomainFrontingTask extends AsyncTask<String, Void,Bitmap > {

        ImageView imageView;

        public DownloadImageDomainFrontingTask(ImageView imageView){
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String notificationMSgImgUrl = params[0];
            Bitmap bitmap = null;
            try {

                String url = "https://www.google.com";
                int index = notificationMSgImgUrl.indexOf(".com");
                String paramString = notificationMSgImgUrl.substring(index+4); // 4 = length(".com")
                url += paramString;

                String Host = notificationMSgImgUrl.substring(notificationMSgImgUrl.indexOf("//")+2,
                        index+4);
                URL myUrl =  new URL(url);
                URLConnection connection = myUrl.openConnection();
                int timeOutSecond = 5000;
                connection.setConnectTimeout(timeOutSecond);
                connection.setReadTimeout(timeOutSecond);
                connection.setRequestProperty("Host", Host);

                InputStream response = connection.getInputStream();

                bitmap = BitmapFactory.decodeStream(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap!=null){
                imageView.setImageBitmap(bitmap);
                imageView.setVisibility(View.VISIBLE);
            }
            dialog.cancel();
        }
    }

}
