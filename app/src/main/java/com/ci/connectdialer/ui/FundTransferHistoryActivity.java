package com.ci.connectdialer.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.AsyncTasks;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Erfan on 5/11/17.
 */

public class FundTransferHistoryActivity extends AppCompatActivity implements View.OnClickListener, AsyncTasks.FundTransferHistoryTask.FundTransferHistoryListener {

    private EditText fromDateTV;
    private EditText toDateTV;
    private int year, month, day;
    private ProgressDialog progressDialog;
    private LinearLayout mHistoryContainer;

    ArrayList<FundTransferHistoryModel> dataModels;
    ListView listView;
    private static FundTransferHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fund_transfer_history);

        mHistoryContainer = (LinearLayout) findViewById(R.id.fundHistoryListContainer);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(this);

        TextView titleTextView = (TextView) findViewById(R.id.titleEditAccount);
        titleTextView.setText("Fund Transfer History");

        Button topRightButton = (Button) findViewById(R.id.saveAccountTv);
        topRightButton.setVisibility(View.GONE);

        fromDateTV = (EditText) findViewById(R.id.fromDateEditText);
        //fromDateTV.setOnClickListener(this);

        toDateTV = (EditText) findViewById(R.id.toDateEditText);
        //toDateTV.setOnClickListener(this);

        fromDateTV.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(fromDateTV.getWindowToken(), 0);
                showDialog(1);

               return true;
            }
        });

        toDateTV.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(fromDateTV.getWindowToken(), 0);
                showDialog(2);

                return true;
            }
        });

        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String yearString = dateString.substring(0, 4).toString();
        String monthString = dateString.substring(5, 7);
        String dayString = dateString.substring(8, 10);
        year = Integer.parseInt(yearString);
        month = Integer.parseInt(monthString);
        month--;
        day = Integer.parseInt(dayString);

        Button showButton = (Button) findViewById(R.id.showFundTransferHistoryButton);
        showButton.setOnClickListener(this);

        listView = (ListView)findViewById(R.id.fundTransferHistoryList);
        listView.setBackgroundColor(Color.parseColor("#ECEFF1"));

        dataModels = new ArrayList<>();

        Collections.sort(dataModels, new Comparator<FundTransferHistoryModel>() {
            public int compare(FundTransferHistoryModel o1, FundTransferHistoryModel o2) {
                return o2.getFundTransferDate().compareTo(o1.getFundTransferDate());
            }
        });

        adapter= new FundTransferHistoryAdapter(dataModels, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(this, mDateSetListenerFromDate, year, month, day);
            case 2:
                return new DatePickerDialog(this, mDateSetListenerToDate, year, month, day);
        }
        return null;
    }

    // updates the date we display in the TextView
    private void updateDisplayFromDate() {
        /*
         * Hide virtual keyboard
         */
        fromDateTV.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(year).append("-").append(month + 1).append("-")
                .append(day).append(""));
    }

    // updates the date we display in the TextView
    private void updateDisplayToDate() {
        /*
         * Hide virtual keyboard
         */
        toDateTV.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(year).append("-").append(month + 1).append("-")
                .append(day).append(""));
    }

    private DatePickerDialog.OnDateSetListener mDateSetListenerFromDate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int myear, int monthOfYear,
                              int dayOfMonth) {
            year = myear;
            month = monthOfYear;
            day = dayOfMonth;
            updateDisplayFromDate();
        }
    };

    private DatePickerDialog.OnDateSetListener mDateSetListenerToDate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int myear, int monthOfYear,
                              int dayOfMonth) {
            year = myear;
            month = monthOfYear;
            day = dayOfMonth;
            updateDisplayToDate();
        }
    };

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fromDateEditText:

                break;

            case R.id.toDateEditText:

                break;

            case R.id.showFundTransferHistoryButton:
                if(isValid())
                {
                    processShowFundTransferHistory();
                }
                break;
            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onResume()
    {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    private boolean isValid() {

        if(fromDateTV.getText().toString().length() < 1)
        {
            Alert.showOk(FundTransferHistoryActivity.this, "Error", "Please select from date");
            return false;
        }

        if(toDateTV.getText().toString().length() < 1)
        {
            Alert.showOk(FundTransferHistoryActivity.this, "Error", "Please select to date");
            return false;
        }
        return true;
    }


    private void processShowFundTransferHistory()
    {
        progressDialog = ProgressDialog.show(FundTransferHistoryActivity.this, "", "Please wait", false);

        String username = PreferenceSetting.getString(FundTransferHistoryActivity.this, DialerSettings.PREF_USERNAME, "");
        String password = PreferenceSetting.getString(FundTransferHistoryActivity.this, DialerSettings.PREF_PASSWORD, "");

        String timeString = new SimpleDateFormat("HH:mm:ss").format(new Date());
        String fromDate = fromDateTV.getText().toString() + " " + timeString;
        String toDate = toDateTV.getText().toString() + " " + timeString;
        new AsyncTasks.FundTransferHistoryTask(this).execute(username, password, fromDate, toDate);
    }

    @Override
    public void onReceivedFundTransferHistory(String result)
    {
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        dataModels.clear();
        if( !result.equals("")){

            try {
                JSONObject json = new JSONObject(result);

                String status = json.getString("status");
                if(status.equals("success")){
                    JSONArray recordArr = json.getJSONArray("records");
                    for(int i = 0; i < recordArr.length(); i++){
                        JSONObject record = recordArr.getJSONObject(i);
                        FundTransferHistoryModel model = new FundTransferHistoryModel(record);
                        dataModels.add(model);
                    }
                }
                else {
                    //Toast.makeText(this, json.getString("message"), Toast.LENGTH_LONG).show();
                    Alert.showOk(FundTransferHistoryActivity.this, "Error", json.getString("message"));
                }

            } catch (Exception e) {
                Log.e("onPaymentHistory", e.toString());
            }
        }
        else {
            //Toast.makeText(this, "Error sending OTP. Please try again later.", Toast.LENGTH_LONG).show();
            Alert.showOk(FundTransferHistoryActivity.this, "Error", "Error redeeming voucher. Please try again later.");
        }
        if(dataModels.size() > 0)
            mHistoryContainer.setVisibility(View.VISIBLE);
        else
            mHistoryContainer.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }
}
