package com.ci.connectdialer.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.Alert;
import com.ci.connectdialer.core.utilities.DialerSettings;
import com.ci.connectdialer.core.utilities.PreferenceSetting;

/**
 * Created by Erfan on 5/15/17.
 */

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener  {
    SwitchCompat echoCancelSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        ImageView backToolbarIv = (ImageView) findViewById(R.id.back_toolbar);
        backToolbarIv.setOnClickListener(this);

        TextView titleTextView = (TextView) findViewById(R.id.titleEditAccount);
        titleTextView.setText("Settings");

        Button topRightButton = (Button) findViewById(R.id.saveAccountTv);
        topRightButton.setVisibility(View.GONE);

        TextView myAccountButton = (TextView) findViewById(R.id.myAccountButtonTV);
        myAccountButton.setOnClickListener(this);

        echoCancelSwitch = (SwitchCompat) findViewById(R.id.echoCancelationSwitch);
        boolean echoCancellationOn = DialerSettings.isEchoCancelationOn(SettingsActivity.this);
        echoCancelSwitch.setChecked(echoCancellationOn);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.myAccountButtonTV:
                showMyAccount();
                break;

            case R.id.back_toolbar:
                onBackPressed();
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        DialerSettings.setEchoCancelationOn(SettingsActivity.this, echoCancelSwitch.isChecked());
        super.onBackPressed();
    }

    private void showMyAccount() {
        View view = getLayoutInflater().inflate(R.layout.my_account, null);

        String username = PreferenceSetting.getString(SettingsActivity.this, DialerSettings.PREF_USERNAME, "");
        //String password = PreferenceSetting.getString(SettingsActivity.this, DialerSettings.PREF_PASSWORD, "");

        TextView userNameTV = (TextView) view.findViewById(R.id.usernameTV);
        userNameTV.setText(username);

        TextView balanceTV = (TextView) view.findViewById(R.id.balanceTV);
        balanceTV.setText(DialerSettings.getCurrentBalance(SettingsActivity.this));
        // disabled click action temporarily as click urls not yet defined
//        fb.setOnClickListener(MainActivity.this);
//        twitter.setOnClickListener(MainActivity.this);
//        gplus.setOnClickListener(MainActivity.this);

        Alert.showView(SettingsActivity.this, null, view);
    }
}
