package com.ci.connectdialer.ui;

import android.content.Context;
import android.media.ToneGenerator;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ci.connectdialer.core.utilities.CallRecorder;
import com.ci.connectdialer.R;

import java.util.HashMap;
import java.util.Map;

public class Dialpad extends LinearLayout implements OnClickListener, OnLongClickListener {

	private OnDialpadEventListener dialpadEventListener;
	private OnDialpadVisibilityListener dialpadVisibilityListener;
	private Context mContext;
	private SwitchCompat mCallRecordSwitch;
	private TextView mCallrecordStateTextView;

	public Dialpad(Context context) {
		super(context);

		init(context);
	}

	public Dialpad(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(context);
	}

	private void init(Context context) {

		mContext = context;
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.dialpad_layout, this, true);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		for (int id : KEYPAD_MAP.keySet()) {

			final View dialpadKey = findViewById(id);

			TextView digit = (TextView) dialpadKey.findViewById(R.id.dialpad_number_digit);
			TextView letter = (TextView) dialpadKey.findViewById(R.id.dialpad_number_letter);

			digit.setText(mContext.getString(KEYPAD_MAP.get(id)[2]));
			if (KEYPAD_MAP.get(id)[3] != -1)
				letter.setText(mContext.getString(KEYPAD_MAP.get(id)[3]));

			dialpadKey.setOnClickListener(this);
			if (id == R.id.zero)
				dialpadKey.setOnLongClickListener(this);
		}

		View addButton = findViewById(R.id.btnAddContact);
		addButton.setOnClickListener(this);

		View delButton = findViewById(R.id.btnDel);
		delButton.setOnClickListener(this);
		delButton.setOnLongClickListener(this);

		View callButton = findViewById(R.id.btnCall);
		callButton.setOnClickListener(this);

		View padButton = findViewById(R.id.btnPadHide);
		padButton.setOnClickListener(this);

		View callRecordView = findViewById(R.id.callRecordView);
		callRecordView.setOnClickListener(this);

		View dialpadtop = findViewById(R.id.dialpadtop);
		dialpadtop.setOnClickListener(this);

		mCallRecordSwitch = (SwitchCompat) findViewById(R.id.callRecordSwitch);
		mCallRecordSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				dialpadEventListener.onCheckedChanged(compoundButton, b);
				if (b)
					mCallrecordStateTextView.setText("On");
				else
					mCallrecordStateTextView.setText("Off");
			}
		});

		mCallrecordStateTextView = (TextView) findViewById(R.id.callRecordStateText);

//		View contactDialpad = findViewById(R.id.dialpad_contact_layout);
//		contactDialpad.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
//		if( v.getId()==R.id.dialpad_contact_layout ) return;
		if( v.getId()==R.id.btnCall || v.getId() == R.id.callRecordView ||
				v.getId()==R.id.btnAddContact || v.getId()==R.id.btnDel || v.getId()==R.id.btnPadHide
				|| v.getId() == R.id.dialpadtop)
			simulateButtonPress(v.getId());
		else {
			simulateKeyPress(v.getId(), true);
		}
	}

	@Override
	public boolean onLongClick(View v) {
		simulateKeyPress(v.getId(), false);
		return true;
	}

	public void simulateKeyPress(int id, boolean shortClick) {
		if (dialpadEventListener != null) {
			if (shortClick) {
				int[] value = KEYPAD_MAP.get(id);

				dialpadEventListener.onClick(value[1], value[0]);
			} else {
				if (id == R.id.zero)
					dialpadEventListener.onClick(KeyEvent.KEYCODE_PLUS, ToneGenerator.TONE_DTMF_0); //Default keypress tone
				else if (id == R.id.btnDel)
					dialpadEventListener.onClick(Dialpad.ButtonEvent_DELETE_LONG, -1); //Default keypress tone
			}
		}
	}

	public void simulateButtonPress(int id) {
		if (dialpadEventListener != null) {
			switch (id) {
				case R.id.btnAddContact:
					dialpadEventListener.onClick(Dialpad.ButtonEvent_ADDCONTACT, -1);
					break;

				case R.id.btnCall:
					CallRecorder.getInstance().setCallRecordRnabled( mCallRecordSwitch.isChecked());
					dialpadEventListener.onClick(Dialpad.ButtonEvent_CALL, -1);
					break;

				case R.id.btnDel:
					dialpadEventListener.onClick(Dialpad.ButtonEvent_DELETE, -1);
					break;
				case R.id.btnPadHide:
				case R.id.dialpadtop:
					dialpadEventListener.onClick(Dialpad.ButtonEvent_PAD, -1);
					break;
				case R.id.callRecordView:
					mCallRecordSwitch.setChecked(!mCallRecordSwitch.isChecked());
					break;
				default:
					break;
			}
		}
	}

	@Override
	public void setVisibility(int visibility){

//		if (getVisibility() != visibility)
//		{
//			if (visibility == VISIBLE)
//			{
//				super.setVisibility(View.VISIBLE);
//				setAlpha(0.0f);
//
//				animate()
//						.translationY(-getHeight()/2)
//						.alpha(1.0f)
////						.setListener(new AnimatorListenerAdapter() {
////							@Override
////							public void onAnimationEnd(Animator animation) {
////								super.onAnimationEnd(animation);
////								Dialpad.this.setVisibility(View.GONE);
////							}
////						})
//						;
//			}
//			else
//			if ((visibility == INVISIBLE) || (visibility == GONE))
//			{
//				animate().translationY(0)
//						.alpha(0.0f);
//			}
//		}

		if(visibility == View.VISIBLE) {
			animateUp();
		}
		else
		{
			if(this.getVisibility() != View.GONE) {
				animateDown();
			}
		}
		super.setVisibility(visibility);
		dialpadVisibilityListener.onDialpadVisibility(visibility);
	}

	private void animateDown() {
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_out_up);
		animation.setDuration(300);
		this.setAnimation(animation);
		this.animate();
		animation.start();
	}

	private void animateUp() {
		Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_up);
		animation.setDuration(300);
		this.setAnimation(animation);
		this.animate();
		animation.start();
	}

	public void animateY(float distanceY) {

	}

	public void setOnDialpadEventListener(OnDialpadEventListener listener) {
		this.dialpadEventListener = listener;
	}
	public void setOnDialpadVisibilityListener(OnDialpadVisibilityListener listener) {
		this.dialpadVisibilityListener = listener;
	}

	public static final int ButtonEvent_ADDCONTACT = -1324;
	public static final int ButtonEvent_CALL = -15624;
	public static final int ButtonEvent_DELETE = -110324;
	public static final int ButtonEvent_DELETE_LONG = -110323;
	public static final int ButtonEvent_PAD = -110325;


	private static final Map<Integer, int[]> KEYPAD_MAP = new HashMap<Integer, int[]>() {
		private static final long serialVersionUID = -6640726621906396734L;

		{
			put(R.id.zero, new int[]{ToneGenerator.TONE_DTMF_0, KeyEvent.KEYCODE_0, R.string.dialpad_zero_digit, R.string.dialpad_zero_letter});
			put(R.id.one, new int[]{ToneGenerator.TONE_DTMF_1, KeyEvent.KEYCODE_1, R.string.dialpad_one_digit, R.string.dialpad_one_letter});
			put(R.id.two, new int[]{ToneGenerator.TONE_DTMF_2, KeyEvent.KEYCODE_2, R.string.dialpad_two_digit, R.string.dialpad_two_letter});
			put(R.id.three, new int[]{ToneGenerator.TONE_DTMF_3, KeyEvent.KEYCODE_3, R.string.dialpad_three_digit, R.string.dialpad_three_letter});
			put(R.id.four, new int[]{ToneGenerator.TONE_DTMF_4, KeyEvent.KEYCODE_4, R.string.dialpad_four_digit, R.string.dialpad_four_letter});
			put(R.id.five, new int[]{ToneGenerator.TONE_DTMF_5, KeyEvent.KEYCODE_5, R.string.dialpad_five_digit, R.string.dialpad_five_letter});
			put(R.id.six, new int[]{ToneGenerator.TONE_DTMF_6, KeyEvent.KEYCODE_6, R.string.dialpad_six_digit, R.string.dialpad_six_letter});
			put(R.id.seven, new int[]{ToneGenerator.TONE_DTMF_7, KeyEvent.KEYCODE_7, R.string.dialpad_seven_digit, R.string.dialpad_seven_letter});
			put(R.id.eight, new int[]{ToneGenerator.TONE_DTMF_8, KeyEvent.KEYCODE_8, R.string.dialpad_eight_digit, R.string.dialpad_eight_letter});
			put(R.id.nine, new int[]{ToneGenerator.TONE_DTMF_9, KeyEvent.KEYCODE_9, R.string.dialpad_nine_digit, R.string.dialpad_nine_letter});
			put(R.id.star, new int[]{ToneGenerator.TONE_DTMF_S, KeyEvent.KEYCODE_STAR, R.string.dialpad_star_digit, -1});
			put(R.id.pound, new int[]{ToneGenerator.TONE_DTMF_P, KeyEvent.KEYCODE_POUND, R.string.dialpad_pound_digit, -1});
		}
	};

	public interface OnDialpadEventListener {
		void onClick(int keyCode, int keyTone);
		void onCheckedChanged(View view, boolean b);
	}

	public interface OnDialpadVisibilityListener {
		void onDialpadVisibility(int visibility);
	}

}

