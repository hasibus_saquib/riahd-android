package com.ci.connectdialer.ui;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.ci.connectdialer.R;
import com.ci.connectdialer.core.utilities.AppInit;

/**
 * Created by Masud Rashid on 1/12/2016.
 */
public class SplashAnimation implements AppInit.ISplashAnimation{

    private Context context;
    public SplashAnimation(Context pContext){
        context = pContext;
    }

    private Animation anim1 = null;
    private Animation anim2 = null;
    private boolean isSplashRunning = false;

    public void initSplashAnimation() {
        try {
            anim1 = AnimationUtils.loadAnimation(context, R.anim.alpha);
            anim2 = AnimationUtils.loadAnimation(context, R.anim.translate);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }
    public void startAnimations() {

        isSplashRunning = true;

        ((Activity)context).setContentView(R.layout.splash_layout_old);

        if(anim1 != null && anim2 != null) {
            final LinearLayout l1 = (LinearLayout) ((Activity) context).findViewById(R.id.splashLayout);
            l1.clearAnimation();
            l1.startAnimation(anim1);

            final LinearLayout l2 = (LinearLayout) ((Activity) context).findViewById(R.id.splashObj);
            l2.clearAnimation();
            l2.startAnimation(anim2);
        }
    }

    public void stopAnimations() {
        if(anim1 != null && anim2 != null) {
            final LinearLayout l1 = (LinearLayout) ((Activity) context).findViewById(R.id.splashLayout);
            if(l1 != null)
                l1.clearAnimation();

            final LinearLayout l2 = (LinearLayout) ((Activity) context).findViewById(R.id.splashLayout);
            if(l2 != null)
                l2.clearAnimation();

            anim1 = null;
            anim2 = null;
        }
        isSplashRunning = false;
    }

    public boolean isSplashRunning() {
        return isSplashRunning;
    }

}
